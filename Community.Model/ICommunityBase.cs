﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model
{
    public interface ICommunityBase
    {
        int Id { get; set; }
        int MultiLingualId {get;set;}
        int CreatedById { get; set; }
        bool Delflag { get; set; }
    }
}
