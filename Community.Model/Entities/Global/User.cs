﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.Module;

namespace Community.Model.Entities.Global
{
    //public class User :  IdentityUser<int>,ICommunityBase
    //public class User : CommunityMandatory, ICommunityBase
    public class User : ICommunityBase
    {
        public User()
        {
            this.Roles = new List<UserRole>();
            this.UserCulture = new List<UserCulture>();
            //this.Modules = new List<Module.Module>();
            //this.OwnedSites = new List<Website>();
            //this.PurchasedThemes = new List<Theme.Theme>();
        }
        public string Fullname{
            get{
                return Firstname + " " + LastName;
            }
        }
         public int Id {get;set;}
        public string Username {get;set;}
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Email {get;set;}
        public string Salt { get; set; }
        public string Password {get;set;}
        public bool IsLocked { get; set; }
        //public int? ActiveThemeId { get; set; }
        //public virtual ICollection<Community.Model.Entities.Module.Module> Modules { get; set; } // Defines the modules 
        //public virtual ICollection<Model.Entities.Theme.Theme> PurchasedThemes { get; set; } // Defines the Activated 
        //public virtual ICollection<Model.Entities.Global.Website> OwnedSites { get; set; } // Defines the total site of 
        //public virtual Entities.Theme.Theme ActiveTheme { get; set; }
         //  public virtual ICollection<UserRole> UserRoles { get; set; }
          public int MultiLingualId {get;set;}
          public int CreatedById {get;set;}
          public bool Delflag {get;set;}
         public virtual ICollection<UserRole> Roles { get; set; }
         public virtual ICollection<UserCulture> UserCulture { get; set; }
        // public virtual ICollection<UserModule> UserModules { get; set; }
         public DateTime CreatedDate{get;set;}
         public string Image {get;set;}
         public string LastLoggedOnIP {get;set;}
         public string RegisteredFromIP {get;set;}
         public DateTime? LastLoggedInDate {get;set;}
         public UserType UserType {get;set;}
         public string Gender {get;set;}
         public string Summary {get;set;}
         public DateTime? DoB {get;set;}
    }
    public enum UserType{
        User,SupportStaff,AccountStaff,CommunityAdmin
    }
    // public class CommunityRoles  : IdentityUserRole<int>
    // {
    //     public int WebsiteId {get;set;}
    //     public Website Website {get;set;}
    // }
}