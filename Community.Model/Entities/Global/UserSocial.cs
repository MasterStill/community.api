﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Setting;

namespace Community.Model.Entities.Global
{
    public class UserSocial : CommunityMandatory,ICommunityBase
    {
        public string Url { get; set; }
        public string SocialId { get; set; }
        public string WebsiteId { get; set; }

        public virtual Social  Social { get; set; } 
        public  virtual  Website Website { get; set; }

    }
}
