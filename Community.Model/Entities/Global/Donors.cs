﻿using System;
namespace Community.Model.Entities.Global 
{
    public class Donors //: ICommunityBase
    {
        public Donors(){
            
        }
        public int Id {get;set;}
        public int CreatedById { get; set; }
        public virtual User CreatedBy {get;set;}
        public string Name { get; set; }
        public DateTime DonatedDate {get;set;}
        public string DonatedMedium {get;set;}
        public string DonationDescription {get;set;} // To Be Filled By Admin Only !
        public string Description {get;set;}
        public double Amount {get;set;}
        public bool Delflag { get; set; }
        public string PersonalUrl {get;set;}        
        public int ParentWebsiteId {get;set;}
        public ParentWebsite ParentWebsite {get;set;}
        public string ReferalId {get;set;}
        public string ReferedById {get;set;}
    }
}