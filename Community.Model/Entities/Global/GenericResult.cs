﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Global
{
    public class GenericResult
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }
        public string[] ArrayData { get; set; }
    }
}
