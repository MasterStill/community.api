﻿namespace Community.Model.Entities.Global
{
    public class Menu : CommunityMandatory,ICommunityBase
    {
        public string  Name { get; set; }
        public string  MenuUrl { get; set; }
        public string Url { 
            get{
                return Culture.Title + "/" + MenuUrl;
            }
        }
        public int Order {get;set;}
        public int? ParentMenu {get;set;}
        public bool NewWindow {get;set;}
        
        public int WebsiteId { get; set; }
        public Website Website { get; set; }
    }
}