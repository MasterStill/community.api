﻿namespace Community.Model.Entities.Global
{
    public class Error : CommunityMandatory, ICommunityBase
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string Url { get; set; }
        //public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
    }
}