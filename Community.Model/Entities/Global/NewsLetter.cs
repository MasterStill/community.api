﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Global
{
    public class Newsletter //: CommunityMandatory, ICommunityBase
    {
        public String Email { get; set; }
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
    }
}
