﻿using System;
namespace Community.Model.Entities.Global 
{
    public class ParentWebsite 
    {
        public ParentWebsite(){
            
        }
        public int Id {get;set;}
        public string Name {get;set;}
        public DateTime RegistedDate {get;set;}
        public DateTime ExpiryDate {get;set;}
        public string FacebookPageUrl {get;set;}
        public string TwitterPageUrl {get;set;}
    }
}