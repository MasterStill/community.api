﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Setting;
using Community.Model.Entities.Globalization;
namespace Community.Model.Entities.Global 
{
    public class WebsiteCulture //: CommunityMandatory, ICommunityBase
    {
       public int WebsiteId {get;set;}
       public int CultureId {get;set;}
       public virtual Website Website {get;set;}
       public virtual Culture Culture {get;set;}
    }
}