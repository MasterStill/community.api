﻿using System.Collections.Generic;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.Global;
using Community.Model.Entities.Album.Photo;
using Community.Model.Entities.Pages;
using Community.Model.Entities.Module;
namespace Community.Model.Entities.Global
{
    public class Website : ICommunityBase
    {
        public Website()
        {
            this.WebsitePage = new HashSet<WebsitePage>();
            this.Culture = new List<WebsiteCulture>();
            this.PhotoAlbum = new List<PhotoAlbum>();
            this.WebsiteModule = new List<WebsiteModule>();
            this.Components = new List<WebsiteComponent>();
            this.SocialWebsiteValue = new List<SocialWebsiteValue>();
        }
        public int Id { get; set; }
        public bool Delflag { get; set; }
        public int MultiLingualId { get; set; }
        public int CreatedById { get; set; }
        public virtual User CreatedBy { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string GoogleAnalyticsCode { get; set; }
        public bool Construction { get; set; }
        public string Theme { get; set; }
        //public string SuffixTitle {get;set;}
        public bool Redirect { get; set; }
        public string RedirectToUrl { get; set; }
        public string Logo { get; set; }
        public string DisplayWebsiteOf { get; set; }
        public int AllocatedSpace { get; set; }
        public int? DefaultCultureId { get; set; }
        public virtual Culture DefaultCulture { get; set; }
        public int SpaceUsed { get; set; }
        public bool PaidWebsite { get; set; }
        public bool Paid { get; set; }
        public bool Comments { get; set; } // Master Is Comment Alloweded ?
        public string Disqus_Code { get; set; }
        public string Custom_Layout_Css { get; set; }
        public string Custom_Layout_Script { get; set; }
        public bool IIsCreated { get; set; }
        public WebsiteType WebstieType { get; set; }
        // public Website()
        // {
        //     //this.Menu = new List<Menu>();
        //     //this.Socials = new List<Social>();
        //    // this.AdvancedSetting = new AdvancedSetting();
        //    // this.GlobalWebsiteSettings = new GlobalWebsiteSetting();

        // }
        //public virtual  ICollection<Menu>  Menu { get; set; }
        ///public virtual  GlobalWebsiteSetting GlobalWebsiteSettings { get; set; }
        //public virtual  ICollection<Social>  Socials { get; set; }
        // public virtual  AdvancedSetting AdvancedSetting { get; set; }
        public virtual ICollection<PhotoAlbum> PhotoAlbum { get; set; }
        // public virtual  ICollection<WebsiteCulture>  Culture { get; set; }
        public virtual List<WebsiteCulture> Culture { get; set; }
        //public virtual List<Module.Module> Modules {get;set;}
        public ICollection<WebsitePage> WebsitePage { get; set; }
        public ICollection<WebsiteModule> WebsiteModule { get; set; }
        public virtual List<WebsiteComponent> Components { get; set; }
        public ICollection<SocialWebsiteValue> SocialWebsiteValue { get; set; }
        public virtual List<Document> Documents { get; set; }

    }
    public enum WebsiteType
    {
        Personal, Business
    }
}
public class Document
{
    public int Id { get; set; }
    public string Link_Url { get; set; }
    public string Description { get; set; }
}
public class Social
{
    public int Id { get; set; }
    public string AdminIco { get; set; }
    public string Name { get; set; }
    public string Logo { get; set; }
    public ICollection<SocialWebsiteValue> SocialWebsiteValue { get; set; }
}
public class SocialWebsiteValue
{
    public int SocialId { get; set; }
    public string Value { get; set; }
    public int WebsiteId { get; set; }
    public virtual Social Social { get; set; }
    public virtual Website Website { get; set; }

}