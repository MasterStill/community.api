﻿namespace Community.Model.Entities.Global
{
    public class StaticGlobalization : CommunityMandatory,ICommunityBase
    {
        public string Key {get;set;}
        public string Content {get;set;}
        public int? step {get;set;}                
    }
}