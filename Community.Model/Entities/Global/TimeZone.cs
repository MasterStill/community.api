﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Global
{
    public class TimeZone
    {
        public int Id { get; set; }
        public int CreatedBy { get; set; }
        public string Title { get; set; }
        public string UTCCode { get; set; }
    }
}
