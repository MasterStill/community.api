﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Global
{
    public class Contact
    {
        public String Name { get; set; }
        public String Address { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public String Message { get; set; }
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
    }
}