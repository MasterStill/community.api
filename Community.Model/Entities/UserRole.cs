using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Global;
namespace Community.Model.Entities
{
    //public class UserRole : CommunityMandatory ,ICommunityBase
    public class UserRole : ICommunityBase
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public int WebsiteId {get;set;}
        public  Role Role { get; set; }
        public  User User {get;set;}
        public  Website Website {get;set;}
        public int CreatedById {get;set;}
        //public Datetime CreatedDate{get;set;}
        public int MultiLingualId {get;set;}
        public bool Delflag{get;set;}
    }
}