﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.News
{
    public class News : CommunityMandatory, ICommunityBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public string ThumbnameUrl { get; set; }
        public virtual ICollection<NewsImage> Images { get; set; }
        public DateTime PublishDate { get; set; }
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown

        public string password { get; set; }
    }
}