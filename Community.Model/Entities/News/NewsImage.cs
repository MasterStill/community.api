﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.News
{
    public class NewsImage : CommunityMandatory, ICommunityBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }


    }
}
