﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.News
{
    public class Tag : CommunityMandatory, ICommunityBase
    {
        public  string Name { get; set; }
        public  ICollection<News> News { get; set; }
    }
}
