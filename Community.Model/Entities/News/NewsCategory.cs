﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.News
{
        public class NewsCategory
        {
            public  int NewsId { get; set; }
            public  int CategoryId { get; set; }
            public  News News { get; set; }
            public  Category Category { get; set; }
        }
    }
