﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.News
{
    public class Category
    {
        public  int? ParentCategoryId { get; set; }
        public  Category ParentCategory { get; set; }
        public  ICollection<Category> ChildCategories { get; set; }
        public  ICollection<News> News { get; set; }
        public  string password { get; set; }
        public  bool Verified {get;set;}
        public  int WebsiteId {get;set;}
        public  Website Website {get;set;}
    }
}
