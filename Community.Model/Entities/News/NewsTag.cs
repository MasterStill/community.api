﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.News
{
    public class NewsTag
    {
        public  int PostId { get; set; }
        public  int TagId { get; set; }
        public  News News { get; set; }
        public  Tag Tag { get; set; }
    }
}
