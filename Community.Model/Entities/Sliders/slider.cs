using System.Collections.Generic;
using Community.Model.Entities.Global;

namespace Community.Model.Entities.Sliders
{
    public class Slider : CommunityMandatory,ICommunityBase
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Image { get; set; }
        public string BackgroundImage { get; set; }
        public int CategoryId {get;set;}
        public int WebsiteId {get;set;}
        public SliderCategory Category{get;set;}
        public virtual Website Website{get;set;}
        public string Url {get;set;}
    }
    public class SliderCategory: CommunityMandatory,ICommunityBase
    {
        public string Title {get;set;}       
        public int WebsiteId {get;set;}
        public virtual Website Website{get;set;}
    }
}