﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Audit
{
    public enum AuditActionType
    {
        Create = 1,
        Update,
        Delete
    }
}