﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Audit
{
    public class AuditChange
    {
        public string DateTimeStamp { get; set; }
        //public AuditActionType AuditActionType { get; set; }
        //public string AuditActionTypeName { get; set; }
      
        public int Id {get;set;}
        public string KeyName {get;set;}
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
        public string Difference {get;set;}
       
        // public List<AuditDelta> Changes { get; set; }
        // public AuditChange()
        // {
        //     Changes = new List<AuditDelta>();
        // }
    }
}
