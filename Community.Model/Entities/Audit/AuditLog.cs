﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Audit
{
  public class AuditLog : CommunityMandatory, ICommunityBase
    {
        //public Guid Id { get; set; }
        //public int Id {get;set;}
        public int KeyFieldID { get; set; }
        public string KeyName { get; set; }
        public string DataModel { get; set; }
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
        public string Changes { get; set; }
        public int AuditActionTypeENUM { get; set; }

    }
}
