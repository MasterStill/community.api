﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.Album.Photo 
{
    public class PhotoAlbum : CommunityMandatory, ICommunityBase
    {
        public PhotoAlbum()
        {
            //Photos = new List<Photo>();
        }
        public string ThumbnailUrl
        {
         get
            {
                return Photos.Where(x=>x.Delflag==false).Where(x=>x.Verified == true).Where(x => x.AlbumThumbNail == true).Select(x => x.ImageUrl).FirstOrDefault();
            }
        }
        public string URL{
            get{
                try{
                    if(!Website.Construction)
                    return "http://" + Website.Name + "/Gallery/" + Title.Replace(" ","-");
                    return "http://" + Globals.Global.OnConstructionWebsiteName(Website.Alias) + "/Gallery/" + Title.Replace(" ","-");
                }
                catch{
                    if(!Website.Construction)
                    return "http://" + Website.Name + "/Gallery/" + Title;
                    return "http://" + Globals.Global.OnConstructionWebsiteName(Website.Alias) + "/Gallery/" + Title;
                }

            }
        }
        public int TotalPhotos
        {
            get { return Photos.Where(x=>x.Delflag ==false).Count(); }
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public ICollection<PhotoAlbumPhoto> Photos { get; set; }    
        public int WebsiteId { get; set; }
        public string Password { get; set; }
        //public int? CategoryId { get; set; }
        public virtual  PhotoAlbumCategory Category { get; set; }
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
    }
}