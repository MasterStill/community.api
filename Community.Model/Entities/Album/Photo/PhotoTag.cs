﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Photo
{
    public class PhotoAlbumPhotoPhotoAlbumTag
    {
        public virtual int PostId { get; set; }
        public virtual int TagId { get; set; }
        public virtual PhotoAlbumPhoto Photo { get; set; }
        public virtual PhotoAlbum Tag { get; set; }

    }
}
