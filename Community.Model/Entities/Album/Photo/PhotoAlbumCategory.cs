﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.Album.Photo
{
    public class PhotoAlbumCategory : CommunityMandatory,ICommunityBase
    {
        public PhotoAlbumCategory()
        {
          //  this.Photos = new List<Photo>();
//            this.PhotoAlbums = new List<PhotoAlbum>();
        }
       public string Title { get; set; }

       public int WebsiteId {get;set;}
       public virtual Website Website {get;set;}

        //public virtual ICollection<PhotoAlbum> PhotoAlbums { get; set; }
        //public virtual ICollection<Photo> Photos { get; set; }
    }
}
