﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Photo
{
    public class PhotoAlbumPhoto : CommunityMandatory, ICommunityBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public virtual PhotoAlbum Album { get; set; }
        public int AlbumId { get; set; }
        public bool AlbumThumbNail { get; set; }
        public int WebsiteId {get;set;}
    }
}