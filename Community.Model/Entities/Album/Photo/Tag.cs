﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Photo
{
    public class PhotoAlbumTag
    {
        public virtual string Name { get; set; }
        public virtual ICollection<PhotoAlbumPhoto> Photos { get; set; }
    }
}