﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Video
{
    public class Video : CommunityMandatory, ICommunityBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Uri { get; set; }
        public virtual VideoAlbum Album { get; set; }
        public int AlbumId { get; set; }
    }
}