﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Video
{
    public class VideoAlbum : CommunityMandatory, ICommunityBase
    {
        public VideoAlbum()
        {
            Videos = new List<Video>();
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Video> Videos { get; set; }
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown

        public string password { get; set; }
    }
}
