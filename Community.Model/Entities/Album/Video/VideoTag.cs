﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Video
{
    public class VideoTag
    {
        public virtual int PostId { get; set; }
        public virtual int TagId { get; set; }
        public virtual Video Video { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
