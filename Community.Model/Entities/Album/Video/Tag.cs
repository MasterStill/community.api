﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Album.Video
{
    public class Tag
    {
        public virtual string Name { get; set; }
        public virtual ICollection<Video> Videos { get; set; }
    }
}
