﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.video
{
    public class Genre : CommunityMandatory,ICommunityBase
    {
        public Genre()
        {
            Videos = new List<Community.Model.Entities.Album.Video.Video>();
        }
        //public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Community.Model.Entities.Album.Video.Video> Videos { get; set; }
    }
}