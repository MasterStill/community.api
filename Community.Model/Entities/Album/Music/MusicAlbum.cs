﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Music
{
    public class MusicAlbum : CommunityMandatory, ICommunityBase
    {
        public MusicAlbum()
        {
            Musics = new List<Music>();
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Music> Musics { get; set; }
        public string password { get; set; }
        public string WebsiteId { get; set; }
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
    }
}