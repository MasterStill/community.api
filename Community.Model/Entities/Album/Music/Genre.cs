﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Music
{
    public class Genre : CommunityMandatory, ICommunityBase
    {
        public Genre()
        {
            Musics = new List<Community.Model.Entities.Album.Music.Music>();
        }
        //public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Community.Model.Entities.Album.Music.Music> Musics { get; set; }
    }
}