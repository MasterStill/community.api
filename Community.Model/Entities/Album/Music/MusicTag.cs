﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Music
{
    public class MusicTag
    {
        public virtual int PostId { get; set; }
        public virtual int TagId { get; set; }
        public virtual Music Music { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
