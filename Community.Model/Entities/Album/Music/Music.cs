﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Album.Music
{
    public class Music : CommunityMandatory, ICommunityBase
    {
        //public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Uri { get; set; }
        public virtual MusicAlbum Album { get; set; }
        public int AlbumId { get; set; }
    }
}