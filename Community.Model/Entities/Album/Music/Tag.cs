﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Album.Music
{
    public class Tag
    {
        public virtual string Name { get; set; }
        public virtual ICollection<Music> Posts { get; set; }
    }
}
