﻿using System;
using System.Collections.Generic;
namespace Community.Model.Entities.ViewModels
{
    public class SupportTicketSingleViewModel //: CreateMandatory
    {
        public int Id {get;set;}
        public string TicketId { get; set; }
        public string Status {get;set;}
        public string Subject {get;set;}
        public string CreatedBy {get;set;}
        public string CreatedDate {get;set;}
        public string ScreenShot {get;set;}
        public string Department {get;set;}
        public string Description {get;set;}
        public List<SupportTicketReplyViewModel> Reply {get;set;}
    }
}