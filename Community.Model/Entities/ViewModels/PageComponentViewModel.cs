﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Pages;

namespace Community.Model.Entities.ViewModels
{
    public class PageComponentViewModel //: CreateMandatory
    {
       public LayoutType LayoutType {get;set;}
       public List<WebsiteComponentViewModel> Components {get;set;}
       public List<UserComponentViewModel> BodyLeft {get;set;}
       public List<UserComponentViewModel> Body {get;set;}
       public List<UserComponentViewModel> BodyRight {get;set;}
       public List<UserComponentViewModel> Footer {get;set;}

    }
    public class PageComponentCreateViewModel //: CreateMandatory
    {       
       public List<UserComponentCreateViewModel> BodyLeft {get;set;}
       public List<UserComponentCreateViewModel> Body {get;set;}
       public List<UserComponentCreateViewModel> BodyRight {get;set;}
       public List<UserComponentCreateViewModel> Footer {get;set;}

    }
    public class PageViewModel{
        public int Id {get;set;}
        public string Name {get;set;}
    }
    public class LayoutType{
        public bool BodyLeft {get;set;}
        public bool Body {get;set;}
        public bool BodyRight {get;set;}
        public bool Footer {get;set;}
    }
    public class UserComponentViewModel{
        
        public int Id {get;set;}
        public string Name {get;set;}
        public int Order {get;set;}
        public int Count {get;set;}
        public Guid Hash {get;set;}
        
    }
    public class UserComponentCreateViewModel{
        
        public int Id {get;set;}
        public int Order {get;set;} 
         public int Count {get;set;}    
          
    }
public enum ComponentType{
    Main , Sidebar
}

    public class WebsiteComponentViewModel{
        public int Id {get;set;}
        public string Name {get;set;}
        public string ThemeComponentName {get;set;}
        public string Type{get;set;}
        public string Module{get;set;}
        public Guid Guid {
            get{
                return Guid.NewGuid();
            }
        }
    }
}
