﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.ViewModels
{
    public class WebsiteViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Redirect { get; set; }
        public string AllocatedSpace { get; set; }
        public string UsedSpace { get; set; }
        public string RedirectToUrl { get; set; }
        public string DisplayWebsiteOf { get; set; }
        public string Verified { get; set; }
        public string URL { get; set; }
        public bool Construction { get; set; }
        public List<ClientModuleViewModel> Modules { get; set; }
    }
    public class ModuleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool CurrentlyUsed { get; set; }
    }
    public class ClientModuleViewModel
    {
        public string DisplayName { get; set; }
        public string MenuJson { get; set; }
    }
}
