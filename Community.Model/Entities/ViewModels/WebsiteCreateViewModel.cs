﻿using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Pages;
namespace Community.Model.Entities.ViewModels
{
    public class WebsiteCreateViewModel //: CreateMandatory
    {
        [Required]
        public string Name { get; set; }
        public bool Redirect {get;set;}
        public string RedirectToUrl { get; set; }
        public int MultiLingualId {get;set;}
        public string Theme {get;set;}
        public string Alias{get;set;}
        public int[] Cultures {get;set;}
        public int[] Modules {get;set;}
    }
    public class WebsitePageLayoutCreateViewModel{
        [Required]
        public int PageId {get;set;}
        [Required]
        public SinglePageLayout Layout{get;set;}
        [Required]
        public int WebsiteId {get;set;}        
    }
}