﻿using System.ComponentModel.DataAnnotations;

namespace Community.Model.Entities.ViewModels
{
    public class GenericWhatValueViewModel
    {
        [Required]
        public string What { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
        public string Description {get;set;}
        public string Url {get;set;}
        public string Icon {get;set;}

    }
}
