﻿namespace Community.Model.Entities.ViewModels
{
      public class SiteMap
        {
            public string WebsiteName { get; set; }
            public string URL { get; set; }
        }
}