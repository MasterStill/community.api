﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.ViewModels
{
    public class SliderViewModel
    {
        public int Id{get;set;}
        public string Title { get; set; }
        public string Summary {get;set;}
        public int CategoryId { get; set; } 
        public string Image { get; set; }
        public string BackgroundImage{get;set;}   
        public string Url {get;set;}    
    }
    public class SliderCategoryViewModel
    {
        public int Id{get;set;}
        public string Title { get; set; }               
    }
    public class SliderCategoryCreateViewModel : CreateMandatory
    {
        public int Id{get;set;}
        public string Title { get; set; }               
    }
     public class SliderCreateViewModel : CreateMandatory
    {
        public int Id{get;set;}
        [Required]
        public string Title { get; set; }
        public string Summary {get;set;}
        public string Image { get; set; }
        public string BackgroundImage{get;set;}
        public int CategoryId { get; set; }     
        public string Url {get;set;}     
    }
}
