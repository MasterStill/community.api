﻿namespace Community.Model.Entities.ViewModels
{
        public class PhotoAlbumPhotoCreateViewModel : CreateMandatory
        {
        public int AlbumId {get;set;}
        public string[] Photos{get;set;}         
        }
        
        public class PhotoAlbumPhotoEditViewModel : CreateMandatory
        {
        public int AlbumId {get;set;}
        public int Id {get;set;}
        public string Description {get;set;}
        public string Title {get;set;}
        public string ImageUrl {get;set;}
        public bool AlbumThumbNail { get; set; }
        }
}