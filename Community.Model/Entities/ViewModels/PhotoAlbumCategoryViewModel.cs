﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.ViewModels
{
    public class PhotoAlbumCategoryViewModel
    {
        public int Id { get; set; }
        public string Title {get;set;}
        public bool Verified {get;set;}
    }
}
