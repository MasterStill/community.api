﻿namespace Community.Model.Entities.ViewModels
{
    public class BlogCategoryViewModel
    {
        public int Id {get;set;}
        public string Title { get; set; }
        public string URL { get; set; }
    }
}
