﻿using System.Collections.Generic;
namespace Community.Model.Entities.ViewModels
{
    public class TcViewModel
    {
        public List<ThemeComponenttcfmViewModel> ThemeComponent { get; set; }
        public List<ThemeComponentFieldtcfmViewModel> ThemeComponentField { get; set; }
    }
    public class ThemeComponentFieldtcfmViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Selected { get; set; }
    }
    public class ThemeComponenttcfmViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}