﻿namespace Community.Model.Entities.ViewModels
{
    public class MultiLingualViewModel
    {
        public int Id {get;set;}
        public bool Converted {get;set;}
        public int CultureId {get;set;}
        public int MultiLingualId {get;set;}
    }  
}
  