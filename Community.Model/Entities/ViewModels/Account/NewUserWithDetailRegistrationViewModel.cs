﻿using System.ComponentModel.DataAnnotations;
namespace Community.Model.Entities.ViewModels.Account
{
    public class NewUserWithDetailRegistrationViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Website {get;set;}
        [Required]
        public string WebsiteType {get;set;}
        [Required]
        public string Alias {get;set;}
        [Required]
        public string Theme {get;set;}
        [Required]
        public int[] Culture {get;set;} 
        [Required]
        public int[] Module {get;set;}
    }
}