﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.ViewModels
{
    public class BlogTagViewModel
    {
        public string Url { get; set; }
        public string Name { get; set; }
    }
}
