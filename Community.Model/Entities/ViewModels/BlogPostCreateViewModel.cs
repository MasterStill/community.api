﻿using System;
using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Blog;

namespace Community.Model.Entities.ViewModels
{
    public class BlogPostCreateViewModel : CreateMandatory
    {
        public int Id {get;set;}        
        [Required(ErrorMessage = "TitleRequired")]
        public string Title { get; set; }
        [Required(ErrorMessage = "ContentRequired")]
        public string Content {get;set;}
        [Required(ErrorMessage = "SummaryRequired")]
        public string Summary {get;set;}
        public bool CommentAllowed {get;set;}
        public string Password {get;set;}
        [Required(ErrorMessage = "CategoryRequired")]
        public int CategoryId {get;set;}
        public string[] Tag {get;set;}
        [Required(ErrorMessage = "ThumbNailUrlRequired")]
        public string ThumbNailUrl {get;set;}
        [Required(ErrorMessage = "ImageUrlRequired")]
        public string ImageUrl {get;set;}
        [Required(ErrorMessage = "BlogTypeRequired")]
        public BlogType Type {get;set;}
        public DateTime PublishDate{get;set;}
    }
}