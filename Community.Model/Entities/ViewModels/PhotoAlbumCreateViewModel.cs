﻿using System.ComponentModel.DataAnnotations;
namespace Community.Model.Entities.ViewModels
{
        public class PhotoAlbumCreateViewModel : CreateMandatory
        {
        public int Id {get;set;}
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public string Password { get; set; }
    }
}