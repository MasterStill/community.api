﻿using System.ComponentModel.DataAnnotations;
namespace Community.Model.Entities.ViewModels
{
    public class MenuCreateViewModel : CreateMandatory
    {
        public int Id {get;set;}
        [Required]
        public string Name { get; set; }
        public string MenuUrl {get;set;}
        public bool NewWindow {get;set;}
        public int? ParentMenu{get;set;}
        public int Order {get;set;}
    }
}
