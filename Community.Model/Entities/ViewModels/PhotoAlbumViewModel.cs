﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.ViewModels
{
    public class PhotoAlbumViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ThumbnailUrl { get; set; }
        public string URL { get; set; }
        public int TotalPhotos { get; set; }
        public bool Verified { get; set; }
    }
}
