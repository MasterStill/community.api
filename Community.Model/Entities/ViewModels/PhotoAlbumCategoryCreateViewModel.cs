﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.ViewModels
{
    public class PhotoAlbumCategoryCreateViewModel : CreateMandatory
    {
        public string Name{get;set;}
    }
}
