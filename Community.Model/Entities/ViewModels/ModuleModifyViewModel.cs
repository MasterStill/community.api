﻿using System.ComponentModel.DataAnnotations;

namespace Community.Model.Entities.ViewModels
{
 public class ModuleModifyViewModel
        {
            [Required]
            public int[] UsedModule { get; set; }
        }
}