﻿namespace Community.Model.Entities.ViewModels
{
    public class GlobalizationViewModel{        
        public string Key {get;set;}
        public string Content {get;set;}
    }   
}
