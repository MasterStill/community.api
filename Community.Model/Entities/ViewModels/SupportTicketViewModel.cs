﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Support;
namespace Community.Model.Entities.ViewModels
{
    public class SupportTicketViewModel //: CreateMandatory
    {
        public int Id {get;set;}
        public string  CreatedByFullName {get;set;}
        public string TicketId { get; set; }  
        public string Status {get;set;}
        public string Subject {get;set;}
        public string SupportTicketDepartmentName {get;set;}
        public string CreatedDate {get;set;}
        public string  LastRepliedDate {get;set;}
    }
}
