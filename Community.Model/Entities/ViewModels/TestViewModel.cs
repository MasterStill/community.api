﻿using System.ComponentModel.DataAnnotations;
namespace Community.Model.Entities.ViewModels
{
    public class TestViewModel
    {
        public int Id {get;set;}
        public string Title { get; set; }
        public string Name { get; set; }
        public string ThumbnailUrl {get;set;}  
        public string URL {get;set;}
        [MaxLength(10)]
        public string Summary {get;set;}
        public string CreatedDate {get;set;}
        public string Image {get;set;}
    }
}
