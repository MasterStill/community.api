﻿using System.Collections.Generic;

namespace Community.Model.Entities.ViewModels
{
    public class WebisteSuggestionViewModel
    {
        public bool OurDomain {get;set;}
        public bool Status {get;set;}
        public string[] Suggestion {get;set;}

    }
}