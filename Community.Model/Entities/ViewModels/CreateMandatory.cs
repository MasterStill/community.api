﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.ViewModels
{
    public class CreateMandatory
    {
        public int WebsiteId { get; set; }
        public int CultureId {get;set;} // The Current Culture 
        public int MultiLingualId {get;set;} // The Translated Culture for 
    }
}