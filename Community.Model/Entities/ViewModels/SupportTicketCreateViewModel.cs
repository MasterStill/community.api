﻿using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Support;
namespace Community.Model.Entities.ViewModels
{
    public class SupportTicketCreateViewModel : CreateMandatory
    {
        public Status Status {get;set;}
        [Required]
        public string Subject {get;set;}
        [Required]
        public string Description {get;set;}
        public string ScreenShot {get;set;}
        [Required]
        public int SupportTicketDepartmentId {get;set;}
    }
}
