﻿using System;
namespace Community.Model.Entities.ViewModels
{
    public class SupportTicketReplyViewModel //: CreateMandatory
    {
        public string Reply {get;set;}
        public string CreatedBy {get;set;}
        public string Image {get;set;}
        public string UserType {get;set;}
        public string CreatedDate {get;set;}
        public string ScreenShot {get;set;}
    }
}
