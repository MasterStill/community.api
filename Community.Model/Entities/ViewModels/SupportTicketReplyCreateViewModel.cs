﻿using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Support;
namespace Community.Model.Entities.ViewModels
{
    public class SupportTicketReplyCreateViewModel : CreateMandatory
    {
        [Required]
        public int TicketId {get;set;}
        [Required]
        public string Reply {get;set;}
        [Required]
        public Status Status {get;set;}
        public string ScreenShot {get;set;}
    }
}
