﻿namespace Community.Model.Entities.ViewModels
{
    public class ThemeViewModel
    {
        public int Id {get;set;}
        public string Name { get; set; }
        public string Image { get; set; }
         public string Author { get; set; }
         public string Designer { get; set; }
         public string CopyRightNotice { get; set; }
        public int Price { get; set; }
        public string Summary {get;set;}
        public bool Selected {get;set;}
        public string URL {get;set;}
        // public int SeletedTheme { get; set; }
    }
}
