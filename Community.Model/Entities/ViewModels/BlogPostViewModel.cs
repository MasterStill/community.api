﻿namespace Community.Model.Entities.ViewModels
{
    public class BlogPostViewModel
    {
        public int Id {get;set;}
        public string Title { get; set; }
       // public string Content {get;set;}
        public bool Converted {get;set;}
        public string Summary {get;set;}

        public string Type {get;set;}
        public string CreatedDate {get;set;}
        public string PublishedDate {get;set;}
        public string ThumbnailUrl {get;set;}
        public int CultureId {get;set;}
        public string URL {get;set;}
        public int MultiLingualId {get;set;}
        //public bool Verfied {get;set;}
    }  
}
  