﻿using System.ComponentModel.DataAnnotations;
namespace Community.Model.Entities.ViewModels
{
    public class ThemeCreateViewModel
    {
        public int Id {get;set;}
        [Required]
        public string Name { get; set; }
        public string Image { get; set; }
        public string Author { get; set; }
        public string Designer { get; set; }
        public string CopyRightNotice { get; set; }
        public int Price { get; set; }
        public int CultureId {get;set;}
    }
}
