﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.ViewModels
{
    public class AutoItViewModel
    {
        public string WebsiteName { get; set; }
        public int CurrentSize {get;set;}
    }
}
