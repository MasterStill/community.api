﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Community.Model.Entities.ViewModels
{
    public class NotesCreateViewModel{        
        public int Id {get;set;}
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public bool Archive {get;set;}
        public string Image { get; set; }                
        public string Color { get; set; }        
        public DateTime? Reminder {get;set;} // Remind at time
        public int[] Label {get;set;}
        public virtual List<CheckListViewModel> CheckList{get;set;}        
    }
    public class NotesViewModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public bool archive { get; set; }
        public string image { get; set; }
        public DateTime? time { get; set; } // Created Time        
        public string color { get; set; }
        public int createdById { get; set; }
        public DateTime? reminder { get; set; } // Remind at time
        public List<CheckListViewModel> checkList { get; set; }
        public int[] label { get; set; }
    }

    public class CheckListViewModel 
    {
        public int id { get; set; }
        public bool Checked { get; set; }
        public string text { get; set; }
    }
    public class LabelViewModel
    { 
        public int id { get; set; }
        public string name { get; set; }
        public string color { get; set; }
    }

    public class LabelViewModelOnlyID
    {
        public int id { get; set; }
    }
}
