﻿using System.Collections.Generic;

namespace Community.Model.Entities.ViewModels
{
    public class StepsViewModel //: CreateMandatory
    {
        //public List<StaticGlobalization> Values {get;set;}
        public int Step {get;set;} 
        public List<GlobalizationViewModel> Values {get;set;}

    }
}