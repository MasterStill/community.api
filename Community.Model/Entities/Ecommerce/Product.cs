﻿using System;
namespace Community.Model.Entities.Ecommerce
{
    public class Product : CommunityMandatory, ICommunityBase
    {
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Decimal Price { get; set; }
        public string Summary { get; set; }
        public string Speciality { get; set; }
        public string ImageUrl1 { get; set; }
        public string ImageUrl2 { get; set; }
        public string ImageUrl3 { get; set; }
        public string ImageUrl4 { get; set; }
        public string ImageUrl { get; set; }
        public EcommerceCategory Category { get; set; }
        public int WebsiteId {get;set;}
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
    }
}
