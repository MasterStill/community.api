﻿using System;
using System.Collections.Generic;
namespace Community.Model.Entities.Ecommerce
{
    public class Order : CommunityMandatory, ICommunityBase
    {        
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public decimal Total { get; set; }
        public DateTime OrderDate { get; set; }
        public OrderStatus Status {get;set;}
        public PaymentType Payment {get;set;}
        public List<OrderDetail> OrderDetails { get; set; }
    }
}
