﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Ecommerce
{
    public class EcommerceCategory : CommunityMandatory, ICommunityBase
    { 
          public String Title { get; set; }
          public Boolean HasSubCategory { get; set; }
          public int Categoryof { get; set; }
          public int DisplayOrder { get; set; }
          public string Description { get; set; }
          public List<Product> Products { get; set; }
          public string Password { get; set; }
          public int WebsiteId {get;set;} 
          public virtual Model.Entities.Global.Website Website { get; set; }
    }
}