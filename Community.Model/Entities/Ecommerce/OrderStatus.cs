﻿using System.ComponentModel.DataAnnotations;

namespace Community.Model.Entities.Ecommerce
{
    public enum OrderStatus
    {
        [Display(Name = "On Pre-Order(Not Paid)")]
        PreOrder_NotPaid = 1,
        [Display(Name = "Awaiting Check Payment")]
        Awating_Check_Payment = 2,
    }
}
