﻿using System.ComponentModel.DataAnnotations;

namespace Community.Model.Entities.Ecommerce
{
    public enum PaymentType
    {
        [Display(Name = "Cheque")]
        Cheque = 1,
        [Display(Name = "Cash")]
        Cash = 2,
    }
}
