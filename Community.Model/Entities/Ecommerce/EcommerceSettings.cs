﻿using System;
namespace Community.Model.Entities.Ecommerce
{
    public class EcommerceSettings  : CommunityMandatory, ICommunityBase
    {
        public String Title { get; set; }
        public String Description { get; set; }
        public String Summary { get; set; }
        public String ImageUrl { get; set; }
        public int WebsiteId {get;set;}
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
    }
}
