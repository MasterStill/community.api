﻿using System;
namespace Community.Model.Entities.Ecommerce
{
    public class Cart
    {
        public int RecordId { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual Product Product { get; set; }
        public int WebsiteId {get;set;} 
        public virtual Model.Entities.Global.Website Website { get; set; }
    }
}