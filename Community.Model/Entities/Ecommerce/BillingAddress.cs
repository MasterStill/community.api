﻿using System;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.Ecommerce
{
    public class BillingDetails
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public Guid UserId { get; set; }
        public User User {get;set;}
    }
}