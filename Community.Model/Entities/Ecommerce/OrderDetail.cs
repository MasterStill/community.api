﻿using System;
namespace Community.Model.Entities.Ecommerce
{
    public class OrderDetail: CommunityMandatory, ICommunityBase
    {
        public int Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
    }
}
