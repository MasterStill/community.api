using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Global;
namespace Community.Model.Entities
{
    public class Notes// : CommunityMandatory ,ICommunityBase
    {
        public Notes(){
            this.CheckList = new List<TodoList>();
            this.NotesLabel = new List<NotesLabel>();
        }
        public int Id {get;set;}
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Archive {get;set;}
        public string Image { get; set; }
        public DateTime? Time {get;set;} // Created Time        
        public string Color { get; set; }
        public int CreatedById {get;set;}
        public DateTime? Reminder {get;set;} // Remind at time
        public virtual List<TodoList> CheckList{get;set;}
        public virtual User CreatedBy{get;set;}
        public bool DelFlag {get;set;}
        //public virtual List<Label> Label {get;set;}
        public virtual List<NotesLabel> NotesLabel   { get; set; }

    }
public class NotesLabel{
    public NotesLabel(){
        this.Notes = new Notes();
        this.Label = new Label();
    }
    public int NotesId {get;set;}
    public int LabelId {get;set;}
    public virtual Notes Notes {get;set;}
    public virtual Label Label {get;set;}
}
    
    public class TodoList{
        public int Id {get;set;}
        public bool Checked {get;set;}
        public string Text {get;set;}
        public virtual Notes Notes {get;set;}
    }
    public class Label{
        public int Id {get;set;}
        [Required]
        public string Name {get;set;}
        public string Color { get; set; }
        public int CreatedById {get;set;}        
        public virtual User CreatedBy{get;set;}
        public virtual List<NotesLabel> NotesLabel { get; set; }       
    }
}