﻿using System.Collections.Generic;

namespace Community.Model.Entities.Theme
{
    public class Theme : CommunityMandatory, ICommunityBase
    {
        // public Theme(){
        //     Components = new List<ThemeComponent>();
        // }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Author { get; set; }
        public string Designer { get; set; }
        public string CopyRightNotice { get; set; }
        public bool DispalyInPublic { get; set; }
        public string Summary{get;set;}
        public int Price { get; set; }
        public string URL {get;set;}
        public virtual ICollection<ThemeComponent> Components {get; set; }
        //public List<ThemeThemeComponent> ThemeThemeComponent {get;set;}
    }
}