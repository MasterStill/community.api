﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Community.Model.Entities.Theme
{
    public class ThemeComponent : CommunityMandatory, ICommunityBase
    {
        public string Name { get; set; }
    }

    public class ThemeComponentFields
    {
        public int ThemeComponentId {get;set;}
        public int ComponentFieldId {get;set;}
        public virtual ThemeComponent  ThemeComponent{get;set;}
        public virtual ComponentField ComponentField {get;set;}
    }

    public class ThemeComponentValueSingleViewModel{
        [Required]
        public string FieldName {get;set;}
        public string Value {get;set;}
    }
     public class ThemeComponentValueCreateViewModel{
        public int CultureId {get;set;}
        public int WebsiteId {get;set;}
        public List<ThemeComponentValueSingleViewModel> Settings {get;set;}        
    }
    public enum ComponentType{
        Main,List
    }
    // public enum ComponentLocation{
    //     Top = 1, Middle, Bottom, Navigation, Navigation2, Header, Footer, AfterFooter
    // }
    // public enum ComponentPosition { Left = 1, Middle, Right, Above, Below }
}