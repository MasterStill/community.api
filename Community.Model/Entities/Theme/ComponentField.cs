﻿namespace Community.Model.Entities.Theme
{
    public class ComponentField : CommunityMandatory, ICommunityBase
    {
       public string FieldName {get;set;}
        public DataType DataType {get;set;}
    }
    public enum DataType{
         String , Component
     }        
}