﻿using System;
using Community.Model.Entities.Global;
using System.Collections.Generic;
namespace Community.Model.Entities.Globalization
{
    public class UserCulture //: ICommunityBase
    {
        public int UserId {get;set;}
        public int CultureId {get;set;}

        public virtual Culture Culture{get;set;}
        public virtual User User{get;set;}
    }
}