﻿using System;
using Community.Model.Entities.Global;
using System.Collections.Generic;
namespace Community.Model.Entities.Globalization
{
    public class Culture : ICommunityBase
    {
        public int Id {get;set;}
        public string Title { get; set; }
        public string Code { get; set; }
        public int CreatedById { get; set; }
        public bool Delflag { get; set; }
        public bool Verified {get;set;} 
        public int MultiLingualId {get;set;}

        public int CultureId {get;set;}
        public virtual  ICollection<WebsiteCulture>  Website { get; set; }
        public virtual User CreatedBy{get;set;}
        public virtual ICollection<UserCulture> UserCulture { get; set; }
    }
}