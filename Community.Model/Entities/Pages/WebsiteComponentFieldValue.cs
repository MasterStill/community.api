﻿using System.Collections.Generic;
using Community.Model.Entities.Global;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.Theme;
namespace Community.Model.Entities.Pages
{
    public class WebsiteComponentFieldValue //: CommunityMandatory,ICommunityBase
    {              
       public int Id {get;set;}
       public int CultureId {get;set;}
       public int WebsiteComponentId {get;set;}       
       public int ComponentFieldId {get;set;}
       public string Value {get;set;}
       public virtual Culture Culture {get;set;}
       public  WebsiteComponent WebsiteComponent {get;set;}
       public  ComponentField ComponentField {get;set;}
    }
}