﻿using System.Collections.Generic;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.Pages
{
    public class Page : CommunityMandatory,ICommunityBase
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string DisplayName {get;set;}
        
        // public int WebsiteId { get; set; }
        // public virtual Website Website { get; set; }
        public ICollection<WebsitePage> WebsitePage {get;set;}        
    }
}