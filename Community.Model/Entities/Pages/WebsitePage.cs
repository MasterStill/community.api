﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Global;
using Community.Model.Entities.Module;

namespace Community.Model.Entities.Pages
{
    public class WebsitePage //: CommunityMandatory,ICommunityBase
    {
        public WebsitePage(){
//            this.WebsiteComponent = new List<WebsiteComponent>();
        }
        public int WebsiteId { get; set; }
        public int PageId {get;set;}
        
        public int WebsiteComponentId {get;set;}
        public Location Location {get;set;}
        public int Order {get;set;}
        public virtual Website Website { get; set; }

        //public string Title {get;set;}

        public int Count {get;set;}
        public  ModuleOptions Options {get;set;}
        public virtual Page Page {get;set;}
        public virtual WebsiteComponent WebsiteComponent {get;set;}        
    }
    public enum SinglePageLayout{
        LeftMiddle,Middle,MiddleRight,LeftMiddleRight
    }
    public class WebsitePageLayout{
        // public WebsitePageLayout(){
        //     this.Page = new Page();
        //     this.Website = new Website();
        // }
        [Key]
        public int Id {get;set;}
        public int PageId {get;set;}
        public int WebsiteId {get;set;}
        public SinglePageLayout Layout{get;set;}
        //public string Layout{get;set;}
        public Page Page {get;set;}
        public Website Website{get;set;}
    }

    public enum Location{
        Header,Body,Footer,BodyLeftSidePanel,BodyRightSidePanel
    }
}