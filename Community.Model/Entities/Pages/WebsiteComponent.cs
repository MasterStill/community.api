﻿using Community.Model.Entities.Global;
using Community.Model.Entities.Theme;
namespace Community.Model.Entities.Pages
{
    public class WebsiteComponent //: CommunityMandatory,ICommunityBase
    {
       public int Id {get;set;}       
       public string Name {get;set;}

    //    public int?Count {get;set;}

      //public int? ModuleId {get;set;}
       public int WebsiteId {get;set;}
       public int ThemeComponentId {get;set;}
       //public string ThemeComponentName {get;set;}
       //c int?Order {get;set;}
       public int? ModuleId {get;set;}
       public virtual Module.Module Module {get;set;}
       //c int? ComponentOrder {get;set;}
       //  lic int? Location {get;set;}
       public virtual ThemeComponent  ThemeComponent {get;set;}
       public virtual Website Website {get;set;}
       public bool DelFlag {get;set;}
       //c virtual Entities.Module.Module Module {get;set;}
    }
    //  public enum PageThemeComponentLocation{
    //     Body
    //  }
}