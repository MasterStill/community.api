﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Blog
{
    public enum Visibility { Public = 1, Private }
    public enum PublishStatus { Draft = 1, WaitingForReview, Published, Deleted }
    public enum CommentStatus { WaitingForReview = 1, Published, Spam, Deleted }
    public enum ContentType
    {
        Post = 1, Page, Link, Navigation, Html,
        RecentPosts, RelatedPosts, Pages, Categories, TagCloud,
        ContactUs
    }
    public enum WidgetLocation
    { Top = 1, Middle, Bottom, Navigation, Navigation2, Header, Footer, AfterFooter }
    public enum WidgetPosition { Left = 1, Middle, Right, Above, Below }
}
