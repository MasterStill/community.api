﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Blog
{
    public class PageWidget
    {
        public virtual int PageId { get; set; }
        public virtual int WidgetId { get; set; }
        public virtual WidgetLocation WidgetLocation { get; set; }
        public virtual int AttachToWidgetId { get; set; }
        public virtual WidgetPosition AttachToWidgetPosition { get; set; }
        public virtual int WidgetOrder { get; set; }
    }
}
