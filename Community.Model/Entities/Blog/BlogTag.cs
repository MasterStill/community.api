﻿using System.Collections.Generic;
namespace Community.Model.Entities.Blog
{
    public class BlogTag: CommunityMandatory, ICommunityBase
    {
        public BlogTag(){
            this.Tags = new List<BlogPostBlogTag>(); 
        }
        public string Name { get; set; }
        public string URL{
            get{                
                return Culture.Title + "/"+ Tags[0].Post.BlogType.ToString()  +"/Tag/" +  Globals.Global.GetCleanUrl(Name) ;//+ "/" ;  
            }
        }
        public virtual List<BlogPostBlogTag> Tags { get; set; }
    }
}