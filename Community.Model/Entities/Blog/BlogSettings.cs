﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Blog
{
    public class BlogSettings: CommunityMandatory, ICommunityBase
    {
        public virtual string Title { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual PublishStatus PublishStatus { get; set; }
        public virtual Visibility Visibility { get; set; }
        public virtual bool? CommentAllowed { get; set; }
    }
}