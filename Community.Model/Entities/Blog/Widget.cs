﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Blog
{
    public class Widget
    {
        public virtual string Name{ get; set; }
        public virtual ContentType ContentType{ get; set; }
        public virtual int? ContentTypeId{ get; set; }
        public virtual int? MaxDisplayCount{ get; set; }
        public virtual string Content{ get; set; }
        public virtual int? ParentWidgetId{ get; set; }
    }
}
