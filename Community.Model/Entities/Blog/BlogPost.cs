﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.Global;
using Community.Model;
namespace Community.Model.Entities.Blog
{
    public class BlogPost : CommunityMandatory, ICommunityBase
    {
        public BlogPost()
        { 
            Tags = new List<BlogPostBlogTag>();
        }
        public string Url{
            get {
                if(!Website.Construction)
                return "http://" + Website.Name + "/" + Culture.Title + "/" + BlogType   +"/" +  Globals.Global.GetCleanUrl(Category.Title) + "/" +  MultiLingualId + "/" +  Globals.Global.GetCleanUrl(Title);
                  return "http://" + Globals.Global.OnConstructionWebsiteName(Website.Alias) + "/" + Culture.Title + "/" + BlogType   +"/" +  Globals.Global.GetCleanUrl(Category.Title) + "/" +  MultiLingualId + "/" +  Globals.Global.GetCleanUrl(Title);                                  
            }
        }

         [Required(ErrorMessage = "ThumbmailRequired")]
        public string  ThumbnailUrl {get;set;}
         [Required]
        public string  ImageUrl {get;set;}
         [Required]
        // [MinLength(4)]
        // [MaxLength(50)]
        public string Title { get; set; }
         [Required]
        // [MinLength(60)]
        public string Content {get;set;}
        // [Required]
        // [MinLength(30)]
        [Required]
        public string Summary {get;set;}
        
        public bool CommentAllowed { get; set; }
        public string password { get; set; }
        [Required(ErrorMessage = "CategoryRequired")]
        public int CategoryId {get;set;}
        public int WebsiteId {get;set;}

        public virtual PublishStatus PublishStatus { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
        public virtual Visibility Visibility { get; set; }
        public virtual Model.Entities.Global.Website Website { get; set; } // Defines the Site where contents will be shown
        public virtual BlogCategory Category { get; set; }
        public virtual List<BlogPostBlogTag> Tags { get; set; }
        public BlogType BlogType {get;set;}
    }
    public enum BlogType{
        Blog,MusicVideo,News,Movies,Projects,Videos,Page
        // 0 = Blog
        //
        //
        //
        //
        //
        // 6 =page
    }

}
