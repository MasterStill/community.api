﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Blog
{
        public class BlogPostBlogCategory
        {
            public  int PostId { get; set; }
            public  int CategoryId { get; set; }
            public virtual BlogPost Post { get; set; }
            public virtual BlogCategory Category { get; set; }
        }
    }
