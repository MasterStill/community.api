﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Community.Model.Entities.Blog
{
    public class BlogPostComment: CommunityMandatory, ICommunityBase
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Email { get; set; }
        public virtual CommentStatus CommentStatus { get; set; }
        public virtual int? ParentCommentId { get; set; }
        public virtual int PostId { get; set; }
        public virtual BlogPost Post { get; set; }
    }
}
