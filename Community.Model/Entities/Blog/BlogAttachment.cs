﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Blog
{
    public class BlogAttachment: CommunityMandatory, ICommunityBase
    {
        public string Name { get; set; }
        public bool Image  { get; set; }
        public string LinkUrl  { get; set; }

    }
}