﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.Blog
{
    public class BlogCategory: CommunityMandatory, ICommunityBase
    {
        public virtual int? ParentCategoryId { get; set; }
        public string Title {get;set;}
        public virtual BlogCategory ParentCategory { get; set; }

        public string URL {
            get{
                if(!Website.Construction)
                return "http://" + Website.Name + "/" + Culture.Title + "/" + Type  +  "/Category/" +  Globals.Global.GetCleanUrl(Title);
                return "http://" + Globals.Global.OnConstructionWebsiteName(Website.Alias) + "/" + Culture.Title + "/" + Type  +  "/Category/" +  Globals.Global.GetCleanUrl(Title);
            }
        }
        public virtual ICollection<BlogCategory> ChildCategories { get; set; }
        public virtual ICollection<BlogPost> Posts { get; set; }
        public string password { get; set; }
        public int WebsiteId {get;set;}
        public Website Website {get;set;}
        public BlogType Type {get;set;}
    }
}
