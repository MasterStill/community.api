﻿namespace Community.Model.Entities.Blog
{
    public class BlogPostBlogTag
    {
        public BlogPostBlogTag(){
            this.Tag = Tag;
            this.Post =Post;
        }
        public  int PostId { get; set; }
        public  int TagId { get; set; }
        public  virtual BlogPost Post { get; set; } 
        public  virtual BlogTag Tag { get; set; }
    }
}
