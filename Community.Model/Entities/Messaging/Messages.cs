﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;

namespace Community.Model.Entities.Messaging
{
    public class Messages : CommunityMandatory,ICommunityBase
    {
        public string Message { get; set; }


        public int WebsiteId { get; set; }
        public Website Website { get; set; }

    }
}
