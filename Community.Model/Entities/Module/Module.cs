﻿using System.Collections.Generic;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.Module
{
    public class Module : CommunityMandatory, ICommunityBase
    {
        public string Name{ get; set; }
        public string Description { get; set; }
        public  ModuleOptions Options {get;set;}
        public string MenuJson {get;set;}
        public string DisplayName {get;set;}
        public int Price { get; set; }
        public int? ParentModuleId {get;set;}
        public virtual Module ParentModule {get;set;} 
        public virtual ICollection<WebsiteModule> WebsiteModule {get; set; }   
    }
    public enum ModuleOptions{
        Recent,Popular
    }
}  