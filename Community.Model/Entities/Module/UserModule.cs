﻿using Community.Model.Entities.Global;
namespace Community.Model.Entities.Module
{
    public class WebsiteModule
    {
        public int WebsiteId { get; set; }
        public int ModuleId { get; set; }
        public virtual Module Module { get; set; }
        public  virtual  Website Website { get; set; }         
    }
}
