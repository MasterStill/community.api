﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.Global;
using System.ComponentModel.DataAnnotations;
namespace Community.Model.Entities
{
    public class CommunityMandatory
    {
        public CommunityMandatory()
        {
            CreatedDate = DateTime.Now;
            Delflag = false;
            Verified = true;
        }
        [Key]
        public int Id { get; set; }
        public int MultiLingualId {get;set;}
        public DateTime CreatedDate { get; set; }
        public int CreatedById { get; set; }
        // public DateTime? ModifiedDate { get; set; }
        // public int? ModifiedBy { get; set; }
        public bool Verified { get; set; }
        //public string MultiLingual { get; set; }
        //public int BaseCultureId { get; set; } // 
        //public virtual Culture BaseCulture { get; set; }
        public bool Delflag { get; set; }
        public virtual  User CreatedBy { get; set; }
        public int? CultureId {get;set;}
        public Culture Culture {get;set;}
    } 
}