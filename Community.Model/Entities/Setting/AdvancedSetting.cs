﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;
using Community.Model.Entities.Globalization;
namespace Community.Model.Entities.Setting
{
    public class AdvancedSetting : CommunityMandatory, ICommunityBase
    {
        public string HeaderContent { get; set; }
        public string FooterContent { get; set; }
        public  TimeZone TimeZoneId { get; set; }
        public List<Culture> LanguageId { get; set; }
        public virtual ICollection<Culture>  Language { get; set; }
        public virtual  TimeZone TimeZone { get; set; }

        public int WebsiteId { get; set; }
        public virtual Website Website { get; set; }
    }
}
