﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;

namespace Community.Model.Entities.Setting
{
    public class GlobalWebsiteSetting : CommunityMandatory, ICommunityBase
    {
        public GlobalWebsiteSetting()
        {
            this.Social = new List<UserSocial>();
        }



        //public string SiteTitle { get; set; }
        public string SiteTitlePrefix { get; set; }
        public string SiteTitlePostFix { get; set; }
        public string KeyWords { get; set; }
        public  string Description { get; set; }
        public string SiteLogo { get; set; }
        public string TouchIcon { get; set; }
        public string FavIcon { get; set; }

        public string GoogleAnalyticsCode { get; set; }
        public string GoogleAdsenseCode { get; set; }


        public int SiteId { get; set; }

        public List<UserSocial> SocialId { get; set; } 

        public virtual Website Site { get; set; }
        public virtual ICollection<UserSocial> Social { get; set; }
    }
}
