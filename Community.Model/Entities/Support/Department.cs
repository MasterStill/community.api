﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Support
{
    public class SupportTicketDepartment : CommunityMandatory, ICommunityBase
    {
        public string Name { get; set; }
    }
}
