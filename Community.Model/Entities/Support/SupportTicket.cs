﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Community.Model.Entities.Global;
namespace Community.Model.Entities.Support
{
    public class SupportTicket : CommunityMandatory, ICommunityBase
    {
        public SupportTicket()
        {
            SupportTicketReply = new List<SupportTicketReply>();
        }
        public string ScreenShot {get;set;}
        public int SupportTicketDepartmentId {get;set;}
        public string TicketId {get;set;}
        public Status Status {get;set;}
        public string Subject {get;set;}
        public string Description {get;set;}
        
        public DateTime? LastRepliedDate {get;set;}
        public virtual SupportTicketDepartment SupportTicketDepartment {get;set;}
        public virtual List<SupportTicketReply> SupportTicketReply {get;set;}

        
        public int WebsiteId {get;set;}
        public virtual Website Website {get;set;}
    }
}
