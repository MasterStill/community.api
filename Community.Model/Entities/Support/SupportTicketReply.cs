﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Community.Model.Entities.Support
{
    public class SupportTicketReply : CommunityMandatory, ICommunityBase
    {
        public SupportTicketReply(){
            this.Ticket = new SupportTicket();
        }
        public int TicketId {get;set;}
        public string Reply {get;set;}
        public string ScreenShot {get;set;}
        public virtual SupportTicket Ticket {get;set;}
    }
}
 