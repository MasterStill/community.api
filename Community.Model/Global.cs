using System;

namespace Community.Model.Globals
{
    public static class Global
    {
        public static string OnConstructionWebsiteName(string WebsiteName)
        {
            return WebsiteName.ToLower() + ".teamrockmandu.com";
            //return WebsiteName.ToLower().Replace(".com", ".teamrockmandu.com");
        }

        public static string GetCleanUrl(string String)
        {
            String = ToTitleCase(String);
            String =  String                 
                 .Trim()
                 .Replace(",","")
                 .Replace("\"", "")
                 .Replace(" ", "-")
                 .Replace("|", "")
                 .Replace("(", "")
                 .Replace(")", "")
                 .Replace("--", "-")
                 .Replace("--", "-")
                 .Replace("-I-", "-")
                 .Replace("'", "")
                 ;
            if(String.EndsWith("-"))
                 String = String.Substring(0, String.Length - 1);
            return String;
        }
        public static string ToTitleCase(this string str)
        {
            var tokens = str.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < tokens.Length; i++)
            {
                var token = tokens[i];
                tokens[i] = token.Substring(0, 1).ToUpper() + token.Substring(1);
            }

            return string.Join(" ", tokens);
        }
    }
}