﻿using Community.Model.Entities.Album.Photo;
using Community.Model.Entities.Global;
using Community.Model.Entities.Theme;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Community.Model.Entities.Blog;
using Community.Model.Entities.Support;
using Community.Model.Entities;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Linq;
using Community.Model.Entities.Audit;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.Pages;
using Community.Model.Entities.Module;
using Community.Model.Entities.Sliders;

namespace Community.Data.Infrastructure
{
    // public class CommunityContext : IdentityDbContext<User, IdentityRole<int>, int>
    // {
    //     public CommunityContext(DbContextOptions<CommunityContext> options)
    //         : base(options)
    //     { }

    public class CommunityContext : DbContext
    {
        public CommunityContext(DbContextOptions<CommunityContext> options)
                    : base(options)
        { }
        //public CommunityContext(string connectionString) : base(connectionString)
        //{
        //   // Database.SetInitializer<ApplicationContext>(null);
        //}


        //public DbSet<Error> Errors { get; set; }
        //public CommunityContext(DbContextOptions options) : base(options)
        //{
        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // modelBuilder.Entity<site>(entity =>
            // {
            //     mapper.Map(entity);
            // });
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.Relational().TableName = entity.DisplayName();
            }
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            //  foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            //  {
            //      relationship.DeleteBehavior = DeleteBehavior.Restrict;
            //  }

            // modelBuilder.Entity<PhotoAlbum>().HasOne(e => e.Website)
            //     .WithMany(x => x.PhotoAlbumPhoto).Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            //   foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            //     {
            //         relationship.DeleteBehavior = DeleteBehavior.Restrict;
            //     }
            // modelBuilder.Entity<Culture>().OnDelete(DeleteBehavior.Restrict);

            // Photos

            // foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            // {
            //     relationship.DeleteBehavior = DeleteBehavior.Restrict;
            // }

            modelBuilder.Entity<PhotoAlbumPhoto>().Property(p => p.Title).HasMaxLength(100);

            //modelBuilder.Entity<Culture>().OnDelete(DeleteBehaviour.Restrict);

            modelBuilder.Entity<PhotoAlbumPhoto>().Property(p => p.AlbumId).IsRequired();

            // Album
            modelBuilder.Entity<PhotoAlbum>().Property(a => a.Title).HasMaxLength(100);
            modelBuilder.Entity<PhotoAlbum>().Property(a => a.Description).HasMaxLength(500);
            modelBuilder.Entity<PhotoAlbum>().HasMany(a => a.Photos).WithOne(p => p.Album);

            modelBuilder.Entity<BlogPostBlogTag>()
               .HasOne(x => x.Post)
               .WithMany(x => x.Tags)
               .HasForeignKey(x => x.PostId);
            modelBuilder.Entity<BlogPostBlogTag>().HasKey(x => new { x.PostId, x.TagId });


            modelBuilder.Entity<WebsiteCulture>()
              .HasOne(x => x.Website)
              .WithMany(x => x.Culture)
              .HasForeignKey(x => x.WebsiteId);
            modelBuilder.Entity<WebsiteCulture>().HasKey(x => new { x.WebsiteId, x.CultureId });



            //  modelBuilder.Entity<ThemeThemeComponent>()
            //     .HasOne(x => x.Theme)
            //     .WithMany(x => x.ThemeThemeComponent)
            //     .HasForeignKey(x => x.ThemeId);
            // modelBuilder.Entity<ThemeThemeComponent>().HasKey(x => new { x.ThemeId, x.ComponentId });


            modelBuilder.Entity<UserRole>()
                .HasOne(x => x.User)
                .WithMany(x => x.Roles)
                .HasForeignKey(x => x.UserId);
            modelBuilder.Entity<UserRole>().HasKey(x => new { x.UserId, x.RoleId, x.WebsiteId, x.Id });
            modelBuilder.Entity<UserRole>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<UserCulture>()
                .HasOne(x => x.User)
                .WithMany(x => x.UserCulture)
                .HasForeignKey(x => x.UserId);
            modelBuilder.Entity<UserCulture>().HasKey(x => new { x.UserId, x.CultureId });


            modelBuilder.Entity<WebsitePage>()
                .HasOne(x => x.Website)
                .WithMany(x => x.WebsitePage)
                .HasForeignKey(x => x.WebsiteId);
            modelBuilder.Entity<WebsitePage>().HasKey(x => new { x.WebsiteId, x.Order, x.Location, x.PageId });

            modelBuilder.Entity<WebsiteModule>()
                .HasOne(x => x.Website)
                .WithMany(x => x.WebsiteModule)
                .HasForeignKey(x => x.WebsiteId);
            modelBuilder.Entity<WebsiteModule>().HasKey(x => new { x.WebsiteId, x.ModuleId });

            modelBuilder.Entity<SocialWebsiteValue>()
               .HasOne(x => x.Website)
               .WithMany(x => x.SocialWebsiteValue)
               .HasForeignKey(x => x.WebsiteId);

            modelBuilder.Entity<NotesLabel>()
           .HasOne(x => x.Notes)
           .WithMany(x => x.NotesLabel)
           .HasForeignKey(x => x.NotesId);

            modelBuilder.Entity<NotesLabel>().HasKey(x => new { x.NotesId, x.LabelId });

            modelBuilder.Entity<SocialWebsiteValue>().HasKey(x => new { x.WebsiteId, x.SocialId });

            modelBuilder.Entity<ThemeComponentFields>().HasKey(x => new { x.ThemeComponentId, x.ComponentFieldId });


            // modelBuilder.Entity<WebsiteComponent>().HasKey(x => new {x.Name});
            // modelBuilder.Entity<WebsiteComponent>().Property(x=>x.Id).ValueGeneratedOnAdd();

            // modelBuilder.Entity<WebsitePageLayout>().HasKey(x => new {x.Layout,x.PageId,x.WebsiteId});
            // modelBuilder.Entity<WebsitePageLayout>().Property(x=>x.Id).ValueGeneratedOnAdd();

            //modelBuilder.Entity<Module>().HasKey(x => new { x.Id ,x.Name});
            // modelBuilder.Entity<Theme>().HasKey(x => new { x.Id ,x.Name});
            // modelBuilder.Entity<Page>().HasKey(x => new { x.Id ,x.Title});

            // modelBuilder.Entity<BlogPost>().HasMany(a => a.BlogPostBlogCategory).WithOne(p => p.BlogPost);

            // modelBuilder.Entity<PhotoAlbumCategory>().HasOne(c => c.Website).OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);
            // User
            //modelBuilder.Entity<User>().Property(u => u.Username).IsRequired().HasMaxLength(100);
            //modelBuilder.Entity<User>().Property(u => u.Email).IsRequired().HasMaxLength(200);
            //modelBuilder.Entity<User>().Property(u => u.HashedPassword).IsRequired().HasMaxLength(200);
            //modelBuilder.Entity<User>().Property(u => u.Salt).IsRequired().HasMaxLength(200);

            // UserRole
            //modelBuilder.Entity<UserRole>().Property(ur => ur.UserId).IsRequired();
            //modelBuilder.Entity<UserRole>().Property(ur => ur.RoleId).IsRequired();

            // Role
            //modelBuilder.Entity<Role>().Property(r => r.Name).IsRequired().HasMaxLength(50);
        }
        public DbSet<PhotoAlbumPhoto> PhotoAlbumPhotos { get; set; }
        public DbSet<PhotoAlbum> PhotoAlbums { get; set; }
        public DbSet<Theme> Themes { get; set; }
        //public DbSet<User> User { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<BlogPostBlogTag> BlogPostBlogTag { get; set; }
        public DbSet<Community.Model.Entities.Module.Module> Modules { get; set; }
        public DbSet<Community.Model.Entities.Module.WebsiteModule> WebsiteModule { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<WebsiteCulture> WebsiteCulture { get; set; }
        public DbSet<AuditLog> AuditLogs { get; set; }
        public DbSet<SupportTicket> SupportTickets { get; set; }
        public DbSet<SupportTicketReply> SupportTicketReply { get; set; }
        public DbSet<UserCulture> UserCulture { get; set; }
        public DbSet<SupportTicketDepartment> SupportTicketDepartment { get; set; }
        public DbSet<Page> Pages { get; set; }
        //public DbSet<Comments> Comments { get; set; }  Not Needed
        public DbSet<Website> Websites { get; set; }
        public DbSet<BlogTag> BlogTag { get; set; }
        public DbSet<WebsiteComponent> WebsiteComponent { get; set; }
        public DbSet<WebsiteComponentFieldValue> WebsiteComponentFieldValue { get; set; }
        public DbSet<ThemeComponentFields> ThemeComponentFields { get; set; }
        //public DbSet<PageThemeComponentFieldValue> PageThemeComponentFieldValue { get; set; }
        public DbSet<WebsitePage> WebsitePage { get; set; }
        public DbSet<ThemeComponent> ThemeComponents { get; set; }
        //public DbSet<ComponentField> ComponentField {get;set;}
        public DbSet<ComponentField> ComponentField { get; set; }
        public DbSet<WebsitePageLayout> WebsitePageLayout { get; set; }
        public DbSet<Slider> Slider { get; set; }
        public DbSet<SliderCategory> SliderCategory { get; set; }
        public DbSet<BlogCategory> BlogCategory { get; set; }
        public DbSet<Social> Social { get; set; }
        public DbSet<SocialWebsiteValue> SocialWebsiteValue { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Notes> Notes { get; set; }
        public DbSet<Label> Label { get; set; }
        public DbSet<TodoList> TodoList { get; set; }
        public DbSet<StaticGlobalization> StaticGlobalization { get; set; }
    }
}