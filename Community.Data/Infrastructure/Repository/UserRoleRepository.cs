﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities;
namespace Community.Data.Infrastructure.Repository
{
    public class UserRoleRepository : CommunityBaseRepository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(CommunityContext context)
            : base(context)
        { 
           
        }
    }
}