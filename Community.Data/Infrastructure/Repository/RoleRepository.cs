﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities;
namespace Community.Data.Infrastructure.Repository
{
    public class RoleRepository : CommunityBaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(CommunityContext context)
            : base(context)
        { }
    }
}
