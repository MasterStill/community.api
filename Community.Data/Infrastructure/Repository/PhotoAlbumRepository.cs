﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Album.Photo;
namespace Community.Data.Infrastructure.Repository
{
    public class PhotoAlbumRepository : CommunityBaseRepository<PhotoAlbum>, IPhotoAlbumRepository
    {
        public PhotoAlbumRepository(CommunityContext context)
            : base(context)
        { }
    }
}
