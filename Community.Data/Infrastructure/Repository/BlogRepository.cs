﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Blog;
namespace Community.Data.Infrastructure.Repository
{
    public class BlogRepository : CommunityBaseRepository<BlogPost>, IBlogRepository
    {
        public BlogRepository(CommunityContext context)
            : base(context)
        { }
    }
}
