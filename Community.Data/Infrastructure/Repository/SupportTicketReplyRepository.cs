﻿using Community.Model.Entities.Support;
using Community.Data.Infrastructure.Repository.Abstract;
namespace Community.Data.Infrastructure.Repository
{
    public class SupportTicketReplyRepository : CommunityBaseRepository<SupportTicketReply>, ISupportTicketReplyRepository
    {
        public SupportTicketReplyRepository(CommunityContext context)
            : base(context)
        { }
    }
}
   