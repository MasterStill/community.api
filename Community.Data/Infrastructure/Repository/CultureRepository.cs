﻿using Community.Model.Entities.Globalization;
using Community.Data.Infrastructure.Repository.Abstract;
namespace Community.Data.Infrastructure.Repository
{
    public class CultureRepository : CommunityBaseRepository<Culture>, ICultureRepository
    {
        public CultureRepository(CommunityContext context)
            : base(context)
        { }
    }
}