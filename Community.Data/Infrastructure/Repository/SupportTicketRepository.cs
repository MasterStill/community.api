﻿using Community.Model.Entities.Support;
using Community.Data.Infrastructure.Repository.Abstract;
namespace Community.Data.Infrastructure.Repository
{
    public class SupportTicketRepository : CommunityBaseRepository<SupportTicket>, ISupportTicketRepository
    {
        public SupportTicketRepository(CommunityContext context)
            : base(context)
        { }
    }
}
  