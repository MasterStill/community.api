﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Blog;
namespace Community.Data.Infrastructure.Repository
{
    public class BlogTagRepository : CommunityBaseRepository<BlogTag>, IBlogTagRepository
    {
        public BlogTagRepository(CommunityContext context)
            : base(context)
        { }
    }
}