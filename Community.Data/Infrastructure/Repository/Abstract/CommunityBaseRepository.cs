﻿using Community.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
namespace Community.Data.Infrastructure.Repository.Abstract
{
    public class CommunityBaseRepository<T> : ICommunityBaseRepository<T>
        where T : class, ICommunityBase, new()
    {
        private CommunityContext _context;

        #region Properties
        public CommunityBaseRepository(CommunityContext context)
        {
            _context = context;
        }
        #endregion
        public virtual IEnumerable<T> GetAll()
        {
            return _context.Set<T>().AsNoTracking().AsEnumerable().Where(x=>x.Delflag == false);
        }
        public virtual IEnumerable<T> GetAllByUser(int Id)
        {
            return _context.Set<T>().AsEnumerable().Where(x=>x.CreatedById == Id).Where(x=>x.Delflag == false);
        }
        public virtual int Count()
        {
            return _context.Set<T>().Count();
        }
        public virtual IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.AsEnumerable();
        }
        public T GetSingle(int id)
        {
            return _context.Set<T>().FirstOrDefault(x => x.Id == id);
        }
        public T GetSingle(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().FirstOrDefault(predicate);
        }
        public T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.Where(predicate).FirstOrDefault();
        }
        public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }
        public virtual void Add(T entity)
        {
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            if (entity.MultiLingualId <= 0){
                try{
                _context.Set<T>().Add(entity);
                Commit();
                entity.MultiLingualId = entity.Id;
                Update(entity);
                }catch(Exception ex) {
                    Console.WriteLine(ex.Message);
                } 
            }
            else
            {
                _context.Set<T>().Add(entity);    
            }
        }
        public virtual void Update(T entity)
        {
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
        public virtual void Delete(T entity)
        {
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            entity.Delflag = true;
            Update(entity);
            Commit();
            //dbEntityEntry.State = EntityState.Modified;
            
            //dbEntityEntry.State = EntityState.Deleted;

        }
        public virtual void DeleteWhere(Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> entities = _context.Set<T>().Where(predicate);
            foreach (var entity in entities)
            {
                _context.Entry<T>(entity).State = EntityState.Deleted;
            }
        }
        public T ReturnIfExist(Expression<Func<T, bool>> predicate)    
        {
           IEnumerable<T> entities = _context.Set<T>().Where(predicate);
           foreach (var entity in entities)
            {
               return entity;
            }
            return null;
        }

        public virtual bool Duplicate(Expression<Func<T, bool>> predicate)  
        {
            IEnumerable<T> entities = _context.Set<T>().Where(predicate);
            return (entities.Count() > 0) ?  true :  false;
        }

        public virtual void Commit()
        {
            _context.SaveChanges();
        }
    }
}
