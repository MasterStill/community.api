﻿using Community.Model.Entities.Support;
using Community.Model.Entities;
using System.Collections.Generic;
using Community.Model.Entities.Album.Photo;
using Community.Model.Entities.Theme;
using Community.Model.Entities.Global;
using Community.Model.Entities.Blog;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.Audit;
using Community.Model.Entities.Sliders;

namespace Community.Data.Infrastructure.Repository.Abstract
{
    //public interface ILoggingRepository : ICommunityBaseRepository<Error> { }

    public interface IPhotoRepository : ICommunityBaseRepository<PhotoAlbumPhoto> { }
    public interface IPhotoAlbumRepository : ICommunityBaseRepository<PhotoAlbum> { }
    public interface ISliderRepository : ICommunityBaseRepository<Slider> { }
    public interface IPhotoAlbumCategoryRepository : ICommunityBaseRepository<PhotoAlbumCategory> { }
    
    public interface IWebsiteRepository : ICommunityBaseRepository<Website>{}  

    public interface IThemeRepository : ICommunityBaseRepository<Theme>{}

    public interface IBlogRepository : ICommunityBaseRepository<BlogPost>{}
    public interface IBlogCategoryRepository : ICommunityBaseRepository<BlogCategory>{}
    public interface IBlogTagRepository : ICommunityBaseRepository<BlogTag>{}


    public interface IMenuRepository : ICommunityBaseRepository<Menu>{}

    public interface IUserRepository : ICommunityBaseRepository<User>
    {
        User GetSingleByUsername(string username);
        IEnumerable<Role> GetUserRoles(string username);
        IEnumerable<Role> GetUserRoles(string username,int website);
        List<int> GetUserCulture(int UserId);
    }
    public interface IUserRoleRepository : ICommunityBaseRepository<UserRole> { }

    public interface IRoleRepository : ICommunityBaseRepository<Role> { }

    public interface ILoggingRepository : ICommunityBaseRepository<Error> { }

    public interface ICultureRepository : ICommunityBaseRepository<Culture> { }
    
    
    public interface ISupportTicketRepository : ICommunityBaseRepository<SupportTicket> {}

    public interface ISupportTicketReplyRepository : ICommunityBaseRepository<SupportTicketReply> {}

     public interface IAuditRepository : ICommunityBaseRepository<AuditLog> { }
     public interface IGlobalizationRepository : ICommunityBaseRepository<Community.Model.Entities.Global.StaticGlobalization> { } 

    


}

