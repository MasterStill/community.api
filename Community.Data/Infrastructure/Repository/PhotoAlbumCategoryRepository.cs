﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Album.Photo;

namespace Community.Data.Infrastructure.Repository
{
    public class PhotoAlbumCategoryRepository : CommunityBaseRepository<PhotoAlbumCategory>, IPhotoAlbumCategoryRepository  
    {
        public PhotoAlbumCategoryRepository(CommunityContext context)
            : base(context)
        { }
    }
}
