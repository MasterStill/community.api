﻿using Community.Model.Entities.Global;
using Community.Data.Infrastructure.Repository.Abstract;
using System.Collections.Generic;
using Community.Model.Entities;
using System;
namespace Community.Data.Infrastructure.Repository
{
    public class UserRepository : CommunityBaseRepository<User>, IUserRepository
    {
        IRoleRepository _roleReposistory;
        public UserRepository(CommunityContext context)
            : base(context)
        { 
            this._roleReposistory = _roleReposistory;
        }

        public User GetSingleByUsername(string username)
        {
            return this.GetSingle(x => x.Username == username);
        }

        public IEnumerable<Role> GetUserRoles(string username)
        {
            List<Role> _roles = null;

            User _user = this.GetSingle(u => u.Username == username, u => u.Roles);
            if(_user != null)
            {
                _roles = new List<Role>();
                foreach (var _userRole in _user.Roles)
                    _roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
            }

            return _roles;
        }
        public IEnumerable<Role> GetUserRoles(string username,int website)
        {
            Console.WriteLine("Checking UserRole for Username : " + username + " For WebsiteId " + website);
            List<Role> _roles = null;
            User _user = this.GetSingle(u => u.Username == username, u => u.Roles);
            if(_user != null)
            {
                _roles = new List<Role>();
                foreach (var _userRole in _user.Roles)
                    {
                        //Console.WriteLine(_roleReposistory.GetSingle(_userRole.RoleId).Name);
                    }
                    
                    //_roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
                    //var role = _roleReposistory.GetSingle(_userRole.RoleId);
                     
                    // if (role != null){
                    //     _roles.Add(role);
                    // }
                    //_roles.Add(_roleReposistory.GetAll().Where(x=>x.WebsiteId == website).Where(x=>x.RoleId == _userRole.RoleId));
            }
            return _roles;
        }

        public List<int> GetUserCulture(int UserId){
             User _user = this.GetSingle(u => u.Id == UserId, u => u.UserCulture);
            List<int> userCulture = new List<int>();
            foreach(var Cultures in _user.UserCulture){
                userCulture.Add(Cultures.CultureId);
            }
            return userCulture;
        }
    }
}