﻿using Community.Model.Entities.Global;
using Community.Data.Infrastructure.Repository.Abstract;
namespace Community.Data.Infrastructure.Repository
{
    public class MenuRepository : CommunityBaseRepository<Menu>, IMenuRepository  
    {
        public MenuRepository(CommunityContext context)
            : base(context)
        { }
    }
}
