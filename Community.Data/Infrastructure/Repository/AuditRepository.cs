﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Audit;
namespace Community.Data.Infrastructure.Repository
{
 public class AuditRepository : CommunityBaseRepository<AuditLog>, IAuditRepository
    {
        public AuditRepository(CommunityContext context)
            : base(context)
        { }
    }
}
