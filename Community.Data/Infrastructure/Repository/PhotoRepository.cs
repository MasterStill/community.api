﻿using Community.Model.Entities.Album.Photo;
using Community.Data.Infrastructure.Repository.Abstract;
namespace Community.Data.Infrastructure.Repository
{
    public class PhotoRepository : CommunityBaseRepository<PhotoAlbumPhoto>, IPhotoRepository
    {
        public PhotoRepository(CommunityContext context)
            : base(context)
        { }
    }
}
