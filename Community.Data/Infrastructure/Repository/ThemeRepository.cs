﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Theme;
namespace Community.Data.Infrastructure.Repository
{
    public class ThemeRepository : CommunityBaseRepository<Theme>, IThemeRepository
    {
        public ThemeRepository(CommunityContext context)
            : base(context)
        { }
    }
}
