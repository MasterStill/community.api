﻿using Community.Model.Entities.Global;
using Community.Data.Infrastructure.Repository.Abstract;
namespace Community.Data.Infrastructure.Repository
{
    public class WebsiteRepository : CommunityBaseRepository<Website>, IWebsiteRepository
    {
        public WebsiteRepository(CommunityContext context)
            : base(context)
        { }
    }
}
