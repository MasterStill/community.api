﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Global;
namespace Community.Data.Infrastructure.Repository
{
 public class GlobalizationRepository : CommunityBaseRepository<StaticGlobalization>, IGlobalizationRepository
    {
        public GlobalizationRepository(CommunityContext context)
            : base(context)
        { }
    }
}
 