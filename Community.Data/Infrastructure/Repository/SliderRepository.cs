﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Album.Photo;
using Community.Model.Entities.Sliders;

namespace Community.Data.Infrastructure.Repository
{
    public class SliderRepository : CommunityBaseRepository<Slider>, ISliderRepository
    {
        public SliderRepository(CommunityContext context)
            : base(context)
        { }
    }
}
