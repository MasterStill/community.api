﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Blog;
namespace Community.Data.Infrastructure.Repository
{
    public class BlogCategoryRepository : CommunityBaseRepository<BlogCategory>, IBlogCategoryRepository 
    {
        public BlogCategoryRepository(CommunityContext context) 
            : base(context)
        { }
    }
}
