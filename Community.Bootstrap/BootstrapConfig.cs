﻿using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Community.Services;
using Community.Data.Infrastructure;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Community.Model.Entities;

namespace Community.Bootstrap  
{
    
    public static class BootstrapConfig
    {
              private const string RepositoryNamespace = "Community.Data.Infrastructure.Repository";
        private const string ServiceNamespace = "Community.Services";
        

        public static void RegisterApplicationServices(this IServiceCollection services, IConfigurationRoot configuration)
        {
           
            // DbContext
            //            services.AddScoped<DbContext>(x => new DbContext ApplicationContext(configuration["Data:ApplicationContext:ConnectionString"]));


            //services.AddDbContext<CommunityContext>(options =>
            //        options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));




            services.AddDbContext<CommunityContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly("Community.Api")));


            // services.AddIdentity<User,UserRole>()
            //     .AddEntityFrameworkStores<CommunityContext>()
            //     .AddDefaultTokenProviders();

            //  services.AddIdentity<User, Role>()
            //    .AddEntityFrameworkStores<CommunityContext>()
            //    .AddDefaultTokenProviders();
  
            // Repositories
           //var repositoryAssembly = typeof(BootstrapRepository).GetTypeInfo().Assembly;
           // var repositoryRegistrations = repositoryAssembly
           //     .GetExportedTypes()
           //     .Where(type => type.Namespace == RepositoryNamespace && type.GetInterfaces().Any())
           //     .Select(type => new
           //     {
           //         Interface = type.GetInterfaces().Single(),
           //         Implementation = type
           //     });
           // foreach (var reg in repositoryRegistrations)
           // {
           //     services.AddTransient(reg.Interface, reg.Implementation);
           // }

            //// Services
            var serviceAssembly = typeof(BootstrapService).GetTypeInfo().Assembly;
            var serviceRegistrations = serviceAssembly
                .GetExportedTypes()
                .Where(type => type.Namespace == ServiceNamespace && type.GetInterfaces().Any())
                .Select(type => new
                {
                    Interface = type.GetInterfaces().Single(),
                   Implementation = type
                });
            foreach (var reg in serviceRegistrations)
            {
                services.AddTransient(reg.Interface, reg.Implementation);
            }
        }
    }
}