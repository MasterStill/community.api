#include <Date.au3> ; To Get Current Datetime
#include <WinAPIFiles.au3> ; To Read/Write INI Files;
#include <Array.au3> ; To use array function
#include <File.au3> ; to get folders
#include <AutoItConstants.au3> ; for all constants
#include <MsgBoxConstants.au3> ; for message box
$postPath = "http://masterstill.com/Api/PostaboutUserExceedingUsuage"
$path = "C:\inetpub\wwwroot\Community\wwwroot\useruploads\"



Local $Directories = _FileListToArray ( $path , Default,$FLTA_FOLDERS ,False)

While 1
   For $i = 1 To UBound($Directories) - 1
	 Local $pathToUse = $path &  $Directories[$i]
	 Local $spaceAllocated =  ReadAllocatedSpace($pathToUse)
	 Local $directorySize = ReadDirectorySize($pathToUse)
	 local $actualIniPath = $pathToUse & "\settings.ini"
	 Local $reportedToserver =   IniRead($actualIniPath , "Settings", "Reportedtoserver", "false")
   ;  If $directorySize > $spaceAllocated And $reportedToserver  = "false" Then
	;If $directorySize > $spaceAllocated Then
		;Make Directory Readonly
		;SetDirectoryToReadOnly($pathToUse)
		; Size Exceed Call Api to let the Community App to Know About Exceeding Size
		Local $websiteName = $Directories[$i] ;; Website Name
	  if $directorySize > 0 Then
		  AlertDirectorySizeExceed($websiteName,$directorySize)
	  EndIf
		;MsgBox($MB_SYSTEMMODAL, "", $Directories[$i] & " : Size(MegaBytes): " & $directorySize & " Allocated Space " & $spaceAllocated)
	; EndIf
 Next
 Sleep(60*1000*2)
WEnd

;_ArrayDisplay($Directories, "$aFileList")



Exit






Func SetDirectoryToReadOnly($path)
   FileSetAttrib($path,"+R",1)
EndFunc


Func AlertDirectorySizeExceed($websiteName,$size)
	  $postData = "WebsiteName=" &  $websiteName & "&CurrentSize=" & $size
	  $oHTTP = ObjCreate("winhttp.winhttprequest.5.1")
	  $oHTTP.Open("POST", $postPath, False)
	  $oHTTP.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded")
	  $oHTTP.Send($postData)
	  $oReceived = $oHTTP.ResponseText
	  $oStatusCode = $oHTTP.Status
	  local $actualIniPath = $path & "\" & $websiteName & "\settings.ini"
	   If $oStatusCode <> 200 then
		 MsgBox(4096, "Response code", $oStatusCode & " " & $oReceived & " " & $websiteName & " " & $postData)
	  ElseIf $oStatusCode = 200 Then
		 ; Write in file not to post again for current website
		 IniWrite($actualIniPath, "Settings", "Reportedtoserver", "true")
	  EndIf

EndFunc
Func ReadDirectorySize($path)
   Local $dirSize = DirGetSize($path, $DIR_DEFAULT)
   Local $actualIniPath = $path & "/settings.ini"
   Local $usedSpace = Round($dirSize / 1024 / 1024)
   IniWrite($actualIniPath, "Settings", "UsedSpace", $usedSpace)

   return $usedSpace
EndFunc
;Function to Read Allocated Space For A Website in MB
Func ReadAllocatedSpace($path)
	   local $actualIniPath = $path & "\settings.ini"
	   Local $result =   IniRead($actualIniPath , "Settings", "AllocatedSpace", "0")
	   If $result = 0 then
			  ;Need to get request from server and write new INI file
		     IniWrite($actualIniPath, "Settings", "AllocatedSpace", "50")
	  ;Else
		;	 IniWrite($actualIniPath, "Settings", "UsedSpace", $result)
	  EndIf
	  IniWrite($actualIniPath, "Settings", "LastChecked", _Date_Time_SystemTimeToDateTimeStr(_Date_Time_GetSystemTime()) )
	  Return $result
EndFunc