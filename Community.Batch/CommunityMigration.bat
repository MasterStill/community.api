cd C:\projects\Community\Community.Api
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b)
set mytime=%mytime: =_%
dotnet ef migrations add '%mydate%_%mytime%'
dotnet ef database update
TIMEOUT 10000
