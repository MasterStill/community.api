public class WebsiteBasicSettings
{
    public bool OnConstruction { get; set; }
    public bool OnDemoMode { get; set; }
    public string Theme { get; set; }
    public string DefaultCulture { get; set; }
    public string LogoUrl { get; set; }
    public string LogoLink { get; set; }
    public string CopyrightNotice { get; set; }
    public string AliasName {get;set;}
    public string WebsiteName {get;set;}
}