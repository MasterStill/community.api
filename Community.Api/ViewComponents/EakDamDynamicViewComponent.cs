using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Data.Infrastructure;
using Community.Model.Entities.Blog;
using Community.Model.Entities.Theme;
using Community.Model.Entities.ViewModels;
using Community.Services;
using Community.Services.Abstract;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Community.Api.ViewComponents
{
    [AllowAnonymous]
    public class EakDamDynamicViewComponent : ViewComponent
    {
        private CommunityContext _context;
        private IBlogService _blogService;
        private IMapper _mapper;
        private IPhotoAlbumService _photoAlbumService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private IMemoryCache _memoryCache;
        public EakDamDynamicViewComponent(IMemoryCache _memoryCache, IUserService _userService, CommunityContext _context, IBlogService _blogService, IMapper _mapper, IPhotoAlbumService _photoAlbumService)
        {
            this._memoryCache = _memoryCache;
            this._userService = _userService;
            this._photoAlbumService = _photoAlbumService;
            this._context = _context;
            this._blogService = _blogService;
            this._mapper = _mapper;
        }
        string GetRealWebsiteName()
        {
            return ($"{Request.Host}");
        }
        public IViewComponentResult Invoke(string WhatValue, string culture, string Template, int page = 0, int pageSize = 6, bool canEdit = false)
        {
            ViewBag.DivId = WhatValue;
            if (Global.GetOnConstructionWebsiteName(GetRealWebsiteName()).Contains(".teamrockmandu.com"))
            {
                canEdit = true;
                ViewBag.CanEdit = canEdit;
            }
            string WebsiteName = Global.GetWebsiteName($"{Request.Host}");
            if (!_memoryCache.TryGetValue(WebsiteName.Replace(Global.MasterDemoAlias(),"") + "webiteName", out  WebsiteName ))
            {}
            int Count = _context.WebsitePage
                    .Include(x => x.Website)
                    .Where(x => x.Website.Name == WebsiteName || x.Website.Alias ==$"{Request.Host}".ToLower().Replace(".teamrockmandu.com","") )
                    .Where(x => x.WebsiteComponent.Name == WhatValue)
                    .Select(x => x.Count)
                    .FirstOrDefault();

            var test = _context.WebsiteComponent
                        .Include(x => x.Module)
                        .Include(x => x.Website)
                        .Include(x => x.ThemeComponent)
                        .Where(x => x.Name == WhatValue)
                        .Where(x => x.Website.Name == WebsiteName).SingleOrDefault();

            var modal = GetRequiredData(WebsiteName, culture, Count, test.Module.Name);  //new List<TestViewModel>();
            string ViewToSend = (test.ThemeComponent.Name + Template);
            string[] singleViewWithNoTemplate = new string[] { "lisidebar", "liwiththumbnail sidebar" ,"html"};
            foreach (var items in singleViewWithNoTemplate)
            {
                if (ViewToSend.ToLower().Contains(items))
                {
                    ViewToSend = ViewToSend.Replace(Template, "");
                    ViewToSend = ViewToSend.ToLower().Replace(" sidebar", "");
                }
            }
            if (!modal.Any() && WhatValue.ToLower().Contains("slider"))
            {
                var _result = _context.Slider.Where(x => x.Website.Name == WebsiteName)
                                             .Where(x => x.Culture.Title == culture)
                                             .Where(x => x.Delflag == false)
                                             .ToList(); 
                if (_result.Any())
                    modal = _mapper.Map<List<TestViewModel>>(_result);
            }
            var LoadNecessaryDetails = _context.WebsiteComponentFieldValue
                     .Include(x => x.WebsiteComponent)
                     .Include(x => x.ComponentField)
                     .Include(x=>x.Culture)
                     .Where(x => x.WebsiteComponent.Website.Name == WebsiteName)        
                     .Where(x => x.WebsiteComponent.Name == WhatValue)
                     .Where(x=> x.Culture.Title.ToLower() == culture.ToLower());            
            string suffiX =string.Empty;
            if (LoadNecessaryDetails.Any()){
                string SiteId = LoadNecessaryDetails.FirstOrDefault().WebsiteComponent.WebsiteId.ToString();
                string CultureId = LoadNecessaryDetails.FirstOrDefault().CultureId.ToString();
                suffiX = "&WebsiteId=" + SiteId + "&CultureId=" + CultureId;
            }                 
            foreach (var items in LoadNecessaryDetails.Where(x => x.ComponentField.DataType == DataType.String).Where(x => x.Culture.Title.ToLower() == culture.ToLower()))
            {

                string themeComponentFieldName = items.ComponentField.FieldName;
                if (canEdit)
                {
                    var Url = "http://admin.masterstill.com/#/pages/components?Id=";                         
                    if(themeComponentFieldName.ToLower().Contains("image") || themeComponentFieldName.ToLower().Contains("url")){
                        ViewData.Add(themeComponentFieldName, items.Value);
                    } 
                    else{
                       ViewData.Add(themeComponentFieldName, items.Value + "<a href=\"" + Url + @items.WebsiteComponent.Id + suffiX +  "\" target=Blank><i class=\"fa fa-pencil-square-o\"></i></a>");
                    }
                }
                else
                {
                    ViewData.Add(themeComponentFieldName, items.Value);
                }
            }
            
            if(ViewToSend.ToLower().Contains(Template.ToLower())){
                ViewToSend = "~/Themes/" +  Template + "/Components/" + ViewToSend.Replace(Template,"") + ".cshtml";                
            }            
            return View(ViewToSend, modal);              
        }
        public IEnumerable<TestViewModel> GetRequiredData(string WebsiteName, String Culture, int Count, string ModuleName)
        {
            IEnumerable<TestViewModel> modal = new List<TestViewModel>();
            BlogType itemType = new BlogType();
            switch (ModuleName)
            {
                case "Blog Tags":
                case "Blog Category":
                case "Blog":
                    itemType = BlogType.Blog;
                    break;
                case "MusicVideo Tags":
                case "MusicVideo":
                case "MusicVideo Category":
                    itemType = BlogType.MusicVideo;
                    break;
                case "News Tags":
                case "News Category":
                case "News":
                    itemType = BlogType.News;
                    break;
                case "Movies Tags":
                case "Movies Category":
                case "Movies":
                    itemType = BlogType.Movies;
                    break;
                case "Projects Tags":
                case "Projects Category":
                case "Projects":
                    itemType = BlogType.Projects;
                    break;
                case "Videos Tags":
                case "Videos Category":
                case "Videos":
                    itemType = BlogType.Videos;
                    break;
                case "Page Tags":
                case "Page Category":
                case "Page":
                    itemType = BlogType.Page;
                    break;
                default:
                    break;
            }

            switch (ModuleName)
            {
                case "Blog":
                case "News":
                case "MusicVideo":
                case "Videos":
                case "Movies":
                case "Projects":
                case "Page":
                    var _typeResult = _blogService.GetBlogPostsForWebsite(WebsiteName, Culture, 0, Count, itemType);
                    if (_typeResult.Any())
                        modal = _typeResult.Adapt<List<TestViewModel>>();
                    break;
                case "Blog Category":
                case "MusicVideo Category":
                case "News Category":
                case "Videos Category":
                case "Movies Category":
                case "Projects Category":
                case "Page Category":
                    var _categoryResult = _blogService.GetCategoriesForWebsite(WebsiteName, Culture, Count, itemType);
                    if (_categoryResult.Any())
                        modal = _mapper.Map<List<TestViewModel>>(_categoryResult);
                    break;
                case "MusicVideo Tags":
                case "Blog Tags":
                case "News Tags":
                case "Videos Tags":
                case "Movies Tags":
                case "Projects Tags":
                case "Page Tags":
                    var _tagResult = _blogService.GetTagsForWebsite(WebsiteName, Culture, Count, itemType);
                    if (_tagResult.Any())
                        modal = _mapper.Map<List<TestViewModel>>(_tagResult);
                    break;
                case "Gallery":
                    var _galleryResult = _photoAlbumService.GetAlbumsForWebsite(WebsiteName, Culture, 0, Count);
                    modal = _mapper.Map<IEnumerable<TestViewModel>>(_galleryResult);
                    break;
                default:
                    break;
            }
            return modal;
        }
    }
}