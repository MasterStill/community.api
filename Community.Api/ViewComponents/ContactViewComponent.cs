using System.Linq;
using Community.Data.Infrastructure;
using Community.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Community.Api.ViewComponents
{
    [AllowAnonymous]
    public class ContactViewComponent : ViewComponent
    {
       private CommunityContext _context;

        public ContactViewComponent(CommunityContext _context)
        {
            this._context= _context;
        }
        public IViewComponentResult Invoke(string WhatValue,string culture,string Template)
        {
            //  int ThemeComponentId = _context.ThemeComponents.Where(x=>x.Name == (WhatValue + Template)).Select(k=>k.Id).SingleOrDefault();
            // var themeComponentFields = _context.ThemeComponentFields.Where(x=>x.ComponentId == ThemeComponentId).Select(x=> new {x.Id,x.FieldName});
            // foreach(var fields in themeComponentFields){
                
                //string testData = _context.ThemeComponentFieldValue.Include(x=>x.Culture).Where(x=>x.FieldId==fields.Id).Where(x=>x.Culture.Title == culture).Select(x=>x.FieldValue).SingleOrDefault();
                //ViewData.Add(fields.FieldName, testData);
                //ViewData.Add(fields.FieldName, testData);

            //} 

            // return View(Template + WhatValue);
            return View();  
        }
    }
}