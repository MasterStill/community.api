using System;
using System.Collections.Generic;
using Community.Model.Entities.Blog;
using Community.Model.Entities.ViewModels;
using Community.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Community.Api.ViewComponents
{
    [AllowAnonymous]
    public class CultureViewComponent : ViewComponent
    {
        private IMemoryCache _memoryCache;
        private readonly IWebsiteService _websiteService;
        public CultureViewComponent(IWebsiteService _websiteService,IMemoryCache _memoryCache)
        {
            this._memoryCache = _memoryCache;
            this._websiteService = _websiteService;
        }
        public IViewComponentResult Invoke(string Template,string Culture)
        {
          string  Website = Global.GetWebsiteName($"{Request.Host}");
            if (!_memoryCache.TryGetValue(Website.Replace(Global.MasterDemoAlias(),"") + "webiteName", out  Website ))
            {}
            var _cultures  = new List<CultureViewModel>();
            try{
                if (!_memoryCache.TryGetValue(Website.ToLower() + "Culture", out _cultures))
                {
                    List<CultureViewModel> _cultures1 = _websiteService.GetCultures(Website,Culture);
                    _memoryCache.Set(Website.ToLower() + "Culture", _cultures1, new MemoryCacheEntryOptions()
                                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheTime())));
                    _cultures = _cultures1;
                }
                 //cultures = _websiteService.GetCultures(Website,Culture);
            }
            catch{
            }            
            //var cultures = _websiteService.GetCultures(Website,Culture);
            //if (cultures != null)return View(cultures);
            return View(_cultures);
        }       
    }        
}