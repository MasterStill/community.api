using Community.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Community.Api.ViewComponents
{
    [AllowAnonymous]
    public class NavigationViewComponent : ViewComponent
    {
        private readonly IMenuService _menuService;
        private IMemoryCache _memoryCache;
        public NavigationViewComponent(IMemoryCache _memoryCache,IMenuService  _menuService)
        {
            this._memoryCache = _memoryCache;
            this._menuService = _menuService;
        }
        public IViewComponentResult Invoke(string Culture,string Template)
        {
            string WebsiteName =  Global.GetWebsiteName($"{Request.Host}");
            if (!_memoryCache.TryGetValue(WebsiteName.Replace(Global.MasterDemoAlias(),"") + "webiteName", out  WebsiteName ))
            {}     
            var menu = _menuService.GetMenus(Culture,WebsiteName);
            //return View(Template  + "Navigation" , menu);
            return View("~/Themes/" +  Template + "/Components/Navigation.cshtml", menu);  
        }
    }
}