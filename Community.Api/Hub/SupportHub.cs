using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Community.Model.Entities.Global;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Community.Model.Entities;
using AutoMapper;
using Community.Api;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

public class SupportHub : Hub
{

    private readonly JwtIssuerOptions _jwtOptions;
    static List<user> loggedInUsers = new List<user>();
    private readonly JsonSerializerSettings _serializerSettings;
    private CommunityContext _context;
    //private Global Global = new Global();
    private GlobalUser _global = new GlobalUser();
    public SupportHub(IOptions<JwtIssuerOptions> jwtOptions,CommunityContext _context)
    {
        this._context = _context;
        _jwtOptions = jwtOptions.Value;
        Community.Api.Global.ThrowIfInvalidOptions(_jwtOptions);
        _serializerSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented
        };
    }
    public void Connect(string TicketId)
    {
        var id = Context.ConnectionId;
        if (loggedInUsers.Where(x => x.ConnectionId == id).Where(x=>x.name == TicketId).Count() == 0)
        {
            //loggedInUsers.Add(new user { ConnectionId = id, name = TicketId });
            Groups.Add(id,TicketId);
            //Clients.Caller.onConnected(id, "Suvbin", loggedInUsers);
            //Clients.AllExcept(id).onNewUserConnected(id, "Suvbin");
        }
    }
    public void sendToConnectedUsers(string TicketId){

    }
    public override Task OnConnected()
    {
        // var id = Context.ConnectionId;
        // if (loggedInUsers.Where(x => x.ConnectionId == id).Count() == 0)
        // {
        //     loggedInUsers.Add(new user { ConnectionId = id, name = "" });
        //     Clients.Caller.onConnected(id, "Suvbin", loggedInUsers);
        //     Clients.AllExcept(id).onNewUserConnected(id, "Suvbin");
        // }
        // string name = Context.User.Identity.Name;
        // if (name == null) name = "guest";
        // Groups.Add(Context.ConnectionId, name);
         return base.OnConnected();
    }
    public void UserTyping(string connectionId, string msg)
    {
        if (connectionId != null)
        {
            var id = Context.ConnectionId;
            Clients.Client(connectionId).isTyping(id, msg);
        }
    }
    public void SendTicketMessage(string toUserId, string message, string name)
    {

        string fromUserId = Context.ConnectionId;
        var toUser = loggedInUsers.FirstOrDefault(x => x.ConnectionId == toUserId);
        if (toUser == null)
        {
            toUser = loggedInUsers.FirstOrDefault(x => x.name.Replace("!!", "").Trim() == name.Replace("!!", "").Trim());
            if (toUser != null)
            {
                Clients.Caller.updateConnectionId(toUserId, toUser.ConnectionId);
                toUserId = toUser.ConnectionId;
            }
        }
        var fromUser = loggedInUsers.FirstOrDefault(x => x.ConnectionId == fromUserId);
        if (toUser != null && fromUser != null)
        {
            Clients.Client(toUserId).sendPrivateMessage(fromUserId, fromUser.name, message, fromUserId);
            Clients.Caller.sendPrivateMessage(toUserId, fromUser.name, message, toUserId);
        }
    }
    public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
    {
        var item = loggedInUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
        if (item != null)
        {
            loggedInUsers.Remove(item); // list = 
            var id = Context.ConnectionId;
            Clients.Others.newOfflineUser(item);
        }
        return base.OnDisconnected(false);
    }
}
public class user
{
    public string ConnectionId { get; set; }
    public int id { get; set; }
    public string name { get; set; }
    public string fontName { get; set; }
    public string fontSize { get; set; }
    public string fontColor { get; set; }
    public string sex { get; set; }
    public int age { get; set; }
    public string status { get; set; }
    public string memberType { get; set; }
    public string avator { get; set; }
    public string ContextName { get; set; }
}