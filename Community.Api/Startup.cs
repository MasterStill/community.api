using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Community.Data.Infrastructure.Repository;
using Community.Data.Infrastructure.Repository.Abstract;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using Community.Api;
using Community.Bootstrap;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;
using Community.Api.Options;
using Community.Services.Infrastructure.Services;
using Community.Services.Abstract;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;
using Community.Model.Entities.Support;
using Mapster;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Net.WebSockets;
using System.Threading;
using System.Collections.Concurrent;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Cors.Infrastructure;
//using wsweb;
// using Autofac;
// using Autofac.Extensions.DependencyInjection;
// using Community.Services;

namespace Community.Api
{
    public class Startup
    {
        private ConcurrentBag<WebSocket> _sockets = new ConcurrentBag<WebSocket>();
        private MapperConfiguration _mapperConfiguration { get; set; }
        public Startup(IHostingEnvironment env)
        {
            _mapperConfiguration = new MapperConfiguration(cfg =>
            {
                //cfg.AllowNullDestinationValues = false;
                cfg.AddProfile(new AutoMapperProfileConfiguration());
                //cfg.CreateMissingTypeMaps = true; 
            });


            //Mapper.CreateMap<MultiLingualViewModel,BlogPostViewModel>

            // Mapster

            // TypeAdapterConfig<MultiLingualViewModel, BlogPostViewModel>
            // .NewConfig()
            // .IgnoreNullValues(true);


            // TypeAdapterConfig<IEnumerable<MultiLingualViewModel>,IEnumerable<BlogPostViewModel>>
            // .NewConfig()
            // .IgnoreNullValues(true);

            // TypeAdapterConfig<MultiLingualViewModel,BlogPostViewModel>
            // .NewConfig()
            // .IgnoreNullValues(true);

            // TypeAdapterConfig.GlobalSettings.AllowImplicitDestinationInheritance = true;

            // TypeAdapterConfig<IEnumerable<BlogPostViewModel>,IEnumerable<MultiLingualViewModel>>
            // .NewConfig()
            // .IgnoreNullValues(true);

            //  TypeAdapterConfig.GlobalSettings.Default.PreserveReference(true);

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        //public IContainer ApplicationContainer { get; private set; }

        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        private const string SecretKey = "a1d50c31-d124-45dd-bea6-4dd076c1e1d8";
        // public IServiceProvider ConfigureServices(IServiceCollection services)
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR(options =>
                              {
                                  options.Hubs.EnableDetailedErrors = true;
                                  options.Hubs.EnableJavaScriptProxies = true;
                              });
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new ViewLocationExpander());
            });
            services.AddMemoryCache();
            // var pathToDoc = Configuration["Swagger:Path"];
            //            services.AddMemoryCache();
            services.AddSingleton<IMapper>(sp => _mapperConfiguration.CreateMapper());
            services.RegisterApplicationServices(Configuration);
            services.AddOptions();

            // services.AddCors(options =>
            //     {
            //         options.AddPolicy("AnyOrigin", builder =>
            //         {
            //             builder
            //                 .AllowAnyOrigin()
            //                 .AllowAnyMethod();
            //         });
            //     });
            //             services.AddCors(options =>
            //    {
            //        options.AddPolicy("CorsPolicy",
            //            builder => builder.AllowAnyOrigin()
            //            .AllowAnyMethod()
            //            .AllowAnyHeader()

            //    });
            // services.AddMvc(
            //                 config =>
            //                 {
            //                     var policy = new AuthorizationPolicyBuilder()
            //                                     .RequireAuthenticatedUser()
            //                                     .Build();
            //                     config.Filters.Add(new AuthorizeFilter(policy));
            //                 }
            // )
            services.AddMvc()
            .AddViewLocalization()
            .AddDataAnnotationsLocalization()
            .AddJsonOptions(opt =>
             {
                 var resolver = opt.SerializerSettings.ContractResolver;
                 if (resolver != null)
                 {
                     var res = resolver as DefaultContractResolver;
                     res.NamingStrategy = null;
                 }
             });
            services.AddAuthorization(options =>
      {
          options.AddPolicy("Admin",
                            policy => policy.RequireClaim("Role", "Admin"));
          // options.AddPolicy("AllUser",
          //                   policy => policy.RequireClaim("AllUser", "AllUser"));

      });
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            services.Configure<JwtIssuerOptions>(options =>
                 {
                     options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                     options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                     options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
                 });
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "Community API",
                    Description = "Api for Community Web App",
                    TermsOfService = "None"
                });
                //options.IncludeXmlComments(pathToDoc);
                options.DescribeAllEnumsAsStrings();
                options.OperationFilter<SwaggerIConfig>();
                //options.OperationFilter<SwaggerIFileConfig>();
            });



            services.AddTransient<WebsiteBasicSettings>();


            services.AddTransient<IPhotoAlbumRepository, PhotoAlbumRepository>();
            services.AddTransient<IPhotoRepository, PhotoRepository>();
            services.AddTransient<IWebsiteRepository, WebsiteRepository>();
            services.AddTransient<IPhotoAlbumCategoryRepository, PhotoAlbumCategoryRepository>();

            services.AddTransient<IBlogRepository, BlogRepository>();
            services.AddTransient<IBlogCategoryRepository, BlogCategoryRepository>();
            services.AddTransient<IBlogTagRepository, BlogTagRepository>();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IMembershipService, MembershipService>();
            services.AddTransient<IUserRoleRepository, UserRoleRepository>();


            services.AddTransient<ICultureRepository, CultureRepository>();
            services.AddTransient<IMenuRepository, MenuRepository>();
            services.AddTransient<ISliderRepository, SliderRepository>();
            services.AddTransient<IGlobalizationRepository, GlobalizationRepository>();




            services.AddTransient<ISupportTicketRepository, SupportTicketRepository>();
            services.AddTransient<ISupportTicketReplyRepository, SupportTicketReplyRepository>();



            services.AddTransient<IThemeRepository, ThemeRepository>();
            // services.AddScoped<ISearchProvider, SearchProvider>();     

            services.AddTransient<IAuditRepository, AuditRepository>();


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUrlHelper, UrlHelper>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            // services.AddMemoryCache();
            //Autofac Configuration
            var builder = new Autofac.ContainerBuilder();


            // builder.RegisterAssemblyTypes()
            //     .Where(t => t.Name.EndsWith("Repository"))
            //     .AsImplementedInterfaces();


            // builder.RegisterType<BlogService>().As<IBlogService>();
            // builder.RegisterType<BlogRepository>().As<IBlogRepository>();
            //  builder.Populate(services);
            //  this.ApplicationContainer = builder.Build();
            //  return ApplicationContainer.Resolve<IServiceProvider>();



        }








        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
        {

            //app.UseWebSockets();
            // app.Use(async (http, next) =>
            // {
            //     if (http.WebSockets.IsWebSocketRequest)
            //     {
            //         //IHandler handler = WebsocketManager.getHandler(http.Request.Path);

            //         var webSocket = await http.WebSockets.AcceptWebSocketAsync();
            //         while (webSocket.State == WebSocketState.Open)
            //         {
            //             try
            //             {
            //                 _sockets.Add(webSocket);
            //             }
            //             catch
            //             { }//Already added and proceed}


            //             var token = CancellationToken.None;
            //             var buffer = new ArraySegment<Byte>(new Byte[4096]);
            //             var received = await webSocket.ReceiveAsync(buffer, token);
            //             switch (received.MessageType)
            //             {
            //                 case WebSocketMessageType.Text:
            //                     var request = Encoding.UTF8.GetString(buffer.Array,
            //                                             buffer.Offset,
            //                                             buffer.Count);
            //                     var type = WebSocketMessageType.Text;
            //                     var data = Encoding.UTF8.GetBytes("Echo from server :" + request);
            //                     buffer = new ArraySegment<Byte>(data);
            //                     await Task.WhenAll(_sockets.Where(x => x.State == WebSocketState.Open).Select(x => x.SendAsync(buffer, type, true, token)));
            //                     //await webSocket.SendAsync(buffer, type, true, token);
            //                     break;
            //             }
            //         }
            //     }
            //     else
            //     {
            //         await next();
            //     }
            // });
            //   loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            //app.Map("/ws", SocketHandler.Map);

            app.UseStaticFiles();
            //app.us
            // app.UseCors("CorsPolicy");

            app.UseCors(builder =>
                        builder//.WithOrigins("http://gajurel.com", "http://localhost:5000", "http://localhost:3000", "http://admin.gajurel.com", "http://admin.masterstill.com", "http://masterstill.com")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .AllowAnyOrigin()
                        );
            // app.UseCors("AnyOrigin");
            // app.UseCors("AnyOrigin");
            // app.UseCors(builder =>
            // builder.WithOrigins("http://gajurel.com")
            //     .AllowAnyHeader()
            //     .AllowAnyMethod();
            // );

            // app.UseCors(builder =>
            //     builder.AllowAnyHeader()
            // );
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };
            //     app.UseCookieAuthentication(new CookieAuthenticationOptions
            //     {
            //         AutomaticAuthenticate = true,
            //         AutomaticChallenge = true,
            //         AuthenticationScheme = "Cookie",
            //          CookieName = "access_token"//,
            // //         TicketDataFormat = new CustomJwtDataFormat(
            // // SecurityAlgorithms.HmacSha256,
            // // tokenValidationParameters)
            //     });

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });
            //app.UseIdentity();  
            app.UseMvc();
            // (routes =>
            //   {
            //       routes.MapRoute(
            //           name: "Api",
            //           template: "api/Community",
            //           defaults: "/swagger/ui/index.html");                      
            //   });



            app.UseSwagger();
            app.UseSwaggerUi();

            app.UseWebSockets();
            app.UseSignalR();

            // var config = new HubConfiguration();
            // config.EnableJSONP = true;
            // app.MapSignalR(config);     

            // app.Use(async (context, next) =>
            // {
            //     // perform some verification
            //     context.Items["Theme"] = "Mpurpose";
            //     await next.Invoke();
            // });
            //Autofac
            // appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());


        }
    }
}