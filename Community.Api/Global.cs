﻿using System;
using System.Collections.Generic;
using System.Linq;
using Community.Api.Options;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace Community.Api
{
    public class Global
    {
        // private IWebsiteService _websiteService; 
        // public Global(IWebsiteService _websiteService){
        //     this._websiteService = _websiteService;
        // }        
        //    private Global (){}
        public static int Pagesize()
        {
            return 50;
        }
        public static GenericResult AccessDenied()
        {
            return Services.Global.AccessDenied();
        }
        public static int CacheTime()
        {
            return Community.Services.Global.CacheTime();
        }
        public static string GetWebsiteName(string HostName)
        {
            // if(HostName.Contains(".teamrockmandu.com")){
            //   var _result = _websiteService.GetWebsiteName(HostName);  
            // } 
            //return "aryan.sigdel.com";
            // string WebsiteName;
            // if (!_memoryCache.TryGetValue(HostName.Replace(Global.MasterDemoAlias(),"") + "webiteName", out  webisteName ))
            // {

            // }
            //HostName = HostName.ToLower().Replace(".teamrockmandu.com","");            
            HostName = GetOnConstructionWebsiteName(HostName);
            //if (HostName.ToLower().Contains(".ch.com")){HostName = HostName.Replace(".com","");}
            return HostName.ToLower();
        }

        public static string MasterDemoAlias()
        {
            return ".teamrockmandu.com";
        }
        public static string GetOnConstructionWebsiteName(string HostName)
        {
            if (HostName == "localhost:5000") return "isdnepal.com";
            return HostName;
        }
        public static IActionResult SendAsItIs(GenericResult _result)
        {
            if (_result.Succeeded) return new OkObjectResult(_result);
            return new BadRequestObjectResult(_result);
        }
        public static IActionResult SendAsItIs(object What)
        {
            return new OkObjectResult(What);
        }
        public static GenericResult ErrorSettingsnotFound(string Message = "")
        {
            return new GenericResult()
            {
                Succeeded = false,
                Message = Message == "" ? "Website Settings Not Defined" : Message
            };
        }
        public static string DefaultLogoLink()
        {
            return "http://teamrockmandu.com";
        }
        public static string DefaultLogo()
        {
            return Community.Services.Global.DefaultLogo();
        }
        public enum GenericActions
        {
            Create, Edit, Delete
        }
        public static GenericResult Success(string Message = "", string Data = "")
        {
            return new GenericResult()
            {
                Succeeded = true,
                Message = Message,
                Data = Data
            };
        }
        public static GenericResult Error(Exception ex = null, string Message = "")
        {
            Community.Services.Global g = new Community.Services.Global();
            return g.Error(ex, Message);
            // return new GenericResult()
            // {
            //     Succeeded = false,
            //     Message = Message 
            // };  
        }
        public static string CopyRightNotice(string culture)
        {

            if (culture != null) culture = culture.ToLower();

            switch (culture)
            {
                case "nepali":
                    return "© Nepali CopyRight Notice";
                default:
                    return "© Copyright <a href=\"http://www.teamrockmandu.com\">Rockmandu Developers</a> " + DateTime.Now.Year;
            }
        }
        public static IActionResult ModelStateError(ModelStateDictionary ModelState)
        {
            List<string> Errors = new List<string>();
            foreach (var items in ModelState)
            {
                Errors.AddRange(items.Value.Errors.ToList().Select(x => x.ErrorMessage));
            }
            string messageTosend = "{\"Message\":\"" + JsonConvert.SerializeObject(Errors).Substring(1, JsonConvert.SerializeObject(Errors).Count() - 2).Replace("\"", "").Replace(",", "<br>") + "\"}";
            return new BadRequestObjectResult(messageTosend);
        }
        public static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtIssuerOptions.ValidFor));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
            }

            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
            }
        }
    }
}
public class Validate : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var controller = context.Controller as Controller;
        var modelState = controller?.ViewData.ModelState;
        if (!modelState.IsValid)
        {
            context.Result = Community.Api.Global.ModelStateError(modelState);
        }
    }
}
