using AutoMapper;
using Community.Model.Entities.Album.Photo;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Theme;
using Community.Model.Entities.Global;
using Community.Model.Entities.Blog;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.Support;
using Community.Model.Entities.Pages;
using Community.Model.Entities.Sliders;
using Community.Model.Entities;
using System.Linq;

namespace Community.Api
{ 

    public class AutoMapperProfileConfiguration : Profile
    {

        protected override async void Configure()
        {

            CreateMap<Notes, NotesViewModel>()
                .ForMember(x => x.label, o => o.MapFrom(k => k.NotesLabel.Select(x => x.LabelId).ToArray()));
            CreateMap<NotesCreateViewModel, Notes>()
                .ForMember(x => x.NotesLabel, o => o.Ignore())
                .ForMember(x => x.CheckList, o => o.MapFrom(k => k.CheckList))
                ;
            CreateMap<CheckListViewModel, TodoList>();
            CreateMap<TodoList, CheckListViewModel>();
            CreateMap<Label, LabelViewModel>();
            CreateMap<Label, LabelViewModelOnlyID>();

            CreateMap<Slider, SliderViewModel>();
            CreateMap<SliderCreateViewModel, Slider>();
            CreateMap<Slider, TestViewModel>();

            CreateMap<SliderCategoryCreateViewModel, SliderCategory>();
            CreateMap<SliderCategoryViewModel, SliderCategory>();
            CreateMap<SliderCategory, SliderCategoryViewModel>();

            CreateMap<WebsiteComponent, WebsiteComponentViewModel>();
            CreateMap<WebsitePageLayoutCreateViewModel, WebsitePageLayout>();


            CreateMap<PhotoAlbum, PhotoAlbumViewModel>();
            CreateMap<PhotoAlbumViewModel, PhotoAlbum>();
            CreateMap<PhotoAlbumViewModel, TestViewModel>();


            CreateMap<PhotoAlbumCreateViewModel, PhotoAlbum>();
            CreateMap<PhotoAlbumCategoryCreateViewModel, PhotoAlbumCategory>();
            CreateMap<PhotoAlbum, PhotoAlbumCreateViewModel>();

            CreateMap<PhotoAlbumPhoto, PhotoViewModel>();
            CreateMap<PhotoViewModel, PhotoAlbumPhoto>();

            CreateMap<PhotoAlbumCategoryViewModel, PhotoAlbumCategory>();
            CreateMap<PhotoAlbumCategory, PhotoAlbumCategoryViewModel>();

            // CreateMap<Website, WebsiteCreateViewModel>();
            CreateMap<WebsiteCreateViewModel, Website>();

            CreateMap<WebsiteViewModel, Website>();
            CreateMap<Website, WebsiteViewModel>();

            CreateMap<MenuCreateViewModel, Menu>();
            CreateMap<Menu, MenuViewModel>();

            CreateMap<Theme, ThemeViewModel>();
            CreateMap<ThemeViewModel, Theme>();
            CreateMap<ThemeCreateViewModel, Theme>();
            CreateMap<Theme, Theme>();

            CreateMap<BlogPostViewModel, TestViewModel>();
            CreateMap<BlogPost, BlogPostCreateViewModel>();
            CreateMap<BlogPost, BlogPostViewModel>();
            CreateMap<BlogPostCreateViewModel, BlogPost>();
            CreateMap<BlogPostViewModel, BlogPost>();
            CreateMap<BlogPost, BlogPost>();
            CreateMap<BlogPost, BlogPostSingleViewModel>();


            CreateMap<BlogPostBlogTag, BlogTagViewModel>();
            CreateMap<BlogTagViewModel, TestViewModel>();

            CreateMap<BlogPost, TestViewModel>();
            CreateMap<BlogCategoryViewModel, TestViewModel>();

            CreateMap<BlogPost, MultiLingualViewModel>();

            CreateMap<MultiLingualViewModel, BlogPostViewModel>().ForMember(destination => destination.Title, opt => opt.UseDestinationValue());
            //.ForMember(dest => dest.Title, opt => opt.UseDestinationVa  lue());
            //CreateMap<BlogPostViewModel,MultiLingualViewModel>();


            CreateMap<BlogCategoryCreateViewModel, BlogCategory>();
            //CreateMap<BlogCategory,BlogCategoryCreateViewModel>();
            CreateMap<BlogCategory, BlogCategoryViewModel>();
            //CreateMap<BlogCategoryViewModel,BlogCategory>();
            // CreateMap<BlogPost,BlogPostSingleViewModel>();



            CreateMap<CultureViewModel, Culture>();
            CreateMap<Culture, CultureViewModel>();




            CreateMap<SupportTicket, SupportTicketViewModel>();
            CreateMap<SupportTicketCreateViewModel, SupportTicket>();
            CreateMap<SupportTicketReplyCreateViewModel, SupportTicketReply>();
            CreateMap<SupportTicket, SupportTicketSingleViewModel>();
            CreateMap<SupportTicketReply, SupportTicketReplyViewModel>();

            CreateMap<SupportTicketDepartment, SupportTicketDepartmentViewModel>();



            CreateMap<StaticGlobalization, GlobalizationViewModel>();

            //CreateMap<Theme,ThemeViewModel>();

            //CreateMap<ThemeCreateViewModel,Theme>();
        }
    }

}
// namespace Community.Api
// { 
//     public static IMappingExpression<TSource, TDestination> 
//     IgnoreAllNonExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
//     {
//     foreach(var property in expression.TypeMap.GetUnmappedPropertyNames())
//     {
//         expression.ForMember(property, opt => opt.Ignore());
//     }

//     return expression;
// }
// }