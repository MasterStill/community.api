﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Community.Data.Infrastructure;

namespace Community.Api.Migrations
{
    [DbContext(typeof(CommunityContext))]
    [Migration("20160927034206_notesLabel")]
    partial class notesLabel
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Community.Model.Entities.Album.Photo.PhotoAlbum", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CategoryId");

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Description")
                        .HasAnnotation("MaxLength", 500);

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Password");

                    b.Property<string>("Title")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("PhotoAlbum");
                });

            modelBuilder.Entity("Community.Model.Entities.Album.Photo.PhotoAlbumCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Title");

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("PhotoAlbumCategory");
                });

            modelBuilder.Entity("Community.Model.Entities.Album.Photo.PhotoAlbumPhoto", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AlbumId");

                    b.Property<bool>("AlbumThumbNail");

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Description");

                    b.Property<string>("ImageUrl");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Title")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("AlbumId");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("PhotoAlbumPhoto");
                });

            modelBuilder.Entity("Community.Model.Entities.Audit.AuditLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AuditActionTypeENUM");

                    b.Property<string>("Changes");

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<string>("DataModel");

                    b.Property<bool>("Delflag");

                    b.Property<int>("KeyFieldID");

                    b.Property<string>("KeyName");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("ValueAfter");

                    b.Property<string>("ValueBefore");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("AuditLog");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<int?>("ParentCategoryId");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.Property<string>("password");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("ParentCategoryId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("BlogCategory");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogPost", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogType");

                    b.Property<int>("CategoryId");

                    b.Property<bool>("CommentAllowed");

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("ImageUrl")
                        .IsRequired();

                    b.Property<int>("MultiLingualId");

                    b.Property<DateTime>("PublishDate");

                    b.Property<int>("PublishStatus");

                    b.Property<string>("Summary")
                        .IsRequired();

                    b.Property<string>("ThumbnailUrl")
                        .IsRequired();

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<bool>("Verified");

                    b.Property<int>("Visibility");

                    b.Property<int>("WebsiteId");

                    b.Property<string>("password");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("BlogPost");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogPostBlogTag", b =>
                {
                    b.Property<int>("PostId");

                    b.Property<int>("TagId");

                    b.HasKey("PostId", "TagId");

                    b.HasIndex("PostId");

                    b.HasIndex("TagId");

                    b.ToTable("BlogPostBlogTag");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogTag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("BlogTag");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.Error", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Message");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("StackTrace");

                    b.Property<bool>("Verified");

                    b.Property<int?>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Error");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.Menu", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("MenuUrl");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<bool>("NewWindow");

                    b.Property<int>("Order");

                    b.Property<int?>("ParentMenu");

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Menu");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Delflag");

                    b.Property<DateTime?>("DoB");

                    b.Property<string>("Email");

                    b.Property<string>("Firstname");

                    b.Property<string>("Gender");

                    b.Property<string>("Image");

                    b.Property<bool>("IsLocked");

                    b.Property<DateTime?>("LastLoggedInDate");

                    b.Property<string>("LastLoggedOnIP");

                    b.Property<string>("LastName");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Password");

                    b.Property<string>("RegisteredFromIP");

                    b.Property<string>("Salt");

                    b.Property<string>("Summary");

                    b.Property<int>("UserType");

                    b.Property<string>("Username");

                    b.HasKey("Id");

                    b.ToTable("User");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.Website", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AllocatedSpace");

                    b.Property<bool>("Comments");

                    b.Property<bool>("Construction");

                    b.Property<int>("CreatedById");

                    b.Property<string>("Custom_Layout_Css");

                    b.Property<string>("Custom_Layout_Script");

                    b.Property<int?>("DefaultCultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("DisplayWebsiteOf");

                    b.Property<string>("Disqus_Code");

                    b.Property<string>("GoogleAnalyticsCode");

                    b.Property<bool>("IIsCreated");

                    b.Property<string>("Logo");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<bool>("Paid");

                    b.Property<bool>("PaidWebsite");

                    b.Property<bool>("Redirect");

                    b.Property<string>("RedirectToUrl");

                    b.Property<int>("SpaceUsed");

                    b.Property<string>("Theme");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("DefaultCultureId");

                    b.ToTable("Website");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.WebsiteCulture", b =>
                {
                    b.Property<int>("WebsiteId");

                    b.Property<int>("CultureId");

                    b.HasKey("WebsiteId", "CultureId");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsiteCulture");
                });

            modelBuilder.Entity("Community.Model.Entities.Globalization.Culture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<int>("CreatedById");

                    b.Property<int>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Title");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.ToTable("Culture");
                });

            modelBuilder.Entity("Community.Model.Entities.Globalization.UserCulture", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<int>("CultureId");

                    b.HasKey("UserId", "CultureId");

                    b.HasIndex("CultureId");

                    b.HasIndex("UserId");

                    b.ToTable("UserCulture");
                });

            modelBuilder.Entity("Community.Model.Entities.Label", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Label");
                });

            modelBuilder.Entity("Community.Model.Entities.Module.Module", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Description");

                    b.Property<string>("DisplayName");

                    b.Property<string>("MenuJson");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<int>("Options");

                    b.Property<int?>("ParentModuleId");

                    b.Property<int>("Price");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("ParentModuleId");

                    b.ToTable("Module");
                });

            modelBuilder.Entity("Community.Model.Entities.Module.WebsiteModule", b =>
                {
                    b.Property<int>("WebsiteId");

                    b.Property<int>("ModuleId");

                    b.HasKey("WebsiteId", "ModuleId");

                    b.HasIndex("ModuleId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsiteModule");
                });

            modelBuilder.Entity("Community.Model.Entities.Notes", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Archive");

                    b.Property<string>("Color");

                    b.Property<int>("CreatedById");

                    b.Property<bool>("DelFlag");

                    b.Property<string>("Description");

                    b.Property<string>("Image");

                    b.Property<DateTime?>("Reminder");

                    b.Property<DateTime?>("Time");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.ToTable("Notes");
                });

            modelBuilder.Entity("Community.Model.Entities.NotesLabel", b =>
                {
                    b.Property<int>("NotesId");

                    b.Property<int>("LabelId");

                    b.HasKey("NotesId", "LabelId");

                    b.HasIndex("LabelId");

                    b.HasIndex("NotesId");

                    b.ToTable("NotesLabel");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.Page", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("DisplayName");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Summary");

                    b.Property<string>("Title");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("Page");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsiteComponent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("DelFlag");

                    b.Property<int?>("ModuleId");

                    b.Property<string>("Name");

                    b.Property<int>("ThemeComponentId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("ModuleId");

                    b.HasIndex("ThemeComponentId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsiteComponent");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsiteComponentFieldValue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ComponentFieldId");

                    b.Property<int>("CultureId");

                    b.Property<string>("Value");

                    b.Property<int>("WebsiteComponentId");

                    b.HasKey("Id");

                    b.HasIndex("ComponentFieldId");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteComponentId");

                    b.ToTable("WebsiteComponentFieldValue");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsitePage", b =>
                {
                    b.Property<int>("WebsiteId");

                    b.Property<int>("Order");

                    b.Property<int>("Location");

                    b.Property<int>("PageId");

                    b.Property<int>("Count");

                    b.Property<int>("Options");

                    b.Property<int>("WebsiteComponentId");

                    b.HasKey("WebsiteId", "Order", "Location", "PageId");

                    b.HasIndex("PageId");

                    b.HasIndex("WebsiteComponentId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsitePage");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsitePageLayout", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Layout");

                    b.Property<int>("PageId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("PageId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WebsitePageLayout");
                });

            modelBuilder.Entity("Community.Model.Entities.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("Community.Model.Entities.Sliders.Slider", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BackgroundImage");

                    b.Property<int>("CategoryId");

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Image");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Summary");

                    b.Property<string>("Title");

                    b.Property<string>("Url");

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Slider");
                });

            modelBuilder.Entity("Community.Model.Entities.Sliders.SliderCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Title");

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("SliderCategory");
                });

            modelBuilder.Entity("Community.Model.Entities.Support.SupportTicket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Description");

                    b.Property<DateTime?>("LastRepliedDate");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("ScreenShot");

                    b.Property<int>("Status");

                    b.Property<string>("Subject");

                    b.Property<int>("SupportTicketDepartmentId");

                    b.Property<string>("TicketId");

                    b.Property<bool>("Verified");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("SupportTicketDepartmentId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("SupportTicket");
                });

            modelBuilder.Entity("Community.Model.Entities.Support.SupportTicketDepartment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("SupportTicketDepartment");
                });

            modelBuilder.Entity("Community.Model.Entities.Support.SupportTicketReply", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Reply");

                    b.Property<string>("ScreenShot");

                    b.Property<int>("TicketId");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("TicketId");

                    b.ToTable("SupportTicketReply");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.ComponentField", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<int>("DataType");

                    b.Property<bool>("Delflag");

                    b.Property<string>("FieldName");

                    b.Property<int>("MultiLingualId");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("ComponentField");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.Theme", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Author");

                    b.Property<string>("CopyRightNotice");

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Designer");

                    b.Property<bool>("DispalyInPublic");

                    b.Property<string>("Image");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<int>("Price");

                    b.Property<string>("Summary");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.ToTable("Theme");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.ThemeComponent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int?>("CultureId");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.Property<string>("Name");

                    b.Property<int?>("ThemeId");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("CultureId");

                    b.HasIndex("ThemeId");

                    b.ToTable("ThemeComponent");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.ThemeComponentFields", b =>
                {
                    b.Property<int>("ThemeComponentId");

                    b.Property<int>("ComponentFieldId");

                    b.HasKey("ThemeComponentId", "ComponentFieldId");

                    b.HasIndex("ComponentFieldId");

                    b.HasIndex("ThemeComponentId");

                    b.ToTable("ThemeComponentFields");
                });

            modelBuilder.Entity("Community.Model.Entities.TodoList", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Checked");

                    b.Property<int?>("NotesId");

                    b.Property<string>("Text");

                    b.HasKey("Id");

                    b.HasIndex("NotesId");

                    b.ToTable("TodoList");
                });

            modelBuilder.Entity("Community.Model.Entities.UserRole", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<int>("RoleId");

                    b.Property<int>("WebsiteId");

                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedById");

                    b.Property<bool>("Delflag");

                    b.Property<int>("MultiLingualId");

                    b.HasKey("UserId", "RoleId", "WebsiteId", "Id");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("UserRole");
                });

            modelBuilder.Entity("Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Link_Url");

                    b.Property<int?>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Document");
                });

            modelBuilder.Entity("Social", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdminIco");

                    b.Property<string>("Logo");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Social");
                });

            modelBuilder.Entity("SocialWebsiteValue", b =>
                {
                    b.Property<int>("WebsiteId");

                    b.Property<int>("SocialId");

                    b.Property<string>("Value");

                    b.HasKey("WebsiteId", "SocialId");

                    b.HasIndex("SocialId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("SocialWebsiteValue");
                });

            modelBuilder.Entity("Community.Model.Entities.Album.Photo.PhotoAlbum", b =>
                {
                    b.HasOne("Community.Model.Entities.Album.Photo.PhotoAlbumCategory", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany("PhotoAlbum")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Album.Photo.PhotoAlbumCategory", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Album.Photo.PhotoAlbumPhoto", b =>
                {
                    b.HasOne("Community.Model.Entities.Album.Photo.PhotoAlbum", "Album")
                        .WithMany("Photos")
                        .HasForeignKey("AlbumId");

                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Audit.AuditLog", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogCategory", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Blog.BlogCategory", "ParentCategory")
                        .WithMany("ChildCategories")
                        .HasForeignKey("ParentCategoryId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogPost", b =>
                {
                    b.HasOne("Community.Model.Entities.Blog.BlogCategory", "Category")
                        .WithMany("Posts")
                        .HasForeignKey("CategoryId");

                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogPostBlogTag", b =>
                {
                    b.HasOne("Community.Model.Entities.Blog.BlogPost", "Post")
                        .WithMany("Tags")
                        .HasForeignKey("PostId");

                    b.HasOne("Community.Model.Entities.Blog.BlogTag", "Tag")
                        .WithMany("Tags")
                        .HasForeignKey("TagId");
                });

            modelBuilder.Entity("Community.Model.Entities.Blog.BlogTag", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.Error", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.Menu", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.Website", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "DefaultCulture")
                        .WithMany()
                        .HasForeignKey("DefaultCultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Global.WebsiteCulture", b =>
                {
                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany("Website")
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany("Culture")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Globalization.Culture", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");
                });

            modelBuilder.Entity("Community.Model.Entities.Globalization.UserCulture", b =>
                {
                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany("UserCulture")
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.User", "User")
                        .WithMany("UserCulture")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Community.Model.Entities.Module.Module", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Module.Module", "ParentModule")
                        .WithMany()
                        .HasForeignKey("ParentModuleId");
                });

            modelBuilder.Entity("Community.Model.Entities.Module.WebsiteModule", b =>
                {
                    b.HasOne("Community.Model.Entities.Module.Module", "Module")
                        .WithMany("WebsiteModule")
                        .HasForeignKey("ModuleId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany("WebsiteModule")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Notes", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");
                });

            modelBuilder.Entity("Community.Model.Entities.NotesLabel", b =>
                {
                    b.HasOne("Community.Model.Entities.Label", "Label")
                        .WithMany("NotesLabel")
                        .HasForeignKey("LabelId");

                    b.HasOne("Community.Model.Entities.Notes", "Notes")
                        .WithMany("NotesLabel")
                        .HasForeignKey("NotesId");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.Page", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsiteComponent", b =>
                {
                    b.HasOne("Community.Model.Entities.Module.Module", "Module")
                        .WithMany()
                        .HasForeignKey("ModuleId");

                    b.HasOne("Community.Model.Entities.Theme.ThemeComponent", "ThemeComponent")
                        .WithMany()
                        .HasForeignKey("ThemeComponentId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany("Components")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsiteComponentFieldValue", b =>
                {
                    b.HasOne("Community.Model.Entities.Theme.ComponentField", "ComponentField")
                        .WithMany()
                        .HasForeignKey("ComponentFieldId");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Pages.WebsiteComponent", "WebsiteComponent")
                        .WithMany()
                        .HasForeignKey("WebsiteComponentId");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsitePage", b =>
                {
                    b.HasOne("Community.Model.Entities.Pages.Page", "Page")
                        .WithMany("WebsitePage")
                        .HasForeignKey("PageId");

                    b.HasOne("Community.Model.Entities.Pages.WebsiteComponent", "WebsiteComponent")
                        .WithMany()
                        .HasForeignKey("WebsiteComponentId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany("WebsitePage")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Pages.WebsitePageLayout", b =>
                {
                    b.HasOne("Community.Model.Entities.Pages.Page", "Page")
                        .WithMany()
                        .HasForeignKey("PageId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Role", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Sliders.Slider", b =>
                {
                    b.HasOne("Community.Model.Entities.Sliders.SliderCategory", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Sliders.SliderCategory", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Support.SupportTicket", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Support.SupportTicketDepartment", "SupportTicketDepartment")
                        .WithMany()
                        .HasForeignKey("SupportTicketDepartmentId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Community.Model.Entities.Support.SupportTicketDepartment", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Support.SupportTicketReply", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Support.SupportTicket", "Ticket")
                        .WithMany("SupportTicketReply")
                        .HasForeignKey("TicketId");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.ComponentField", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.Theme", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.ThemeComponent", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("Community.Model.Entities.Globalization.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("Community.Model.Entities.Theme.Theme")
                        .WithMany("Components")
                        .HasForeignKey("ThemeId");
                });

            modelBuilder.Entity("Community.Model.Entities.Theme.ThemeComponentFields", b =>
                {
                    b.HasOne("Community.Model.Entities.Theme.ComponentField", "ComponentField")
                        .WithMany()
                        .HasForeignKey("ComponentFieldId");

                    b.HasOne("Community.Model.Entities.Theme.ThemeComponent", "ThemeComponent")
                        .WithMany()
                        .HasForeignKey("ThemeComponentId");
                });

            modelBuilder.Entity("Community.Model.Entities.TodoList", b =>
                {
                    b.HasOne("Community.Model.Entities.Notes", "Notes")
                        .WithMany("CheckList")
                        .HasForeignKey("NotesId");
                });

            modelBuilder.Entity("Community.Model.Entities.UserRole", b =>
                {
                    b.HasOne("Community.Model.Entities.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("Community.Model.Entities.Global.User", "User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("Document", b =>
                {
                    b.HasOne("Community.Model.Entities.Global.Website")
                        .WithMany("Documents")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SocialWebsiteValue", b =>
                {
                    b.HasOne("Social", "Social")
                        .WithMany("SocialWebsiteValue")
                        .HasForeignKey("SocialId");

                    b.HasOne("Community.Model.Entities.Global.Website", "Website")
                        .WithMany("SocialWebsiteValue")
                        .HasForeignKey("WebsiteId");
                });
        }
    }
}
