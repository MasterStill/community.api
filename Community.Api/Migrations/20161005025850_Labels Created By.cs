﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class LabelsCreatedBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CreatedById",
                table: "Label",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Label",
                nullable: false);

            migrationBuilder.CreateIndex(
                name: "IX_Label_CreatedById",
                table: "Label",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Label_User_CreatedById",
                table: "Label",
                column: "CreatedById",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Label_User_CreatedById",
                table: "Label");

            migrationBuilder.DropIndex(
                name: "IX_Label_CreatedById",
                table: "Label");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "Label");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Label",
                nullable: true);
        }
    }
}
