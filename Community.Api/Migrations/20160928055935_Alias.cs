﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class Alias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Error_Website_WebsiteId",
                table: "Error");

            migrationBuilder.DropIndex(
                name: "IX_Error_WebsiteId",
                table: "Error");

            migrationBuilder.DropColumn(
                name: "WebsiteId",
                table: "Error");

            migrationBuilder.AddColumn<string>(
                name: "Alias",
                table: "Website",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Error",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Alias",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "Error");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteId",
                table: "Error",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Error_WebsiteId",
                table: "Error",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Error_Website_WebsiteId",
                table: "Error",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
