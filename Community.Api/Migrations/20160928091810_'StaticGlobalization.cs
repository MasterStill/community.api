﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Community.Api.Migrations
{
    public partial class StaticGlobalization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StaticGlobalization",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    step = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaticGlobalization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StaticGlobalization_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StaticGlobalization_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StaticGlobalization_CreatedById",
                table: "StaticGlobalization",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_StaticGlobalization_CultureId",
                table: "StaticGlobalization",
                column: "CultureId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StaticGlobalization");
        }
    }
}
