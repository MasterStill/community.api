﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class sadfsdF : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Arcive",
                table: "Notes");

            migrationBuilder.AddColumn<bool>(
                name: "Archive",
                table: "Notes",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Archive",
                table: "Notes");

            migrationBuilder.AddColumn<bool>(
                name: "Arcive",
                table: "Notes",
                nullable: false,
                defaultValue: false);
        }
    }
}
