﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Community.Api.Migrations
{
    public partial class WebsiteModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsitePage_WebsiteComponentId",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteComponentId1",
                table: "WebsitePage",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WebsiteComponentName",
                table: "WebsitePage",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WebsiteComponentId1",
                table: "WebsiteComponentFieldValue",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" });

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" });

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebsiteComponent",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "WebsiteComponent",
                nullable: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent",
                columns: new[] { "Id", "Name" });

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" },
                principalTable: "WebsiteComponent",
                principalColumns: new[] { "Id", "Name" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" },
                principalTable: "WebsiteComponent",
                principalColumns: new[] { "Id", "Name" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsitePage_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent");

            migrationBuilder.DropColumn(
                name: "WebsiteComponentId1",
                table: "WebsitePage");

            migrationBuilder.DropColumn(
                name: "WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropColumn(
                name: "WebsiteComponentId1",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropColumn(
                name: "WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteComponentId",
                table: "WebsitePage",
                column: "WebsiteComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebsiteComponent",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "WebsiteComponent",
                nullable: false)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentId",
                principalTable: "WebsiteComponent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId",
                table: "WebsitePage",
                column: "WebsiteComponentId",
                principalTable: "WebsiteComponent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
