﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class Modules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "Module",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Icon",
                table: "Module",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Param",
                table: "Module",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Module",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "Icon",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "Param",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Module");
        }
    }
}
