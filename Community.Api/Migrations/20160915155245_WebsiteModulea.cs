﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class WebsiteModulea : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsitePage_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent");

            migrationBuilder.DropColumn(
                name: "WebsiteComponentId1",
                table: "WebsitePage");

            migrationBuilder.DropColumn(
                name: "WebsiteComponentId1",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteComponentName",
                table: "WebsitePage",
                column: "WebsiteComponentName");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentName");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent",
                column: "Name");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentName",
                principalTable: "WebsiteComponent",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentName",
                table: "WebsitePage",
                column: "WebsiteComponentName",
                principalTable: "WebsiteComponent",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsitePage_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteComponentId1",
                table: "WebsitePage",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WebsiteComponentId1",
                table: "WebsiteComponentFieldValue",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" });

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent",
                columns: new[] { "Id", "Name" });

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" },
                principalTable: "WebsiteComponent",
                principalColumns: new[] { "Id", "Name" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId1_WebsiteComponentName",
                table: "WebsitePage",
                columns: new[] { "WebsiteComponentId1", "WebsiteComponentName" },
                principalTable: "WebsiteComponent",
                principalColumns: new[] { "Id", "Name" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
