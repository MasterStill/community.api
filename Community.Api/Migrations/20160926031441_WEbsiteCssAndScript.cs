﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class WEbsiteCssAndScript : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Custom_Layout_Css",
                table: "Website",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Custom_Layout_Script",
                table: "Website",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Custom_Layout_Css",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "Custom_Layout_Script",
                table: "Website");
        }
    }
}
