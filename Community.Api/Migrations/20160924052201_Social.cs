﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Community.Api.Migrations
{
    public partial class Social : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Social",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdminIco = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Social", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SocialWebsiteValue",
                columns: table => new
                {
                    WebsiteId = table.Column<int>(nullable: false),
                    SocialId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialWebsiteValue", x => new { x.WebsiteId, x.SocialId });
                    table.ForeignKey(
                        name: "FK_SocialWebsiteValue_Social_SocialId",
                        column: x => x.SocialId,
                        principalTable: "Social",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SocialWebsiteValue_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.AddColumn<bool>(
                name: "Comments",
                table: "Website",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Disqus_Code",
                table: "Website",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SocialWebsiteValue_SocialId",
                table: "SocialWebsiteValue",
                column: "SocialId");

            migrationBuilder.CreateIndex(
                name: "IX_SocialWebsiteValue_WebsiteId",
                table: "SocialWebsiteValue",
                column: "WebsiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comments",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "Disqus_Code",
                table: "Website");

            migrationBuilder.DropTable(
                name: "SocialWebsiteValue");

            migrationBuilder.DropTable(
                name: "Social");
        }
    }
}
