﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class ScreenshotSupportTicket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ScreenShot",
                table: "SupportTicketReply",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ScreenShot",
                table: "SupportTicket",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScreenShot",
                table: "SupportTicketReply");

            migrationBuilder.DropColumn(
                name: "ScreenShot",
                table: "SupportTicket");
        }
    }
}
