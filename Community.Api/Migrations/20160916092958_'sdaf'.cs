﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class sdaf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout");

            migrationBuilder.DropIndex(
                name: "IX_WebsitePage_WebsiteComponentName",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentName",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent");

            // migrationBuilder.DropColumn(
            //     name: "WebsiteComponentName",
            //     table: "WebsitePage");

            // migrationBuilder.DropColumn(
            //     name: "WebsiteComponentName",
            //     table: "WebsiteComponentFieldValue");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteComponentId",
                table: "WebsitePage",
                column: "WebsiteComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebsiteComponent",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentId",
                principalTable: "WebsiteComponent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId",
                table: "WebsitePage",
                column: "WebsiteComponentId",
                principalTable: "WebsiteComponent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId",
                table: "WebsitePage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout");

            migrationBuilder.DropIndex(
                name: "IX_WebsitePage_WebsiteComponentId",
                table: "WebsitePage");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId",
                table: "WebsiteComponentFieldValue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent");

            migrationBuilder.AddColumn<string>(
                name: "WebsiteComponentName",
                table: "WebsitePage",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout",
                columns: new[] { "Layout", "PageId", "WebsiteId" });

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteComponentName",
                table: "WebsitePage",
                column: "WebsiteComponentName");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentName");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WebsiteComponent",
                nullable: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteComponent",
                table: "WebsiteComponent",
                column: "Name");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentName",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentName",
                principalTable: "WebsiteComponent",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentName",
                table: "WebsitePage",
                column: "WebsiteComponentName",
                principalTable: "WebsiteComponent",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
