﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class WebsitePageLayout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout",
                columns: new[] { "Layout", "PageId", "WebsiteId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsitePageLayout",
                table: "WebsitePageLayout",
                column: "Id");
        }
    }
}
