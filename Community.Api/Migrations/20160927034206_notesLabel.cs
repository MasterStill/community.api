﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class notesLabel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Label_Notes_NotesId",
                table: "Label");

            migrationBuilder.DropIndex(
                name: "IX_Label_NotesId",
                table: "Label");

            migrationBuilder.DropColumn(
                name: "NotesId",
                table: "Label");

            migrationBuilder.CreateTable(
                name: "NotesLabel",
                columns: table => new
                {
                    NotesId = table.Column<int>(nullable: false),
                    LabelId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotesLabel", x => new { x.NotesId, x.LabelId });
                    table.ForeignKey(
                        name: "FK_NotesLabel_Label_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Label",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NotesLabel_Notes_NotesId",
                        column: x => x.NotesId,
                        principalTable: "Notes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NotesLabel_LabelId",
                table: "NotesLabel",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_NotesLabel_NotesId",
                table: "NotesLabel",
                column: "NotesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NotesLabel");

            migrationBuilder.AddColumn<int>(
                name: "NotesId",
                table: "Label",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Label_NotesId",
                table: "Label",
                column: "NotesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Label_Notes_NotesId",
                table: "Label",
                column: "NotesId",
                principalTable: "Notes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
