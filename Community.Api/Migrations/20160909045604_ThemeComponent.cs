﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Community.Api.Migrations
{
    public partial class ThemeComponent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Delflag = table.Column<bool>(nullable: false),
                    DoB = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Firstname = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    IsLocked = table.Column<bool>(nullable: false),
                    LastLoggedInDate = table.Column<DateTime>(nullable: true),
                    LastLoggedOnIP = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    RegisteredFromIP = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    UserType = table.Column<int>(nullable: false),
                    Username = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Culture",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CultureId = table.Column<int>(nullable: false),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Culture", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Culture_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AuditLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AuditActionTypeENUM = table.Column<int>(nullable: false),
                    Changes = table.Column<string>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    DataModel = table.Column<string>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    KeyFieldID = table.Column<int>(nullable: false),
                    KeyName = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    ValueAfter = table.Column<string>(nullable: true),
                    ValueBefore = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuditLog_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AuditLog_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogTag",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogTag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlogTag_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogTag_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Website",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllocatedSpace = table.Column<int>(nullable: false),
                    Construction = table.Column<bool>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    DefaultCultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    DisplayWebsiteOf = table.Column<string>(nullable: true),
                    GoogleAnalyticsCode = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Redirect = table.Column<bool>(nullable: false),
                    RedirectToUrl = table.Column<string>(nullable: true),
                    SpaceUsed = table.Column<int>(nullable: false),
                    Theme = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Website", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Website_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Website_Culture_DefaultCultureId",
                        column: x => x.DefaultCultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserCulture",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    CultureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCulture", x => new { x.UserId, x.CultureId });
                    table.ForeignKey(
                        name: "FK_UserCulture_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserCulture_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Module",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Options = table.Column<int>(nullable: false),
                    ParentModuleId = table.Column<int>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Module", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Module_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Module_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Module_Module_ParentModuleId",
                        column: x => x.ParentModuleId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Page",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    DisplayName = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Summary = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Page", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Page_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Page_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Role_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Role_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SupportTicketDepartment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportTicketDepartment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupportTicketDepartment_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupportTicketDepartment_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ComponentField",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    DataType = table.Column<int>(nullable: false),
                    Delflag = table.Column<bool>(nullable: false),
                    FieldName = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComponentField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComponentField_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComponentField_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Theme",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Author = table.Column<string>(nullable: true),
                    CopyRightNotice = table.Column<string>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Designer = table.Column<string>(nullable: true),
                    DispalyInPublic = table.Column<bool>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    Summary = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Theme", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Theme_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Theme_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhotoAlbumCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoAlbumCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhotoAlbumCategory_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAlbumCategory_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAlbumCategory_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    ParentCategoryId = table.Column<int>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false),
                    password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlogCategory_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogCategory_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogCategory_BlogCategory_ParentCategoryId",
                        column: x => x.ParentCategoryId,
                        principalTable: "BlogCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogCategory_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Comment = table.Column<string>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Portion = table.Column<string>(nullable: true),
                    PortionId = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Error",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    StackTrace = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Error", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Error_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Error_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Error_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MenuUrl = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NewWindow = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    ParentMenu = table.Column<int>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Menu_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Menu_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Menu_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebsiteCulture",
                columns: table => new
                {
                    WebsiteId = table.Column<int>(nullable: false),
                    CultureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteCulture", x => new { x.WebsiteId, x.CultureId });
                    table.ForeignKey(
                        name: "FK_WebsiteCulture_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsiteCulture_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SliderCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SliderCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SliderCategory_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SliderCategory_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SliderCategory_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebsiteModule",
                columns: table => new
                {
                    WebsiteId = table.Column<int>(nullable: false),
                    ModuleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteModule", x => new { x.WebsiteId, x.ModuleId });
                    table.ForeignKey(
                        name: "FK_WebsiteModule_Module_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsiteModule_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebsitePageLayout",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Layout = table.Column<int>(nullable: false),
                    PageId = table.Column<int>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsitePageLayout", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebsitePageLayout_Page_PageId",
                        column: x => x.PageId,
                        principalTable: "Page",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsitePageLayout_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.UserId, x.RoleId, x.WebsiteId, x.Id });
                    table.ForeignKey(
                        name: "FK_UserRole_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRole_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRole_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SupportTicket",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Subject = table.Column<string>(nullable: true),
                    SupportTicketDepartmentId = table.Column<int>(nullable: false),
                    TicketId = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportTicket", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupportTicket_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupportTicket_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupportTicket_SupportTicketDepartment_SupportTicketDepartmentId",
                        column: x => x.SupportTicketDepartmentId,
                        principalTable: "SupportTicketDepartment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupportTicket_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeComponent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ThemeId = table.Column<int>(nullable: true),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThemeComponent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ThemeComponent_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ThemeComponent_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ThemeComponent_Theme_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Theme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhotoAlbum",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    Title = table.Column<string>(maxLength: 100, nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoAlbum", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhotoAlbum_PhotoAlbumCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "PhotoAlbumCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAlbum_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAlbum_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAlbum_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogPost",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BlogType = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    CommentAllowed = table.Column<bool>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    PublishDate = table.Column<DateTime>(nullable: false),
                    PublishStatus = table.Column<int>(nullable: false),
                    Summary = table.Column<string>(nullable: true),
                    ThumbnailUrl = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    Visibility = table.Column<int>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false),
                    password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPost", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlogPost_BlogCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "BlogCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogPost_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogPost_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogPost_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Slider",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BackgroundImage = table.Column<string>(nullable: true),
                    CategoryId = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Summary = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Slider", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Slider_SliderCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "SliderCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Slider_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Slider_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Slider_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SupportTicketReply",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Reply = table.Column<string>(nullable: true),
                    TicketId = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportTicketReply", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupportTicketReply_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupportTicketReply_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupportTicketReply_SupportTicket_TicketId",
                        column: x => x.TicketId,
                        principalTable: "SupportTicket",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebsiteComponent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModuleId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ThemeComponentId = table.Column<int>(nullable: false),
                    ThemeComponentName = table.Column<string>(nullable: true),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteComponent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebsiteComponent_Module_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsiteComponent_ThemeComponent_ThemeComponentId",
                        column: x => x.ThemeComponentId,
                        principalTable: "ThemeComponent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsiteComponent_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeComponentFields",
                columns: table => new
                {
                    ThemeComponentId = table.Column<int>(nullable: false),
                    ComponentFieldId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThemeComponentFields", x => new { x.ThemeComponentId, x.ComponentFieldId });
                    table.ForeignKey(
                        name: "FK_ThemeComponentFields_ComponentField_ComponentFieldId",
                        column: x => x.ComponentFieldId,
                        principalTable: "ComponentField",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ThemeComponentFields_ThemeComponent_ThemeComponentId",
                        column: x => x.ThemeComponentId,
                        principalTable: "ThemeComponent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhotoAlbumPhoto",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AlbumId = table.Column<int>(nullable: false),
                    AlbumThumbNail = table.Column<bool>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<int>(nullable: true),
                    Delflag = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    MultiLingualId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 100, nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoAlbumPhoto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhotoAlbumPhoto_PhotoAlbum_AlbumId",
                        column: x => x.AlbumId,
                        principalTable: "PhotoAlbum",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAlbumPhoto_User_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAlbumPhoto_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlogPostBlogTag",
                columns: table => new
                {
                    PostId = table.Column<int>(nullable: false),
                    TagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPostBlogTag", x => new { x.PostId, x.TagId });
                    table.ForeignKey(
                        name: "FK_BlogPostBlogTag_BlogPost_PostId",
                        column: x => x.PostId,
                        principalTable: "BlogPost",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogPostBlogTag_BlogTag_TagId",
                        column: x => x.TagId,
                        principalTable: "BlogTag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebsiteComponentFieldValue",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ComponentFieldId = table.Column<int>(nullable: false),
                    CultureId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    WebsiteComponentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteComponentFieldValue", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebsiteComponentFieldValue_ComponentField_ComponentFieldId",
                        column: x => x.ComponentFieldId,
                        principalTable: "ComponentField",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsiteComponentFieldValue_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsiteComponentFieldValue_WebsiteComponent_WebsiteComponentId",
                        column: x => x.WebsiteComponentId,
                        principalTable: "WebsiteComponent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebsitePage",
                columns: table => new
                {
                    WebsiteId = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    Location = table.Column<int>(nullable: false),
                    PageId = table.Column<int>(nullable: false),
                    Count = table.Column<int>(nullable: false),
                    Options = table.Column<int>(nullable: false),
                    WebsiteComponentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsitePage", x => new { x.WebsiteId, x.Order, x.Location, x.PageId });
                    table.ForeignKey(
                        name: "FK_WebsitePage_Page_PageId",
                        column: x => x.PageId,
                        principalTable: "Page",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsitePage_WebsiteComponent_WebsiteComponentId",
                        column: x => x.WebsiteComponentId,
                        principalTable: "WebsiteComponent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WebsitePage_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbum_CategoryId",
                table: "PhotoAlbum",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbum_CreatedById",
                table: "PhotoAlbum",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbum_CultureId",
                table: "PhotoAlbum",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbum_WebsiteId",
                table: "PhotoAlbum",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbumCategory_CreatedById",
                table: "PhotoAlbumCategory",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbumCategory_CultureId",
                table: "PhotoAlbumCategory",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbumCategory_WebsiteId",
                table: "PhotoAlbumCategory",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbumPhoto_AlbumId",
                table: "PhotoAlbumPhoto",
                column: "AlbumId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbumPhoto_CreatedById",
                table: "PhotoAlbumPhoto",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAlbumPhoto_CultureId",
                table: "PhotoAlbumPhoto",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_AuditLog_CreatedById",
                table: "AuditLog",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_AuditLog_CultureId",
                table: "AuditLog",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogCategory_CreatedById",
                table: "BlogCategory",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_BlogCategory_CultureId",
                table: "BlogCategory",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogCategory_ParentCategoryId",
                table: "BlogCategory",
                column: "ParentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogCategory_WebsiteId",
                table: "BlogCategory",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPost_CategoryId",
                table: "BlogPost",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPost_CreatedById",
                table: "BlogPost",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPost_CultureId",
                table: "BlogPost",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPost_WebsiteId",
                table: "BlogPost",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPostBlogTag_PostId",
                table: "BlogPostBlogTag",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPostBlogTag_TagId",
                table: "BlogPostBlogTag",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogTag_CreatedById",
                table: "BlogTag",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_BlogTag_CultureId",
                table: "BlogTag",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_CreatedById",
                table: "Comments",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_CultureId",
                table: "Comments",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_WebsiteId",
                table: "Comments",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Error_CreatedById",
                table: "Error",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Error_CultureId",
                table: "Error",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Error_WebsiteId",
                table: "Error",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Menu_CreatedById",
                table: "Menu",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Menu_CultureId",
                table: "Menu",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Menu_WebsiteId",
                table: "Menu",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Website_CreatedById",
                table: "Website",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Website_DefaultCultureId",
                table: "Website",
                column: "DefaultCultureId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteCulture_CultureId",
                table: "WebsiteCulture",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteCulture_WebsiteId",
                table: "WebsiteCulture",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Culture_CreatedById",
                table: "Culture",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UserCulture_CultureId",
                table: "UserCulture",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCulture_UserId",
                table: "UserCulture",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_CreatedById",
                table: "Module",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Module_CultureId",
                table: "Module",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_ParentModuleId",
                table: "Module",
                column: "ParentModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteModule_ModuleId",
                table: "WebsiteModule",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteModule_WebsiteId",
                table: "WebsiteModule",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Page_CreatedById",
                table: "Page",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Page_CultureId",
                table: "Page",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponent_ModuleId",
                table: "WebsiteComponent",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponent_ThemeComponentId",
                table: "WebsiteComponent",
                column: "ThemeComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponent_WebsiteId",
                table: "WebsiteComponent",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_ComponentFieldId",
                table: "WebsiteComponentFieldValue",
                column: "ComponentFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_CultureId",
                table: "WebsiteComponentFieldValue",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteComponentFieldValue_WebsiteComponentId",
                table: "WebsiteComponentFieldValue",
                column: "WebsiteComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_PageId",
                table: "WebsitePage",
                column: "PageId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteComponentId",
                table: "WebsitePage",
                column: "WebsiteComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePage_WebsiteId",
                table: "WebsitePage",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePageLayout_PageId",
                table: "WebsitePageLayout",
                column: "PageId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePageLayout_WebsiteId",
                table: "WebsitePageLayout",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_CreatedById",
                table: "Role",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Role_CultureId",
                table: "Role",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_CategoryId",
                table: "Slider",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_CreatedById",
                table: "Slider",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_CultureId",
                table: "Slider",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_WebsiteId",
                table: "Slider",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SliderCategory_CreatedById",
                table: "SliderCategory",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_SliderCategory_CultureId",
                table: "SliderCategory",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_SliderCategory_WebsiteId",
                table: "SliderCategory",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicket_CreatedById",
                table: "SupportTicket",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicket_CultureId",
                table: "SupportTicket",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicket_SupportTicketDepartmentId",
                table: "SupportTicket",
                column: "SupportTicketDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicket_WebsiteId",
                table: "SupportTicket",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicketDepartment_CreatedById",
                table: "SupportTicketDepartment",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicketDepartment_CultureId",
                table: "SupportTicketDepartment",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicketReply_CreatedById",
                table: "SupportTicketReply",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicketReply_CultureId",
                table: "SupportTicketReply",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_SupportTicketReply_TicketId",
                table: "SupportTicketReply",
                column: "TicketId");

            migrationBuilder.CreateIndex(
                name: "IX_ComponentField_CreatedById",
                table: "ComponentField",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_ComponentField_CultureId",
                table: "ComponentField",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_Theme_CreatedById",
                table: "Theme",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Theme_CultureId",
                table: "Theme",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeComponent_CreatedById",
                table: "ThemeComponent",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeComponent_CultureId",
                table: "ThemeComponent",
                column: "CultureId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeComponent_ThemeId",
                table: "ThemeComponent",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeComponentFields_ComponentFieldId",
                table: "ThemeComponentFields",
                column: "ComponentFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeComponentFields_ThemeComponentId",
                table: "ThemeComponentFields",
                column: "ThemeComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_RoleId",
                table: "UserRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_UserId",
                table: "UserRole",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_WebsiteId",
                table: "UserRole",
                column: "WebsiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhotoAlbumPhoto");

            migrationBuilder.DropTable(
                name: "AuditLog");

            migrationBuilder.DropTable(
                name: "BlogPostBlogTag");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Error");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "WebsiteCulture");

            migrationBuilder.DropTable(
                name: "UserCulture");

            migrationBuilder.DropTable(
                name: "WebsiteModule");

            migrationBuilder.DropTable(
                name: "WebsiteComponentFieldValue");

            migrationBuilder.DropTable(
                name: "WebsitePage");

            migrationBuilder.DropTable(
                name: "WebsitePageLayout");

            migrationBuilder.DropTable(
                name: "Slider");

            migrationBuilder.DropTable(
                name: "SupportTicketReply");

            migrationBuilder.DropTable(
                name: "ThemeComponentFields");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "PhotoAlbum");

            migrationBuilder.DropTable(
                name: "BlogPost");

            migrationBuilder.DropTable(
                name: "BlogTag");

            migrationBuilder.DropTable(
                name: "WebsiteComponent");

            migrationBuilder.DropTable(
                name: "Page");

            migrationBuilder.DropTable(
                name: "SliderCategory");

            migrationBuilder.DropTable(
                name: "SupportTicket");

            migrationBuilder.DropTable(
                name: "ComponentField");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "PhotoAlbumCategory");

            migrationBuilder.DropTable(
                name: "BlogCategory");

            migrationBuilder.DropTable(
                name: "Module");

            migrationBuilder.DropTable(
                name: "ThemeComponent");

            migrationBuilder.DropTable(
                name: "SupportTicketDepartment");

            migrationBuilder.DropTable(
                name: "Website");

            migrationBuilder.DropTable(
                name: "Theme");

            migrationBuilder.DropTable(
                name: "Culture");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
