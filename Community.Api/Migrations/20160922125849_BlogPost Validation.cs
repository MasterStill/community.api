﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Community.Api.Migrations
{
    public partial class BlogPostValidation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "BlogPost",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "ThumbnailUrl",
                table: "BlogPost",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Summary",
                table: "BlogPost",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "ImageUrl",
                table: "BlogPost",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "BlogPost",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "BlogPost",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ThumbnailUrl",
                table: "BlogPost",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Summary",
                table: "BlogPost",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ImageUrl",
                table: "BlogPost",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "BlogPost",
                nullable: true);
        }
    }
}
