using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Razor;
public class ViewLocationExpander : IViewLocationExpander
{
    private const string THEME_KEY = "Themes";
    //private ISession _session => _httpContextAccessor.HttpContext.Session;
    //private IMemoryCache _memoryCache;
    // public ViewLocationExpander(IMemoryCache _memoryCache){
    //     this._memoryCache = _memoryCache;
    // }
    public void PopulateValues(ViewLocationExpanderContext context)
    {
        //WebsiteBasicSettings _websiteBasicSettings = new WebsiteBasicSettings();
        //if (_memoryCache.TryGetValue("localhost:5000", out _websiteBasicSettings))
        //context.Values[THEME_KEY] = context.ActionContext.HttpContext.User.
        context.Values[THEME_KEY] = (string)context.ActionContext.HttpContext.Items["Theme"];
    }
    public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
    {
        string theme = null;
        if (context.Values.TryGetValue(THEME_KEY, out theme))
        {
            viewLocations = new[] {
                //$"/Themes/{theme}/{{1}}/{{0}}.cshtml",
                $"/Themes/{theme}/{{0}}.cshtml",
                // $"/Themes/{theme}/Components/{{0}}.cshtml",
                // $"/Themes/{theme}/Components/{{1}}.cshtml",
                // $"/Themes/{theme}/Components/{{2}}.cshtml",
                $"/Views/Shared/{{0}}.cshtml",
            }
            .Concat(viewLocations);
        }
        return viewLocations;
    }
     string GetRealWebsiteName()
        {
            return "";
            //return ($"{Request.Host}");
        }

}