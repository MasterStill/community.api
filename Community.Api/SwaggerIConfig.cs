﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Community.Data.Infrastructure.Repository.Abstract;
using System.Security.Claims;
using System.Security.Principal;
using Swashbuckle.Swagger;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
namespace Community.Api
{
    public class SwaggerIConfig : IOperationFilter
    {
      public void Apply(Operation operation, OperationFilterContext context)
{
    var filterPipeline = context.ApiDescription.ActionDescriptor.FilterDescriptors;
    var isAuthorized = filterPipeline.Select(filterInfo => filterInfo.Filter).Any(filter => filter is AuthorizeFilter);
                var allowAnonymous = filterPipeline.Select(filterInfo => filterInfo.Filter).Any(filter => filter is IAllowAnonymousFilter);

    if (isAuthorized && !allowAnonymous)
    {
        if (operation.Parameters == null)
            operation.Parameters = new List<IParameter>();

        operation.Parameters.Add(new NonBodyParameter
        {                    
            Name = "Authorization",
            In = "header",
            Description = "access token",
            Required = true,
            Type = "string",
            Default ="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwianRpIjoiMzA2MDIyNjUtZjhlZC00Y2UyLWI5YmYtZmM1ZDZiODcxMmM1IiwiaWF0IjoxNDcxMzM1Njk2LCJSb2xlIjoiQWRtaW4iLCJuYmYiOjE0NzEzMzU2OTUsImV4cCI6MTUwMTMzNTY5NSwiaXNzIjoiYTFkNTBjMzEtZDEyNC00NWRkLWJlYjYtNGRkMDc2YzFlOGQ3IiwiYXVkIjoiYTlkNTZjMzAtZDcyNi00NWRkLWJlYTEtNGRkMDc2YzFlMmQ3In0.WyCvqyGucXt5EfWmAouD5ygVdfmsn9K4UuVfqKj1bM4"
        });
    }
}
}
}     
   