using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Community.Model.Entities.Global;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Community.Model.Entities;
using AutoMapper;
namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/Notes")]
    public class NotesController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private IPhotoAlbumService _photoAlbumService;
        private IHostingEnvironment _hostingEnvironment;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper Mapper { get; set; }
        List<string> Errors = new List<string>();
        public NotesController(IMapper Mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IPhotoAlbumService PhotoAlbumService, IHostingEnvironment HostingEnvironment)
        {
            this.Mapper = Mapper;

            this._context = _context;
            this._hostingEnvironment = HostingEnvironment;
            this._photoAlbumService = PhotoAlbumService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<PhotoAlbumController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpGet("")]
        public IActionResult Get()
        {
            var _result = _context.Notes.Include(x => x.NotesLabel).Include(x => x.CheckList).Where(x => x.DelFlag == false).Where(x => x.CreatedById == _global.UserIdFromJWT(User)).ToList();
            var _sendResult = Mapper.Map<List<NotesViewModel>>(_result);
            return new OkObjectResult(_sendResult);
        }

        [HttpGet("{Noteid}")]
        public IActionResult Get(int NoteId)
        {
            var _result = _context.Notes.Include(x => x.NotesLabel).Include(x => x.CheckList).Where(x => x.CreatedById == _global.UserIdFromJWT(User)).Where(x => x.Id == NoteId).SingleOrDefault();
            var _sendResult = Mapper.Map<NotesViewModel>(_result);
            return new OkObjectResult(_sendResult);
        }

        [HttpPost("")]
        public IActionResult SaveNote([FromBody] NotesCreateViewModel n)
        {
            if (!ModelState.IsValid)
            {
                foreach (var items in ModelState)
                {
                    Errors.AddRange(items.Value.Errors.ToList().Select(x => x.ErrorMessage));
                }
                string messageTosend = "{\"Message\":\"" + JsonConvert.SerializeObject(Errors).Substring(1, JsonConvert.SerializeObject(Errors).Count() - 2).Replace("\"", "").Replace(",", "<br>") + "\"}";

                return new BadRequestObjectResult(messageTosend);
            }
            GenericResult _result = new GenericResult();
            try
            {
                Notes newNote = Mapper.Map<Notes>(n);
                if (n.Label != null)
                {
                    foreach (int items in n.Label)
                    {
                        NotesLabel l = new NotesLabel();
                        l.LabelId = items;
                        newNote.NotesLabel.Add(l);
                    }
                }
                newNote.CheckList = Mapper.Map<List<TodoList>>(n.CheckList);
                newNote.CreatedById = _global.UserIdFromJWT(User);
                newNote.Time = DateTime.Now;

                _context.Notes.Add(newNote);
                _context.SaveChanges();
                _result = Global.Success("Notes Added", newNote.Id.ToString());
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }
        [HttpPost("CheckList/{CheckListId}")]
        public IActionResult CheckList(int CheckListId)
        {
            var _dbCheckList = _context.TodoList.Where(x => x.Id == CheckListId).SingleOrDefault();
            _dbCheckList.Checked = !_dbCheckList.Checked;
            _context.TodoList.Update(_dbCheckList);
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Global.Error(ex);
            }
            return Ok();
        }
        [HttpPost("{Noteid}")]
        [Validate]
        public IActionResult EditNote(int NoteId, [FromBody] NotesCreateViewModel n)
        {
            GenericResult _result = new GenericResult();
            var _dataBaseNote = _context.Notes.Include(x => x.CheckList).Include(x => x.NotesLabel).Where(x => x.CreatedById == _global.UserIdFromJWT(User)).Where(x => x.Id == NoteId).ToList().SingleOrDefault();
            _dataBaseNote.Archive = n.Archive;
            var CheckListToRemove = _context.TodoList.Where(x => x.Notes.Id == NoteId);
            _context.TodoList.RemoveRange(CheckListToRemove);
            _context.SaveChanges();
            foreach (var items in n.CheckList)
            {
                items.id = 0;
            }
            _dataBaseNote.CheckList = Mapper.Map<List<TodoList>>(n.CheckList);
            _dataBaseNote.Description = n.Description;
            _dataBaseNote.Color = n.Color;
            _dataBaseNote.Image = n.Image;
            _dataBaseNote.Title = n.Title;
            try
            {
                _context.Notes.Update(_dataBaseNote);
                _context.SaveChanges();
                return new OkObjectResult(Mapper.Map<NotesViewModel>(_dataBaseNote));
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }


        [HttpGet("Labels")]
        public IActionResult GetLabes()
        {
            var _result = _context.Label.ToList().Where(x=>x.CreatedById == _global.UserIdFromJWT(User));            
            var _sendResult = Mapper.Map<List<LabelViewModel>>(_result);
            return new OkObjectResult(_sendResult);
        }
        [HttpPost("Labels")]
        [Validate]
        public IActionResult CreateLables([FromBody] Label label)
        {
            GenericResult _result = new GenericResult();
            try
            {
                label.CreatedById = _global.UserIdFromJWT(User);
                _context.Label.Add(label);
                _context.SaveChanges();
                return new OkObjectResult(Mapper.Map<LabelViewModel>(label));
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }


        [HttpPost("Labels/{LabelId}")]
        [Validate]
        public IActionResult CreateLables(int LabelId, [FromBody] Label label)
        {
            GenericResult _result = new GenericResult();
            try
            {
                _context.Label.Update(label);
                _context.SaveChanges();
                _result = Global.Success("Notes Added");
                return new OkObjectResult(Mapper.Map<LabelViewModel>(label));
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }

        [HttpDelete("Labels/{LabelId}")]
        public IActionResult CreateLables(int LabelId)
        {
            GenericResult _result = new GenericResult();
            try
            {
                var _labeltoDelete = _context.Label.Where(x => x.Id == LabelId).SingleOrDefault();
                _context.Remove(_labeltoDelete);
                _context.SaveChanges();
                _result = Global.Success("Label Deleted");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }

        [HttpDelete("{NotesId}")]
        public IActionResult DeleteNote(int NotesId)
        {
            GenericResult _result = new GenericResult();
            var _deleteResult = _context.Notes.Where(x => x.Id == NotesId).Where(x => x.CreatedById == _global.UserIdFromJWT(User)).SingleOrDefault();
            try
            {
                _deleteResult.DelFlag = true;
                _context.Notes.Update(_deleteResult);
                _context.SaveChanges();
                _result = Global.Success("Notes Deleted");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }
    }
}