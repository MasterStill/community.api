using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
namespace Community.Api.Controllers
{
    [Authorize]
    [Route("api/photoalbum")]
    public class PhotoAlbumController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private IPhotoAlbumService _photoAlbumService;
        private IHostingEnvironment _hostingEnvironment;
        private GlobalUser _global = new GlobalUser();
        public PhotoAlbumController(ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IPhotoAlbumService PhotoAlbumService, IHostingEnvironment HostingEnvironment)
        {
            this._hostingEnvironment = HostingEnvironment;
            this._photoAlbumService = PhotoAlbumService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<PhotoAlbumController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpGet("{WebsiteId}/{page?}")] // Get Album
        public IEnumerable<PhotoAlbumViewModel> Get(int WebsiteId, int? page = 0)
        {
            return _photoAlbumService.GetAlbums(_global.UserIdFromJWT(User), page.Value, Global.Pagesize(), WebsiteId);
        }

        [HttpGet("{id}/Photos/{page?}")]
        public IEnumerable<PhotoViewModel> GetPhotos(int id, int? page = 0)
        {
            return _photoAlbumService.GetPhotos(1, id, page.Value, Global.Pagesize());
        }
        [HttpDelete("Website/{WebsiteId}/Photo/{id:int}")]
        public IActionResult Delete(int id, int websiteId)
        {
            var _result = _photoAlbumService.DeletePhoto(_global.UserIdFromJWT(User), id, websiteId);
            return Global.SendAsItIs(_result);
        }
        [HttpDelete("Album/{id:int}")]
        public IActionResult Delete(int id)
        {
            var _result = _photoAlbumService.DeleteGallery(_global.UserIdFromJWT(User), id);
            return Global.SendAsItIs(_result);
        }
        [HttpPost("Photos")]
        public IActionResult CreateNewPhotos([FromBody] PhotoAlbumPhotoCreateViewModel pvm)
        {
            var _result = _photoAlbumService.CreatePhoto(_global.UserIdFromJWT(User), pvm);
            return Global.SendAsItIs(_result);
        }
        [HttpPost("Photos/{photoId}")]
        public IActionResult EditPhoto([FromBody] PhotoAlbumPhotoEditViewModel pvm)
        {
            var _result = _photoAlbumService.EditPhoto(_global.UserIdFromJWT(User), pvm);
            return Global.SendAsItIs(_result);
        }

        [HttpPost]
        [Validate]
        public IActionResult Get([FromBody] PhotoAlbumCreateViewModel photos, IFormCollection formCollection)
        {
            var _result = _photoAlbumService.CreateAlbum(_global.UserIdFromJWT(User), photos, formCollection.Files, _hostingEnvironment);
            return Global.SendAsItIs(_result);
        }

        [HttpPost("Album/{AlbumId}")]
        [Validate]
        public IActionResult Get([FromBody] PhotoAlbumCreateViewModel photos, IFormCollection formCollection, int AlbumId)
        {
            photos.Id = AlbumId;
            var _result = _photoAlbumService.CreateAlbum(_global.UserIdFromJWT(User), photos, formCollection.Files, _hostingEnvironment);
            return Global.SendAsItIs(_result);
        }

        [HttpPost("UpdateAlbum")]
        [Validate]
        public IActionResult UpdateAlbumDetails([FromBody] PhotoAlbumCreateViewModel photos, IFormCollection formCollection)
        {
            var _result = _photoAlbumService.CreateAlbum(_global.UserIdFromJWT(User), photos, formCollection.Files, _hostingEnvironment);
            return Global.SendAsItIs(_result);
        }

        [HttpPost("Categories")]
        public IActionResult CreateCategory([FromBody] PhotoAlbumCategoryCreateViewModel pac)
        {
            var _result = _photoAlbumService.CreateCategoy(_global.UserIdFromJWT(User), pac);
            return Global.SendAsItIs(_result);
        }
        [HttpGet("Categories/{WebsiteId}")]
        public IActionResult GetCategory([FromBody] int WebsiteId)
        {
            return new ObjectResult(_photoAlbumService.GetCategories(WebsiteId));
        }
    }
}