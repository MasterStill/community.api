using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Community.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using System.ComponentModel.DataAnnotations;

namespace Community.Api.Controllers
{
    [Authorize]
    [Route("api/FilesAndDirectory")]
    [Produces("application/json")]
    public class FilesAndDirectoryController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private GlobalUser _global = new GlobalUser();
        private IHostingEnvironment _hostingEnviroment;
        private IFileAndDirectoryService _fdService;
        private IUserService _userService;
        public FilesAndDirectoryController(IHostingEnvironment _hostingEnviroment, IFileAndDirectoryService _fdService, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IUserService _userService)
        {
            this._fdService = _fdService;
            this._hostingEnviroment = _hostingEnviroment;
            this._userService = _userService;

            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<FilesAndDirectoryController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpPost("{WebsiteId}/{Path}")]
        public IActionResult UploadContents(int WebsiteId, IFormCollection formCollection, string Path = "")
        {
            var _result = _fdService.CreateFiles(WebsiteId, Path, formCollection, _hostingEnviroment, _global.UserIdFromJWT(User));
          return  Global.SendAsItIs(_result);
            // if (_result.Succeeded) return new OkObjectResult(_result);
            // return BadRequest(_result);
        }

        public class FileSearchOption
        {
            public string SearchTerm { get; set; }
            public string Path { get; set; }
        }
        [HttpGet("{WebsiteId}/Path/{Path}/SearchTerm/{SearchTerm}")]
        public FileAndDirectoryViewModel SearchContents(int WebsiteId, string SearchTerm, string Path)
        {
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), WebsiteId, (new string[] { "Admin", "Owner", "Editor" }))) return new FileAndDirectoryViewModel();
            return _fdService.Search(WebsiteId, "", Path, SearchTerm, _hostingEnviroment, _global.UserIdFromJWT(User));
        }

        public class DeleteContentViewModel
        {
            
            [Required]
            public string FileName { get; set; }
        }
        [HttpPost("Website/{WebsiteId}/Delete")]
        [Validate]
        public IActionResult DeleteContent([FromBody] DeleteContentViewModel d, int WebsiteId)
        {
            var _result = _fdService.DeleteContent(_global.UserIdFromJWT(User), WebsiteId, d.FileName);
            return Global.SendAsItIs(_result);
        }
    }
}