using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Community.Services.Abstract;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
// using Community.Model.Entities.Global;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
//  using System.Security.Principal;
//  using System.Security.Claims;
// using System.IdentityModel.Tokens.Jwt;
//using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using Community.Services;
using Community.Model.Entities;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.ViewModels;
using Community.Data.Infrastructure;
using Community.Model.Entities.Module;
using Community.Model.Entities.Support;
using Community.Model.Entities.Theme;
using Community.Model.Entities.Pages;

namespace Community.Api.Controllers
{
    // [Authorize(Policy = "Admin")]
    [AllowAnonymous]
    [Route("Api/Seed")]
    public class SeedController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;

        private readonly IWebsiteService _websiteService;
        private readonly IMembershipService _membershipService;
        private readonly ICultureService _cultureService;
        private readonly IRoleService _roleService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private IBlogService _blogService;
        private IThemeService _themeService;
        private CommunityContext _context;
        //private UserManager<User> _userManager;
        public SeedController(ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions,
                                IWebsiteService _websiteService, IUserService _userService,
                                IMembershipService _membershipService,
                                ICultureService _cultureService, IRoleService _roleService,
                                IBlogService _blogService,
                                IThemeService _themeService,
                                CommunityContext _context
                                )
        {
            this._context = _context;
            this._cultureService = _cultureService;
            this._websiteService = _websiteService;
            this._membershipService = _membershipService;
            this._userService = _userService;
            this._blogService = _blogService;
            this._roleService = _roleService;
            this._themeService = _themeService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpPost("SeedInitialData")]
        public IActionResult CreateWebsite()
        {
            AddPage();
            AddModules();
            return Ok();
            //goto LL;
            List<GenericResult> _results = new List<GenericResult>();
            try
            {
                User _user = _membershipService.CreateUser("MasterStill", "masterstill@gmail.com", "123!@#Subinn!!", new int[] { 1 },"Subin Adhikari");
                Culture cultureEnglish = new Culture();  // Created ID 1
                cultureEnglish.Title = "English";
                cultureEnglish.Code = "En";
                cultureEnglish.CultureId = 1;
                cultureEnglish.Verified = true;

                Culture cultureEnglishNepali = new Culture();
                cultureEnglishNepali.Title = "अङ्रेजी";
                cultureEnglishNepali.Code = "En";
                cultureEnglishNepali.CultureId = 2;
                cultureEnglishNepali.Verified = true;
                cultureEnglishNepali.MultiLingualId = 1;


                Culture cultureEnglishHindi = new Culture();
                cultureEnglishHindi.Title = "अङ्रेज";
                cultureEnglishHindi.Code = "En";
                cultureEnglishHindi.CultureId = 3;
                cultureEnglishHindi.Verified = true;
                cultureEnglishHindi.MultiLingualId = 1;



                Culture cultureNepali = new Culture(); // Created ID 2
                cultureNepali.Title = "Nepali";
                cultureNepali.Code = "Np";
                cultureNepali.CultureId = 1;
                cultureNepali.Verified = true;


                Culture cultureNepaliNepali = new Culture();
                cultureNepaliNepali.Title = "नेपाली";
                cultureNepaliNepali.Code = "Np";
                cultureNepaliNepali.CultureId = 2;
                cultureNepaliNepali.Verified = true;
                cultureNepaliNepali.MultiLingualId = 2;

                Culture cultureNepaliHindi = new Culture();
                cultureNepaliHindi.Title = "नेपाली";
                cultureNepaliHindi.Code = "Np";
                cultureNepaliHindi.CultureId = 3;
                cultureNepaliHindi.Verified = true;
                cultureNepaliHindi.MultiLingualId = 2;

                Culture cultureHindi = new Culture();  // Created ID 3
                cultureHindi.Title = "Hindi";
                cultureHindi.Code = "Hi";
                cultureHindi.Verified = true;
                cultureHindi.CultureId = 1;

                Culture cultureHindiNepali = new Culture();
                cultureHindiNepali.Title = "हिन्दी";
                cultureHindiNepali.Code = "Hi";
                cultureHindiNepali.Verified = true;
                cultureHindiNepali.CultureId = 2;
                cultureHindiNepali.MultiLingualId = 3;

                Culture cultureHindiHindi = new Culture();
                cultureHindiHindi.Title = "हिन्दी";
                cultureHindiHindi.Code = "Hi";
                cultureHindiHindi.Verified = true;
                cultureHindiHindi.CultureId = 3;
                cultureHindiHindi.MultiLingualId = 3;


                // Culture cultureChinese = new Culture(); // Created ID 4
                // cultureChinese.Title = "Chinese";
                // cultureChinese.Code = "Cn";
                // cultureChinese.Verified = true;
                // cultureChinese.CultureId = 1;


                // Culture cultureChineseNepali = new Culture();
                // cultureChineseNepali.Title = "चाइनिज";
                // cultureChineseNepali.Code = "Cn";
                // cultureChineseNepali.Verified = true;
                // cultureChineseNepali.CultureId = 2;
                // cultureChineseNepali.MultiLingualId = 4;


                // Culture cultureChineseHindi = new Culture();
                // cultureChineseHindi.Title = "चाइनिज";
                // cultureChineseHindi.Code = "Cn";
                // cultureChineseHindi.Verified = true;
                // cultureChineseHindi.CultureId = 3;
                // cultureChineseHindi.MultiLingualId = 4;


                GenericResult _cultureResultEnglish = _cultureService.CreateCulture(_user.Id, cultureEnglish);
                GenericResult _cultureResultNepali = _cultureService.CreateCulture(_user.Id, cultureNepali);
                GenericResult _cultureResultHindi = _cultureService.CreateCulture(_user.Id, cultureHindi);
                // GenericResult _cultureResultChinese = _cultureService.CreateCulture(_user.Id, cultureChinese);

                GenericResult _cultureResultEnglishNepali = _cultureService.CreateCulture(_user.Id, cultureEnglishNepali);
                GenericResult _cultureResultHindiNepali = _cultureService.CreateCulture(_user.Id, cultureHindiNepali);
                // GenericResult _cultureResultChineseNepali = _cultureService.CreateCulture(_user.Id, cultureChineseNepali);
                GenericResult _cultureResultNepaliNepali = _cultureService.CreateCulture(_user.Id, cultureNepaliNepali);


                GenericResult _cultureResultEnglishHindi = _cultureService.CreateCulture(_user.Id, cultureEnglishHindi);
                GenericResult _cultureResultNepaliHindi = _cultureService.CreateCulture(_user.Id, cultureNepaliHindi);
                GenericResult _cultureResultHindiHindi = _cultureService.CreateCulture(_user.Id, cultureHindiHindi);
                //  GenericResult _cusltureResultChineseHindi = _cultureService.CreateCulture(_user.Id, cultureChineseHindi);



                Role roleAdmin = new Role();
                roleAdmin.Name = "Admin";
                roleAdmin.CultureId = 1;

                Role roleAdmin1 = new Role();
                roleAdmin1.Name = "Owner";
                roleAdmin1.CultureId = 1;

                Role roleAdmi2 = new Role();
                roleAdmi2.Name = "Editor";
                roleAdmi2.CultureId = 1;

                Role roleAdmi3 = new Role();
                roleAdmi3.Name = "Content Writer";
                roleAdmi3.CultureId = 1;

                GenericResult _adminRoleResult = _roleService.CreateRole(_user.Id, roleAdmin);
                GenericResult _adminRoleResult1 = _roleService.CreateRole(_user.Id, roleAdmin1);
                GenericResult _adminRoleResult2 = _roleService.CreateRole(_user.Id, roleAdmi2);
                GenericResult _adminRoleResult3 = _roleService.CreateRole(_user.Id, roleAdmi3);


                string[] componentFields = new string[] {
                  "HeaderText","SubHeaderText","FooterText","SubFooterText","ButtonText","ButtonUrl","Image","CopyrightNotice","ReadMore","VideoUrl","AudioUrl","EnterYourEmailAddress","Html"};
                int intComponentField = 0;

                List<ComponentField> mainComponentField = new List<ComponentField>();
                foreach (var fieldS in componentFields)
                {
                    intComponentField += intComponentField;
                    ComponentField cf = new ComponentField();
                    cf.FieldName = fieldS;
                    cf.CultureId = 1;
                    cf.CreatedDate = DateTime.Now;
                    cf.MultiLingualId = intComponentField;
                    cf.CreatedById = 1;
                    cf.DataType = DataType.String;
                    mainComponentField.Add(cf);
                }
                try
                {
                    _context.ComponentField.AddRange(mainComponentField);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                string[] themeComponent = new string[] {
                  "Silder",
                  "LiSideBar",
                  "Component1",
                  "LiWithThumbnail",
                  "TopIntroduction",                  
                  "Settings",
                  "OnConstructionSettings",
                  "Html",
                  };
                int intThemeComponentField = 0;
                List<ThemeComponent> mainThemeComponentField = new List<ThemeComponent>();
                foreach (var fieldS in themeComponent)
                {
                    intThemeComponentField += intThemeComponentField + 1;
                    ThemeComponent cf = new ThemeComponent();
                    cf.Name = fieldS;
                    cf.CultureId = 1;
                    cf.CreatedDate = DateTime.Now;
                    cf.MultiLingualId = intThemeComponentField;
                    cf.CreatedById = 1;
                    mainThemeComponentField.Add(cf);
                }
                try
                {
                    _context.ThemeComponents.AddRange(mainThemeComponentField);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                //Blog,MusicVideo,News,Movies,Projects,Videos,Page



                CultureViewModel cvm = new CultureViewModel();
                cvm.Id = 1;
                CultureViewModel cvm1 = new CultureViewModel();
                cvm1.Id = 2;


                WebsiteCreateViewModel websiteCommon = new WebsiteCreateViewModel();

                websiteCommon.Name = "commonWebsiteSettings";
                websiteCommon.Theme = "Mpurpose";

                _websiteService.CreateWebsite(_user.Id, websiteCommon);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //_context.SaveChanges();




            string[] supportTicketDepartMent = new string[] { "Support", "Account" };
            int intsupportTicketDepartMent = 0;
            foreach (var fieldS in supportTicketDepartMent)
            {
                SupportTicketDepartment std = new SupportTicketDepartment();
                intsupportTicketDepartMent += intsupportTicketDepartMent + 1;
                std.CultureId = 1;
                std.CreatedById = 1;
                std.Name = fieldS;
                std.CreatedDate = DateTime.Now;
                std.Verified = true;
                std.MultiLingualId = intsupportTicketDepartMent;
                try
                {
                    _context.SupportTicketDepartment.Add(std);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            intsupportTicketDepartMent = 0;
            ThemeCreateViewModel tcvm = new ThemeCreateViewModel();
            tcvm.Name = "MiniPort";
            tcvm.CultureId = 1;
            try
            {
                _themeService.AddOrEditTheme(1, tcvm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            ThemeCreateViewModel tcvm1 = new ThemeCreateViewModel();
            tcvm1.Name = "Mpurpose";
            tcvm1.CultureId = 1;
            try
            {
                _themeService.AddOrEditTheme(1, tcvm1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            UserCulture uc = new UserCulture();
            uc.CultureId = 1;
            uc.UserId = 1;

            UserCulture uc1 = new UserCulture();
            uc1.CultureId = 1;
            uc1.UserId = 2;
            GenericResult _result = new GenericResult()
            {
                Succeeded = true,
                Message = "Success : Seeded !!.",
            };


            return new OkObjectResult(_result);
            //return BadRequest(_result);    
            // LL:

            //           for(int x=1;x<=200000;x++){
            //                   Culture cultureChineseHind1i = new Culture();
            //                   cultureChineseHind1i.Title = x.ToString() + Guid.NewGuid().ToString();
            //                   cultureChineseHind1i.Code = 1.ToString();
            //                   cultureChineseHind1i.Verified = true;
            //                   cultureChineseHind1i.CultureId = 1;
            //                   _cultureService.CreateCulture(1,cultureChineseHind1i);
            //               }
            //               return new OkResult();
        }
        private void AddPage()
        {
            int intA = 0;
            //Blog,MusicVideo,News,Movies,Projects,Videos,Page
            string[] Pages = new string[] { "Index",
                                                "Construction",
                                                "Gallery Single",
                                                "Blog Single",
                                                "MusicVideo Single",
                                                "News Single",
                                                "Movies Single",
                                                "Projects Single",
                                                "Videos Single",
                                                "Page Single",
                                                "Blog List",
                                                "MusicVideo List",
                                                "News List",
                                                "Movies List",
                                                "Projects List",
                                                "Videos List",
                                                "Page List",
                                                "Blog Category List",
                                                "MusicVideo Category List",
                                                "News Category List",
                                                "Movies Category List",
                                                "Projects Category List",
                                                "Videos Category List",
                                                "Page Category List",
                                                "Blog Tag List",
                                                "MusicVideo Tag List",
                                                "News Tag List",
                                                "Movies Tag List",
                                                "Projects Tag List",
                                                "Videos Tag List",
                                                "Page Tag List",
            };
            try
            {
                foreach (var fieldS in Pages)
                {
                    intA += 1;
                    Page p = new Page();
                    p.Title = fieldS;
                    p.CultureId = 1;
                    p.MultiLingualId = intA;
                    p.CreatedDate = DateTime.Now;
                    p.CreatedById = 1;
                    p.Verified = true;
                    _context.Pages.Add(p);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void AddModules()
        {
            string[] moduleNameEnglish = new string[] {
                                "Blog","Blog Category","Blog Tags",
                                "MusicVideo","MusicVideo Category","MusicVideo Tags",
                                "News","News Category","News Tags",
                                "Movies","Movies Category","Movies Tags",
                                "Projects","Projects Category","Projects Tags",
                                "Videos","Videos Category","Videos Tags",
                                "Page","Page Category","Page Tags",
                                "Gallery","Top Introduction","Settings","Always Load"};
            List<Module> mainNodule = new List<Module>();
            int intmoduleNameEnglish = 0;
            try
            {
                foreach (var module in moduleNameEnglish)
                {
                    intmoduleNameEnglish += 1;
                    Module m = new Module();
                    m.CreatedById = 1;
                    m.CreatedDate = DateTime.Now;
                    m.Name = module;
                    m.CultureId = 1;
                    m.Verified = true;
                    m.Price = 0;
                    m.MultiLingualId = intmoduleNameEnglish;
                    m.Options = ModuleOptions.Popular;
                    if (module.Contains(" "))
                    {
                        string term = module.Substring(0, module.IndexOf(" "));
                        try
                        {
                            m.ParentModuleId = _context.Modules.Where(x => x.Name.ToLower() == term).SingleOrDefault().Id;
                        }
                        catch { }
                    }
                    _context.Modules.Add(m);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}