using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Community.Services;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Data.Infrastructure;
using Community.Model.Entities.Pages;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Community.Model.Entities.Global;
using Community.Model.Entities.Theme;
using Community.Model.Entities.Globalization;
namespace Community.Api.Controllers
{
    [Route("Api/Me")]
    [Produces("application/json")]
    [Authorize]
    public class MeController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private GlobalUser _global = new GlobalUser();
        private IWebsiteService _websiteService;
        private IUserService _userService;
        private IMapper Mapper { get; set; }
        private CommunityContext _context;
        private CommunityContext _context1;
        private ICultureService _cultureService;
        public MeController(ICultureService _cultureService, CommunityContext _context, CommunityContext _context1, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService _websiteService, IUserService _userService, IMapper Mapper)
        {
            this._cultureService = _cultureService;
            this.Mapper = Mapper;
            this._context = _context;
            this._context1 = _context1;
            this._userService = _userService;
            this._websiteService = _websiteService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<BlogController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        public class MeMainViewModel
        {
            public List<ModuleViewModel> Modules { get; set; }
            public List<WebsiteViewModel> Website
            { get; set; }
            public MeViewModel Me { get; set; }
        }
        public class MeViewModel
        {
            public string Image { get; set; }
            public string FullName { get; set; }
            public string UserName { get; set; }
            public string UserType { get; set; }
        }

        public class UserUpdateCultureViewModel
        {
            public List<CultureViewModel> Cultures { get; set; }
        }

        public class UserCultureViewModel
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public bool CurrentlyUsed { get; set; }
        }
        [HttpGet("Languages")]
        [Authorize]
        public List<UserCultureViewModel> getLanguages()
        {
            List<UserCultureViewModel> ucvm = new List<UserCultureViewModel>();
            var _result = _context.UserCulture.Include(x => x.Culture).Where(x => x.UserId == _global.UserIdFromJWT(User)).Select(x => x.Culture).ToList();
            var _allCultures = _cultureService.GetCulturesInCultureLanguage();
            foreach (var items in _allCultures)
            {
                bool used = false;
                foreach (var currentCulture in _result)
                {
                    if (items.Id == currentCulture.Id)
                    {
                        used = true;
                        break;
                    }
                }
                UserCultureViewModel u = new UserCultureViewModel();
                u.Id = items.Id;
                u.Title = items.Title;
                u.CurrentlyUsed = used;
                ucvm.Add(u);

            }
            return ucvm;
        }
        [HttpPost("UpdateLanguage")]
        [Authorize]
        public IActionResult UpdateCulture([FromBody] UserUpdateCultureViewModel cvm)
        {
            GenericResult _result = new GenericResult();
            var _currentRegisteredCultures = _context.UserCulture.Where(x => x.UserId == _global.UserIdFromJWT(User)).ToList();
            _context.UserCulture.RemoveRange(_currentRegisteredCultures);
            _context.SaveChanges();
            List<UserCulture> mainWc = new List<UserCulture>();
            foreach (var culture in cvm.Cultures)
            {
                UserCulture wc = new UserCulture();
                wc.CultureId = culture.Id;
                wc.UserId = _global.UserIdFromJWT(User);
                mainWc.Add(wc);
            }
            try
            {
                _context.UserCulture.AddRange(mainWc);
                _context.SaveChanges();
                _result = Global.Success("User Languages Updated");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }
        [HttpGet]
        public MeMainViewModel Get()
        {
            MeMainViewModel m = new MeMainViewModel();
            MeViewModel me = new MeViewModel();
            ModuleViewModel module = new ModuleViewModel();
            List<ModuleViewModel> Modules = new List<ModuleViewModel>();
            var _user = _userService.GetSingleById(_global.UserIdFromJWT(User));
            m.Website = new List<WebsiteViewModel>(_websiteService.GetWebsites(_global.UserIdFromJWT(User), 0, 20));
            me.FullName = _user.Fullname;
            me.Image = _user.Image;
            me.UserName = _user.Username;
            me.UserType = _user.UserType.ToString();
            m.Me = me;
            return m;
        }
        [HttpGet("Website/{WebsiteId}/ThemeComponents/Culture/{CultureId}")]
        public List<WebsiteComponent> GetThemeComponent(int websiteId)
        {
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), websiteId, (new string[] { "Admin", "Owner", "Editor" }))) { return new List<WebsiteComponent>(); }
            var _result = _context.WebsiteComponent.Include(x => x.ThemeComponent).OrderBy(x => x.Name).Where(x => x.WebsiteId == websiteId);
            var themeComponentField = _context.ThemeComponentFields.Select(x => x.ThemeComponent.Name).Distinct();
            _result = from item in _result
                      where themeComponentField.Contains(item.ThemeComponent.Name)
                      select item;

            return _result.ToList();
        }

        [HttpGet("ThemeComponents/{ComponentId}/Culture/{CultureId}")]
        public List<ThemeComponentValueSingleViewModel> GetThemeComponentFildsAndValues(int ComponentId, int CultureId)
        {
            var _componentResult = _context.WebsiteComponent.Include(x => x.ThemeComponent).Where(x => x.Id == ComponentId).SingleOrDefault();
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), _componentResult.WebsiteId, (new string[] { "Admin", "Owner", "Editor" }))) { return new List<ThemeComponentValueSingleViewModel>(); }
            string ComponentName = _componentResult.ThemeComponent.Name.ToLower();
            int ComponentID = _componentResult.Id;
            var _result = _context.ThemeComponentFields
                        .Include(x => x.ThemeComponent)
                        .Include(x => x.ComponentField)
                        .OrderBy(x => x.ComponentFieldId)
                        .Where(x => x.ThemeComponent.Name.ToLower() == ComponentName).ToList();
            List<ThemeComponentValueSingleViewModel> mainTc = new List<ThemeComponentValueSingleViewModel>();
            foreach (var items in _result)
            {
                ThemeComponentValueSingleViewModel tc = new ThemeComponentValueSingleViewModel();
                tc.FieldName = items.ComponentField.FieldName;
                try
                {
                    tc.Value = _context1.WebsiteComponentFieldValue
                        .Include(x => x.ComponentField)
                        .Include(x => x.WebsiteComponent)
                        .Where(x => x.ComponentField.FieldName == items.ComponentField.FieldName)
                        .Where(x => x.WebsiteComponentId == ComponentId)
                        .Where(x => x.CultureId == CultureId)
                        .SingleOrDefault().Value;
                }
                catch { }
                mainTc.Add(tc);
            }
            return mainTc;
        }

        [HttpPost("ThemeComponents/{ComponentId}")]
        [Validate]
        public GenericResult SaveThemeComponentFildsAndValues([FromBody]ThemeComponentValueCreateViewModel tcvcm, int ComponentId)
        {

            if (!_userService.IsInRole(_global.UserIdFromJWT(User), tcvcm.WebsiteId, (new string[] { "Admin", "Owner", "Editor" }))) return Global.AccessDenied();
            foreach (var items in tcvcm.Settings)
            {
                int intComponentFieldId = _context.ComponentField.Where(x => x.FieldName.ToLower() == items.FieldName).SingleOrDefault().Id;
                var OldComponentFieldValue = _context.WebsiteComponentFieldValue
                            .Where(x => x.ComponentFieldId == intComponentFieldId)
                            .Where(x => x.WebsiteComponentId == ComponentId)
                            .Where(x => x.CultureId == tcvcm.CultureId).SingleOrDefault();

                if ((OldComponentFieldValue == null) && (items.Value != null) && (items.Value != ""))
                {
                    WebsiteComponentFieldValue wcfv = new WebsiteComponentFieldValue();
                    wcfv.ComponentFieldId = intComponentFieldId;
                    wcfv.CultureId = tcvcm.CultureId;
                    wcfv.WebsiteComponentId = ComponentId;
                    wcfv.Value = items.Value;
                    _context.WebsiteComponentFieldValue.Add(wcfv);
                    _context.SaveChanges();
                }
                else
                {
                    if (!(OldComponentFieldValue == null))
                    {
                        OldComponentFieldValue.Value = items.Value;
                        _context.WebsiteComponentFieldValue.Update(OldComponentFieldValue);
                        _context.SaveChanges();
                    }
                }
            }
            GenericResult _result = new GenericResult();
            _result.Succeeded = true;
            _result.Message = "Received";
            _result.Data = JsonConvert.SerializeObject(tcvcm);//.ToString();
            return _result;
        }
        [HttpPost("Website/{WebsiteId}/Page/{PageId}")]
        public IActionResult SaveDetails([FromBody] PageComponentCreateViewModel pccvm, int websiteId, int PageId)
        {
            GenericResult gr = new GenericResult();
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), websiteId, (new string[] { "Admin", "Owner", "Editor" }))) { gr = Global.AccessDenied(); return BadRequest(gr); }
            try
            {
                var _removeResult = _context.WebsitePage.Where(x => x.PageId == PageId)
                                   .Where(x => x.WebsiteId == websiteId);
                _context.WebsitePage.RemoveRange(_removeResult);
                _context.SaveChanges();
                foreach (var items in pccvm.Body)
                {
                    WebsitePage wp = new WebsitePage();
                    wp.WebsiteId = websiteId;
                    wp.WebsiteComponentId = items.Id;
                    wp.Order = items.Order;
                    wp.PageId = PageId;
                    wp.Count = items.Count;
                    wp.Location = Location.Body;
                    _context.WebsitePage.Add(wp);
                }
                foreach (var items in pccvm.BodyRight)
                {
                    WebsitePage wp = new WebsitePage();
                    wp.WebsiteId = websiteId;
                    wp.WebsiteComponentId = items.Id;
                    wp.Order = items.Order;
                    wp.PageId = PageId;
                    wp.Count = items.Count;
                    wp.Location = Location.BodyRightSidePanel;
                    _context.WebsitePage.Add(wp);
                }
                foreach (var items in pccvm.BodyLeft)
                {
                    WebsitePage wp = new WebsitePage();
                    wp.WebsiteId = websiteId;
                    wp.WebsiteComponentId = items.Id;
                    wp.Order = items.Order;
                    wp.PageId = PageId;
                    wp.Count = items.Count;
                    wp.Location = Location.BodyLeftSidePanel;
                    _context.WebsitePage.Add(wp);
                }

                _context.SaveChanges();
                gr = Global.Success("Succeeded to change the Components on Page");
            }
            catch (Exception ex)
            {
                Global.Error(ex);
            }
            return Global.SendAsItIs(gr);
        }
        [HttpGet("Website/{WebsiteId}/Page/{PageId}")]
        public PageComponentViewModel GetDetails(int websiteId, int PageId)
        {
            PageComponentViewModel pcvm = new PageComponentViewModel();
            pcvm.Components = new List<WebsiteComponentViewModel>();
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), websiteId, (new string[] { "Admin", "Owner", "Editor" }))) { return pcvm; }
            var _currentPage = _context.Pages.Where(x => x.Id == PageId).SingleOrDefault();
            LayoutType layoutType = new LayoutType();
            ICollection<WebsiteComponentViewModel> Components = new List<WebsiteComponentViewModel>();
            var _componentsResult = _context.WebsiteComponent.Include(x => x.ThemeComponent).Include(x => x.Module).Where(x => x.WebsiteId == websiteId).ToList();
            var _WebsiteModules = _context.WebsiteModule.Include(x => x.Module).Where(x => x.WebsiteId == websiteId).ToList().Select(x => x.Module);
            foreach (var items in _componentsResult)
            {
                foreach (var selectedModules in _WebsiteModules)
                {
                    if (items.Module.Name.ToLower().Contains(selectedModules.Name.ToLower()))
                    {
                        WebsiteComponentViewModel wcvm = new WebsiteComponentViewModel();
                        wcvm.Id = items.Id;
                        wcvm.Name = items.Name;
                        wcvm.ThemeComponentName = items.ThemeComponent.Name;
                        wcvm.Type = Community.Model.Entities.ViewModels.ComponentType.Main.ToString();
                        pcvm.Components.Add(wcvm);
                    }
                }
            }
            foreach (var items in pcvm.Components)
            {
                if (items.ThemeComponentName.ToString().ToLower().Contains("sidebar"))
                {
                    items.Type = Community.Model.Entities.ViewModels.ComponentType.Sidebar.ToString();
                }
                else
                {
                    items.Type = Community.Model.Entities.ViewModels.ComponentType.Main.ToString();
                }
            }
            List<UserComponentViewModel> BodyParts = new List<UserComponentViewModel>();
            var _specificComponents = _context.WebsitePage.Include(x => x.WebsiteComponent)
                    //.Where(x=>x.Location == Location.Body)
                    .Where(x => x.WebsiteId == websiteId)
                    .Where(x => x.Page.Id == PageId).ToList();
            foreach (var items in _specificComponents.Where(x => x.Location == Location.Body))
            {
                UserComponentViewModel ucvm = new UserComponentViewModel();
                ucvm.Id = items.WebsiteComponentId;
                ucvm.Name = items.WebsiteComponent.Name;
                ucvm.Hash = Guid.NewGuid();
                ucvm.Count = items.Count;
                ucvm.Order = items.Order;
                BodyParts.Add(ucvm);
            }
            pcvm.Body = BodyParts;
            List<UserComponentViewModel> leftBodyParts = new List<UserComponentViewModel>();
            foreach (var items in _specificComponents.Where(x => x.Location == Location.BodyLeftSidePanel))
            {
                UserComponentViewModel ucvm = new UserComponentViewModel();
                ucvm.Id = items.WebsiteComponentId;
                ucvm.Name = items.WebsiteComponent.Name;
                ucvm.Order = items.Order;
                ucvm.Count = items.Count;
                ucvm.Hash = Guid.NewGuid();
                leftBodyParts.Add(ucvm);
            }
            pcvm.BodyLeft = leftBodyParts;
            List<UserComponentViewModel> rightParts = new List<UserComponentViewModel>();
            foreach (var items in _specificComponents.Where(x => x.Location == Location.BodyRightSidePanel))
            {
                UserComponentViewModel ucvm = new UserComponentViewModel();
                ucvm.Id = items.WebsiteComponentId;
                ucvm.Name = items.WebsiteComponent.Name;
                ucvm.Order = items.Order;
                ucvm.Count = items.Count;
                ucvm.Hash = Guid.NewGuid();
                rightParts.Add(ucvm);
            }
            pcvm.BodyRight = rightParts;
            List<UserComponentViewModel> footerPart = new List<UserComponentViewModel>();
            foreach (var items in _specificComponents.Where(x => x.Location == Location.Footer))
            {
                UserComponentViewModel ucvm = new UserComponentViewModel();
                ucvm.Id = items.WebsiteComponentId;
                ucvm.Name = items.WebsiteComponent.Name;
                ucvm.Order = items.Order;
                ucvm.Count = items.Count;
                ucvm.Hash = Guid.NewGuid();
                footerPart.Add(ucvm);
            }
            pcvm.Footer = footerPart;
            string _stringLayoutType = _context.WebsitePageLayout.Where(x => x.PageId == _currentPage.Id).Where(x => x.WebsiteId == websiteId).Select(x => x.Layout.ToString()).SingleOrDefault();
            if (_stringLayoutType.ToLower().Contains("middle")) layoutType.Body = true;
            if (_stringLayoutType.ToLower().Contains("left")) layoutType.BodyLeft = true;
            if (_stringLayoutType.ToLower().Contains("right")) layoutType.BodyRight = true;
            layoutType.Footer = true;
            pcvm.LayoutType = layoutType;
            return pcvm;
        }
    }
}