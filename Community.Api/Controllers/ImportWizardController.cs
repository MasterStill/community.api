using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/Import")]
    [Produces("application/json")]
    public class ImportWizardController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebsiteService _websiteService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper _mapper;
        private IMemoryCache _memoryCache;

        public ImportWizardController(IMemoryCache _memoryCache, IMapper _mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService websiteservice, IUserService _userService)//,UserManager<User> _userManager)
        {
            this._memoryCache = _memoryCache;
            this._mapper = _mapper;
            this._context = _context;
            _websiteService = websiteservice;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        class Post
        {
            public string feed { get; set; }            
        }
                
        [HttpGet("{BlogSpotURL}")]
        public string ImportFromBlogSpot(string BlogSpotURL)
        {

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://friendly-encounters.blogspot.co.uk/feeds/posts/default?alt=json");
                    var response = client.GetAsync("");
                    //response.EnsureSuccessStatusCode(); // Throw in not success
                    
                    var stringResponse = response.Result.Content.ReadAsStringAsync();
                    var posts = JsonConvert.DeserializeObject<IEnumerable<Post>>(stringResponse.Result);

                    Console.WriteLine($"Got {posts.Count()} posts");
                    Console.WriteLine($"First post is {JsonConvert.SerializeObject(posts.First())}");
                    var aa = ($"Got {posts.Count()} posts");
                    return aa;
                }
                catch (HttpRequestException e)
                {
                    var aa = ($"Request exception: {e.Message}");
                    return aa;
                }
            }
        }

    }
}