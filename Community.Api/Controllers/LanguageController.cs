using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Services;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Filters;
namespace Community.Api.Controllers
{
    [Route("api/Language")]
    [Authorize]
    public class LanguageController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private GlobalUser _global = new GlobalUser();
        private IGlobalizationService _globalizationService;
        public LanguageController(ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IGlobalizationService _globalizationService)
        {
            this._globalizationService = _globalizationService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<BlogController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [AllowAnonymous]
        [HttpGet("WebsiteCreate/Culture/{CultureId}")]
        public IActionResult Get(int CultureId)
        {
            return Global.SendAsItIs("");
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var controller = context.Controller as Controller;
            var modelState = context.ModelState;
            if (!modelState.IsValid)
            {
                context.Result = new Microsoft.AspNetCore.Mvc.StatusCodeResult(422);
            }
        }
    }
}