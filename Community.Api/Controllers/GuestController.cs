using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
namespace Community.Api.Controllers
{
    [Route("Api/Guest")]
    [Produces("application/json")]
    public class GuestController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebsiteService _websiteService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper _mapper;
        private IMemoryCache _memoryCache;
        private ICultureService _cultureService;
        private IModuleService _moduleService;
        private IGlobalizationService _globalizationService;
        public GuestController(IGlobalizationService _globalizationService,ICultureService _cultureService, IModuleService _moduleService, IMemoryCache _memoryCache, IMapper _mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService websiteservice, IUserService _userService)//,UserManager<User> _userManager)
        {
            this._globalizationService = _globalizationService;
            this._cultureService = _cultureService;
            this._moduleService = _moduleService;
            this._memoryCache = _memoryCache;
            this._mapper = _mapper;
            this._context = _context;
            _websiteService = websiteservice;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
      
        [AllowAnonymous]
        [HttpGet("getResources/{CultureId}")]
        public string CheckIfAvailable(int CultureId)
        {
            var _result = _globalizationService.ForNewUseRegistration(CultureId).Distinct().Where(x => x.Step == x.Step);                                   
            int totalSteps = _result.Count();
            string StepstoSend = string.Empty;
            string ModuleTosend =string.Empty; 
            string ThemesToSend =string.Empty; 
            StepstoSend = "{ "  + "\"Steps\" :[{";
            for(int i=0;i<totalSteps;i++){                
                if (i < totalSteps && i != 0) StepstoSend += ","; 
                StepstoSend +=  "\"" + i + "\" :" + JsonConvert.SerializeObject(_result.Where(x=>x.Step == i).Select(x=>x.Values)).Replace("[[","[").Replace("]]","]");
            }
             StepstoSend +=  "}],";
            ModuleTosend = "\"Modules\" :" + JsonConvert.SerializeObject(_moduleService.WebsiteModules(0,CultureId)).Replace("[[","[").Replace("]]","]");  
            ModuleTosend +=  ",";            
            ThemesToSend = "\"Themes\" :" + JsonConvert.SerializeObject(_context.Themes.ToList().Select(x=> new {x.Name,x.Image,x.URL})).Replace("[[","[").Replace("]]","]");
            ThemesToSend +=  "}";            
            return (StepstoSend + ModuleTosend + ThemesToSend) ;
        }
    }
}