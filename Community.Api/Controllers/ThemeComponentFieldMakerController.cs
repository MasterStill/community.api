using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Community.Model.Entities.Theme;
namespace Community.Api.Controllers
{
    [AllowAnonymous]
    //[Authorize]
    [Route("tcfm")]
    public class ThemeComponentFieldMakerController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebsiteService _websiteService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper _mapper;
        public ThemeComponentFieldMakerController(IMapper _mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService websiteservice, IUserService _userService)//,UserManager<User> _userManager)
        {
            this._mapper = _mapper;
            this._context = _context;
            _websiteService = websiteservice;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpPost("GetFields/{ComponentId}")]
        public IActionResult Postitman([FromForm]IFormCollection c, int ComponentId)
        {
            var _resultToRemove = _context.ThemeComponentFields.Where(x => x.ThemeComponentId == ComponentId);
            _context.ThemeComponentFields.RemoveRange(_resultToRemove);
            _context.SaveChanges();
            foreach (var items in c)
            {
                ThemeComponentFields tcf = new ThemeComponentFields();
                tcf.ThemeComponentId = ComponentId;
                tcf.ComponentFieldId = Int32.Parse(items.Value);
                _context.ThemeComponentFields.Add(tcf);
            }
            try
            {
                _context.SaveChanges();
            }
            catch
            {
                Console.WriteLine("Message");
            }
            return View("Getitman/ComponentId");
        }

        [HttpGet("GetFields/{ComponentId}")]

        public IActionResult Getitman(int ComponentId)
        {
            var _ThemeComponents = _context.ThemeComponents.Where(x => x.Delflag == false).ToList();
            var _ThemeComponentField = _context.ComponentField.ToList();
            TcViewModel mainViewModel = new TcViewModel();
            List<ThemeComponenttcfmViewModel> Tc = new List<ThemeComponenttcfmViewModel>();
            List<ThemeComponentFieldtcfmViewModel> Tcf = new List<ThemeComponentFieldtcfmViewModel>();
            foreach (var items in _ThemeComponents)
            {
                ThemeComponenttcfmViewModel tc = new ThemeComponenttcfmViewModel();
                tc.Id = items.Id;
                tc.Title = items.Name;
                Tc.Add(tc);
            }

            var _selectedThemeComponent = _context.ThemeComponentFields.Include(x => x.ComponentField).Where(x => x.ThemeComponentId == ComponentId).ToList().Select(x => x.ComponentField);


            foreach (var field in _ThemeComponentField)
            {
                bool isSelected = false;
                foreach (var selectedField in _selectedThemeComponent)
                {
                    if (selectedField.FieldName == field.FieldName)
                    {
                        isSelected = true;
                        break;
                    }
                }
                ThemeComponentFieldtcfmViewModel tcf = new ThemeComponentFieldtcfmViewModel();
                tcf.Selected = isSelected;
                tcf.Title = field.FieldName;
                tcf.Id = field.Id;
                Tcf.Add(tcf);
            }

            mainViewModel.ThemeComponent = Tc;
            mainViewModel.ThemeComponentField = Tcf;
            return View(mainViewModel);
            // var _result = new GenericResult();
            // if (_result.Succeeded) return new OkObjectResult(_result);
            // return BadRequest(_result);            
        }
    }
}