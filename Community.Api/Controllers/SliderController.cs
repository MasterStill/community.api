using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using AutoMapper;
using Community.Model.Entities.Sliders;
namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/Slider")]
    [Produces("application/json")]
    public class SliderController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper _mapper;
        public SliderController(IMapper _mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IUserService _userService)
        {
            this._mapper = _mapper;
            this._context = _context;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpGet("Website/{WebsiteId}/Culture/{CultureId}")] // Get Websites for User
        public IEnumerable<SliderViewModel> Get(int WebsiteId, int CultureId)
        {
            var _result = _context.Slider.Where(x => x.WebsiteId == WebsiteId).Where(x => x.CultureId == CultureId).Where(x => x.Delflag == false).ToList();
            return _mapper.Map<List<SliderViewModel>>(_result);
        }
        [HttpDelete("{SliderId}")]
        public GenericResult Delete(int SliderId)
        {
            Slider _slidertoDelete = _context.Slider.Single(x => x.Id == SliderId);
            if (_userService.IsInRole(_global.UserIdFromJWT(User), _slidertoDelete.WebsiteId, (new string[] { "Admin", "Editor", "Owner" })))
                try
                {
                    _slidertoDelete.Delflag = true;
                    _context.Update(_slidertoDelete);
                    _context.SaveChanges();
                    return Global.Success("Success : Slider Deleted.");
                }
                catch (Exception ex)
                {
                    return Global.Error(ex,"Error : Slider Deletion Failed." );
                }
            return Global.AccessDenied();            
        }
        [HttpGet("Categories/Website/{WebsiteId}/Culture/{CultureId}")]
        public IEnumerable<SliderCategoryViewModel> GetCategories(int WebsiteId, int CultureId)
        {
            var _result = _context.SliderCategory.Where(x => x.WebsiteId == WebsiteId).ToList();
            return _mapper.Map<List<SliderCategoryViewModel>>(_result);
        }
        [HttpPost("Categories")]
        public IActionResult SliderCategoryCreate([FromBody] SliderCategoryCreateViewModel sliderCreateViewModel)
        {
            GenericResult _result = new GenericResult();
            SliderCategory slider = new SliderCategory();
            if (sliderCreateViewModel.Id > 0)
            {
                try
                {
                    SliderCategory _oldSlider = new SliderCategory();
                    _oldSlider = _context.SliderCategory.Single(x => x.Id == sliderCreateViewModel.Id);
                    slider = _mapper.Map<SliderCategoryCreateViewModel, SliderCategory>(sliderCreateViewModel, _oldSlider);
                    _context.SliderCategory.Update(slider);
                    _context.SaveChanges();
                   Global.Success( "Slider Updated");
                }
                catch (Exception ex)
                {
                   _result =Global.Error(ex,"Slider Failed to update" + ex.Message);
                }
            }
            else
            {
                slider = _mapper.Map<SliderCategory>(sliderCreateViewModel);
                try
                {
                    slider.CreatedById = _global.UserIdFromJWT(User);
                    slider.CreatedDate = DateTime.Now;
                    _context.SliderCategory.Add(slider);
                    _context.SaveChanges();
                    _result = Global.Success("Slider Created");
                    _result.Data = JsonConvert.SerializeObject(slider);
                }
                catch (Exception ex)
                {
                    _result = Global.Error(ex,"Slider Creation Failed" + ex.Message);
                }
            }
            return    Global.SendAsItIs(_result);
        }
        [HttpPost]
        [Validate]
        public IActionResult Get([FromBody] SliderCreateViewModel sliderCreateViewModel)
        {
            GenericResult _result = new GenericResult();
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), sliderCreateViewModel.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }))) return BadRequest(Global.AccessDenied());
            Slider slider = new Slider();
            if (sliderCreateViewModel.Id > 0)
            {
                try
                {
                    Slider _oldSlider = new Slider();
                    _oldSlider = _context.Slider.Single(x => x.Id == sliderCreateViewModel.Id);
                    slider = _mapper.Map<SliderCreateViewModel, Slider>(sliderCreateViewModel, _oldSlider);
                    _context.Slider.Update(slider);
                    _context.SaveChanges();
                    _result = Global.Success("Slider Updated");
                }
                catch (Exception ex)
                {
                    _result = Global.Error(ex,"Slider Failed To Update" + ex.Message);
                }
            }
            else
            {
                slider = _mapper.Map<Slider>(sliderCreateViewModel);
                try
                {
                    slider.CreatedById = _global.UserIdFromJWT(User);
                    slider.CreatedDate = DateTime.Now;
                    _context.Slider.Add(slider);
                    _context.SaveChanges();
                   _result = Global.Success("Slider Created",slider.Id.ToString());                    
                }
                catch (Exception ex)
                {
                    _result = Global.Error(ex,"Slider Creation Failed");
                }
            }
           return Global.SendAsItIs(_result);
        }
    }
}