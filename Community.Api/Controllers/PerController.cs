using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using Community.Model.Entities.Blog;
using Microsoft.EntityFrameworkCore;

namespace Community.Api.Controllers
{
    [Authorize]
    public class PerController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebsiteService _websiteService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper _mapper;
        private IMemoryCache _memoryCache;

        public PerController(IMemoryCache _memoryCache, IMapper _mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService websiteservice, IUserService _userService)//,UserManager<User> _userManager)
        {
            this._memoryCache = _memoryCache;
            this._mapper = _mapper;
            this._context = _context;
            _websiteService = websiteservice;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpGet("GetPerformance")]
        public IActionResult GetPerformance([FromForm]DateTime StartDate, DateTime EndDate, int UserId = 0)
        {
            StartDate = DateTime.Today;
            List<BlogPost> bp = new List<BlogPost>();
            bp = _context.BlogPosts
                    .Include(x => x.CreatedBy)
                    .Include(x => x.Website)
                    .Include(x => x.Category)
                    .Include(x => x.Culture)
                    .Where(x => x.Delflag == false)
                    .ToList();
            if (StartDate.Year > 2010)
            {
                bp = bp.Where(x => x.CreatedDate >= StartDate).ToList();
            }
            if (EndDate.Year > 2010)
            {
                bp.Where(x => x.CreatedDate <= EndDate).ToList();
            }
            if (UserId > 0)
            {
                bp.Where(x => x.CreatedById == UserId);
            }
            GenericResult _result = new GenericResult();
            _result.Succeeded = true;
            _result.Data = JsonConvert.SerializeObject(bp.Select(x => x.Url));
            ViewBag.Data = _result.Data;
            return View("GetPerformance", bp);
        }
    }
}