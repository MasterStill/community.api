using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Model.Entities.Global;
using Community.Data.Infrastructure;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/Themes")]
    [Produces("application/json")]
    public class ThemeController : Controller
    {
        
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IThemeService _themeService;
        //private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private IAuditService _auditService;
        private CommunityContext _context;
        private IHostingEnvironment _hostingEnviroment;
        private IFileAndDirectoryService _fdService;
        public ThemeController(IFileAndDirectoryService _fdService, IHostingEnvironment _hostingEnviroment, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IThemeService _themeService, IAuditService _auditService)
        {
            this._fdService = _fdService;
            this._hostingEnviroment = _hostingEnviroment;
            this._context = _context;
            this._auditService = _auditService;
            this._themeService = _themeService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpGet("Culture/{CultureId}/Website/{WebsiteId}/{page?}")] // Get Themes
        [AllowAnonymous]
        public IEnumerable<ThemeViewModel> Get(int CultureId, int WebsiteId, int? page = 0)
        {
            return _themeService.getThemes(page.Value, Global.Pagesize(), CultureId, WebsiteId);

        }

        [HttpPost("Create")]


        public IActionResult CreateTheme([FromBody]ThemeCreateViewModel tcvm,IFormCollection fc)
        {            
            var _result = _themeService.ThemeAddingPoriton(tcvm,_hostingEnviroment,_global.UserIdFromJWT(User),fc);
            if (_result.Succeeded) return new OkObjectResult(_result);
            return BadRequest(_result);

        }
        [HttpPost("website/{WebsiteId}")]
        public IActionResult ChangeWebsiteTheme([FromBody] WebsiteThemeChangeViewModel wTheme, int websiteId)
        {
            var _websiteThemeToChange = _context.Websites.Where(x => x.Id == websiteId).SingleOrDefault();
            var _result = new GenericResult();
            try
            {
                _websiteThemeToChange.Theme = wTheme.Name;
                _context.Websites.Update(_websiteThemeToChange);
                _context.SaveChanges();
                _result =Global.Success("Success : Theme Changed.");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
             return Global.SendAsItIs(_result);
        }
    }
}