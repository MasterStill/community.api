using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using AutoMapper;
using Community.Model.Entities.Pages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/Websites")]
    [Produces("application/json")]
    public class WebsiteController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebsiteService _websiteService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper _mapper;
        private IMemoryCache _memoryCache;

        public WebsiteController(IMemoryCache _memoryCache, IMapper _mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService websiteservice, IUserService _userService)//,UserManager<User> _userManager)
        {
            this._memoryCache = _memoryCache;
            this._mapper = _mapper;
            this._context = _context;
            _websiteService = websiteservice;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        public class CultureChangeViewModel
        {
            [Required]
            public int WebsiteId { get; set; }
            [Required]
            public int DefaultCultureId { get; set; }
        }


        [AllowAnonymous]
        [HttpGet("IsAvailable/{WebsiteName}")]
        public IActionResult CheckIfAvailable(string WebsiteName)
        {
            //return new OkObjectResult("{\"Status\":" + _websiteService.checkIfAvailable(WebsiteName).ToString().ToLower() + "}");
            return new OkObjectResult(_websiteService.checkIfAvailable(WebsiteName));
        }
        [HttpGet("{WebsiteId}/Module")]
        public List<string> GetWebsiteModule(int websiteId)
        {
            var _result = _websiteService.GetWebsiteModule(1, websiteId);
            List<string> testString = new List<string>();
            foreach (var items in _result)
            {
                if (items != null)
                    testString.Add(items.MenuJson);
            }
            return testString;
        }

        [HttpGet("Search/{SearchTerm}")]
        [AllowAnonymous]
        public IEnumerable<WebsiteViewModel> SearchWebsite(string SearchTerm)
        {
            return _websiteService.SearchForWebsite(SearchTerm, 0, 50);
        }
        [HttpPost("{WebsiteId}/DefaultCulture")]
        [Authorize]
        [Validate]
        public IActionResult ChangeWebsiteCulture([FromBody]CultureChangeViewModel cvm, int WebsiteId)
        {
            GenericResult _result = new GenericResult();
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), WebsiteId, (new string[] { "Admin", "Owner" }))) return new BadRequestObjectResult(Global.AccessDenied());
            var _website = _context.Websites.Where(x => x.Id == WebsiteId).SingleOrDefault();
            _website.DefaultCultureId = cvm.DefaultCultureId;
            try
            {
                _context.Websites.Update(_website);
                _context.SaveChanges();
                _result = Global.Success("Default Culture Changed");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            if (_result.Succeeded) return new OkObjectResult(_result);
            return BadRequest(_result);
        }

        [HttpGet("{page?}")]
        public IEnumerable<WebsiteViewModel> Get(int? page = 0)
        {
            return _websiteService.GetWebsites(_global.UserIdFromJWT(User), page.Value, Global.Pagesize());
        }
        public class DefaultWebsiteCultureViewModel
        {
            public string Culture { get; set; }
        }
        [HttpGet("DefaultCulture/{websiteId}")]
        public DefaultWebsiteCultureViewModel GetCultures(int websiteId)
        {
            DefaultWebsiteCultureViewModel dvm = new DefaultWebsiteCultureViewModel();
            dvm.Culture = _context.Websites.Where(x => x.Id == websiteId).SingleOrDefault().DefaultCultureId.ToString();
            return dvm;
        }

        [HttpGet("{websiteId}/Culture/{cultureId}")]
        public List<CultureViewModel> GetCultures(int websiteId, int cultureId)
        {
            return _websiteService.GetCultures(websiteId, cultureId);
        }
        [HttpGet("{websiteId}/Menu/{cultureId}")]
        public List<MenuViewModel> GetMenus(int websiteId, int cultureId)
        {
            return _websiteService.GetMenu(websiteId, cultureId);
        }
        [HttpPost]
        [Validate]
        public IActionResult Get([FromBody] WebsiteCreateViewModel website)
        {
            var _result = _websiteService.CreateWebsite(_global.UserIdFromJWT(User), website);
            return Global.SendAsItIs(_result);
        }
        public class WebsitePageLayoutDisplayViewModel
        {
            public List<string> Layouts { get; set; }
            public List<WebsitePageLayoutViewModel> Pages { get; set; }
        }
        public class WebsitePageLayoutViewModel
        {
            public int Id { get; set; }
            public string Layout { get; set; }
            public int PageId { get; set; }
            public string Name { get; set; }
            public string Summary { get; set; }
        }
        [HttpGet("Page/PageLayouts/Website/{WebsiteId}/Culture/{CultureId}")]
        public IActionResult GetWebsitePageLayout(int WebsiteId)
        {

            if (!_userService.IsInRole(_global.UserIdFromJWT(User), WebsiteId, (new string[] { "Admin", "Owner", "Editor" }))) return new BadRequestObjectResult(Global.AccessDenied());
            List<WebsitePageLayoutViewModel> mainWpvm = new List<WebsitePageLayoutViewModel>();
            List<string> Layouts = new List<string>();
            Layouts.Add(SinglePageLayout.LeftMiddle.ToString());
            Layouts.Add(SinglePageLayout.Middle.ToString());
            Layouts.Add(SinglePageLayout.MiddleRight.ToString());
            var _result = _context.WebsitePageLayout
                          .Include(x => x.Page)
                          .Where(x => x.WebsiteId == WebsiteId);
            foreach (var items in _result)
            {
                WebsitePageLayoutViewModel wpvm = new WebsitePageLayoutViewModel();
                wpvm.Id = items.Id;
                wpvm.Layout = items.Layout.ToString();
                wpvm.PageId = items.PageId;
                wpvm.Name = items.Page.Title;
                wpvm.Summary = items.Page.Summary;
                mainWpvm.Add(wpvm);
            }

            WebsitePageLayoutDisplayViewModel wpldvm = new WebsitePageLayoutDisplayViewModel();
            wpldvm.Pages = mainWpvm;
            wpldvm.Layouts = Layouts;
            //return wpldvm;

            return new OkObjectResult(wpldvm);


        }

        [HttpPost("Page/{PageLayoutId}")]
        [Validate]
        public IActionResult ChangeWebsitePageLayout([FromBody] WebsitePageLayoutCreateViewModel websitePageLayout, int PageLayoutId)
        {
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), websitePageLayout.WebsiteId, (new string[] { "Admin", "Owner" }))) { return new BadRequestObjectResult(Global.AccessDenied()); }
            WebsitePageLayout wpl = _mapper.Map<WebsitePageLayout>(websitePageLayout);
            wpl.Id = PageLayoutId;
            GenericResult _result = new GenericResult();
            try
            {
                if (PageLayoutId == 0)
                {
                    _context.WebsitePageLayout.Add(wpl);
                }
                else
                {
                    var pageToBeUpdated = _context.WebsitePageLayout.SingleOrDefault(x => x.Id == PageLayoutId);
                    pageToBeUpdated.Layout = websitePageLayout.Layout;
                    pageToBeUpdated.PageId = websitePageLayout.PageId;
                    pageToBeUpdated.WebsiteId = websitePageLayout.WebsiteId;
                    _context.WebsitePageLayout.Update(pageToBeUpdated);
                }
                _context.SaveChanges();
                _result = Global.Success("Layout Changed");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }
        [HttpDelete("{id}")]
        public IActionResult Get(int id)
        {
            var _result = _websiteService.DeleteWebsite(_global.UserIdFromJWT(User), id);
            return Global.SendAsItIs(_result);
        }
        public class UpdateCultureViewModel
        {
            public List<CultureViewModel> Cultures { get; set; }
        }

        [HttpPost("Construction/Website/{WebsiteId}/{Status}")]
        public IActionResult Construction(int WebsiteId, bool Status)
        {
            GenericResult _result = _websiteService.SetConstruction(_global.UserIdFromJWT(User), WebsiteId, Status);
            return Global.SendAsItIs(_result);
        }

        [HttpPost("UpdateCulture/Website/{WebsiteId}")]
        [Authorize]
        public IActionResult UpdateCulture([FromBody] UpdateCultureViewModel cvm, int WebsiteId)
        {
            GenericResult _result = new GenericResult();
            var _website = _context.Websites.Where(x => x.Id == WebsiteId).SingleOrDefault();

            // Need to fix this later EF didnt supported this so going for loop end
            var _currentRegisteredCultures = _context.WebsiteCulture.Where(x => x.WebsiteId == WebsiteId).ToList();
            _context.WebsiteCulture.RemoveRange(_currentRegisteredCultures);
            _context.SaveChanges();

            List<WebsiteCulture> mainWc = new List<WebsiteCulture>();
            foreach (var culture in cvm.Cultures)
            {
                WebsiteCulture wc = new WebsiteCulture();
                wc.CultureId = culture.Id;
                wc.WebsiteId = WebsiteId;
                mainWc.Add(wc);
            }
            _website.Culture = mainWc;
            try
            {
                _context.Websites.Update(_website);
                _context.SaveChanges();
                _memoryCache.Remove(_website.Name.ToLower() + "Culture");
                _result = new GenericResult()
                {
                    Succeeded = true,
                    Message = "Languages Updated"
                };
            }
            catch (Exception ex)
            {
                _result = new GenericResult()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
            return Global.SendAsItIs(_result);
        }

        public class websiteWhatValueViewModel
        {
            public List<GenericWhatValueViewModel> Profile { get; set; }
        }

        [HttpGet("UpdateWebsiteDetails/Website/{WebsiteId}/Culture/{CultureId}")]
        [Authorize]
        public websiteWhatValueViewModel GetDetails(int WebsiteId, int CultureId)
        {
            websiteWhatValueViewModel w = new websiteWhatValueViewModel();
            List<GenericWhatValueViewModel> gw = new List<GenericWhatValueViewModel>();

            var _website = _context.Websites.Where(x => x.Id == WebsiteId).SingleOrDefault();

            GenericWhatValueViewModel gAnalytics = new GenericWhatValueViewModel();
            gAnalytics.What = "GoogleAnalyticsCode";
            gAnalytics.Value = _website.GoogleAnalyticsCode;
            gAnalytics.Type = "int";
            gAnalytics.Description = "Analytics Description";
            gAnalytics.Url = "http://aryan.sigdel.com/English/Helpfile";
            gw.Add(gAnalytics);

            GenericWhatValueViewModel Disqus_Code = new GenericWhatValueViewModel();
            Disqus_Code.What = "Disqus_Code";
            Disqus_Code.Value = _website.Disqus_Code;
            Disqus_Code.Type = "string";
            gw.Add(Disqus_Code);

            GenericWhatValueViewModel Logo = new GenericWhatValueViewModel();
            Logo.What = "Logo";
            Logo.Value = _website.Logo;
            Logo.Type = "image";
            Logo.Path = "logo";
            //Logo.Icon = "icon-image";
            gw.Add(Logo);

            GenericWhatValueViewModel Comments = new GenericWhatValueViewModel();
            Comments.What = "Comments";
            Comments.Value = _website.Comments.ToString().ToLower();
            Comments.Type = "bool";
            gw.Add(Comments);

            GenericWhatValueViewModel Css = new GenericWhatValueViewModel();
            Css.What = "Custom_Layout_Css";
            Css.Value = _website.Custom_Layout_Css;
            Css.Icon = "icon-language-css3";
            Css.Type = "image";
            Css.Path = "Files-css";
            gw.Add(Css);

            GenericWhatValueViewModel Ico = new GenericWhatValueViewModel();
            Ico.What = "Custom_Layout_Ico";
            Ico.Value = _website.Custom_Layout_Css;
            Ico.Icon = "icon-language-Ico";
            Ico.Type = "image";
            Ico.Path = "Files-Ico";
            gw.Add(Ico);
            // GenericWhatValueViewModel Script = new GenericWhatValueViewModel();
            // Script.What = "Custom_Layout_Script";
            // Script.Value = _website.Custom_Layout_Script;
            // Script.Type = "image";
            // Script.Path  = "Script";
            // gw.Add(Script);

            w.Profile = gw;
            return w;


        }

        [HttpGet("{WebsiteId}/Documents")]
        [Authorize]
        public List<Document> GetWebsiteDocuments(int WebsiteId)
        {
            var _result = _context.Websites.Where(x => x.Id == WebsiteId).SingleOrDefault().Documents;
            return _result;
        }

        [HttpPost("{WebsiteId}/Documents")]
        [Authorize]
        public IActionResult UpdateWebsiteDocument([FromBody] List<Document> d, int WebsiteId)
        {
            GenericResult _result = new GenericResult();
            var _websiteResult = _context.Websites.Where(x => x.Id == WebsiteId).SingleOrDefault();
            try
            {
                _websiteResult.Documents.AddRange(d);
                _context.Websites.Update(_websiteResult);
                _context.SaveChanges();
                _result = Global.Success("Documents Updated");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }

        [HttpPost("UpdateWebsiteDetails/Website/{WebsiteId}")]
        [Authorize]
        [Validate]
        public IActionResult ChangeDetails([FromBody]websiteWhatValueViewModel gvm, int WebsiteId)
        {
            GenericResult _result = new GenericResult();
            try
            {
                // string What = gvm.What;
                // string Value = gvm.Value;
                var _Website = _context.Websites.Where(x => x.Id == WebsiteId).SingleOrDefault();//_global.UserIdFromJWT(User)).SingleOrDefault();
                foreach (var items in gvm.Profile)
                {
                    string What = items.What;
                    string Value = items.Value;
                    switch (What.ToLower())
                    {
                        case "logo":
                            _Website.Logo = Value;
                            break;
                        case "googleanalyticscode":
                            _Website.GoogleAnalyticsCode = Value;
                            break;
                        case "comments":
                            _Website.Comments = Convert.ToBoolean(Value);
                            break;
                        case "disqus_code":
                            _Website.Disqus_Code = Value;
                            break;
                        case "Custom_Layout_Css":
                            _Website.Custom_Layout_Css = Value;
                            break;
                        case "Custom_Layout_Script":
                            _Website.Custom_Layout_Script = Value;
                            break;
                        default:
                            break;
                    }
                }
                _context.Update(_Website);
                _context.SaveChanges();
                _result = Global.Success("Webiste Updated");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }

        [AllowAnonymous]
        [HttpGet("GetListForIISCreation")]
        public List<string> CreateIISWebsite()
        {
            return _context.Websites.Where(x => x.IIsCreated == false).Select(x => x.Name).ToList();
        }


        [AllowAnonymous]
        [HttpPost("IISCreated/{websiteName}")]
        public void WebsiteCreated(string websiteName)
        {
            if (websiteName != null & websiteName != "")
            {
                var _website = _context.Websites.Where(x => x.IIsCreated == false).Where(x => x.Name == websiteName).SingleOrDefault();
                _website.IIsCreated = true;
                _context.Websites.Update(_website);
                _context.SaveChanges();
            }
        }

        [AllowAnonymous]
        [HttpPost("CheckAlias/{Alias}")]
        public IActionResult aliasNameAvailable(string Alias)
        {
            if (Alias.IndexOf(" ") > -1)
            {
                GenericResult _result = new GenericResult();
                _result.Succeeded = false;
                _result.Message = "Space Not Alloweded";
                return new BadRequestObjectResult(_result);
            }
            Regex r = new Regex("^[a-zA-Z]*$");
            if (!r.IsMatch(Alias))
            {
                GenericResult _result = new GenericResult();
                _result.Succeeded = false;
                _result.Message = "Special Characters And Numbers Not Alloweded";
                return new BadRequestObjectResult(_result);
            }
            return new OkObjectResult("{\"Status\":" + _websiteService.isAliasAvailable(Alias).ToString().ToLower() + "}");
        }
    }
}