using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Community.Services;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Data.Infrastructure;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;

namespace Community.Api.Controllers
{
    [Route("api/SiteMap")]
    public class SiteMapController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private IHostingEnvironment _hostingEnvironment;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IBlogService _blogService;
        private IPhotoAlbumService _photoAlbum;
        public SiteMapController(IPhotoAlbumService _photoAlbum, IBlogService _blogService, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IHostingEnvironment HostingEnvironment, CommunityContext _context)
        {
            this._photoAlbum = _photoAlbum;
            this._blogService = _blogService;
            this._hostingEnvironment = HostingEnvironment;
            this._context = _context;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<BlogController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }


        [AllowAnonymous]
        [HttpGet("Generate")]
        public List<SiteMap> Get()
        {
            return SiteMap();
        }

        [AllowAnonymous]
        [HttpGet("GenerateSiteMap/{WebsiteName}")]
        public List<SiteMap> GetSomeSiteMap(string WebsiteName)
        {
            return SiteMap(WebsiteName);
        }

        private List<SiteMap> SiteMap(string WebsiteName=null){            
             List<SiteMap> _siteMap = new List<SiteMap>();
            _siteMap.AddRange(_blogService.SiteMapBlogPost(WebsiteName));
            _siteMap.AddRange(_blogService.SiteMapBlogCategory(WebsiteName));
            _siteMap.AddRange(_blogService.SiteMapBlogTags(WebsiteName));
            _siteMap.AddRange(_photoAlbum.SiteMapPhotoAlbum(WebsiteName));
            return _siteMap.OrderBy(x => x.WebsiteName).ToList();
        }
    }
}