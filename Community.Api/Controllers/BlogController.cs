using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
// using Community.Model.Entities.Global;
using Community.Services;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Model.Entities.Blog;
namespace Community.Api.Controllers
{
    [Route("api/Blogs")]
    [Authorize]
    public class BlogController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private IBlogService _blogService;
        private IHostingEnvironment _hostingEnvironment;
        private GlobalUser _global = new GlobalUser();
        List<string> Errors = new List<string>();
        public BlogController(ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IBlogService _blogService, IHostingEnvironment HostingEnvironment)
        {
            this._hostingEnvironment = HostingEnvironment;
            this._blogService = _blogService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<BlogController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [AllowAnonymous]

        [HttpGet("Website/{WebsiteName}/Culture/{CultureName}/{Type}/Category/{Category}/{page}")]
        public IEnumerable<TestViewModel> GetBlogForWebsiteForCategory(string WebsiteName, string CultureName, BlogType Type, int page, string Category)
        {
            return _blogService.GetBlogPostsForCategory(WebsiteName, CultureName, Category, page, Type);
        }
        [AllowAnonymous]
        [HttpGet("/AllBlogs/Website/{WebsiteName}/Culture/{CultureName}/{page}")]
        public IEnumerable<TestViewModel> GetAllBlogForWebsite(string WebsiteName, string CultureName, int page)
        {
            return _blogService. GetBlogPostsForWebsite(WebsiteName, CultureName,page,Global.Pagesize(),  BlogType.Blog);
        }
        // [HttpGet("Website/{WebsiteName}/Culture/{CultureName}/{Type}/Category/{Category}/{page}")]
        // public IEnumerable<TestViewModel> GetBlogForWebsiteForCategory(string WebsiteName, string CultureName, BlogType Type, int page, string Category)
        // {
        //     return _blogService.GetBlogPostsForCategory(WebsiteName, CultureName, Category, page, Type);
        // }
        [HttpGet("Website/{WebsiteId}/Culture/{CultureId}/Type/{Type}/{page?}")]
        public IEnumerable<BlogPostViewModel> Get(int WebsiteId, int CultureId, BlogType Type, int? page = 0)
        {
            return _blogService.GetBlogPosts(_global.UserIdFromJWT(User), page.Value, Global.Pagesize(), WebsiteId, CultureId, Type);
        }
        [AllowAnonymous]
        [HttpGet("{BlogId}")]
        public BlogPostCreateViewModel Get(int BlogId)
        {
            return _blogService.GetBlogPost(BlogId);
        }
        [HttpDelete("{BlogId}")]
        public IActionResult DeleteBlog(int BlogId)
        {
            var _result = _blogService.DeleteBlog(BlogId, _global.UserIdFromJWT(User));
            return Global.SendAsItIs(_result);
        }
        [HttpGet("Website/{WebsiteId}/Culture/{CultureId}/Type/{Type}/Categories")]
        public IActionResult GetCategories(int WebsiteId, int CultureId, BlogType Type)
        {
            return new ObjectResult(_blogService.GetCategories(_global.UserIdFromJWT(User), 0, Global.Pagesize(), WebsiteId, CultureId, Type));
        }
        [HttpPost]
        [Validate]
        public IActionResult Get([FromBody] BlogPostCreateViewModel bLogVm)
        {
            var _result = _blogService.CreateBlogPost(_global.UserIdFromJWT(User), bLogVm);
            return Global.SendAsItIs(_result);
        }
        [Validate]
        [HttpPost("Category")]
        public IActionResult CreateCategory([FromBody] BlogCategoryCreateViewModel photos)
        {
            var _result = _blogService.CreateCategory(_global.UserIdFromJWT(User), photos);
            return Global.SendAsItIs(_result);
        }
    }
}
