using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using AutoMapper;
using Community.Model.Entities.Pages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.ComponentModel.DataAnnotations;

namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/Social")]
    [Produces("application/json")]
    public class SocialController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebsiteService _websiteService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMapper _mapper;
        private IMemoryCache _memoryCache;

        public SocialController(IMemoryCache _memoryCache, IMapper _mapper, CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService websiteservice, IUserService _userService)//,UserManager<User> _userManager)
        {
            this._memoryCache = _memoryCache;
            this._mapper = _mapper;
            this._context = _context;
            _websiteService = websiteservice;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        public class SocialWebsiteViewModel
        {
            public int SocialId { get; set; }
            public string Name { get; set; }
            public string Icon { get; set; }
            public string Value { get; set; }
        }

        [HttpGet("Website/{WebsiteId}")]
        public List<SocialWebsiteViewModel> GetSocialsWebsite(int websiteId)
        {
            List<SocialWebsiteViewModel> SWVM = new List<SocialWebsiteViewModel>();
            var _AllSocial = _context.Social.ToList();
            var _websiteSocial = _context.SocialWebsiteValue
                                .Include(x => x.Social)
                                .Where(x => x.WebsiteId == websiteId);
            foreach (var social in _AllSocial)
            {
                bool hasValue = false;
                foreach (var selectedSocial in _websiteSocial)
                {
                    if (selectedSocial.SocialId == social.Id)
                    {
                        SocialWebsiteViewModel swvm = new SocialWebsiteViewModel();
                        swvm.SocialId = social.Id;
                        swvm.Name = social.Name;
                        swvm.Value = selectedSocial.Value;
                        swvm.Icon = social.AdminIco;
                        SWVM.Add(swvm);

                        hasValue = true;
                        break;
                    }
                }
                if (hasValue == false)
                {
                    SocialWebsiteViewModel swvm = new SocialWebsiteViewModel();
                    swvm.SocialId = social.Id;
                    swvm.Name = social.Name;
                    swvm.Icon = social.AdminIco;
                    SWVM.Add(swvm);
                }
            }
            return SWVM;
        }

        public class SocialCreateSingle
        {
            [Required]
            public int SocialId { get; set; }
            public string Value { get; set; }
        }
        public class SocialCreate
        {
            public List<SocialCreateSingle> Socials { get; set; }
        }


        [HttpPost("Website/{WebsiteId}")]
        [Validate]
        public IActionResult SaveSocals([FromBody] SocialCreate socials, int WebsiteId)
        {
            GenericResult _result = new GenericResult();
            try
            {
                var _removeresults = _context.SocialWebsiteValue.Where(x => x.WebsiteId == WebsiteId);
                _context.SocialWebsiteValue.RemoveRange(_removeresults);
                _context.SaveChanges();
                foreach (var items in socials.Socials)
                {
                    SocialWebsiteValue swv = new SocialWebsiteValue();
                    swv.SocialId = items.SocialId;
                    swv.WebsiteId = WebsiteId;
                    swv.Value = items.Value;
                    _context.SocialWebsiteValue.Add(swv);
                }
                _result  = Global.Success("User Social Updated Successfully");
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                _result = Global.Error(ex); 
            }
             return Global.SendAsItIs(_result);
        }
    }
}