using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using System.Linq;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.SignalR.Infrastructure;
using Community.Model.Entities.Global;

namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/SupportTickets")]
    [Produces("application/json")]
    public class SupportTicketController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly ISupportTicketService _ticketService;
        private IMapper Mapper { get; set; }
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IConnectionManager _connectionManager;

        public SupportTicketController(IMapper Mapper, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, ISupportTicketService _ticketService, CommunityContext _context, IConnectionManager _connectionManager)
        {
            this._connectionManager = _connectionManager;

            this.Mapper = Mapper;
            this._ticketService = _ticketService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            this._context = _context;
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpGet("Website/{websiteId}")]
        public IEnumerable<SupportTicketViewModel> GetbyWebsite(int websiteId)
        {
            Console.WriteLine(websiteId);
            return _ticketService.GetSupportTickets(websiteId, _global.UserIdFromJWT(User));
        }

        [HttpGet("User/{UserId}")]
        public IEnumerable<SupportTicketViewModel> GetbyUser(int UserId)
        {
            return _ticketService.GetSupportTicketsUserId(UserId);
        }

        [HttpGet("{TicketId}")]
        public SupportTicketSingleViewModel GetSupportTicketReply(int TicketId)
        {
            return _ticketService.GetSupportTicketReply(TicketId, _global.UserIdFromJWT(User));
        }

        [HttpGet("Departments")]
        public List<SupportTicketDepartmentViewModel> GetSupportTicketDepartments()
        {
            var Departments = _context.SupportTicketDepartment.ToList();
            return Mapper.Map<List<SupportTicketDepartmentViewModel>>(Departments);
        }

        [HttpPost]
        [Validate]
        public IActionResult Get([FromBody] SupportTicketCreateViewModel ticket)
        {
            var _result = _ticketService.CreateSupportTicket(_global.UserIdFromJWT(User), ticket);
            return Global.SendAsItIs(_result);
        }
        [HttpPost("Reply")]
        [Validate]
        public IActionResult GetReply([FromBody] SupportTicketReplyCreateViewModel reply)
        {
            var _result = _ticketService.CreateSupportTicketReply(_global.UserIdFromJWT(User), reply);
            HubWalaReply h = new HubWalaReply();
            User u = _context.Users.Where(x => x.Id == _global.UserIdFromJWT(User)).SingleOrDefault();
            h.CreatedBy = u.Fullname;
            h.CreatedDate = DateTime.Now.ToString();
            h.Image = u.Image;
            h.UserType = u.UserType.ToString();
            h.Reply = reply.Reply;
            h.ScreenShot = reply.ScreenShot;
            h.TicketId = reply.TicketId;
            if (_result.Succeeded == true) {
                //_connectionManager.GetHubContext<SupportHub>().Clients.All.publishPost(h);
                _connectionManager.GetHubContext<SupportHub>().Clients.Group(h.TicketId.ToString()).publishPost(h);
            }
            return Global.SendAsItIs(_result);
        }
        private class HubWalaReply
        {
            public string CreatedBy { get; set; }
            public string ScreenShot { get; set; }
            public string CreatedDate { get; set; }
            public string Image { get; set; }
            public string Reply { get; set; }
            public string UserType { get; set; }

            public int TicketId { get; set; }
        }
    }
}