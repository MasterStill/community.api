using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
namespace Community.Api.Controllers
{
    [AllowAnonymous]
    [Route("api/Culture")]
    [Produces("application/json")]
    public class CultureController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private ICultureService _cultureService;
        private GlobalUser _global = new GlobalUser();

        public CultureController(ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, ICultureService _cultureService)
        {
            this._cultureService = _cultureService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpGet] 
        [AllowAnonymous]
        public List<CultureViewModel> Get()
        {
            return _cultureService.GetCulturesInCultureLanguage();
        }
    }
}