using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Principal;
using Community.Api.Options;
using Community.Model.Entities.Global;
using Community.Model.Entities;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels.Account;
using Community.Services.Infrastructure.Core;
using Community.Data.Infrastructure;
using System.Linq;
using Community.Services;
using Microsoft.AspNetCore.Hosting;
using Community.Model.Entities.ViewModels;

namespace Community.Api.Controllers
{
    [Route("Api/Account")]
    // [Route("api/[controller]/[action]")]  
    public class AccountController : Controller
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IMembershipService _membershipService;
        private IUserService _userService;
        private readonly JsonSerializerSettings _serializerSettings;
        private CommunityContext _context;
        private IHostingEnvironment _hostingEnvironment;
        private GlobalUser _global = new GlobalUser();
        private Community.Services.Global _GlobalService = new Community.Services.Global();
        private IWebsiteService _websiteService;
        public AccountController(IWebsiteService _websiteService, CommunityContext _context, IOptions<JwtIssuerOptions> jwtOptions, IMembershipService _membershipService, IUserService _userService, IHostingEnvironment _hostingEnvironment)
        {
            this._websiteService = _websiteService;
            this._hostingEnvironment = _hostingEnvironment;
            this._context = _context;
            this._userService = _userService;
            this._membershipService = _membershipService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [AllowAnonymous]
        [HttpPost("ValidateEmail/{Email}")]
        public IActionResult ValidateEmail(string Email)
        {
            return new OkObjectResult("{\"Status\":" + _membershipService.isEmailUsed(Email).ToString().ToLower() + "}");
        }

        [AllowAnonymous]
        [HttpPost("CreateUserWithWebsiteDetails")]
        [Validate]
        public Task<IActionResult> RegisterNewUserWithDetails([FromBody] NewUserWithDetailRegistrationViewModel nvm)
        {
            //First Register the User and get User Id
            RegistrationViewModel rvm = new RegistrationViewModel();
            rvm.Email = nvm.Email;
            rvm.Password = nvm.Password;
            rvm.FullName = nvm.Name;
            rvm.Username = nvm.Email;
            IActionResult _registrationResult = Register(rvm);

            User _user = _context.Users.Where(x => x.Username == nvm.Email).SingleOrDefault();

            LoginViewModel lvm = new LoginViewModel();
            lvm.Password = nvm.Password;
            lvm.Username = nvm.Email;

            // Create Website And Related Contents;
            WebsiteCreateViewModel wcvm = new WebsiteCreateViewModel();
            wcvm.Cultures = nvm.Culture;
            wcvm.Modules = nvm.Module;
            wcvm.Name = nvm.Website;
            wcvm.Redirect = false;
            wcvm.RedirectToUrl = null;
            wcvm.Theme = nvm.Theme;
            wcvm.Alias = nvm.Alias;
            GenericResult _websiteCreateResult = _websiteService.CreateWebsite(_user.Id, wcvm);
            return LoginUser(lvm);
        }
        [HttpGet("Logout")]
        [AllowAnonymous]
        public IActionResult Logout()
        {
            return new OkObjectResult("");
        }
        private async Task<IActionResult> LoginUser(LoginViewModel User)
        {
            MembershipContext _userContext = _membershipService.ValidateUser(User.Username, User.Password, ($"{Request.HttpContext.Connection.RemoteIpAddress}")); if (_userContext.User != null)
            {
                IEnumerable<Role> _roles = _userService.GetUserRoles(User.Username, 2);
                List<Claim> _claims = new List<Claim>();
                foreach (Role role in _roles)
                {
                    Claim _claim = new Claim(ClaimTypes.Role, role.Name, ClaimValueTypes.String, User.Username);
                    _claims.Add(_claim);
                }

                var identity = await GetClaimsIdentity(User);
                var claims = new[]
                {
                        // new Claim(JwtRegisteredClaimNames.Sub, User.Username),
                        new Claim(JwtRegisteredClaimNames.Sub, _userService.GetUserId(User.Username).ToString()),
                        new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                        new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                        identity.FindFirst("Role")
                      };
                var jwt = new JwtSecurityToken(
                    issuer: _jwtOptions.Issuer,
                    audience: _jwtOptions.Audience,
                    claims: claims,
                    notBefore: _jwtOptions.NotBefore,
                    expires: _jwtOptions.Expiration,
                    signingCredentials: _jwtOptions.SigningCredentials);

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                var response = new
                {
                    access_token = encodedJwt,
                    expires_in = (int)_jwtOptions.ValidFor.TotalSeconds
                };
                var json = JsonConvert.SerializeObject(response, _serializerSettings);
                return new OkObjectResult(json);
            }
            else
            {
                return Global.SendAsItIs(Global.Error(null, "Error : Invalid Username / Password"));
            }
        }
        [HttpPost("Login")]
        [AllowAnonymous]
        [Validate]
        public async Task<IActionResult> Login([FromBody] LoginViewModel User)
        {
            MembershipContext _userContext = _membershipService.ValidateUser(User.Username, User.Password, ($"{Request.HttpContext.Connection.RemoteIpAddress}"));
            if (_userContext.User != null)
            {
                IEnumerable<Role> _roles = _userService.GetUserRoles(User.Username, 2);
                List<Claim> _claims = new List<Claim>();
                foreach (Role role in _roles)
                {
                    Claim _claim = new Claim(ClaimTypes.Role, role.Name, ClaimValueTypes.String, User.Username);
                    _claims.Add(_claim);
                }

                var identity = await GetClaimsIdentity(User);
                var claims = new[]
                {
                        new Claim(JwtRegisteredClaimNames.Sub, _userService.GetUserId(User.Username).ToString()),
                        new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                        new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                        identity.FindFirst("Role")
                      };
                var jwt = new JwtSecurityToken(
                    issuer: _jwtOptions.Issuer,
                    audience: _jwtOptions.Audience,
                    claims: claims,
                    notBefore: _jwtOptions.NotBefore,
                    expires: _jwtOptions.Expiration,
                    signingCredentials: _jwtOptions.SigningCredentials);

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                var response = new
                {
                    access_token = encodedJwt,
                    expires_in = (int)_jwtOptions.ValidFor.TotalSeconds
                };
                var json = JsonConvert.SerializeObject(response, _serializerSettings);
                return new OkObjectResult(json);
            }
            else
            {
                return Global.SendAsItIs(Global.Error(null, "Error : Invalid Username / Password"));
            }
        }
        [HttpPost("Register")]
        [AllowAnonymous]
        [Validate]
        public IActionResult Register([FromBody] RegistrationViewModel user)
        {
            GenericResult _registrationResult = null;
            string encodedJwt;
            try
            {
                if (ModelState.IsValid)
                {
                    User _user = _membershipService.CreateUser(user.Username, user.Email, user.Password, new int[] { 1 }, user.FullName);
                    if (_user != null)
                    {
                        var jwt = new JwtSecurityToken(
                        issuer: _jwtOptions.Issuer,
                        audience: _jwtOptions.Audience,
                        notBefore: _jwtOptions.NotBefore,
                        expires: _jwtOptions.Expiration,
                        signingCredentials: _jwtOptions.SigningCredentials);
                        encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                        _registrationResult = Global.Success("Registration succeeded");
                        _registrationResult.Data = encodedJwt;
                    }
                }
                else
                {
                    return Global.SendAsItIs(Global.Error(null, "Invalid fields."));
                }
            }
            catch (Exception ex)
            {
                _registrationResult = Global.Error(ex);
            }
            return Global.SendAsItIs(_registrationResult);
        }
        public class UserImage
        {
            public string Image { get; set; }
        }
        [Authorize]
        [HttpGet("GetProfilePicture/{UserName}")]
        public UserImage GetProfilePicture(string UserName)
        {
            string Image = _context.Users.Where(x => x.Username == UserName).SingleOrDefault().Image;
            UserImage image = new UserImage();
            image.Image = Image;
            return image;
        }
        public class abcViewModel
        {
            public List<GenericWhatValueViewModel> Profile { get; set; }
        }
        public class UserProfileViewModel
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Gender { get; set; }
            public string Image { get; set; }
            public string Summary { get; set; }
            public DateTime? DoB { get; set; }
            public DateTime? LastLoggedInDate { get; set; }
            public string FullName { get; set; }
            public string UserName { get; set; }
        }
        [HttpGet("UserDetails")]
        [Authorize]
        public UserProfileViewModel GetUserDetails()
        {
            var _user = _context.Users.Where(x => x.Id == _global.UserIdFromJWT(User)).SingleOrDefault();
            UserProfileViewModel upvm = new UserProfileViewModel();
            upvm.DoB = _user.DoB;
            upvm.FirstName = _user.Firstname;
            upvm.Gender = _user.Gender;
            upvm.LastName = _user.LastName;
            upvm.FullName = _user.Fullname;
            upvm.LastLoggedInDate = _user.LastLoggedInDate;
            upvm.Image = _user.Image;
            upvm.Summary = _user.Summary;
            upvm.UserName = _user.Username;
            return upvm;
        }
        [HttpPost("UpdateUserDetails")]
        [Authorize]
        public IActionResult ChangeProfilePicture([FromBody]abcViewModel gvm)
        {
            GenericResult _pChangeResult = new GenericResult();
            try
            {
                var _user = _context.Users.Where(x => x.Id == _global.UserIdFromJWT(User)).SingleOrDefault();
                foreach (var items in gvm.Profile)
                {
                    string What = items.What;
                    string Value = items.Value;
                    switch (What.ToLower())
                    {
                        case "image":
                            _user.Image = Value;
                            _pChangeResult = new GenericResult()
                            {
                                Succeeded = true,
                                Message = "Profile Picture Updated"
                            };
                            break;
                        case "firstname":
                            _user.Firstname = Value;
                            _pChangeResult = new GenericResult()
                            {
                                Succeeded = true,
                                Message = "FirstName Updated"
                            };
                            break;
                        case "lastname":
                            _user.LastName = Value;
                            _pChangeResult = new GenericResult()
                            {
                                Succeeded = true,
                                Message = "LastName Updated"
                            };
                            break;
                        case "gender":
                            _user.Gender = Value;
                            _pChangeResult = new GenericResult()
                            {
                                Succeeded = true,
                                Message = "Gender Updated"
                            };
                            break;
                        case "dob":
                            _user.DoB = DateTime.Parse(Value);
                            _pChangeResult = new GenericResult()
                            {
                                Succeeded = true,
                                Message = "Date Of Birth Updated"
                            };
                            break;
                        case "summary":
                            _user.Summary = Value;
                            _pChangeResult = new GenericResult()
                            {
                                Succeeded = true,
                                Message = "User Summary Updated"
                            };
                            break;
                        default:
                            break;
                    }
                }
                _context.Update(_user);
                _context.SaveChanges();
                _pChangeResult = Global.Success("User Profile Updated");
            }
            catch (Exception ex)
            {
                _pChangeResult = Global.Error(ex, "Invalid fields." + ex.Message);
            }
            return Global.SendAsItIs(_pChangeResult);
        }
        private static long ToUnixEpochDate(DateTime date)
          => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);


        private static Task<ClaimsIdentity> GetClaimsIdentity(LoginViewModel user)
        {
            return Task.FromResult(new ClaimsIdentity(new GenericIdentity(user.Username, "Token"),
            new[]
            {
                    new Claim("Role", "Admin")
            }));
        }
    }
}