using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Community.Services.Abstract;
using Microsoft.AspNetCore.Http;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using Community.Model.Entities.Pages;
using Microsoft.AspNetCore.Hosting;
using System;
using Community.Model.Entities.Global;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Blog;
using AutoMapper;
using Newtonsoft.Json;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Community.Services;
using Microsoft.Extensions.Caching.Memory;
namespace Community.Api.Controllers
{
    public class HomeController : Controller
    {
        private IBlogService _blogService;
        private IHttpContextAccessor _contextAccessor;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly ILogger _logger;
        private CommunityContext _context;
        private IHostingEnvironment _hostingEnviroment;
        private IFileAndDirectoryService _fdService;
        private IMapper _Mapper;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        public WebsiteBasicSettings _websiteBasicSettings;
        private IPhotoAlbumService _albumService;
        private IMemoryCache _memoryCache;
        public HomeController(IMemoryCache _memoryCache, IUserService _userService, IOptions<JwtIssuerOptions> jwtOptions, IPhotoAlbumService _albumService, IMapper _Mapper, IBlogService _blogService, CommunityContext _context, IHostingEnvironment _hostingEnviroment, IFileAndDirectoryService _fdService, ILoggerFactory loggerFactory, IHttpContextAccessor _contextAccessor)//,,)
        {
            this._websiteBasicSettings = new WebsiteBasicSettings();
            this._contextAccessor = _contextAccessor;
            this._memoryCache = _memoryCache;
            this._userService = _userService;
            this._context = _context;
            this._albumService = _albumService;
            this._hostingEnviroment = _hostingEnviroment;
            this._blogService = _blogService;
            this._fdService = _fdService;
            this._Mapper = _Mapper;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<HomeController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        public IActionResult LoadOnConstructionComponent(string websiteName, string culture)
        {
            var otherDetails = _context.WebsiteComponentFieldValue
               .Include(x => x.ComponentField)
               .Include(x => x.WebsiteComponent)
               .ThenInclude(y => y.Website)
               .Where(x => x.Culture.Title == culture)
               .Where(x => x.WebsiteComponent.Name == "On Construction Setting")
               .Where(x => x.WebsiteComponent.Website.Name == websiteName);
            foreach (var items in otherDetails)
            {
                ViewData.Add(items.ComponentField.FieldName, items.Value);
            }
            return View("Construction");
        }
        [AllowAnonymous]
        [HttpGet("{culture}")]
        public IActionResult Index(string culture)
        {
            if (culture == "sitemap.txt")
            {
                List<SiteMap> sm = new List<SiteMap>();
                sm.AddRange(_blogService.SiteMapBlogCategory(GetWebsiteName()));
                sm.AddRange(_blogService.SiteMapBlogPost(GetWebsiteName()));
                sm.AddRange(_blogService.SiteMapBlogTags(GetWebsiteName()));
                sm.AddRange(_albumService.SiteMapPhotoAlbum(GetWebsiteName()));
                ViewBag.SiteMap = sm;
                return View("SiteMap");
            }
            if (culture == "favicon.ico") return new BadRequestResult();//null;
            try
            {
                string currentPage = "Index";
                string websiteName = GetWebsiteName();
                if (OnConstruction(websiteName, culture) && (!_websiteBasicSettings.OnDemoMode))
                {
                    return LoadOnConstructionComponent(GetWebsiteName(), culture);
                }
                if (culture == null) { culture = _websiteBasicSettings.DefaultCulture; }
                string templateName = _websiteBasicSettings.Theme;
                if (templateName == null) return ObjectErrorSettingsnotFound();
                if (websiteName != null && websiteName != "")
                {
                    GetCopyRightMessage(websiteName, culture, currentPage);
                    return View(currentPage);
                }
                return ObjectErrorSettingsnotFound();
            }
            catch (Exception ex)
            {
                ObjectErrorSettingsnotFound(ex.Message);
            }
            return ObjectErrorSettingsnotFound();
        }
        [AllowAnonymous]
        [HttpGet("{culture}/Contact")]
        public IActionResult Contact(string culture)
        {
            // if (culture == "sitemap.txt")
            // {
            //     List<SiteMap> sm = new List<SiteMap>();
            //     sm.AddRange(_blogService.SiteMapBlogCategory(GetWebsiteName()));
            //     sm.AddRange(_blogService.SiteMapBlogPost(GetWebsiteName()));
            //     sm.AddRange(_blogService.SiteMapBlogTags(GetWebsiteName()));
            //     sm.AddRange(_albumService.SiteMapPhotoAlbum(GetWebsiteName()));
            //     ViewBag.SiteMap = sm;
            //     return View("SiteMap");
            // }
            return View("");
            // if (culture == "favicon.ico") return new BadRequestResult();//null;
            // try
            // {
            //     string currentPage = "Index";
            //     string websiteName = GetWebsiteName();
            //     if (OnConstruction(websiteName, culture) && (!_websiteBasicSettings.OnDemoMode))
            //     {
            //         return LoadOnConstructionComponent(GetWebsiteName(), culture);
            //     }
            //     if (culture == null) { culture = _websiteBasicSettings.DefaultCulture; }
            //     string templateName = _websiteBasicSettings.Theme;
            //     if (templateName == null) return ObjectErrorSettingsnotFound();
            //     if (websiteName != null && websiteName != "")
            //     {
            //         GetCopyRightMessage(websiteName, culture, currentPage);
            //         return View(currentPage);
            //     }
            //     return ObjectErrorSettingsnotFound();
            // }
            // catch (Exception ex)
            // {
            //     ObjectErrorSettingsnotFound(ex.Message);
            // }
            // return ObjectErrorSettingsnotFound();
        }
        [HttpGet("{culture}/{Type}/Category/{Category}/{page?}")]
        [AllowAnonymous]
        public IActionResult BlogCategory(string culture, string category, BlogType Type, int? page = 0)
        {
            string websiteName = GetWebsiteName();
            if (OnConstruction(websiteName, culture) && (!_websiteBasicSettings.OnDemoMode))
            {
                return LoadOnConstructionComponent(websiteName, culture);
            }
            string TemplateName = _websiteBasicSettings.Theme;
            if (TemplateName == null) { return ObjectErrorSettingsnotFound(); }
            ViewBag.BreadCrumbText = category.Replace("_", " "); ;
            GetCopyRightMessage(websiteName, culture, Type + " Category List");
            ViewBag.ApiUrl = "http://" + websiteName + "/api/blogs/Website/" + websiteName + "/Culture/" + culture + "/" + Type.ToString() + "/Category/" + category + "/";
            ViewBag.ContentType = Type;
            return View("List");
        }
        [HttpGet("{culture}/{Type}/Tag/{Tag}/{page?}")]
        [AllowAnonymous]
        public IActionResult BlogTagsList(string culture, string Tag, BlogType Type, int? page = 0)
        {
            string websiteName = GetWebsiteName();
            if (OnConstruction(websiteName, culture) && (!_websiteBasicSettings.OnDemoMode))
            {
                return LoadOnConstructionComponent(websiteName, culture);
            }
            string TemplateName = _websiteBasicSettings.Theme;
            if (TemplateName == null) { return ObjectErrorSettingsnotFound(); }
            ViewBag.BreadCrumbText = Tag.Replace("_", " ");
            IEnumerable<BlogPostViewModel> bpvm = _blogService.GetBlogPostsForTags(websiteName, culture, Tag, page.Value, Type).OrderByDescending(x => x.CreatedDate);
            IEnumerable<TestViewModel> bptvm;
            bptvm = _Mapper.Map<IEnumerable<TestViewModel>>(bpvm);
            GetCopyRightMessage(websiteName, culture, "BlogList");
            ViewBag.Data = JsonConvert.SerializeObject(bptvm);
            return View("List", bptvm);
        }
        // [HttpGet("{culture}/{Type}/")]
        // [AllowAnonymous]
        // public IActionResult BlogSingleType(string culture, BlogType Type)
        // {
        //     if (culture.ToLower()== "signalr")return View();
        //     string websiteName = GetWebsiteName();
        //     if (OnConstruction(websiteName, culture) && (!_websiteBasicSettings.OnDemoMode))
        //     {
        //         return LoadOnConstructionComponent(websiteName, culture);
        //     }
        //     string TemplateName = _websiteBasicSettings.Theme;
        //     if (TemplateName == null) { return ObjectErrorSettingsnotFound(); }
        //     List<TestViewModel> bpvm = _blogService.GetBlogPostsForWebsite(websiteName, culture, 0, 100, Type).OrderByDescending(x => x.CreatedDate).ToList();
        //     GetCopyRightMessage(websiteName, culture, "BlogList");
        //     return View("List", bpvm);
        // }
        [HttpGet("{culture}/{Type}/{Category}/{BlogId:int}/{title}")]
        [AllowAnonymous]
        public IActionResult Blog(string culture, int BlogId, string Type)
        {
            string websiteName = GetWebsiteName();
            if (OnConstruction(websiteName, culture) && (!_websiteBasicSettings.OnDemoMode))
            {
                return LoadOnConstructionComponent(websiteName, culture);
            }
            string PageName = Type + " " + "single";
            string currentPage = PageName.Replace(" ", "");
            string templatename = string.Empty;
            templatename = _websiteBasicSettings.Theme;
            if (templatename == null) { return ObjectErrorSettingsnotFound(); }
            ViewBag.Template = templatename;
            var _blogPost = _blogService.GetBlogPost(BlogId, culture);
            GetCopyRightMessage(websiteName, culture, PageName);
            ViewBag.Title = _blogPost.Title;
            return View("SingleType", _blogPost);
        }
        [HttpGet("")]
        [AllowAnonymous]
        public IActionResult DefaultIndex()
        {
            return Index(null);
        }
        public void GetCopyRightMessage(string websiteName, string culture, string PageName)
        {
            ViewBag.Culture = culture;
            bool canEdit = false; ;
            if (Global.GetOnConstructionWebsiteName(GetRealWebsiteName()).Contains(Global.MasterDemoAlias()))
            {
                canEdit = true;
                ViewBag.CanEdit = canEdit;
            }
            var otherDetails = _context.WebsiteComponentFieldValue
                .Include(x => x.ComponentField)
                .Include(x => x.WebsiteComponent)
                .Where(x => x.Culture.Title == culture)
                .Where(x => x.WebsiteComponent.Name == PageName || x.WebsiteComponent.Name == "AlwaysLoad" || x.WebsiteComponent.Name == "Settings" || x.ComponentField.FieldName == "CopyrightNotice");
            string title = _context.WebsiteComponentFieldValue.Where(x => x.WebsiteComponent.Website.Name == websiteName)
                                       .Where(x => x.ComponentField.FieldName == "PageTitle")
                                       .Where(x => x.Culture.Title == culture)
                                       .Select(x => x.Value).SingleOrDefault();
            ViewBag.Title = title;//Title == null ? "HomePage - " + TitleSuffix : Title + " - " + TitleSuffix;  
            if (otherDetails.Any())
            {
                foreach (var items in otherDetails.Where(x => x.WebsiteComponent.Name == "Settings" || x.ComponentField.FieldName == "CopyrightNotice").Where(x => x.WebsiteComponent.Website.Name == websiteName))
                {
                    ViewData.Add(GetRealWebsiteName() + items.ComponentField.FieldName, items.Value);
                }
                foreach (var items in otherDetails.Where(x => x.WebsiteComponent.Name != "Settings" || x.ComponentField.FieldName != "CopyrightNotice").Where(x => x.WebsiteComponent.Name == "AlwaysLoad" || x.WebsiteComponent.Name == PageName))
                {
                    ViewData.Add(items.ComponentField.FieldName, items.Value);
                }
            }
            string _strlayoutType = string.Empty;
            try
            {
                var _layoutResult = _context.WebsitePageLayout.Include(x => x.Website).Where(x => x.Website.Name.ToLower() == websiteName.ToLower())
                    .Where(x => x.Page.Title == PageName).Select(x => new { x.Layout, x.PageId, x.Id }).SingleOrDefault();
                _strlayoutType = _layoutResult.Layout.ToString();
                var LayoutEditId = _layoutResult.Id;
                var pageId = _layoutResult.PageId;
                if (canEdit)
                {
                    int CultureId = 0;
                    int WebsiteId = 0;
                    if (otherDetails.Any())
                    {
                        WebsiteId = otherDetails.Where(x => x.WebsiteComponent.Website.Name.ToLower() == websiteName.ToLower()).FirstOrDefault().WebsiteComponent.WebsiteId;
                        CultureId = otherDetails.Where(x => x.WebsiteComponent.Website.Name.ToLower() == websiteName.ToLower()).FirstOrDefault().CultureId;
                        ViewBag.WebsiteId = WebsiteId;
                        ViewBag.CultureId = CultureId;
                    }
                    ViewBag.EditPageLayoutUrl = "http://admin.masterstill.com/#/design/page/" + pageId + "?WebsiteId=" + WebsiteId + "&CultureId=" + CultureId;
                    ViewBag.EditPageLayoutType = "http://admin.masterstill.com/#/pages?Id=" + LayoutEditId + "&WebsiteId=" + WebsiteId + "&CultureId=" + CultureId;
                }
            }
            catch
            {
                _strlayoutType = "middle";
            }
            ViewBag.LayoutType = _strlayoutType;

            List<WebsiteComponentsForPage> _websiteComponentForPage = new List<WebsiteComponentsForPage>();

            if (!InDemoMode())
            {
                if (!_memoryCache.TryGetValue(websiteName.ToLower() + PageName.ToLower(), out _websiteComponentForPage))
                {
                    List<WebsiteComponentsForPage> _websiteComponentForPage1 = LoadPageComponents(websiteName, PageName);
                    _memoryCache.Set(websiteName.ToLower() + PageName.ToLower(), _websiteComponentForPage1, new MemoryCacheEntryOptions()
                                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheTime())));
                    _websiteComponentForPage = _websiteComponentForPage1;
                }
            }
            else
            {
                _websiteComponentForPage = LoadPageComponents(websiteName, PageName);
            }

            if (_strlayoutType.ToLower().Contains("right"))
                ViewData["ComponentsToLoadBodyRightSidePanel"] = _websiteComponentForPage
                    .Where(x => x.Location == Location.BodyRightSidePanel)
                    .Select(x => x.Name).ToArray();
            if (_strlayoutType.ToLower().Contains("left"))
                ViewData["ComponentsToLoadBodyLeftSidePanel"] = _websiteComponentForPage
               .Where(x => x.Location == Location.BodyLeftSidePanel)
               .Select(x => x.Name).ToArray();
            ViewData["ComponentsToLoadBody"] = _websiteComponentForPage
             .Where(x => x.Location == Location.Body)
             .Select(x => x.Name).ToArray();
            ViewData["ComponentsToLoadFooter"] = _websiteComponentForPage
             .Where(x => x.Location == Location.Footer)
             .Select(x => x.Name).ToArray();

        }
        public class WebsiteComponentsForPage
        {
            public string Name { get; set; }
            public Location Location { get; set; }
        }
        public List<WebsiteComponentsForPage> LoadPageComponents(string websiteName, string Page)
        {
            List<WebsiteComponentsForPage> _websiteComponentForPage = new List<WebsiteComponentsForPage>();
            var _result = _context.WebsitePage
                                   .Include(x => x.WebsiteComponent)
                                   .Include(x => x.Website)
                                   .Where(x => x.Page.Title.ToLower() == Page.ToLower())
                                   .Where(x => x.Website.Name == websiteName).ToList().Select(x => new { x.Location, x.WebsiteComponent.Name });
            foreach (var items in _result)
            {
                WebsiteComponentsForPage wpfp = new WebsiteComponentsForPage();
                wpfp.Location = items.Location;
                wpfp.Name = items.Name;
                _websiteComponentForPage.Add(wpfp);
            }
            return _websiteComponentForPage;
        }
        bool OnConstruction(string website, string culture = "English")
        {
            if (!InDemoMode())
            {
                if (!_memoryCache.TryGetValue(website, out _websiteBasicSettings))
                {
                    _websiteBasicSettings = new WebsiteBasicSettings();
                    string Website = Global.GetOnConstructionWebsiteName(($"{Request.Host}"));
                    bool OnConstruction = false;
                    var _website = _context.Websites
                                    .Include(x => x.DefaultCulture)
                                    .Include(x => x.Culture)
                                    .ThenInclude(y => y.Culture)
                                   .Where(x => x.Name == website)
                                   .SingleOrDefault();

                    if (!(_website == null))
                    {
                        OnConstruction = _website.Construction;
                        _websiteBasicSettings.OnConstruction = OnConstruction;
                        _websiteBasicSettings.Theme = _website.Theme;
                        _websiteBasicSettings.AliasName = _website.Alias;
                        _websiteBasicSettings.WebsiteName = _website.Name;
                        _websiteBasicSettings.LogoUrl = _website.Logo;
                        if (_website.Paid == false || _website.PaidWebsite == false)
                        {
                            _websiteBasicSettings.LogoUrl = Global.DefaultLogo();
                            _websiteBasicSettings.LogoLink = Global.DefaultLogoLink();
                            _websiteBasicSettings.CopyrightNotice = Global.CopyRightNotice(culture);
                        }
                        if (_website.DefaultCulture == null) { _websiteBasicSettings.DefaultCulture = _website.Culture.FirstOrDefault().Culture.Title; }
                        else { _websiteBasicSettings.DefaultCulture = _website.DefaultCulture.Title; }
                        _memoryCache.Set(website, _websiteBasicSettings, new MemoryCacheEntryOptions()
                                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheTime())));
                    }
                }
            }
            else
            {
                _websiteBasicSettings = new WebsiteBasicSettings();
                string Website = Global.GetOnConstructionWebsiteName(($"{Request.Host}"));
                bool OnConstruction = false;
                var _website = _context.Websites.Where(x => x.Name == website || x.Alias == AliasName()).Select(x => new { x.Construction, x.Theme, x.DefaultCulture.Title, x.Logo, x.Name, x.Alias }).SingleOrDefault();
                if (!(_website == null))
                {
                    OnConstruction = _website.Construction;
                    _websiteBasicSettings.OnConstruction = OnConstruction;
                    _websiteBasicSettings.Theme = _website.Theme;
                    _websiteBasicSettings.AliasName = _website.Alias;
                    _websiteBasicSettings.WebsiteName = _website.Name;
                    _websiteBasicSettings.LogoUrl = _website.Logo;
                    _websiteBasicSettings.DefaultCulture = _website.Title;
                    if (Website.ToLower().Contains(Global.MasterDemoAlias()))
                    {
                        _websiteBasicSettings.OnDemoMode = true;
                        _websiteBasicSettings.LogoUrl = _website.Logo;
                    }
                }
            }
            _contextAccessor.HttpContext.Items.Add("Theme", _websiteBasicSettings.Theme);
            _contextAccessor.HttpContext.Items.Add(website, _websiteBasicSettings);
            string WebsiteName;
            if (!_memoryCache.TryGetValue(_websiteBasicSettings.AliasName + "webiteName", out WebsiteName))
            {
                _memoryCache.Set(website + "webiteName", _websiteBasicSettings.WebsiteName, new MemoryCacheEntryOptions()
                                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheTime())));
            }
            ViewBag.WebsiteLogo = _websiteBasicSettings.LogoUrl;
            ViewBag.Template = _websiteBasicSettings.Theme;
            ViewBag.WebsiteName = GetRealWebsiteName();
            ViewBag.CopyrightNotice = _websiteBasicSettings.CopyrightNotice;
            return _websiteBasicSettings.OnConstruction;
        }
        string AliasName()
        {
            return Global.GetOnConstructionWebsiteName($"{Request.Host}").Replace(Global.MasterDemoAlias(), "");
        }
        string GetWebsiteName()
        {
            return GetWebsiteName((Global.GetOnConstructionWebsiteName($"{Request.Host}")));
        }
        string GetRealWebsiteName()
        {
            return ($"{Request.Host}");
        }
        bool InDemoMode()
        {
            if (Global.GetOnConstructionWebsiteName(GetRealWebsiteName()).Contains(Global.MasterDemoAlias())) return true;
            return false;
        }
        [HttpGet("Albums")]
        public PaginationSet<PhotoAlbumViewModel> getPagedAlbums()
        {
            return _albumService.GetPagedAlbum(GetWebsiteName(), "English", 0, 2);
        }
        public IActionResult ObjectErrorSettingsnotFound(string Message = "")
        {
            return new ObjectResult(Global.ErrorSettingsnotFound(Message));
        }
        public string GetWebsiteName(string HostName)
        {
            string webisteName = HostName;
            // return "aryan.sigdel.com";             
            if (!_memoryCache.TryGetValue(HostName.Replace(Global.MasterDemoAlias(), "") + "webiteName", out webisteName))
            {
                if (HostName.Contains(Global.MasterDemoAlias()))
                {
                    try
                    {
                        string aliasName = AliasName();
                        var _result = _context.Websites.Where(x => x.Name == HostName || x.Alias == aliasName).SingleOrDefault();
                        _memoryCache.Set(HostName.Replace(Global.MasterDemoAlias(), "") + "webiteName", _result.Name, new MemoryCacheEntryOptions()
                                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheTime())));
                        return _result.Name;

                    }
                    catch (Exception ex)
                    {
                        Global.Error(ex);
                    }
                }
                else
                {
                    webisteName = HostName;

                }
            }
            return webisteName;
        }
    }
}