using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;
namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/PostaboutUserExceedingUsuage")]
    [Produces("application/json")]
    public class PostaboutUserExceedingUsuage : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebsiteService _websiteService;
        private IUserService _userService;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        public PostaboutUserExceedingUsuage(CommunityContext _context, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IWebsiteService websiteservice, IUserService _userService)//,UserManager<User> _userManager)
        {
            this._context = _context;
            _websiteService = websiteservice;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Get([FromForm] AutoItViewModel Website)
        {
            try
            {
                Website website = _context.Websites.Where(x => x.Name == Website.WebsiteName).SingleOrDefault();
                _context.Entry(website).State = EntityState.Modified;
                website.SpaceUsed = Website.CurrentSize;
                _context.SaveChanges();
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}