using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
namespace Community.Api.Controllers
{
    [Authorize]
    [Route("Api/menu")]
    public class MenuController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private IMenuService _menuService;
        private GlobalUser _global = new GlobalUser();
        public MenuController(ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, IMenuService _menuService)
        {
            this._menuService = _menuService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<MenuController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpGet("Website/{websiteID}/Culture/{CultureId}")] // Get Menu
        public IEnumerable<MenuViewModel> Get(int WebsiteId, int Cultureid)
        {
            return _menuService.GetMenus(_global.UserIdFromJWT(User), 0, Global.Pagesize(), WebsiteId, Cultureid);
        }

        [HttpPost]
        [Validate]
        public IActionResult Get([FromBody] MenuCreateViewModel menu)
        {
            var _result = _menuService.CreateMenu(_global.UserIdFromJWT(User), menu);
            return Global.SendAsItIs(_result);
        }

        [HttpDelete("{MenuId}")]
        public IActionResult DeleteMenu(int MenuId)
        {
            var _result = _menuService.DeleteMenu(_global.UserIdFromJWT(User), MenuId);
            return Global.SendAsItIs(_result);
        }
    }
}
