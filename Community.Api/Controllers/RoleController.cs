using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Services.Abstract;
using Community.Services;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Model.Entities.Global;
using Community.Data.Infrastructure;
using Community.Model.Entities;
using Microsoft.EntityFrameworkCore;
namespace Community.Api.Controllers
{
    [Route("api/Role")]
    [Authorize]
    public class RoleController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IUserService _userService;
        public RoleController(IUserService _userService, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, CommunityContext _context)
        {
            this._context = _context;
            this._userService = _userService;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<BlogController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpPost("AddUserToRole/Website/{WebsiteId}/User/{UserName}/Role/{RoleName}")]
        public IActionResult AddUserToRole(int WebsiteId, string UserName, string RoleName)
        {
            GenericResult _result = null;
            try
            {
                if (!_userService.IsInRole(_global.UserIdFromJWT(User), WebsiteId, (new string[] { "Admin", "Owner" }))) return new BadRequestObjectResult(Global.AccessDenied());
                var _checkIfRoleIsThere = _context.UserRoles
                           .Where(x => x.Role.Name.ToLower() == RoleName.ToLower())
                           .Where(x => x.User.Username.ToLower() == UserName.ToLower())
                           .Where(x => x.WebsiteId == WebsiteId).SingleOrDefault();
                if (_checkIfRoleIsThere != null)
                {
                    _result = Global.Error(null, "UserRole Already Exists");
                    return new BadRequestObjectResult(_result);
                }
                var user = _context.Users.Where(x => x.Username.ToLower() == UserName.ToLower()).SingleOrDefault();
                int userId = user.Id;
                int roleId = _context.Roles.Where(x => x.Name.ToLower() == RoleName.ToLower()).SingleOrDefault().Id;
                UserRole ur = new UserRole();
                ur.UserId = userId;
                ur.RoleId = roleId;
                ur.CreatedById = _global.UserIdFromJWT(User);
                ur.MultiLingualId = 0;
                ur.WebsiteId = WebsiteId;
                // ur.Id = Int32.Parse(WebsiteId.ToString() + userId.ToString() + roleId.ToString()); 
                _context.UserRoles.Add(ur);
                _context.SaveChanges();
                _result = Global.Success("UserAdded To Role");
            }
            catch (Exception ex)
            {
                _result = new GenericResult()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
            return Global.SendAsItIs(_result);
        }

        [HttpGet("Website/{WebsiteId}")]
        public IActionResult GetUserAndRoleForWebsite(int WebsiteId)
        {
            GenericResult _result = new GenericResult();
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), WebsiteId, (new string[] { "Admin", "Owner" }))) return BadRequest(Global.AccessDenied());

            List<RoleViewModel> _returnRoleViewModel = new List<RoleViewModel>();
            var _Roleresult = _context.UserRoles.Include(x => x.User).Include(x => x.Role).Where(x => x.WebsiteId == WebsiteId);
            foreach (var items in _Roleresult)
            {
                RoleViewModel rvm = new RoleViewModel();
                rvm.UserFullName = items.User.Fullname;
                rvm.RoleName = items.Role.Name;
                rvm.Id = items.Id;
                rvm.Image = items.User.Image;
                rvm.UserName = items.User.Username;
                _returnRoleViewModel.Add(rvm);
            }
            return new OkObjectResult(_returnRoleViewModel);
        }
        [HttpDelete("{Id}")]
        public IActionResult RemoveUserFromRole(int Id)
        {
            GenericResult _result = new GenericResult();
            UserRole u = _context.UserRoles.Where(x => x.Id == Id).SingleOrDefault();
            if (!_userService.IsInRole(_global.UserIdFromJWT(User), u.WebsiteId, (new string[] { "Admin", "Owner" }))) return BadRequest(Global.AccessDenied());
            try
            {
                _context.UserRoles.Remove(u);
                _context.SaveChanges();
                _result = Global.Success("User Removed From Role");
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return Global.SendAsItIs(_result);
        }
        public class RoleViewModel
        {
            public int Id { get; set; }
            public string Image { get; set; }
            public string UserName { get; set; }
            public string UserFullName { get; set; }
            public string RoleName { get; set; }
        }

        [HttpGet("AllRoles")]
        public List<Role> GetAllRoles()
        {
            return _context.Roles.ToList();
        }
    }
}