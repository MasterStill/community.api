using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Community.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Community.Api.Options;
using Microsoft.Extensions.Options;
using Community.Services;
using Community.Data.Infrastructure;
using Microsoft.Extensions.Caching.Memory;
using Community.Services.Abstract;
namespace Community.Api.Controllers
{
    [Authorize]
    [Route("api/Module")]
    public class ModuleController : Controller
    {
        private readonly ILogger _logger;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private GlobalUser _global = new GlobalUser();
        private CommunityContext _context;
        private IMemoryCache _memoryCache;
        private IModuleService _moduleService;
        public ModuleController(IModuleService _moduleService, IMemoryCache _memoryCache, ILoggerFactory loggerFactory, IOptions<JwtIssuerOptions> jwtOptions, CommunityContext _context)
        {
            this._moduleService = _moduleService;
            this._memoryCache = _memoryCache;
            this._context = _context;
            _jwtOptions = jwtOptions.Value;
            Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<WebsiteController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpGet("Website/{WebsiteId}")]
        public List<ModuleViewModel> WebsiteModules(int WebsiteId)
        {
            return _moduleService.WebsiteModules(WebsiteId,2);
        } 
        [HttpPost("Website/{WebsiteId}")]
        [Validate]
        public IActionResult ModifyWebstieModule([FromBody]ModuleModifyViewModel websiteModule, int WebsiteId)
        {
            int[] SelectedModules = websiteModule.UsedModule.ToArray();
            var _result = _moduleService.ModifyWebstieModule(SelectedModules,WebsiteId,_global.UserIdFromJWT(User));            
            if (_result.Succeeded) return new OkObjectResult(_result);
            return BadRequest(_result);
        }
    }
}