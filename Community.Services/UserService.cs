﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.Global;
using Community.Model.Entities;
namespace Community.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private IRoleRepository _roleReposistory;
        private IMapper Mapper { get; set; }
        public UserService(IMapper Mapper,IUserRepository _userRepository,IRoleRepository _roleReposistory)
        {
            this._roleReposistory = _roleReposistory;
            this._userRepository = _userRepository;
            this.Mapper = Mapper;
        }
        public User GetSingleByUsername(string username){
            return _userRepository.GetSingleByUsername(username);
        }
  
        public List<int> GetUserCulture(int UserId){
            return _userRepository.GetUserCulture(UserId);
        }
        public User GetSingleById(int UserId){
            return _userRepository.GetAll().Where(x=>x.Id == UserId).SingleOrDefault();
        }

        public IEnumerable<Role> GetUserRoles(string username){
            return _userRepository.GetUserRoles(username);
        }

        public bool IsCommunityAdmin(int UserId){
            if(_userRepository.GetSingle(UserId).UserType == UserType.CommunityAdmin) return true;
            return false;            
        }
        public bool IsInRole(int UserID, int Website,string RoleName){
            if (IsCommunityAdmin(UserID))return true;
            IEnumerable<Role> _roles = GetUserRoles(UserID,Website);
            var role = _roles.Where(x=>x.Name.ToLower() == RoleName.ToLower());
            if (role!=null){
                return true;
            }
            return false;
        }

        public bool IsInRole(int UserID, int Website,string[] RoleName){
            if (IsCommunityAdmin(UserID))return true;
            IEnumerable<Role> _roles = GetUserRoles(UserID,Website);
            foreach (var roleToCheck in RoleName)
            {
                var role = _roles.Where(x=>x.Name.ToLower() == roleToCheck.ToLower());
                if (role.Any()){
                    return true;
                }    
            }
            return false;
        }

        public IEnumerable<Role> GetUserRoles(string username,int website){
           Console.WriteLine("Checking UserRole for Username : " + username + " For WebsiteId " + website);
            List<Role> _roles = null;
            User _user = _userRepository.GetSingle(u => u.Username == username, u => u.Roles);
            if(_user != null)
            {
                _roles = new List<Role>();
                foreach (var _userRole in _user.Roles.Where(x=>x.WebsiteId == website))
                    {
                        _roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
                    }
            }
            return _roles;
        }
        public IEnumerable<Role> GetUserRoles(int userId,int website){
            Console.WriteLine("Checking UserRole for UserID : " + userId + " For WebsiteId " + website);
            List<Role> _roles = null;
            User _user = _userRepository.GetSingle(u => u.Id == userId, u => u.Roles);
            if(_user != null)
            {
                _roles = new List<Role>();
                foreach (var _userRole in _user.Roles.Where(x=>x.WebsiteId == website))
                    {
                        _roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
                    }
            }
            return _roles;
        }
        public int GetUserId(string username){
             //var User =  = _userRepository.GetAll().Where(x=>x.Username == username).Select(x=>x.Id).SingleOrDefault();
            var User = GetSingleByUsername(username);

             Console.WriteLine("Service bata ayeko userId " + User.Id + " For Username : " + username);
             return Convert.ToInt32(User.Id);
        }
    } 
}