using Community.Model.Entities;
using Community.Model.Entities.Global;
using Community.Services.Abstract; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Infrastructure.Core;
namespace Community.Services.Infrastructure.Services
{
    public class MembershipService : IMembershipService
    {
        #region Variables
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IEncryptionService _encryptionService;
        private Global g = new Global();
        #endregion
        public MembershipService(IUserRepository userRepository, IRoleRepository roleRepository,
        IUserRoleRepository userRoleRepository, IEncryptionService encryptionService)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
            _encryptionService = encryptionService;
        }

        #region IMembershipService Implementation

        public MembershipContext ValidateUser(string username, string password,string ip)
        {
            var membershipCtx = new MembershipContext();
            try{
            var user = _userRepository.GetSingleByUsername(username);
            if (user != null && isUserValid(user, password))
            {
                // user.LastLoggedOnIP = ip;
                // user.LastLoggedInDate  = DateTime.Now;
                // _userRepository.Update(user);
                // _userRepository.Commit();

                var userRoles = GetUserRoles(user.Username);
                membershipCtx.User = user;

                var identity = new GenericIdentity(user.Username);
                membershipCtx.Principal = new GenericPrincipal(
                    identity,
                    userRoles.Select(x => x.Name).ToArray());
            }
}
            catch(Exception ex){
                g.Error(ex);
            }
            return membershipCtx;
        }
        public User CreateUser(string username, string email, string password, int[] roles,string FullName)
        {
            var existingUser = _userRepository.GetSingleByUsername(username);
            try{
            if (existingUser != null)
            {
                throw new Exception("Username is already in use");
            }
            var passwordSalt = _encryptionService.CreateSalt();
            var user = new User()
            {
                Username = username,
                Salt = passwordSalt,
                Email = email,
                IsLocked = false,
                Password = _encryptionService.EncryptPassword(password, passwordSalt),
                CreatedDate = DateTime.Now,
                Firstname = FullName.Substring(0,FullName.IndexOf(" ")),
                LastName = FullName.Substring(FullName.IndexOf(" ")),    
                Image = Global.DefaultProfilePicture()        
            };

            _userRepository.Add(user);

            _userRepository.Commit();

            // if (roles != null || roles.Length > 0)
            // {
            //     foreach (var role in roles)
            //     {        
            //         addUserToRole(user, role);
            //     }
            // }
            // _userRepository.Commit();
            return user;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return new User();
            
        }

        public User GetUser(int userId)
        {
            return _userRepository.GetSingle(userId);
        }

        public List<Role> GetUserRoles(string username)
        {
            List<Role> _result = new List<Role>();

            var existingUser = _userRepository.GetSingleByUsername(username);

            if (existingUser != null)
            {
                foreach (var userRole in existingUser.Roles)
                {
                    _result.Add(userRole.Role);
                }
            }

            return _result.Distinct().ToList();
        }
        #endregion

        #region Helper methods
        private void addUserToRole(User user, int roleId)
        {
            var role = _roleRepository.GetSingle(roleId);
            if (role == null)
                return;
                //throw new Exception("Role doesn't exist.");

            var userRole = new UserRole()
            {
                RoleId = role.Id,
                UserId = user.Id
            };
            _userRoleRepository.Add(userRole);
            _userRepository.Commit();
        }

        private bool isPasswordValid(User user, string password)
        {
            return string.Equals(_encryptionService.EncryptPassword(password, user.Salt), user.Password);
        }
        private bool isUserValid(User user, string password)
        {
            if (isPasswordValid(user, password))
            {
                return !user.IsLocked;
            }

            return false;
        }

        public bool isEmailUsed(string Email){
            var _result = _userRepository.GetAll().Where(x=>x.Email == Email).SingleOrDefault();
            if(_result != null)return false;
            return true;            
        }
        #endregion
    }
}