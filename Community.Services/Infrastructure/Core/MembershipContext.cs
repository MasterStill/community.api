﻿using Community.Model.Entities.Global;
using System.Security.Principal;
namespace Community.Services.Infrastructure.Core
{
    public class MembershipContext
    {
        public IPrincipal Principal { get; set; }
        public User User { get; set; }
        public bool IsValid()
        {
            return Principal != null;
        }
    }
}
