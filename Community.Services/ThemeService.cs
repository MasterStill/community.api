﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System.Collections.Generic;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Theme;
using System.Linq;
using Community.Data.Infrastructure;
using System;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.IO.Compression;
namespace Community.Services
{
    public class ThemeService : IThemeService
    {
        public Global Global = new Global();
        private const string RenderNavigation = "@await Component.InvokeAsync(\"Navigation\", new {Template = @ViewBag.Template, Culture = @ViewBag.Culture})";
        private const string RenderCulture = "@await Component.InvokeAsync(\"Culture\", new {Template = @ViewBag.Template, Culture = @ViewBag.Culture})";
        private const string RenderBody = "@RenderBody()";
        private const string RenderCopyRightNotice = "@Html.Raw(@ViewBag.CopyrightNotice)";
        private const string RenderPageTitle = "@ViewBag.Title";
        private string[] RenderComponentStartTestViewModel = new string[]{"@using Community.Model.Entities.ViewModels;" ,
                                                    "@model IEnumerable<TestViewModel>" ,
                                                     "@if (Model.Any()){"};
        private string RenderComponentStartMenu = "@using Community.Model.Entities.ViewModels;" + System.Environment.NewLine +
                                                        "@model IEnumerable<MenuViewModel>";
        private const string RenderComponentEnd = "}";
        private string RenderBodyMiddleParts = "@if(ViewData[\"ComponentsToLoadBody\"] != null){" + System.Environment.NewLine +
                            "@foreach(var items in ViewData[\"ComponentsToLoadBody\"] as string[]){" + System.Environment.NewLine +
                                    "@await Component.InvokeAsync(\"EakDamDynamic\", new {WhatValue = items , culture= @ViewBag.Culture, template = @ViewBag.Template})" + System.Environment.NewLine +
                            "}" + System.Environment.NewLine +
                        "}" + System.Environment.NewLine;
        private string RenderPageLayoutBottomStuffs = "@RenderSection(\"scripts\", required: false)" + System.Environment.NewLine +
                                                             "@Html.Partial(\"_CultureOnTop\") " + System.Environment.NewLine +
                                                             "@Html.Partial(\"_AlwaysOnTop\") " + System.Environment.NewLine +
                                                             "@Html.Partial(\"_MatchHeight\")"
                                                             ;
        private readonly IThemeRepository _themeRepository;
        private IMapper Mapper { get; set; }
        private IAuditService _auditService { get; set; }
        private IUserService _userService;
        private CommunityContext _context;
        private IFileAndDirectoryService _fdService;
        public ThemeService(IFileAndDirectoryService _fdService, IThemeRepository _themeRepository, IMapper Mapper, IAuditService _auditService, IUserService _userService, CommunityContext _context)
        {
            this._fdService = _fdService;
            this._context = _context;
            this._userService = _userService;
            this._auditService = _auditService;
            this._themeRepository = _themeRepository;
            this.Mapper = Mapper;
        }

        public IEnumerable<ThemeViewModel> getThemes(int page, int pageSize, int CultureId, int WebsiteId)
        {
            List<ThemeViewModel> tvm = new List<ThemeViewModel>();
            string _currentTheme = _context.Websites.Where(x => x.Id == WebsiteId).SingleOrDefault().Theme;
            var allThemes = _themeRepository.GetAll().Where(x=>x.Verified == true).Where(x=>x.DispalyInPublic == true).Where(x=>x.Delflag == false).ToList();
            foreach (var items in allThemes)
            {
                ThemeViewModel t = new ThemeViewModel();
                t.Author = items.Author;
                t.CopyRightNotice = items.CopyRightNotice;
                t.Designer = items.Designer;
                t.Id = items.Id;
                t.Name = items.Name;
                t.Image = items.Image;
                t.Summary = items.Summary;

                if (items.Name.ToLower() == _currentTheme.ToLower())
                {
                    t.Selected = true;
                    //break;
                }

                t.Price = items.Price;
                tvm.Add(t);
            }

            //            tvm.Where(x=>x.Name.ToLower() == _currentTheme.ToLower()).SingleOrDefault().Selected = true;            
            return tvm;
        }

        public bool AddOrEditTheme(int UserID, ThemeCreateViewModel editedTheme)
        {
            if (editedTheme.Id > 0)
            {
                Theme oldTheme = _themeRepository.GetAll().Where(x => x.Id == editedTheme.Id).SingleOrDefault();
                Theme theme = Mapper.Map<Theme>(oldTheme);
                theme = Mapper.Map<ThemeCreateViewModel, Theme>(editedTheme, theme);
                //if(_userService.IsInRole(UserID,theme.WebsiteId,(new string[] {"Admin","Editor","Owner"}))){
                //theme.Verfied == true; 
                _themeRepository.Update(theme);
                _auditService.SaveAuditData(theme.Id, UserID, oldTheme, theme);
            }
            else
            {
                Theme newTheme = Mapper.Map<Theme>(editedTheme);
                newTheme.CreatedDate = DateTime.Now;
                newTheme.CreatedById = UserID;
                if (!_themeRepository.Duplicate(x => x.Name == newTheme.Name))
                {
                    _themeRepository.Add(newTheme);
                }
            }
            return true;
        }
        public GenericResult ThemeAddingPoriton(ThemeCreateViewModel tcvm, IHostingEnvironment _hostingEnviroment, int UserId, IFormCollection fc)
        {
            GenericResult _result = new GenericResult();
            try
            {
                tcvm = new ThemeCreateViewModel();
                if (fc.Files.Count > 1) return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Only one themes can be uploaded at a time!"
                };
                if (!fc.Files.FirstOrDefault().FileName.Contains(".zip")) return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Only Zip Files Are Supported!"
                };
                List<string> TotalFiles = new List<string>();
                string strPath = _fdService.CreateFiles(1, "Themes", fc, _hostingEnviroment, UserId).ArrayData.SingleOrDefault().Replace("http://masterstill.com/", "wwwroot\\");
                var uploadedTo = Path.Combine(_hostingEnviroment.WebRootPath, strPath);

                using (ZipArchive za = ZipFile.OpenRead(strPath))
                {
                    string _saveToPath = "C:/inetpub/wwwroot/community/themes/" + fc.Files.FirstOrDefault().FileName.Replace(".zip", "");
                    string _saveToPathAssets = "C:/inetpub/wwwroot/community/wwwroot/designassets/" + fc.Files.FirstOrDefault().FileName.Replace(".zip", "");
                    Directory.CreateDirectory(_saveToPath);
                    Directory.CreateDirectory(_saveToPathAssets);
                    Directory.CreateDirectory(_saveToPath + "/Components");
                    foreach (ZipArchiveEntry zaItem in za.Entries)
                    {
                        TotalFiles.Add(zaItem.FullName);
                    }
                    _result = ValidFiles(TotalFiles);
                    if (_result.Succeeded)
                    {
                        foreach (ZipArchiveEntry zaItem in za.Entries)
                        {
                            if (zaItem.Length == 0) // Folder Create It!
                            {
                                if (zaItem.FullName.Contains("assets/"))
                                {
                                    Directory.CreateDirectory(_saveToPathAssets + "/" + zaItem.FullName);
                                }
                                else
                                {
                                    Directory.CreateDirectory(_saveToPath + "/" + zaItem.FullName);
                                }
                            }
                            else
                            {
                                if (zaItem.FullName.Contains("assets/"))
                                {
                                    zaItem.ExtractToFile(_saveToPathAssets + "/" + zaItem.FullName);
                                }
                                else
                                {
                                    zaItem.ExtractToFile(_saveToPath + "/" + zaItem.FullName.Replace(".html", ".cshtml"));
                                }
                            }
                        }
                        // Process for other Objective // easy banauna lai
                        string FileNAme = fc.Files.SingleOrDefault().FileName.ToLower().Replace(".zip", "");
                        foreach (var files in validFileList().Where(x => x.ToLower().Contains(".html")))
                        {
                            ProceedFurther(_saveToPath + "/" + files.Replace(".html", ".cshtml"), FileNAme);
                        }
                        tcvm.Name = FileNAme;
                        tcvm.Price = 0;
                        tcvm.CultureId = 1;
                        AddOrEditTheme(UserId, tcvm);
                    }
                }
            }
            catch (Exception ex)
            {
                _result = Global.Error(ex);
            }
            return _result;
        }
        public GenericResult ProceedFurther(string FileName, string TemplateName)
        {
            var codes = System.IO.File.ReadAllLines(FileName);
            List<string> newCodes = new List<string>();
            newCodes.Add(replacefiledataStart(FileName));
            foreach (var items in codes)
            {
                string srtLineStringToChange = items;
                if (srtLineStringToChange.ToLower().Contains("src=\"assets/"))
                {
                    srtLineStringToChange = srtLineStringToChange.Replace("src=\"assets/", "src=\"~/DesignAssets/" + TemplateName + "/assets/");
                }
                if (srtLineStringToChange.ToLower().Contains("href=\"assets/"))
                {
                    srtLineStringToChange = srtLineStringToChange.Replace("href=\"assets/", "href=\"~/DesignAssets/" + TemplateName + "/assets/");
                }
                if (srtLineStringToChange.Contains("{{") || srtLineStringToChange.ToLower().Contains("</body>"))
                {
                    newCodes.Add(ReplaceProperData(srtLineStringToChange));
                }
                else
                {
                    newCodes.Add(srtLineStringToChange);
                }
            }
            newCodes.Add(replacefiledataEnd(FileName));
            var logPath = FileName;//System.IO.Path.GetTempFileName();
            var logFile = System.IO.File.Create(logPath);
            var logWriter = new System.IO.StreamWriter(logFile);
            foreach (var items in newCodes)
            {
                logWriter.WriteLine(items);
            }
            logWriter.Dispose();

            return new GenericResult();
        }
        public string replacefiledataStart(string filename)
        {
            string strToSend = string.Empty;
            if (filename.ToLower().Contains("singletype.cshtml"))
            {
                return "@using Community.Model.Entities.ViewModels" + System.Environment.NewLine +
                        "@model BlogPostSingleViewModel";
            }
            if (filename.ToLower().Contains("component1.cshtml") || filename.ToLower().Contains("slider.cshtml"))
            {
                foreach (var items in RenderComponentStartTestViewModel)
                {
                    strToSend += items + System.Environment.NewLine;
                }
            }
            if (filename.ToLower().Contains("navigation.cshtml"))
            {
                return RenderComponentStartMenu;
            }
            return null;
        }
        public string replacefiledataEnd(string filename)
        {
            if (filename.ToLower().Contains("component1.cshtml") || filename.ToLower().Contains("slider.cshtml"))
            {
                return "}";
            }
            // if(filename.ToLower().Contains("navigation.cshtml")){
            //     return RenderComponentStartMenu;
            // }
            return null;
        }
        public string ReplaceProperData(string ForWhat)
        {
            ForWhat = ForWhat.ToLower();
            if (ForWhat.IndexOf("{{websitelogourl}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{websitelogourl}}", "http://@ViewBag.WebsiteName/@ViewBag.Culture");
            }
            if (ForWhat.IndexOf("{{pagetitle}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{pagetitle}}", RenderPageTitle);
            }
            if (ForWhat.IndexOf("{{body}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{body}}", RenderBody);
            }
            if (ForWhat.IndexOf("{{navigation}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{navigation}}", RenderNavigation);
            }
            if (ForWhat.IndexOf("{{culture}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{culture}}", RenderCulture);
            }
            if (ForWhat.IndexOf("{{copyrightnotice}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{copyrightnotice}}", RenderCopyRightNotice);
            }
            if (ForWhat.IndexOf("{{bodymiddlepartstart}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodymiddlepartstart}}", "@if(ViewData[\"ComponentsToLoadBody\"] != null){");
            }
            if (ForWhat.IndexOf("{{bodymiddleparts}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodymiddleparts}}", RenderBodyMiddleParts);
            }
            if (ForWhat.IndexOf("{{bodymiddlepartend}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodymiddlepartend}}", "}");
            }
            if (ForWhat.IndexOf("{{bodyrightsideparts}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodyrightsideparts}}", RenderBodyMiddleParts.Replace("ComponentsToLoadBody", "ComponentsToLoadBodyRightSidePanel"));
            }
            if (ForWhat.IndexOf("{{bodyleftsideparts}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodyleftsideparts}}", RenderBodyMiddleParts.Replace("ComponentsToLoadBody", "ComponentsToLoadBodyLeftSidePanel"));
            }
            if (ForWhat.IndexOf("{{bodyleftpartstart}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodyleftpartstart}}", "@if(ViewData[\"ComponentsToLoadBodyLeftSidePanel\"] != null){");
            }
            if (ForWhat.IndexOf("{{bodyleftpartend}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodyleftpartend}}", "}");
            }
            if (ForWhat.IndexOf("{{bodyrightpartstart}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodyrightpartstartv}}", "@if(ViewData[\"ComponentsToLoadBodyRightSidePanel\"] != null){");
            }
            if (ForWhat.IndexOf("{{bodyrightpartend}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{bodyrightpartend}}", "}");
            }
            if (ForWhat.IndexOf("{{title}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{title}}", "@Model.Title");
            }
            if (ForWhat.IndexOf("{{image}}") >= 0)
            {
                string ImageUrl = "@if(Model.ImageUrl != null){" + System.Environment.NewLine +
                                "@if(!Model.ImageUrl.ToLower().Contains(\".\")){" + System.Environment.NewLine +
                                "	<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/@Model.ImageUrl\" frameborder=\"0\" allowfullscreen></iframe>" + System.Environment.NewLine +
                                "}else" + System.Environment.NewLine +
                                "{" + System.Environment.NewLine +
                                "	<img src=\"@Model.ImageUrl\">" + System.Environment.NewLine +
                                "}" + System.Environment.NewLine +
                            "}" + System.Environment.NewLine;
                ForWhat = ForWhat.Replace("{{image}}", ImageUrl);
            }

            if (ForWhat.IndexOf("{{tagurl}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{tagurl}}", "~/@Model.Tag.URL");
            }
            if (ForWhat.IndexOf("{{tagname}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{tagname}}", "@Model.Tag.Name");
            }
            if (ForWhat.IndexOf("{{content}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{content}}", "@Html.Raw(Model.Content)");
            }
            if (ForWhat.IndexOf("{{comment}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{comment}}", "@if(Model.CommentAllowed){@Html.Partial(\"Disq\")}");
            }
            if (ForWhat.IndexOf("{{breadcrumbtext}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{breadcrumbtext}}", "@ViewBag.BreadCrumbText");
            }
            if (ForWhat.IndexOf("{{loopstart}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopstart}}", "@foreach(var item in Model){");
            }
            if (ForWhat.IndexOf("{{loopend}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopend}}", "}");
            }
            if (ForWhat.IndexOf("{{loopcreateddate}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopcreateddate}}", "@item.CreatedDate");
            }
            if (ForWhat.IndexOf("{{loopurl}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopurl}}", "@item.URL");
            }
            if (ForWhat.IndexOf("{{loopthumbnail}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopthumbnail}}", "@item.ThumbnailUrl");
            }
            if (ForWhat.IndexOf("{{looptitle}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{looptitle}}", "@item.Title");
            }
            if (ForWhat.IndexOf("{{loopsummary}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopsummary}}", "@item.Summary");
            }
            if (ForWhat.IndexOf("{{loopreadmorestart}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopreadmorestart}}", "@if((ViewBag.ReadMore != null) && (ViewBag.ReadMore != \"\")){");
            }
            if (ForWhat.IndexOf("{{loopreadmoreend}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopreadmoreend}}", "}");
            }
            if (ForWhat.IndexOf("{{loopreadmore}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopreadmore}}", "@Html.Raw(ViewBag.ReadMore)");
            }
            if (ForWhat.IndexOf("{{loopsliderimage}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{loopsliderimage}}", "@item.Image");
            }
            if (ForWhat.IndexOf("{{websitelogo}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{websitelogo}}", "@ViewBag.WebsiteLogo");
            }
            if (ForWhat.IndexOf("{{menuname}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{menuname}}", "@item.Name");
            }
            if (ForWhat.IndexOf("{{menuurl}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{menuurl}}", "~/@item.MenuUrl");
            }
            if (ForWhat.IndexOf("{{gotohome}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{gotohome}}", "http://@ViewBag.WebsiteName/@ViewBag.Culture");
            }
            if (ForWhat.IndexOf("</body>") >= 0)
            {
                ForWhat = ForWhat.Replace("</body>", "</body>" + RenderPageLayoutBottomStuffs);
            }
            if (ForWhat.IndexOf("{{componentheadertext}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{componentheadertext}}", "@Html.Raw(ViewBag.HeaderText)");
            }
            if (ForWhat.IndexOf("{{componentfootertext}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{componentfootertext}}", "@Html.Raw(ViewBag.Footertext)");
            }
            if (ForWhat.IndexOf("{{componentsubheadertext}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{componentsubheadertext}}", "@Html.Raw(ViewBag.SubHeaderText)");
            }
            if (ForWhat.IndexOf("{{buttontext}}") >= 0)
            {
                ForWhat = "@if (@ViewBag.ButtonText != null){" + ForWhat.Replace("{{componentbuttontext}}", "@Html.Raw(ViewBag.ButtonText)" + "}");
            }
            if (ForWhat.IndexOf("{{componentbuttonurl}}") >= 0)
            {
                ForWhat = ForWhat.Replace("{{componentbuttonurl}}", "@ViewBag.ButtonUrl");
            }
            if (ForWhat.IndexOf("componentimage") >= 0)
            {
                ForWhat = ForWhat.Replace("{{componentimage}}", "@ViewBag.Image");
            }
            if (ForWhat.IndexOf("componentconstructionyoutubeurl") >= 0)
            {
                ForWhat = ForWhat.Replace("{{componentconstructionyoutubeurl}}", "@if(@ViewBag.YouTubeUrl != null && @ViewBag.YouTubeUrl != \"\"){" + System.Environment.NewLine +
                                    "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/@ViewBag.YouTubeUrl\" frameborder=\"0\" allowfullscreen></iframe>" + System.Environment.NewLine +
                                    "}" + System.Environment.NewLine);
            }
            if (ForWhat.IndexOf("componentconstructionimage") >= 0)
            {
                ForWhat = ForWhat.Replace("{{componentconstructionimage}}", "@if(@ViewBag.Image != null && @ViewBag.Image != \"\"){" + System.Environment.NewLine +
                                    "<img src=\"@ViewBag.Image\" width=\"560\" height=\"315\"/>" + System.Environment.NewLine +
                                    "}" + System.Environment.NewLine);
            }
            return ForWhat;
        }
        private List<string> validFileList()
        {
            List<string> ValidFiles = new List<string>();
            ValidFiles.Add("Index.html");
            ValidFiles.Add("Layout.html");
            ValidFiles.Add("List.html");
            ValidFiles.Add("SingleType.html");
            ValidFiles.Add("Construction.html");
            ValidFiles.Add("Components/");
            ValidFiles.Add("Components/Component1.html");
            ValidFiles.Add("Components/Navigation.html");
            ValidFiles.Add("Components/Slider.html");
            ValidFiles.Add("Components/TopIntroduction.html");
            return ValidFiles;
        }
        private GenericResult ValidFiles(List<string> fileNames)
        {
            List<string> ValidFiles = validFileList();
            foreach (var items in fileNames)
            {
                bool valid = false;
                foreach (var tocheckItems in ValidFiles)
                {
                    if (tocheckItems.ToLower() == items.ToLower() || tocheckItems.Contains(".css")
                        || tocheckItems.Contains(".js")
                        || tocheckItems.Contains(".jpg")
                        || tocheckItems.Contains(".png")
                        || tocheckItems.Contains(".gif")
                        || tocheckItems.Contains(".eot")
                        || tocheckItems.Contains(".svg")
                        || tocheckItems.Contains(".ttf")
                        || tocheckItems.Contains(".woff")
                        || tocheckItems.Contains(".woff2")
                        || tocheckItems.Contains(".html")

                    )
                    {
                        valid = true;
                        //ValidFiles.Remove(tocheckItems);
                        break;
                    }
                }
                if (valid == false) return Global.Error(null,"Error : Files Not Valid");
            }
            if (ValidFiles.Where(x => x.Contains(".cshtml")).Count() > fileNames.Where(x => x.Contains(".cshtml")).Count())
            {
                var MissingFiles = ValidFiles.Except(fileNames);
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Mandatory File / Files Missing",
                    ArrayData = MissingFiles.ToArray()
                };
                //return Global.ErrorSettingsnotFound("Error : All Files Not Found");
            }
            return new GenericResult()
            {
                Succeeded = true
            };
        }
    }
}