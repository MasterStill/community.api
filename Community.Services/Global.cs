﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Security.Claims;
using System.Collections.Generic;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Community.Services.Abstract;

namespace Community.Services
{
    public class MultilingualAI
    {
        public List<int> idToRemove { get; set; }
        public IEnumerable<MultiLingualViewModel> items { get; set; }
    }
    public class Global
    {
        // private IErrorService _errorService;
        // public Global(IErrorService _errorService){
        //     this._errorService = _errorService;
        // }
        public static GenericResult AccessDenied()
        {
            return new GenericResult()
            {
                Succeeded = false,
                Message = "Permission Denied."
            };
        }
        public static string DefaultProfilePicture()
        {
            return "http://masterstill.com/Images/no-thumb.jpg";
        }
        public static string DefaultLogo()
        {
            return "http://masterstill.com/Images/Rocklogo.png";
        }
        public GenericResult Error(Exception ex = null, string Message = null, string data = null, string[] arrayData = null)
        {
            if (ex != null)
            {
                string FileName = "C:/Errors/" + DateTime.Now.ToString().Replace(":", "").Replace(" ", "_").Replace("/", "-") + ".txt";
                var logPath = FileName;//System.IO.Path.GetTempFileName();
                var logFile = System.IO.File.Create(logPath);
                var logWriter = new System.IO.StreamWriter(logFile);
                List<string> newCodes = new List<string>();
                newCodes.Add("Exception" + ex);
                newCodes.Add("Exception Message" + ex.Message);
                // newCodes.Add("InnerException Source : " + ex.InnerException.Source);
                // newCodes.Add("InnerException Data : " + ex.InnerException.Data);
                newCodes.Add("Execption Source : " + ex.Source);
                newCodes.Add("Execption HelpLink : " + ex.HelpLink);
                newCodes.Add("Execption Stack Trace : " + ex.StackTrace);

                foreach (var items in newCodes)
                {
                    logWriter.WriteLine(items);
                }
                logWriter.Dispose();
            }

            return new GenericResult()
            {
                Succeeded = false,
                Message = Message,
                ArrayData = arrayData,
                Data = data
            };
        }
        public static GenericResult Succeeded(string SuccesMessage)
        {
            return new GenericResult()
            {
                Succeeded = true,
                Message = "Success: " + SuccesMessage
            };
        }
        public static int CacheTime()
        {
            return 10;
        }

        public static string SaveAndGetImage(IFormFile file, string path, IHostingEnvironment _hostingEnvironment, string fileNameToSave)
        {
            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, path);
            Directory.CreateDirectory(uploads);
            string NewfileName = GetFileName(file);
            int lastPart = GetFileName(file).LastIndexOf(".");
            string FirstPart = NewfileName.Substring(0, lastPart);
            int intRemoveFirstPartTrailing = FirstPart.LastIndexOf("-");
            if (intRemoveFirstPartTrailing > 0) FirstPart = FirstPart.Substring(0, intRemoveFirstPartTrailing);
            string LastPart = NewfileName.Substring(lastPart + 1);
            if (fileNameToSave.Length > 0)
            { // Predefined Filename should be saved.
                NewfileName = string.Format("{0}.{1}", fileNameToSave, LastPart);
                goto CheckPoint;
            }
            if (System.IO.File.Exists(uploads + "/" + NewfileName))
            {
                int counter = 0;
                do
                {
                    counter += 1;
                    NewfileName = string.Format("{0}-{1}.{2}", FirstPart, counter.ToString(), LastPart);
                } while (System.IO.File.Exists(uploads + "/" + NewfileName));
            }
        CheckPoint:
            using (var fileStream = new FileStream(Path.Combine(uploads, NewfileName), FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            return (path + '/' + NewfileName);
        }
        // private static string ChangeFileName(string FileName){
        //     return NewMethod1() +FileName;
        // }
        private static string GetFileName(IFormFile file)
        {
            return file.FileName;
        }
        private static string NewMethod1()
        {
            return NewMethod().Replace("-", "_");
        }
        private static string NewMethod()
        {
            return Guid.NewGuid().ToString();
        }
        public int DefaultWebsiteSize()
        {
            return 50;
        }
        //   public FileAndDirectory GetDirectoriesandFiles(string website,string filetype=".jpg",string Path=""){
        //         FileAndDirectory fd = new FileAndDirectory();
        //         List<string> Files = new List<string>();
        //         List<string> Directories = new List<string>();
        //         string fixedPath = "c:/inetpub/wwwroot/community/wwwroot/";
        //         string path =  fixedPath + "UserUploads/" + website + "/" +  Path;
        //         try{
        //             foreach (string file in Directory.EnumerateFiles(path,filetype,SearchOption.TopDirectoryOnly))
        //             {
        //                 Files.Add(file.Replace(fixedPath,""));          
        //             }
        //         }
        //         catch{}
        //         try{
        //             foreach (string directory in Directory.EnumerateDirectories(path,"*",SearchOption.TopDirectoryOnly))
        //             {
        //                 Directories.Add(directory.Replace(fixedPath,""));
        //             }
        //         }
        //         catch{}
        //         fd.Files = Files ;
        //         fd.Directories = Directories ;
        //         return fd;
        //     }
    }
    public class GlobalUser
    {
        public int UserIdFromJWT(ClaimsPrincipal ci)
        {
            var identity = ci.Identity as ClaimsIdentity;
            var claims = from c in identity.Claims
                         select new
                         {
                             subject = c.Subject.Name,
                             type = c.Type,
                             value = c.Value
                         };
            var UserId = claims.Where(x => x.type.Contains("nameidentifier")).Select(x => x.value).SingleOrDefault();
            return Int32.Parse(UserId);
        }
    }    // public static class RockmanduHtmlRemoval
    // {
    //     public static string StripTagsRegex(string source)
    //     {
    //         return Regex.Replace(source, "<.*?>", string.Empty);
    //     }
    //     static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
    //     public static string StripTagsRegexCompiled(string source)
    //     {
    //         return _htmlRegex.Replace(source, string.Empty);
    //     }
    //     public static string StripTagsCharArray(string source)
    //     {
    //         char[] array = new char[source.Length];
    //         int arrayIndex = 0;
    //         bool inside = false;    //         for (int i = 0; i < source.Length; i++)
    //         {
    //             char let = source[i];
    //             if (let == '<')
    //             {
    //                 inside = true;
    //                 continue;
    //             }
    //             if (let == '>')
    //             {
    //                 inside = false;
    //                 continue;
    //             }
    //             if (!inside)
    //             {
    //                 array[arrayIndex] = let;
    //                 arrayIndex++;
    //             }
    //         }
    //         return new string(array, 0, arrayIndex);
    //     }
    // }
    public class MultiLingualGlobal
    {
        public MultilingualAI GenericMultiLingualRepo(IEnumerable<MultiLingualViewModel> multiObject, List<int> _userKnownCulture, int CultureId)
        {
            List<int> convertedIDs = new List<int>();
            IEnumerable<MultiLingualViewModel> objToCheck = multiObject;
            foreach (var items in objToCheck)
            {
                if (items.CultureId == CultureId)
                {
                    items.Converted = true;
                    if (items.MultiLingualId != items.Id)
                    {
                        convertedIDs.Add(items.MultiLingualId);
                    }
                }
            }
            foreach (var items in objToCheck.Where(x => x.Converted == false))
            {
                bool knownLanguageByUser = false;
                foreach (var knownCulture in _userKnownCulture)
                {
                    if (items.CultureId == knownCulture)
                    {
                        foreach (var sknownCulture in _userKnownCulture)
                        {
                            if (sknownCulture == CultureId)
                            {
                                knownLanguageByUser = true;
                                break;
                            }
                        }
                    }
                }
                foreach (var convertedItems in objToCheck.Where(x => x.Converted == true))
                {
                    if (convertedItems.MultiLingualId == items.MultiLingualId)
                    {
                        convertedIDs.Add(items.Id);
                    }
                }
                if (!knownLanguageByUser)
                {
                    convertedIDs.Add(items.Id);
                }
            }
            IEnumerable<MultiLingualViewModel> results = from item in objToCheck
                                                         where !convertedIDs.Contains(item.Id)
                                                         //where _userKnownCulture.Contains(item.CultureId)
                                                         select item;
            MultilingualAI test = new MultilingualAI();
            test.items = results;
            test.idToRemove = convertedIDs; return test;
        }
    }
}