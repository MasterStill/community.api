﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Community.Services.Diff;
using Community.Model.Entities.Audit;
using System.Xml.Serialization;
//using KellermanSoftware.CompareNetObjects;
using System.Reflection;
//using KellermanSoftware.CompareNetObjects;
namespace Community.Services
{  
    public class AuditService : IAuditService
    {
        private readonly IAuditRepository _auditRepository;
        private IMapper Mapper { get; set; }
        public AuditService(IMapper Mapper,IAuditRepository _auditRepository)
        {
            this._auditRepository = _auditRepository;
            this.Mapper = Mapper;
        }

        //All Websites by User
         public Boolean SaveAuditData(int libId,int userid,object oldObject, object newObject)
            {
                AuditLog rockAuditLog = new AuditLog();
                rockAuditLog = AuditTrail(AuditActionType.Update, libId, oldObject, newObject, userid);
                return false;
            }


             public  AuditLog AuditTrail(AuditActionType Action, int KeyFieldID, Object OldObject, Object NewObject,int UserId){
                 var ObjectName =  OldObject.ToString();//.Replace("Community.Model.Entities.","");
                 //Console.WriteLine("Yestko Name Ho Yo " + OldObject.ToString());
                 var oOldRecord = OldObject;
                 var oNewRecord = NewObject;
                 var oType = oOldRecord.GetType();
                 List<AuditDelta> deltaList = new List<AuditDelta>();
                  foreach (var oProperty in oType.GetProperties())
                    {
                        var oOldValue = oProperty.GetValue(oOldRecord, null);
                        var oNewValue = oProperty.GetValue(oNewRecord, null);
                        // this will handle the scenario where either value is null
                        if (!object.Equals(oOldValue, oNewValue))
                        {
                            // Handle the display values when the underlying value is null
                            var sOldValue = oOldValue == null ? "null" : oOldValue.ToString();
                            var sNewValue = oNewValue == null ? "null" : oNewValue.ToString();
                            AuditDelta delta = new AuditDelta();
                            delta.FieldName = oProperty.Name;
                            delta.ValueBefore = sOldValue;
                            delta.ValueAfter =  sNewValue;
                            deltaList.Add(delta);
                             AuditLog audit = new AuditLog();
                             audit.KeyName = oProperty.Name;
                             audit.CreatedById = UserId;   
                             audit.DataModel = ObjectName;
                             audit.AuditActionTypeENUM = (int)Action;
                             audit.CreatedDate = DateTime.Now;
                             audit.KeyFieldID = KeyFieldID;  
                             audit.ValueBefore = sOldValue; //JsonConvert.SerializeObject(OldObject);
                             audit.ValueAfter = sNewValue; //JsonConvert.SerializeObject(NewObject);
                             _auditRepository.Add(audit);
                            //System.Diagnostics.Debug.WriteLine("Property " + oProperty.Name + " was: " + sOldValue + "; is: " + sNewValue);
                        }
                    }
                    _auditRepository.Commit();
                    // audit.Changes = JsonConvert.SerializeObject(deltaList);
                    return new AuditLog();
             }
            // public static  AuditLog AuditTrail(AuditActionType Action, int KeyFieldID, Object OldObject, Object NewObject,int UserId)
            // {
            // // get the differance
            // CompareLogic compObjects = new CompareLogic();
            // compObjects.Config.MaxDifferences = 99;
            // ComparisonResult compResult = compObjects.Compare(OldObject, NewObject);
            // List<AuditDelta> deltaList = new List<AuditDelta>();
            // foreach (var change in compResult.Differences)
            // {
            //     AuditDelta delta = new AuditDelta();
            //     if (change.PropertyName.Substring(0, 1) == ".")
            //     delta.FieldName = change.PropertyName.Substring(1, change.PropertyName.Length - 1);
            //     delta.ValueBefore = change.Object1Value;
            //     delta.ValueAfter = change.Object2Value;
            //     deltaList.Add(delta);
            // }
            // AuditLog audit = new AuditLog();
            // audit.AuditActionTypeENUM = (int)Action;
            // //audit.DataModel = this.GetType().Name;
            // audit.CreatedDate = DateTime.Now;
            // audit.KeyFieldID = KeyFieldID;
            // audit.ValueBefore = JsonConvert.SerializeObject(OldObject);
            // audit.CreatedById = UserId;
            // audit.ValueAfter = JsonConvert.SerializeObject(NewObject);
            // audit.Changes = JsonConvert.SerializeObject(deltaList);
            // return audit;
            // }
        public List<AuditChange> GetAudit(int Id,string strObject)
        {
            List<AuditChange> rslt = new List<AuditChange>();
            var AuditTrail = _auditRepository.GetAll().Where(s => s.KeyFieldID == Id).Where(x=>x.DataModel == strObject).OrderBy(s => s.Id);
           // var serializer = new XmlSerializer(typeof(AuditDelta));
            foreach (var record in AuditTrail)
            {
                AuditChange Change = new AuditChange();
                Change.DateTimeStamp = record.CreatedDate.ToString();
                //Change.AuditActionType = (AuditActionType)record.AuditActionTypeENUM;
                //Change.AuditActionTypeName = Enum.GetName(typeof(AuditActionType), record.AuditActionTypeENUM);
                Change.ValueBefore = record.ValueBefore;
                Change.ValueAfter = record.ValueAfter;
                Change.KeyName = record.KeyName;
                Change.Id = record.Id;
                RockDiff rd = new RockDiff(record.ValueBefore,record.ValueAfter);
                Change.Difference =rd.Build();
                // List<AuditDelta> delta = new List<AuditDelta>();
                // delta = JsonConvert.DeserializeObject<List<AuditDelta>>(record.Changes);
                // Change.Changes.AddRange(delta);
                // foreach (var items in Change.Changes)
                // {
                //     RockDiff rd = new RockDiff(items.ValueBefore,items.ValueAfter);
                //     items.Difference = rd.Build();
                //     if (items.ValueBefore.Length > 100)
                //     {
                //         items.ValueBefore = HtmlRemoval.StripTagsCharArray(items.ValueBefore);
                //         items.ValueBefore = String.Concat(items.ValueBefore.Substring(0,100),"...");
                //     }
                //     if (items.ValueAfter.Length > 100)
                //     {
                //         items.ValueAfter = HtmlRemoval.StripTagsCharArray(items.ValueAfter);
                //         items.ValueAfter = String.Concat(items.ValueAfter.Substring(0, 100), "...");
                //     }
                // }
                rslt.Add(Change);
            }
            return rslt;
        }
    }
    public static class HtmlRemoval
    {
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}
