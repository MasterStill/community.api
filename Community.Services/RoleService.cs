﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.Global;
using Community.Model.Entities;
namespace Community.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private IMapper Mapper { get; set; }
        public RoleService(IMapper Mapper,IRoleRepository _roleRepository)
        {
            this._roleRepository = _roleRepository;
            this.Mapper = Mapper;
        }

        //Create Role
        public GenericResult CreateRole(int UserId,Role role)
        {
            role.CreatedById = UserId;
            if(DuplicateRole(role)){
                 return new GenericResult()
                    {
                        Succeeded = false,
                        Message = "Error : Role Already Exist.",
                    };
                 }
            try
            {
                _roleRepository.Add(role);
                _roleRepository.Commit();
                 return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : " + role.Name + " Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : Role could not be Created." + ex.Message 
                };   
            }
        }
        public bool DuplicateRole(Role bc){
            int category = _roleRepository.GetAll().Where(x=>x.CreatedById == bc.CreatedById).Where(x=>x.Name.ToString().ToLower() == bc.Name.ToLower()).Where(x=>x.Delflag == false).Count();  
            if (category > 0 ){
                return true; 
            }
            return false;
        }
    }
}