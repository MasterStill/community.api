﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Community.Model.Entities.Globalization;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Community.Services
{
    public class CultureService : ICultureService
    {
        private readonly ICultureRepository _cultureRepository;
        private IMapper Mapper { get; set; }
        private CommunityContext _context;
        public CultureService(CommunityContext _context, IMapper Mapper, ICultureRepository _cultureRepository)
        {
            this._context = _context;
            this._cultureRepository = _cultureRepository;
            this.Mapper = Mapper;
        }
        public List<Culture> GetCultures()
        {
            return _cultureRepository.GetAll().ToList().OrderBy(x => x.Id).ToList();
        }

        public int GetCultureId(string CultureName)
        {
            return _cultureRepository.GetSingle(x => x.Title.ToLower() == CultureName.ToLower()).Id;
        }
        public List<CultureViewModel> GetCultures(int CultureId)
        {
            List<CultureViewModel> culture = new List<CultureViewModel>();
            var neededCulture = GetCultures().Where(x => x.CultureId == CultureId);
            foreach (var item in neededCulture)
            {
                CultureViewModel cvm = new CultureViewModel()
                {
                    Id = item.MultiLingualId,
                    Title = item.Title
                };
                culture.Add(cvm);
            }
            return culture.OrderBy(x => x.Id).ToList();
        }

        public List<CultureViewModel> GetCulturesInCultureLanguage()
        {
            List<CultureViewModel> culture = new List<CultureViewModel>();
            var neededCulture = GetCultures().Where(x => x.MultiLingualId == x.CultureId).Where(x=>x.Verified == true);
            foreach (var item in neededCulture)
            {
                CultureViewModel cvm = new CultureViewModel()
                {
                    Id = item.MultiLingualId,
                    Title = item.Title
                };
                culture.Add(cvm);
            }
            return culture.OrderBy(x => x.Id).ToList();
        }

        public List<CultureViewModel> GetCultures(int WebsiteId, int CultureId)
        {
            List<CultureViewModel> culture = new List<CultureViewModel>();
            List<int> cultureToInclue = new List<int>();
            var _websiteCulture = _context.Websites.Include(x => x.Culture)
                .Where(x => x.Id == WebsiteId).Select(x => x.Culture);
            foreach (var items in _websiteCulture)
            {
                foreach (var item in items)
                {
                    cultureToInclue.Add(item.CultureId);
                };
            }
            //var neededCulture = GetCultures().Where(x => x.CultureId == CultureId);
            var neededCulture = GetCultures().Where(x => x.MultiLingualId == x.CultureId);
            neededCulture = from item in neededCulture
                            where cultureToInclue.Contains(item.MultiLingualId)
                            select item;
            foreach (var item in neededCulture)
            {
                CultureViewModel cvm = new CultureViewModel()
                {
                    Id = item.MultiLingualId,
                    Title = item.Title,
                    URL = _context.WebsiteCulture.Where(x => x.CultureId == item.MultiLingualId).Select(x => x.Culture.Title).FirstOrDefault()
                };
                culture.Add(cvm);
            }
            return culture.OrderBy(x => x.Id).ToList();
        }

        public GenericResult CreateCulture(int UserId, Culture culture)
        {
            culture.CreatedById = UserId;
            if (DuplicateCulture(culture))
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : Culture Already Exist.",
                };
            }
            try
            {
                _cultureRepository.Add(culture);
                _cultureRepository.Commit();
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : " + culture.Title + " Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : Culture could not be Created." + ex.Message
                };
            }
        }

        public bool DuplicateCulture(Culture bc)
        {
            int culture = _cultureRepository.GetAll().Where(x => x.Title.ToString().ToLower() == bc.Title.ToLower()).Where(x => x.Delflag == false).Where(x => x.CultureId == bc.CultureId).Count();
            if (culture > 0)
            {
                return true;
            }
            return false;
        }
    }
}