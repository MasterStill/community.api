﻿using Community.Services.Abstract;
using Community.Data.Infrastructure;
using System;
namespace Community.Services
{
    public class ErrorService : IErrorService
    {
       public CommunityContext _context;
        public ErrorService(CommunityContext _context)
        {
            this._context = _context;            
        }
      public void Error(Exception ex){
          Community.Model.Entities.Global.Error error  =new Community.Model.Entities.Global.Error();
          error.Message = ex.Message;
          error.StackTrace = ex.StackTrace;          
          _context.Errors.Add(error);
          _context.SaveChanges();
      }
    }
}