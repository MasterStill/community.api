﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System.Collections.Generic;
using AutoMapper;
using Community.Model.Entities.Global;

using Community.Data.Infrastructure;
using System;
using System.Linq;
using Community.Model.Entities.ViewModels;

namespace Community.Services
{
    public class GlobalizationService : IGlobalizationService
    {        
        private IMapper Mapper { get; set; }
        private Global Global = new Global();
        IGlobalizationRepository _globalizationRepository;        
        public GlobalizationService(IMapper Mapper, IGlobalizationRepository _globalizationRepository, CommunityContext _context)
        {
            this.Mapper = Mapper;            
            this._globalizationRepository = _globalizationRepository;
        }
        public List<StaticGlobalization> Step(int StepId)
        {
            return new List<StaticGlobalization>();
        }
        public List<StepsViewModel> ForNewUseRegistration(int Culture)
        {
            List<StaticGlobalization> _result = _globalizationRepository.GetAll().Where(x => x.CultureId == Culture).ToList();
            List<StepsViewModel> SVM = new List<StepsViewModel>();
            //int Step = 0;
            int TotalStep = _result.Max(x=>x.step.Value);
            for(int i=0;i<=TotalStep;i++){
            // foreach (var items in _result.Where(x => x.step == i))
            //     {
                    
                    StepsViewModel svm = new StepsViewModel();
                    svm.Step = i;
                    svm.Values = Mapper.Map<List<GlobalizationViewModel>>(_result.Where(x => x.step == i).ToList());
                    SVM.Add(svm);

                // }
            }
            return SVM;            
        }
        public GenericResult Add(StaticGlobalization sg)
        {
            try
            {
                _globalizationRepository.Add(sg);
                return Global.Succeeded("Added");
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }
        public StaticGlobalization GlobalizationbyKey(string Key)
        {
            return new StaticGlobalization();
        }
    }
}