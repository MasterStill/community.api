﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Album.Photo;
using Community.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
namespace Community.Services
{
    public class SliderService : ISliderService
    {
        private IUserService _userService;
        private IMapper _mapper { get; set; }
        private IWebsiteService _websiteService;
        private ISliderRepository _sliderRepository;
        public SliderService(ISliderRepository _sliderRepository,IMapper _mapper, IUserService _userService, IWebsiteService _websiteService)
        {
            this._sliderRepository = _sliderRepository;
            this._websiteService = _websiteService;
            this._mapper = _mapper;
            this._userService = _userService;
            //this._roleReposistory = _roleReposistory;
        }
        // All Photos by User
        public List<SliderViewModel> CheckIfImageIsUsed(string ImagePath){
              var _result = _sliderRepository.GetAll().Where(x=>x.Delflag == false).Where(x=>x.Image == ImagePath || x.BackgroundImage==ImagePath).ToList();
              return _mapper.Map<List<SliderViewModel>>(_result);
          }
    }
}