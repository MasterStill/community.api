﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Model.Entities.Album.Photo;
using Community.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Community.Services
{
    public class PhotoAlbumService : IPhotoAlbumService
    {
        private const string path = "Sadfasdf";
        private readonly IPhotoRepository photoRepository;
        private readonly IPhotoAlbumRepository photoAlbumRepository;
        private readonly IPhotoAlbumCategoryRepository photoAlbumCategoryRepository;
        private IUserService _userService;
        //private IRoleRepository _roleReposistory
        //private static IHostingEnvironment _hostingEnvironment;
        private IMapper Mapper { get; set; }
        private IWebsiteService _websiteService;
        private CommunityContext _context;
        public PhotoAlbumService(CommunityContext _context, IPhotoRepository photoRepository, IPhotoAlbumRepository photoAlbumRepository, IMapper Mapper, IPhotoAlbumCategoryRepository photoAlbumCategoryRepository, IUserService _userService, IWebsiteService _websiteService)
        {
            this._context = _context;
            //_hostingEnvironment = hostingEnvironment;
            this._websiteService = _websiteService;
            this.photoRepository = photoRepository;
            this.photoAlbumRepository = photoAlbumRepository;
            this.photoAlbumCategoryRepository = photoAlbumCategoryRepository;
            this.Mapper = Mapper;
            this._userService = _userService;
            //this._roleReposistory = _roleReposistory;
        }
        // All Photos by User
        public IEnumerable<PhotoViewModel> GetPhotos(int UserId, int albumId, int page, int pageSize)
        {
            Console.WriteLine("AlbumID Requested : " + albumId);
            IEnumerable<PhotoViewModel> photos = Mapper.Map<IEnumerable<PhotoViewModel>>(photoRepository.AllIncluding(x => x.CreatedBy, x => x.Album)
                .Where(x => x.AlbumId == albumId)
                //.Where(x => x.CreatedById == UserId)
                .Where(x => x.Album.Delflag == false)
                .Where(x => x.Delflag == false));
            return photos.Skip(page * pageSize).Take(pageSize);
            //.Where(x=>x.Album.WebSite.Name == WebSite));
            // int _totalPhotos = photos.Count();
            // return new IEnumerable<PhotoViewModel>()
            // {
            //     Page = page,
            //     TotalCount = _totalPhotos,
            //     TotalPages = (int)Math.Ceiling((decimal)_totalPhotos / pageSize),
            //     Items = photos.Skip(page * pageSize).Take(pageSize)
            // };
        }
        // All Albums by User

        public IEnumerable<PhotoAlbumViewModel> GetAlbums(int UserId, int page, int pageSize, int WebsiteId)
        {
            var _result = photoAlbumRepository
                .AllIncluding(x => x.Photos, x => x.Website, x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.WebsiteId == WebsiteId)
                .Skip(page * pageSize).Take(pageSize).ToList();
            List<PhotoAlbumViewModel> albums = Mapper.Map<List<PhotoAlbumViewModel>>(_result);
            // foreach(var items in _result){
            //     PhotoAlbumViewModel pvm = new PhotoAlbumViewModel();
            //     pvm.Id = items.Id;
            //     pvm.ThumbnailUrl = items.ThumbnailUrl;
            //     pvm.Description = items.Description; 
            //     pvm.Title = items.Title;
            //     pvm.TotalPhotos = items.TotalPhotos;
            //     pvm.URL = items.URL;
            //     pvm.Verified = items.Verified;
            //     albums.Add(pvm);
            // }
            return albums;
            //.Where(x=>x.Website.Name == website));
            // int _totalAlbums = albums.Count();
            // IEnumerable<PhotoAlbumViewModel> pagedAlbum = null;
            // pagedAlbum = new IEnumerable<PhotoAlbumViewModel>()
            // {
            //     Page = Page,
            //     TotalCount = _totalAlbums,
            //     TotalPages = (int)Math.Ceiling((decimal)_totalAlbums / Pagesize),
            //     Items = albums.Skip(Page * Pagesize).Take(Pagesize)
            // };
            //return pagedAlbum;
        }

        public PaginationSet<PhotoAlbumViewModel> GetPagedAlbum(string Website, string Culture, int page, int pageSize)
        {
            IEnumerable<PhotoAlbumViewModel> albums = Mapper.Map<IEnumerable<PhotoAlbumViewModel>>(photoAlbumRepository
                .AllIncluding(x => x.Photos, x => x.Website, x => x.Culture) //                                 
                .Where(x => x.Delflag == false)
                .Where(x => x.Verified == true)
                .Where(x => x.Website.Name == Website)
                );
            var pagedAlbum = new PaginationSet<PhotoAlbumViewModel>()
            {
                Page = page,
                TotalCount = albums.Count(),
                TotalPages = (int)Math.Ceiling((decimal)albums.Count() / pageSize),
                Items = albums.Skip(page * pageSize).Take(pageSize)
            };
            return pagedAlbum;
        }
        public IEnumerable<PhotoAlbumViewModel> GetAlbumsForWebsite(string Website, string Culture, int page, int pageSize)
        {
            IEnumerable<PhotoAlbumViewModel> albums = Mapper.Map<IEnumerable<PhotoAlbumViewModel>>(photoAlbumRepository
                .AllIncluding(x => x.Photos, x => x.Website, x => x.Culture) //                                 
                .Where(x => x.Delflag == false)
                .Where(x => x.Verified == true)
                .Where(x => x.Website.Name == Website)
                );
            return albums.Skip(page * pageSize).Take(pageSize);
        }
        public List<PhotoAlbumCategoryViewModel> GetCategories(int WebsiteId)
        {
            List<PhotoAlbumCategoryViewModel> categories = Mapper.Map<List<PhotoAlbumCategoryViewModel>>(photoAlbumCategoryRepository
                 .AllIncluding(x => x.Website)
                 .Where(x => x.Delflag == false)
                 .Where(x => x.WebsiteId == WebsiteId));
            return categories;
        }
        public List<SiteMap> SiteMapPhotoAlbum(string website)
        {
            List<SiteMap> _siteMap = new List<SiteMap>();
            // if (website != null && website != "")
            // {
                var _galleryUrl = _context.PhotoAlbums
                                              .Include(x => x.Website)
                                              .Where(x => x.Verified == true)
                                              .Where(x => x.Delflag == false)
                                              .Where(x=>x.Website.Name.Contains(website))
                                              .Where(x => x.Website.Construction == false)
                                              .ToList()
                                              .Select(x => new { x.Website.Name, x.URL })
                                              ;

                foreach (var items in _galleryUrl.OrderBy(x => x.Name))
                {
                    SiteMap sm = new SiteMap();
                    sm.WebsiteName = items.Name;
                    sm.URL = items.URL;
                    _siteMap.Add(sm);
                }
            // }
            // else
            // {
            //     var _galleryUrl = _context.PhotoAlbums
            //                       .Include(x => x.Website)
            //                       .Where(x => x.Verified == true)
            //                       .Where(x => x.Delflag == false)
            //                       .Where(x => x.Website.Construction == false)
            //                       .ToList()
            //                       .Select(x => new { x.Website.Name, x.URL })
            //                       ;

            //     foreach (var items in _galleryUrl.OrderBy(x => x.Name))
            //     {
            //         SiteMap sm = new SiteMap();
            //         sm.WebsiteName = items.Name;
            //         sm.URL = items.URL;
            //         _siteMap.Add(sm);
            //     }
            // }
            return _siteMap;
        }

        public IEnumerable<PhotoViewModel> CheckIfImageIsUsed(string ImagePath)
        {
            IEnumerable<PhotoViewModel> photos = Mapper.Map<IEnumerable<PhotoViewModel>>(photoRepository.AllIncluding(x => x.CreatedBy, x => x.Album)
              .Where(x => x.Album.Delflag == false)
              .Where(x => x.Delflag == false)
              .Where(x => x.ImageUrl == ImagePath));
            return photos;
        }

        public PhotoAlbumPhoto GetSinglePhoto(int UserId, int PhotoId)
        {
            return photoRepository.AllIncluding(x => x.CreatedBy)
                    .Where(x => x.CreatedById == UserId)
                    .Where(x => x.Id == PhotoId).SingleOrDefault();
        }
        public GenericResult DeleteGallery(int UserId, int GalleryId)
        {
            PhotoAlbum _albumToDelete = photoAlbumRepository.AllIncluding(x => x.Website, x => x.Photos).Single(x => x.Id == GalleryId);
            if (!_userService.IsInRole(UserId, _albumToDelete.WebsiteId, (new string[] { "Admin", "Owner" }))) return Global.AccessDenied();
            try
            {
                try
                {
                    if (_albumToDelete.TotalPhotos > 0)
                    {
                        return new GenericResult()
                        {
                            Succeeded = false,
                            Message = "Album Must Be Empty To Be Deleted."
                        };
                    }
                }
                catch { }
                _albumToDelete.Delflag = true;
                photoAlbumRepository.Update(_albumToDelete);
                photoAlbumRepository.Commit();
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Album Deleted."
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error :" + ex.Message
                };
            }
        }
        public GenericResult DeletePhoto(int UserId, int PhotoId, int WebsiteId)
        {
            PhotoAlbumPhoto _phototoRemove = photoRepository
                    .AllIncluding(x => x.CreatedBy, x => x.Album)
                    .Where(x => x.Album.WebsiteId == WebsiteId)
                    //.Where(x => x.CreatedById == UserId)
                    .Where(x => x.Id == PhotoId).SingleOrDefault();
            if (_phototoRemove != null)
            {
                if (!_userService.IsInRole(UserId, _phototoRemove.Album.WebsiteId, (new string[] { "Admin", "Owner" }))) return Global.AccessDenied();
                _phototoRemove.Delflag = true;
                photoRepository.Update(_phototoRemove);
                photoRepository.Commit();
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Photo removed."
                };
            }
            else
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : Photo could not be removed."
                };
            }

        }
        public GenericResult CreateAlbum(int UserId, PhotoAlbumCreateViewModel photoAlbum, IFormFileCollection files, IHostingEnvironment HostingEnvironment)
        {
            if (!(photoAlbum.Id > 0)) { goto createPhoto; }
            PhotoAlbum _dbAlbum = photoAlbumRepository.GetSingle(x => x.Id == photoAlbum.Id);
            if (!_userService.IsInRole(UserId, _dbAlbum.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }))) return Global.AccessDenied();

            PhotoAlbum _updatedAlbum = Mapper.Map<PhotoAlbumCreateViewModel, PhotoAlbum>(photoAlbum, _dbAlbum);
            photoAlbumRepository.Update(_updatedAlbum);
            photoAlbumRepository.Commit();
            return new GenericResult()
            {
                Succeeded = true,
                Message = "Success : Album Created.",
            };

        createPhoto:
            PhotoAlbum album = Mapper.Map<PhotoAlbum>(photoAlbum);
            if (!_userService.IsInRole(UserId, album.WebsiteId, (new string[] { "Admin", "Editor", "Owner", "Content Writer" })))
            {
                return Global.AccessDenied();
            }
            try
            {
                album.Verified = _userService.IsInRole(UserId, album.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }));
                album.CreatedById = (UserId);
                photoAlbumRepository.Add(album);
                photoAlbumRepository.Commit();
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Album Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Photos could not be Uploaded." + ex.Message
                };
            }
        }
        public GenericResult CreateCategoy(int UserId, PhotoAlbumCategoryCreateViewModel Category)
        {
            PhotoAlbumCategory category = Mapper.Map<PhotoAlbumCategory>(Category);
            //PhotoAlbum album = Mapper.Map<PhotoAlbum>(photoAlbum);
            try
            {
                // Remaning to check if the user is Adminrole  for the Site or else to set the value for verified as true or false
                // album.CreatedOn = DateTime.UtcNow;
                category.CreatedById = UserId;
                category.Verified = _userService.IsInRole(UserId, Category.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }));
                photoAlbumCategoryRepository.Add(category);
                photoAlbumCategoryRepository.Commit();
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Category Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : Category Creation Failed" + ex.Message
                };
            }
        }
        public GenericResult CreatePhoto(int UserId, int webSiteId, int albumid, IFormFileCollection files, IHostingEnvironment HostingEnvironment)
        {
            string websiteName = _websiteService.GetWebsiteName(webSiteId);
            if (_websiteService.spaceExceeded(websiteName) == true)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : Space Exceeded. Please contact Administrator or write open a Support Ticket"
                };
            }
            try
            {
                foreach (var file in files)
                {
                    PhotoAlbumPhoto photo = new PhotoAlbumPhoto();
                    photo.ImageUrl = Global.SaveAndGetImage(file, "UserUploads/" + websiteName + "/PhotoAlbum", HostingEnvironment, "");
                    photo.Verified = _userService.IsInRole(UserId, photo.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }));
                    photo.CreatedById = (UserId);
                    photo.AlbumId = albumid;
                    photoRepository.Add(photo);
                    photoRepository.Commit();
                }
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Uploaded Succeeded."
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Photos could not be Uploaded." + ex.Message
                };
            }
        }
        public GenericResult CreatePhoto(int UserId, PhotoAlbumPhotoCreateViewModel papcvm)
        {
            bool checkedForThumbnail = false;
            try
            {
                foreach (var photos in papcvm.Photos)
                {
                    PhotoAlbumPhoto photo = new PhotoAlbumPhoto();
                    if (checkedForThumbnail == false)
                    {
                        PhotoAlbumPhoto photoWithThumbNail = photoRepository.GetAll()
                            .Where(x => x.AlbumId == papcvm.AlbumId)
                            .Where(x => x.AlbumThumbNail == true).FirstOrDefault();
                        if (photoWithThumbNail == null)
                        {
                            photo.AlbumThumbNail = true;
                            checkedForThumbnail = true;
                        }
                    }

                    photo.CreatedById = (UserId);
                    photo.Verified = _userService.IsInRole(UserId, photo.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }));
                    photo.AlbumId = papcvm.AlbumId;
                    photo.ImageUrl = photos;
                    photoRepository.Add(photo);
                    photoRepository.Commit();
                }
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Uploaded Succeeded."
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Photos could not be Uploaded." + ex.Message
                };
            }
        }
        public GenericResult EditPhoto(int UserId, PhotoAlbumPhotoEditViewModel papevm)
        {
            PhotoAlbumPhoto photo = photoRepository.GetSingle(x => x.Id == papevm.Id);
            // UserId = 2;
            if (!_userService.IsInRole(UserId, papevm.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }))) return Global.AccessDenied();
            try
            {

                //photo.CeatedById = (UserId);
                photo.Verified = _userService.IsInRole(UserId, papevm.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }));
                photo.AlbumId = papevm.AlbumId;
                photo.ImageUrl = papevm.ImageUrl;
                photo.Title = papevm.Title;
                photo.AlbumThumbNail = papevm.AlbumThumbNail;
                photo.Description = papevm.Description;
                photoRepository.Update(photo);
                //photoRepository.Commit();          

                if (papevm.AlbumThumbNail == true)
                {
                    PhotoAlbumPhoto photoToRemove = photoRepository.GetAll()
                        .Where(x => x.AlbumId == papevm.AlbumId)
                        .Where(x => x.Id != papevm.Id)
                        .Where(x => x.AlbumThumbNail == true).FirstOrDefault();

                    if (photoToRemove != null)
                    {
                        photoToRemove.AlbumThumbNail = false;
                        photoRepository.Update(photoToRemove);
                        photoRepository.Commit();
                        return new GenericResult()
                        {
                            Succeeded = true,
                            Message = "Success : Album Thumbnail Changed."
                        };
                    }
                }
                photoRepository.Commit();
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Photo Edited."
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Photos could not be Edited." + ex.Message
                };
            }
        }
    }
}