﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Community.Model.Entities.Module;
using Community.Data.Infrastructure;
using Community.Model.Entities.Pages;
using System;
using Microsoft.Extensions.Caching.Memory;
using Community.Model.Entities;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
//using Newtonsoft.Json; 
namespace Community.Services
{
    public class WebsiteService : IWebsiteService
    {
        private readonly IWebsiteRepository _websiteRepository;
        private IMapper Mapper { get; set; }
        private IMenuService _menuService;
        private ICultureService _cultureService;
        private Global _global = new Global();
        private CommunityContext _context;
        private IBlogService _blogService;
        private IMemoryCache _memoryCache;
        private IUserService _userService;
        private IModuleService _moduleService;
        public WebsiteService(IModuleService _moduleService, IUserService _userService, IMemoryCache _memoryCache, IBlogService _blogService, IMapper Mapper, IWebsiteRepository _websiteRepository, ICultureService _cultureService, IMenuService _menuService, CommunityContext _context)
        {
            this._moduleService = _moduleService;
            this._userService = _userService;
            this._memoryCache = _memoryCache;
            this._menuService = _menuService;
            this._websiteRepository = _websiteRepository;
            this._cultureService = _cultureService;
            this.Mapper = Mapper;
            this._context = _context;
            this._blogService = _blogService;
        }

        public WebisteSuggestionViewModel checkIfAvailable(string websiteName)
        {
            WebisteSuggestionViewModel wvm = new WebisteSuggestionViewModel();
            var domainWeOwn = false;
            if (websiteName.IndexOf(" ") >= 0)
            {
                domainWeOwn = DomainWEOwn(websiteName.Substring(websiteName.LastIndexOf(" ") + 1));
                if (domainWeOwn)
                {
                    wvm.OurDomain = true;
                    string toOurDomain = websiteName.Replace(" ", ".").ToLower() + ".com";
                    var _ownedDomainResult = _websiteRepository.GetAll().Where(x => x.Name == toOurDomain);
                    if (_ownedDomainResult.Any())
                    {
                        wvm.Status = false;
                        wvm = checkIfAvailable(websiteName.Substring(0, 1) + " " + websiteName.Substring(websiteName.LastIndexOf(" ") + 1));
                        if (wvm.Status == true) { wvm.Status = false; }
                        wvm.Suggestion = new string[] {wvm.Suggestion[0] ,
                                                        (websiteName.Replace(" ", "").ToLower() + ".com.np").ToLower() ,
                                                        (websiteName.Substring(websiteName.LastIndexOf(" ") + 1) + websiteName.Substring(0,websiteName.IndexOf(" ")) + ".com.np").ToLower(),
                                                        (websiteName.Substring(0,1) +  websiteName.Substring(websiteName.LastIndexOf(" ") + 1) + ".com.np").ToLower(),
                                                       (websiteName.Substring(websiteName.LastIndexOf(" ") + 1) + websiteName.Substring(0,1)  + ".com.np").ToLower()
                                                        };
                        return wvm;
                    }
                    else
                    {
                        wvm.Status = true;
                        wvm.Suggestion = new string[] { toOurDomain };
                    }
                }
                else
                { // We Dont Own The Domain suggest for .com.np
                    wvm = checkIfAvailable(websiteName.Replace(" ", "").ToLower() + ".com.np");
                    wvm.Suggestion = new string[] {wvm.Suggestion[0] ,
                                                       (websiteName.Substring(websiteName.LastIndexOf(" ") + 1) + websiteName.Substring(0,websiteName.IndexOf(" ")) + ".com.np").ToLower(),
                                                       (websiteName.Substring(0,1) +  websiteName.Substring(websiteName.LastIndexOf(" ") + 1) + ".com.np").ToLower(),
                                                       (websiteName.Substring(websiteName.LastIndexOf(" ") + 1) + websiteName.Substring(0,1)  + ".com.np").ToLower()
                                                        };
                }
            }
            else
            {
                var _result = _websiteRepository.GetAll().Where(x => x.Name.ToLower() == websiteName.ToLower());
                if (_result.Any())
                {
                    wvm.Status = false;
                    if(websiteName.Contains(".com")) {
                        wvm.Suggestion = new string[] {websiteName.Replace(".com",".com.np").ToLower()};
                    }
                }
                else
                {
                    wvm.Suggestion = new string[] { websiteName.ToLower() };
                    wvm.Status = true;
                    // return wvm;
                }
            }

            return wvm;
        }
        public bool DomainWEOwn(string MainDomainName)
        {

            string[] OwnedDomains = new string[] { "gajurel", "sigdel" };
            foreach (var items in OwnedDomains)
            {
                if (MainDomainName.ToLower().Contains(items))
                    return true;
            }
            return false;
        }
        public bool isAliasAvailable(string aliasName)
        {
            try
            {
                var _result = _websiteRepository.GetAll().Where(x => x.Alias.ToLower() == aliasName.ToLower()).SingleOrDefault();
                if(_result != null)
                return false;
            }
            catch
            {
                return true;
            }
            return true;
        }
        public IEnumerable<WebsiteViewModel> SearchForWebsite(string WebsiteName, int PageNo, int PageSize)
        {
            List<WebsiteViewModel> websites = new List<WebsiteViewModel>();
            websites = Mapper.Map<List<WebsiteViewModel>>(_websiteRepository.GetAll().Where(x => x.Name.ToLower().Contains(WebsiteName.ToLower())));
            return websites;
        }
        public List<Module> GetWebsiteModule(int UserId, int WebsiteId)
        {
            var _resultAdmin = _context.Websites.
                           Include(x => x.WebsiteModule)
                           .ThenInclude(y => y.Module)
                           .Where(x => x.Id == WebsiteId)
                           .Where(x => x.Delflag == false).Where(x => x.Name.ToLower() != "commonwebsitesettings")
                           .SingleOrDefault()
                           ;
            var _ModuleToSend = _resultAdmin.WebsiteModule.Select(x => x.Module).ToList();
            return _ModuleToSend;

        }
        public IEnumerable<WebsiteViewModel> GetWebsites(int UserId, int page, int pageSize)
        {
            List<WebsiteViewModel> websites = new List<WebsiteViewModel>();
            if (_context.Users.Where(x => x.Id == UserId).SingleOrDefault().UserType == UserType.CommunityAdmin)
            {
                var _resultAdmin = _context.Websites.
                            Include(x => x.WebsiteModule)
                            .ThenInclude(y => y.Module)
                            .Where(x => x.Delflag == false).Where(x => x.Name.ToLower() != "commonwebsitesettings");

                foreach (var items in _resultAdmin)
                {
                    WebsiteViewModel wvm = new WebsiteViewModel();
                    List<ClientModuleViewModel> Modules = new List<ClientModuleViewModel>();
                    wvm.Id = items.Id;
                    wvm.Name = items.Name;
                    wvm.AllocatedSpace = items.AllocatedSpace.ToString();//+ " Mb";
                    wvm.UsedSpace = items.SpaceUsed.ToString();//+ " Mb";
                    wvm.Construction = items.Construction;
                    wvm.Redirect = items.Redirect;
                    wvm.RedirectToUrl = items.RedirectToUrl;
                    if(items.Alias != null)
                    wvm.URL = "http://" + Community.Model.Globals.Global.OnConstructionWebsiteName(items.Alias);

                    //wvm.URL = items.Construction ? "http://" + Community.Model.Globals.Global.OnConstructionWebsiteName(items.Alias) : "http://" + wvm.Name;
                    if (items.Construction == true && !wvm.URL.Contains(".teamrockmandu.com"))
                    {
                        wvm.URL = "http://" + wvm.Name + ".teamrockmandu.com";
                    }
                    foreach (var websiteModule in items.WebsiteModule.Where(x => x.Module.ParentModuleId == null))
                    {
                        if (!websiteModule.Module.Name.Contains(" "))
                        {
                            ClientModuleViewModel module = new ClientModuleViewModel();
                            module.DisplayName = websiteModule.Module.DisplayName;
                            module.MenuJson = websiteModule.Module.MenuJson;
                            // module.Param = websiteModule.Module.Param;
                            // module.Icon = websiteModule.Module.Icon;
                            // module.State = websiteModule.Module.State;
                            Modules.Add(module);
                        }
                    }

                    wvm.Modules = Modules;
                    websites.Add(wvm);
                }
                return websites.Skip(page * pageSize).Take(pageSize);
            }
            var _result = _context.UserRoles
                        .Include(x => x.Website)
                        .ThenInclude(x => x.WebsiteModule)
                        .ThenInclude(x => x.Module)
                        .Where(x => x.UserId == UserId);
            foreach (var items in _result.Where(x => x.Delflag == false))
            {
                WebsiteViewModel wvm = new WebsiteViewModel();
                List<ClientModuleViewModel> Modules = new List<ClientModuleViewModel>();

                wvm.Id = items.Website.Id;
                wvm.Name = items.Website.Name;
                wvm.Construction = items.Website.Construction;
                wvm.Redirect = items.Website.Redirect;
                wvm.AllocatedSpace = items.Website.AllocatedSpace.ToString();//+ " Mb";
                wvm.UsedSpace = items.Website.SpaceUsed.ToString();//+ " Mb";
                wvm.RedirectToUrl = items.Website.RedirectToUrl;
                wvm.URL = "http://" + Community.Model.Globals.Global.OnConstructionWebsiteName(items.Website.Alias);                
                // wvm.URL = items.Website.Construction ? "http://" + wvm.Name.Replace(".com", ".teamrockmandu.com") : "http://" + wvm.Name;
                // if (items.Website.Construction == true && !wvm.URL.Contains(".teamrockmandu.com"))
                // {
                //     wvm.URL = "http://" + wvm.Name + ".teamrockmandu.com";
                // }
                foreach (var websiteModule in items.Website.WebsiteModule.Where(x => x.Module.ParentModuleId == null))
                {
                    if (!websiteModule.Module.Name.Contains(" "))
                    {
                        ClientModuleViewModel module = new ClientModuleViewModel();
                        module.DisplayName = websiteModule.Module.DisplayName;
                        module.MenuJson = websiteModule.Module.MenuJson;
                        // module.Param = websiteModule.Module.Param;
                        // module.Icon = websiteModule.Module.Icon;
                        // module.State = websiteModule.Module.State;
                        Modules.Add(module);
                    }
                }
                wvm.Modules = Modules;

                websites.Add(wvm);
            }
            return websites.Skip(page * pageSize).Take(pageSize);
            // int _totalWebsites = websites.Count();
            // return new IEnumerable<WebsiteViewModel>()
            // {
            //     Page = page,
            //     TotalCount = _totalWebsites,
            //     TotalPages = (int)Math.Ceiling((decimal)_totalWebsites / pageSize),
            //     Items = websites.Skip(page * pageSize).Take(pageSize)
            // };
        }
        public string GetWebsiteName(string AliasId)
        {
            string websiteName = _websiteRepository.GetAll().Where(x => x.Alias == AliasId).FirstOrDefault().Name.Replace(".teamrockmandu.com", "");
            //    if (websiteName == "localhost:5000")
            //     websiteName = "localhost5000";
            return websiteName;
        }
        public string GetWebsiteName(int websiteId)
        {
            string websiteName = _websiteRepository.GetSingle(websiteId).Name;
            //    if (websiteName == "localhost:5000")
            //     websiteName = "localhost5000";
            return websiteName;
        }
        public int GetWebsiteId(string WebsiteName)
        {
            int websiteId = _websiteRepository.GetSingle(x => x.Name.ToLower() == WebsiteName.ToLower()).Id;
            return websiteId;
        }
        //Create Website
        public GenericResult CreateWebsite(int UserId, WebsiteCreateViewModel Websitecreate)
        {
            Regex r = new Regex("^[a-zA-Z.]*$");
            if (!r.IsMatch(Websitecreate.Name)){
                return _global.Error(null,"Special Character And Space Not Alloweded");
            }
            string defaultLogoUrl = Global.DefaultLogo();
            int intCultureEnglish = 1;
            Website website = Mapper.Map<Website>(Websitecreate);
            website.Logo = defaultLogoUrl;
            try
            {
                if (_websiteRepository.Duplicate(x => x.Name == website.Name))
                {
                    return _global.Error(null,"Error : Website " + website.Name + " Already Exist.");                    
                }
                website.CreatedById = UserId;
                website.AllocatedSpace = _global.DefaultWebsiteSize();
                List<WebsiteCulture> WC = new List<WebsiteCulture>();
                if (Websitecreate.Cultures != null)
                {
                    foreach (var Cultures in Websitecreate.Cultures)
                    {
                        WebsiteCulture wc = new WebsiteCulture();
                        wc.CultureId = _cultureService.GetCultures().Where(x => x.Id == Cultures).SingleOrDefault().Id;
                        WC.Add(wc);
                    }
                    website.Culture = WC;
                }
                else
                {
                    if (website.Name != "commonWebsiteSettings")
                    {
                        WebsiteCulture wc = new WebsiteCulture();
                        wc.CultureId = _cultureService.GetCultures().Where(x => x.Id == 1).Select(x => x.Id).SingleOrDefault();
                        WC.Add(wc);
                        // WebsiteCulture wc1 = new WebsiteCulture();
                        // wc1.CultureId = _cultureService.GetCultures().Where(x => x.Id == 2).Select(x => x.Id).SingleOrDefault();
                        // WC.Add(wc1);
                        website.Culture = WC;
                    }
                }
                if (website.Theme == null) website.Theme = "mpurpose";
                _websiteRepository.Add(website);
                _websiteRepository.Commit();
                if (website.Name != "commonWebsiteSettings")
                {
                    UserRole ur = new UserRole();
                    ur.RoleId = 1;
                    ur.UserId = UserId;
                    ur.WebsiteId = website.Id;
                    ur.CreatedById = UserId;
                    ur.MultiLingualId = 0;
                    _context.UserRoles.Add(ur);
                    _context.SaveChanges();
                    try
                    {
                        WebsitePageLayout wpl = new WebsitePageLayout();
                        wpl.Layout = SinglePageLayout.Middle;
                        wpl.PageId = 1;
                        wpl.WebsiteId = website.Id;
                        _context.WebsitePageLayout.Add(wpl);
                    }
                    catch { }
                    _blogService.SeedBlogPost(website.Id, UserId);
                    _context.SaveChanges();
                }
                _moduleService.ModifyWebstieModule(Websitecreate.Modules, website.Id, UserId);
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Website Created.",
                    Data = website.Id.ToString()
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Website could not be Created." + ex.Message + " for Data CreateViewMode" //+ JsonConvert.SerializeObject(Websitecreate).ToString() + "For Destination Webiste Final Data " +  JsonConvert.SerializeObject(website).ToString()
                };
            }
        }
        public bool spaceExceeded(int websiteId)
        {
            Website website = _websiteRepository.GetSingle(websiteId);
            if (website.SpaceUsed >= website.AllocatedSpace)
            {
                return true;
            }
            return false;
        }
        public bool spaceExceeded(string websiteName)
        {
            if (websiteName == "localhost:5000") return false;
            Website website = _websiteRepository.GetAll().Where(x => x.Name == websiteName).SingleOrDefault();
            if (website.SpaceUsed >= website.AllocatedSpace)
            {
                return true;
            }
            return false;
        }

        public GenericResult SetConstruction(int UserId, int websiteId, bool onConstruction)
        {
            if (!_userService.IsInRole(UserId, websiteId, (new string[] { "Admin", "Owner", "Editor" }))) { return (Global.AccessDenied()); }
            Website website = _websiteRepository.GetSingle(websiteId);
            try
            {
                website.Construction = onConstruction;
                _websiteRepository.Update(website);
                _websiteRepository.Commit();
                _memoryCache.Remove(website.Name);
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Updated"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error: " + ex.Message
                };
            }



        }
        public GenericResult DeleteWebsite(int UserId, int Id)
        {
            Website website = _websiteRepository.GetSingle(Id);
            if (!_userService.IsInRole(UserId, website.Id, (new string[] { "Admin", "Owner" }))) { return Global.AccessDenied(); }
            if (website == null)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Website Not Found"
                };
            }
            try
            {
                _websiteRepository.Delete(website);
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Website " + website.Name + " Deleted"
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Website could not be Deleted." + ex.Message //+ " for Data Website" + JsonConvert.SerializeObject(website).ToString()
                };
            }
        }
        public List<CultureViewModel> GetCultures(int WebsiteId, int CultureId)
        {
            return _cultureService.GetCultures(WebsiteId, CultureId);
        }
        public List<CultureViewModel> GetCultures(string Website, string Culture)
        {
            int WebisteId = _websiteRepository.GetSingle(x => x.Name.ToLower() == Website.ToLower()).Id;
            int intCulture = _cultureService.GetCultureId(Culture);
            return GetCultures(WebisteId, intCulture);
        }
        public List<MenuViewModel> GetMenu(string CultureName, string WebsiteName)
        {
            return _menuService.GetMenus(CultureName, WebsiteName);
        }
        public List<MenuViewModel> GetMenu(int CultureName, int WebsiteName)
        {
            return new List<MenuViewModel>();
        }
    }
}