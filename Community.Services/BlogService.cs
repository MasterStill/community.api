﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Community.Model.Entities.Blog;
using Community.Model;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Mapster;
using Community.Data.Infrastructure;

namespace Community.Services
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;
        private IMapper Mapper { get; set; }
        private IUserService _userService;
        private Global Global = new Global();
        private IBlogTagRepository _blogTagRepository;
        private readonly IBlogCategoryRepository _categoryRepository;
        private IAuditService _auditService { get; set; }

        private MultiLingualGlobal _global = new MultiLingualGlobal();
        private CommunityContext _context;
        public BlogService(IMapper Mapper, IBlogRepository _blogRepository, IBlogTagRepository _blogTagRepository, IUserService _userService, IBlogCategoryRepository _categoryRepository, IAuditService _auditService, CommunityContext _context)
        {
            this._auditService = _auditService;
            this._categoryRepository = _categoryRepository;
            this._blogRepository = _blogRepository;
            this._blogTagRepository = _blogTagRepository;
            this.Mapper = Mapper;
            this._userService = _userService;
            this._context = _context;
        }

        public void SeedBlogPost(int WebsiteId, int UserId)
        {
            int intCultureEnglish = 1;
            BlogCategoryCreateViewModel bcvm = new BlogCategoryCreateViewModel();
            bcvm.Title = "General";
            bcvm.WebsiteId = WebsiteId;
            bcvm.CultureId = intCultureEnglish;
            bcvm.Type = BlogType.Blog;
            GenericResult _newCategory = CreateCategory(UserId, bcvm);
            //need to workaround for below 1 line
            int intblogCategoryId = GetCategories(UserId, 0, 10, WebsiteId, intCultureEnglish, BlogType.Blog).Select(x => x.Id).SingleOrDefault();
            BlogPostCreateViewModel bpcvm = new BlogPostCreateViewModel();
            bpcvm.CategoryId = intblogCategoryId;
            bpcvm.CommentAllowed = false;
            bpcvm.CultureId = intCultureEnglish;
            bpcvm.PublishDate = DateTime.Now;
            bpcvm.Content = "Welcome Content English";
            bpcvm.Summary = "Welcome Summary English";
            bpcvm.Title = "Welcome to Community CMS";
            bpcvm.ImageUrl = "http://sermonspiceuploads.s3.amazonaws.com/3265/fp_69207/stagelightswelcomehd_full.jpg";
            bpcvm.ThumbNailUrl = "http://amnplify.com.au/wp-content/uploads/2015/01/Cute_Fall_Welcome_CMN-HD.jpg";
            bpcvm.Type = BlogType.Blog;
            bpcvm.WebsiteId = WebsiteId;
            BlogPostCreateViewModel bpcvm1 = new BlogPostCreateViewModel();
            bpcvm1.CategoryId = intblogCategoryId;
            bpcvm1.CommentAllowed = true;
            bpcvm1.CultureId = intCultureEnglish;
            bpcvm1.PublishDate = DateTime.Now;
            bpcvm1.Summary = "Welcome Summary English";
            bpcvm1.Content = "About Site Constent";
            bpcvm1.Title = "Know How's About Site";
            bpcvm1.ImageUrl = "http://www.microiways.com/How_To/images/howto.jpg";
            bpcvm1.ThumbNailUrl = "http://cdn2.pcadvisor.co.uk/cmsdata/features/3572640/How-to-start-a-blog.jpg";
            bpcvm1.Type = BlogType.Blog;
            bpcvm1.WebsiteId = WebsiteId;
            BlogPostCreateViewModel bpcvm2 = new BlogPostCreateViewModel();
            bpcvm2.CategoryId = intblogCategoryId;
            bpcvm2.CommentAllowed = true;
            bpcvm2.CultureId = intCultureEnglish;
            bpcvm2.PublishDate = DateTime.Now;
            bpcvm2.Content = "Monetization Contents Here";
            bpcvm2.Summary = "Amazing Summary of Things That Can Be Acheived including Monetization";
            bpcvm2.Title = "Amaazing Things To Do Now !";
            bpcvm2.ImageUrl = "http://tiffanyteeworld.com/wp-content/uploads/2016/06/Awesome-People-Up-to-Amazing-Things-800x675.jpg";
            bpcvm2.ThumbNailUrl = "http://images6.fanpop.com/image/photos/36900000/Amazing-Things-quotes-36902984-500-334.jpg";
            bpcvm2.Type = BlogType.Blog;
            bpcvm2.WebsiteId = WebsiteId;
            CreateBlogPost(UserId, bpcvm2);
            CreateBlogPost(UserId, bpcvm1);
            CreateBlogPost(UserId, bpcvm);
        }
        public IEnumerable<TestViewModel> GetBlogPostsForWebsite(string WebsiteName, string Culture, int page, int pageSize, BlogType Type)
        {
            var result = _blogRepository.AllIncluding(x => x.Website, x => x.Culture, x => x.Category)
                .Where(x => x.Website.Name == WebsiteName)
                .Where(x => x.Culture.Title.ToLower() == Culture.ToLower())
                .Where(x => x.Delflag == false)
                .Where(x => x.Verified == true)
                .Where(x => x.BlogType == Type)
                .OrderByDescending(x => x.CreatedDate)
                .Skip(page * pageSize).Take(pageSize).ToList();
            var objToReturn = Mapper.Map<IEnumerable<TestViewModel>>(result);


            return objToReturn;
        }
        // public IEnumerable<TestViewModel> GetBlogPostsOnlyForWebsite(string WebsiteName, string CultureName,BlogType BlogType, int page)
        // {
        //     int pageSize =  20;
        //     var result = _blogRepository.AllIncluding(x => x.Website, x => x.Culture, x => x.Category)
        //         .Where(x => x.Website.Name == WebsiteName)
        //         .Where(x => x.Culture.Title.ToLower() == CultureName.ToLower())
        //         .Where(x => x.Delflag == false)
        //         .Where(x => x.Verified == true)
        //         .Where(x => x.BlogType == BlogType)
        //         .OrderByDescending(x => x.CreatedDate)
        //         .Skip(page * pageSize).Take(pageSize).ToList();
        //     var objToReturn = Mapper.Map<IEnumerable<TestViewModel>>(result);


        //     return objToReturn;
        // }
        //  public PaginationSet<BlogPostViewModel> GetBlogPostsForWebsiteBlogPage(string WebsiteName, string Culture, int page, int pageSize, BlogType Type)
        // {
        //     var result = _blogRepository.AllIncluding(x => x.Website, x => x.Culture, x => x.Category)
        //         .Where(x => x.Website.Name == WebsiteName)
        //         .Where(x => x.Culture.Title.ToLower() == Culture.ToLower())
        //         .Where(x => x.Delflag == false)
        //         .Where(x => x.Verified == true)
        //         .Where(x => x.BlogType == Type)
        //         .OrderByDescending(x => x.CreatedDate)
        //         .Skip(page * pageSize).Take(pageSize).ToList();            
        //     var objToReturn = Mapper.Map<IEnumerable<TestViewModel>>(result);           
        //     return objToReturn;
        // }
        // All BlogPost by Admin End
        public IEnumerable<BlogPostViewModel> GetBlogPosts(int UserId, int page, int pageSize, int WebsiteId, int CultureId, BlogType Type)
        {
            var result = _blogRepository
                .AllIncluding(x => x.Culture, x => x.Website, x => x.Category)
                .Where(x => x.Delflag == false)
                .Where(x => x.BlogType == Type)
                .Where(x => x.WebsiteId == WebsiteId).Skip(page * pageSize).Take(pageSize);
            List<int> _userKnownCulture = _userService.GetUserCulture(UserId);
            MultilingualAI MAI = _global.GenericMultiLingualRepo(Mapper.Map<IEnumerable<MultiLingualViewModel>>(result), _userKnownCulture, CultureId);
            var _result = from item in result
                          where !MAI.idToRemove.Contains(item.Id)
                          select item;
            //IEnumerable<BlogPostViewModel> objToReturn = Mapper.Map<IEnumerable<BlogPostViewModel>>(_result);
            var objToReturn = Mapper.Map<IEnumerable<BlogPostViewModel>>(_result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
            }
            return objToReturn;


            // IEnumerable<MultiLingualViewModel> testItems = Mapper.Map<IEnumerable<MultiLingualViewModel>>(MAI.items);
            //Mapper.Map(testItems,objToReturn); //TypeAdapter.Adapt(testItems,objToReturn);//Mapper.Map(testItems,objToReturn);
            // Mapper.Map(objToReturn,blogPosts)   ;
            //objToReturn=   TypeAdapter.Adapt(testItems,objToReturn);
            //TypeAdapter.Adapt(testItems,blogPosts);
            // TypeAdapter.Adapt<IEnumerable<MultiLingualViewModel>,IEnumerable<BlogPostViewModel>>(testItems,blogPosts);
            //TypeAdapter.Adapt(testItems,objToReturn);
            //   objToReturn = Mapper.Map(testItems,objToReturn);

            //  ICollection<BlogPostViewModel> objToReturn = Mapper.Map<ICollection<BlogPostViewModel>>(sub_result);
            //IEnumerable<BlogPostViewModel> blogPosts1 = new IEnumerable<BlogPostViewModel>();
            //var blogPosts1 = TypeAdapter.Adapt(testItems,blogPosts);

            //var Herum =   Mapper.Map<IEnumerable<MultiLingualViewModel>,IEnumerable<BlogPostViewModel>>(test.items,blogPosts);
            //IEnumerable<BlogPostViewModel> _finalResult= Mapper.Map<IEnumerable<MultiLingualViewModel>,IEnumerable<BlogPostViewModel>>(testItems,blogPosts);
            //blogPosts= Mapper.Map(testItems,blogPosts);
            //IEnumerable<BlogPostViewModel> FinalResult; //= new IEnumerable<BlogPostViewModel>();
            //blogPosts = TypeAdapter.Adapt(blogPosts,testItems);
            //IEnumerable<BlogPostViewModel> _finalTry ;//= new IEnumerable<BlogPostViewModel>();
            // _finalTry = TypeAdapter.Adapt<IEnumerable<MultiLingualViewModel>,IEnumerable<BlogPostViewModel>>(testItems);

            //var blogPosts1 = _finalTry.Adapt<BlogPostViewModel>();
            //var kk = TypeAdapter.Adapt<IEnumerable<MultiLingualViewModel>,IEnumerable<BlogPostViewModel>>(testItems,_finalTry);
            // var __result = TypeAdapter.Adapt<IEnumerable<BlogPostViewModel>>(testItems);

            //IEnumerable<BlogPostViewModel> blogPosts = Mapper.Map<IEnumerable<BlogPostViewModel>>(result);
            //List<int> convertedIDs = 
            // foreach (var items in blogPosts)
            // {
            //     if (items.CultureId == CultureId)
            //     {
            //         items.Converted = true;
            //         if (items.MultiLingualId != items.Id)
            //         {
            //             convertedIDs.Add(items.MultiLingualId);
            //         }
            //     }
            // }

            // List<int> _userKnownCulture = _userService.GetUserCulture(UserId);

            // foreach(var items in blogPosts.Where(x=>x.Converted == false)){
            //                 bool knownLanguageByUser = false;
            //                 foreach(var knownCulture in _userKnownCulture){
            //                     if (items.CultureId == knownCulture){
            //                         foreach(var sknownCulture in _userKnownCulture){
            //                             if(sknownCulture == CultureId){
            //                                 knownLanguageByUser = true;
            //                                 break;
            //                             }
            //                         }
            //                     }
            //                 }


            //                 foreach(var convertedItems in blogPosts.Where(x=>x.Converted == true))
            //                 {
            //                     if (convertedItems.MultiLingualId == items.MultiLingualId){
            //                         convertedIDs.Add(items.Id);    
            //                     }
            //                 }

            //                 if (!knownLanguageByUser){
            //                     convertedIDs.Add(items.Id);                                    
            //                 }

            // }

            //var results = testBlog;//from item in blogPosts
            //where !convertedIDs.Contains(item.Id)
            //where _userKnownCulture.Contains(item.CultureId)
            //select item;
            // return results;//.Skip(page * pageSize).Take(pageSize);
        }

        // public BlogPostSingleViewModel GetBlogPost(int BlogId,int CultureId)
        // {
        //     BlogPostSingleViewModel blogPost = Mapper.Map<BlogPostSingleViewModel>(_blogRepository.AllIncluding(x=>x.Tags)
        //         .Where(x=>x.Id == BlogId)
        //         .Where(x=>x.CultureId == CultureId)
        //         .Where(x=>x.Delflag==false).SingleOrDefault());
        //     return blogPost;
        // }  

        public GenericResult DeleteBlog(int BlogId, int UserId)
        {
            BlogPost blogPost = _blogRepository.GetSingle(u => u.Id == BlogId);//, u => u.Roles);
            try
            {
                if (!_userService.IsInRole(UserId, blogPost.WebsiteId, (new string[] { "Admin", "Owner" }))) return Global.AccessDenied();
                _blogRepository.Delete(blogPost);
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Blog Deleted.",
                };
            }
            catch
            {
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Blog Deletion Failed.",
                };
            }
        }
        public BlogPostSingleViewModel GetBlogPost(int BlogId, string Culture)
        {
            // List<string> Tags = new List<string>();
            // var blogpost = _blogRepository
            // .AllIncluding(x => x.Tags, x => x.Category,x=>x.Culture,x=>x.Tags)
            // .Where(x => x.MultiLingualId == BlogId)
            // .Where(x => x.Delflag == false);
            //.SingleOrDefault();
            BlogPostSingleViewModel blogPost = new BlogPostSingleViewModel();


            var blogpost = _context.BlogPosts
               .Include(x => x.Tags)
               .ThenInclude(v => v.Tag)
               .Include(x => x.Category)
               .Include(x => x.Culture)
               .Include(x => x.Website)
               .Where(x => x.MultiLingualId == BlogId)
               .Where(x => x.Delflag == false);

            try
            {
                var multilingualBlogPost = blogpost.Where(x => x.Culture.Title.ToLower() == Culture.ToLower()).SingleOrDefault();
                if (multilingualBlogPost == null)
                {

                    blogPost = Mapper.Map<BlogPostSingleViewModel>(blogpost.FirstOrDefault());
                }
                else
                {
                    blogPost = Mapper.Map<BlogPostSingleViewModel>(multilingualBlogPost);
                    foreach (var items in multilingualBlogPost.Tags)
                    {
                        blogPost.Tag = items.Tag;
                    }

                    blogPost.Translated = true;
                }
            }
            catch (Exception ex)
            {
                Global.Error(ex);
            }

            //if(blogpo)

            //blogPost.BlogTags = (Tags.ToArray());
            return blogPost;
        }
        public BlogPostCreateViewModel GetBlogPost(int BlogId)
        {
            var blogpost = _blogRepository.AllIncluding(x => x.Tags, x => x.Category)
            .Where(x => x.Id == BlogId)
                .Where(x => x.Delflag == false).SingleOrDefault();
            BlogPostCreateViewModel blogPost = Mapper.Map<BlogPostCreateViewModel>(blogpost);
            List<string> Tags = new List<string>();
            foreach (var items in blogpost.Tags)
            {
                Tags.Add(_context.BlogTag.Where(x => x.Id == items.TagId).Select(x => x.Name).SingleOrDefault());
            }
            blogPost.Tag = (Tags.ToArray());
            return blogPost;
        }
        //All BlogPost By Category
        public IEnumerable<BlogPostViewModel> GetBlogPostsForCategory(int UserId, int CategoryId, int page, int pageSize, int WebsiteId)
        {
            IEnumerable<BlogPostViewModel> websites = Mapper.Map<IEnumerable<BlogPostViewModel>>(_blogRepository
                .AllIncluding(x => x.CreatedBy)
                .Where(x => x.Delflag == false)
                .Where(x => x.CategoryId == CategoryId)
                .Where(x => x.WebsiteId == WebsiteId));

            return websites.Skip(page * pageSize).Take(pageSize);
        }

        public IEnumerable<TestViewModel> GetBlogPostsForCategory(string Website, string Culture, string Category, int page, BlogType Type)
        {
            Category = Category.Replace("_", " ");
            int pageSize = 10;
            var _result = _blogRepository
                .AllIncluding(x => x.Category, x => x.Website, x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.Category.Title.ToLower() == Category.ToLower())
                .Where(x => x.Website.Name == Website)
                .Where(x => x.BlogType.ToString().ToLower() == Type.ToString().ToLower())
                .Where(x => x.Culture.Title.ToLower() == Culture.ToLower())
                .OrderByDescending(x => x.PublishDate)
                .ToList();
            IEnumerable<BlogPostViewModel> blogPostWithCategory = Mapper.Map<IEnumerable<BlogPostViewModel>>(_result);
            //  IEnumerable<TestViewModel> bptvm ;            
            return Mapper.Map<IEnumerable<TestViewModel>>(blogPostWithCategory.Skip(page * pageSize).Take(pageSize));
        }

        public PaginationSet<BlogPost> GetPagedBlogPostForTags(string Website, string Culture, string Tags, int page, BlogType Type)
        {
            Tags = Tags.Replace("_", " ");
            int pageSize = 10;
            var _result = _context.BlogPosts
                .Where(x => x.BlogType == Type)
                .Where(x => x.Delflag == false)
                .Where(x => x.Website.Name == Website)
                .Where(x => x.Culture.Title == Culture)
                .Include(x => x.Tags)
                .ThenInclude(y => y.Tag)
                .Include(x => x.Category)
                .Include(x => x.Website)
                .Include(x => x.Culture)
                .ToList();
            List<int> blogToSend = new List<int>();
            foreach (var blog in _result)
            {
                foreach (var items in blog.Tags)
                {
                    if (items.Tag.Name == Tags)
                    {
                        blogToSend.Add(blog.Id);
                    }
                }
            }
            var _finalResult = from item in _result
                               where blogToSend.Contains(item.Id)
                               select item;
            IEnumerable<BlogPostViewModel> blogPostWithCategory = Mapper.Map<IEnumerable<BlogPostViewModel>>(_finalResult);
            PaginationSet<BlogPost> pbpvm = new PaginationSet<BlogPost>();
            //pbpvm.Count = blogPostWithCategory.Count();
            pbpvm.Items = _finalResult;
            pbpvm.Page = page;    
            return pbpvm;
        }

        public IEnumerable<BlogPostViewModel> GetBlogPostsForTags(string Website, string Culture, string Tags, int page, BlogType Type)
        {
            return Mapper.Map<IEnumerable<BlogPostViewModel>>(GetPagedBlogPostForTags(Website, Culture, Tags, page, Type).Items);            
            //return ;
            // Tags = Tags.Replace("_", " ");
            // int pageSize = 10;
            // var _result = _context.BlogPosts
            //     .Where(x => x.BlogType == Type)
            //     .Where(x => x.Delflag == false)
            //     .Where(x => x.Website.Name == Website)
            //     .Where(x => x.Culture.Title == Culture)
            //     .Include(x => x.Tags)
            //     .ThenInclude(y => y.Tag)
            //     .Include(x => x.Category)
            //     .Include(x => x.Website)
            //     .Include(x => x.Culture)
            //     .ToList();

            // List<int> blogToSend = new List<int>();
            // foreach (var blog in _result)
            // {
            //     foreach (var items in blog.Tags)
            //     {
            //         if (items.Tag.Name == Tags)
            //         {
            //             blogToSend.Add(blog.Id);
            //         }
            //     }
            // }
            // var _finalResult = from item in _result
            //                    where blogToSend.Contains(item.Id)
            //                    select item;
            // IEnumerable<BlogPostViewModel> blogPostWithCategory = Mapper.Map<IEnumerable<BlogPostViewModel>>(_finalResult);
            // return blogPostWithCategory.Skip(page * pageSize).Take(pageSize);
        }
        //Create Blog
        public List<BlogPostBlogTag> SaveTags(string[] Tags, int UserId, int CultureId, bool Verified, int? blogId = 0)
        {
            try
            {
                if (blogId != 0)
                {
                    var _tagstoRemove = _context.BlogPostBlogTag.Where(x => x.PostId == blogId);
                    _context.BlogPostBlogTag.RemoveRange(_tagstoRemove);
                    _context.SaveChanges();
                }
            }
            catch { }

            List<BlogPostBlogTag> bpbt = new List<BlogPostBlogTag>();
            foreach (var tag in Tags)
            {
                var dbResultforDuplication = _blogTagRepository.ReturnIfExist(x => x.Name == tag);
                if (dbResultforDuplication == null)
                {
                    BlogTag bt = new BlogTag();
                    bt.Name = tag;
                    bt.CreatedById = UserId;//NewblogPost.CreatedById;
                    bt.CultureId = CultureId;
                    bt.Verified = Verified;
                    _blogTagRepository.Add(bt);
                    _blogTagRepository.Commit();

                    BlogPostBlogTag blogpostblogtag = new BlogPostBlogTag();
                    blogpostblogtag.Tag = bt;
                    if (blogId > 0)
                    {
                        blogpostblogtag.PostId = blogId.Value;
                    }
                    bpbt.Add(blogpostblogtag);
                }
                else
                {
                    BlogPostBlogTag blogpostblogtag = new BlogPostBlogTag();
                    blogpostblogtag.Tag = dbResultforDuplication;
                    if (blogId > 0)
                    {
                        blogpostblogtag.PostId = blogId.Value;
                    }
                    bpbt.Add(blogpostblogtag);
                }
            }
            if (blogId > 0)
            {
                _context.BlogPostBlogTag.AddRange(bpbt);
                _context.SaveChanges();
            }
            return bpbt;
        }
        public List<SiteMap> SiteMapBlogTags(string WebsiteName)
        {
            var _result = _context.BlogPostBlogTag
                        .Include(x => x.Post)
                        .ThenInclude(y => y.Website)
                        .Include(x => x.Tag)
                        .Where(x => x.Post.Website.Name.Contains(WebsiteName))
                        .Where(x => x.Post.Website.Construction == false)
                        //.Where(x => x.Post.Website.Name == Website)
                        //.Where(x => x.Post.BlogType == Type)
                        ;
            List<SiteMap> tags = new List<SiteMap>();
            foreach (var items in _result)
            {
                SiteMap btvm = new SiteMap();
                btvm.URL = "http://" + items.Post.Website.Name + "/" + items.Tag.URL;
                btvm.WebsiteName = items.Post.Website.Name;
                tags.Add(btvm);
            }

            return tags;
        }
        public List<SiteMap> SiteMapBlogCategory(string WebsiteName)
        {
            List<SiteMap> _siteMap = new List<SiteMap>();
            //var _blogCategory = new object();

            var _blogCategory = _context.BlogCategory
                                .Include(x => x.Culture)
                                .Include(x => x.Website)
                                .Where(x => x.Website.Construction == false)
                                .Where(x => x.Verified == true)
                                .Where(x => x.Delflag == false)
                                .Where(x => x.Website.Name.Contains(WebsiteName))
                                .ToList()
                                .Select(x => new { x.Website.Name, x.URL })
                                ;


            foreach (var items in _blogCategory.OrderBy(x => x.Name))
            {
                SiteMap sm = new SiteMap();
                sm.WebsiteName = items.Name;
                sm.URL = items.URL;
                _siteMap.Add(sm);
            }
            return _siteMap;
        }
        public List<SiteMap> SiteMapBlogPost(string WebsiteName)
        {
            List<SiteMap> _siteMap = new List<SiteMap>();
            var _blogsUrl = _context.BlogPosts
                    .Include(x => x.Website)
                    .Include(x => x.Culture)
                    .Include(x => x.Category)
                    .Where(x => x.Website.Name.Contains(WebsiteName))
                    .Where(x => x.Website.Construction == false)
                    .Where(x => x.Delflag == false)
                    .Where(x => x.Verified == true)
                    .Where(x => x.Title != "Amaazing Things To Do Now !")
                    .Where(x => x.Title != "Know How's About Site")
                    .Where(x => x.Title != "Welcome to Community CMS")
                    //.Where(x => x.BlogType == BlogType.Blog)
                    //.Where(x => x.WebsiteId == 2)
                    //.Where(x=>x.Website.Construction == true)                    
                    //.Take(2)
                    .ToList()
                    .Select(x => new { x.Website.Name, x.Url })
                    ;

            //.Select(x=>new {x.Website.Name , x.Url});
            foreach (var items in _blogsUrl.OrderBy(x => x.Name))
            {
                SiteMap sm = new SiteMap();
                sm.WebsiteName = items.Name;
                sm.URL = items.Url;
                _siteMap.Add(sm);
            }
            return _siteMap;
        }
        public GenericResult CreateBlogPost(int UserId, BlogPostCreateViewModel blogPost)
        {
            if (blogPost.Id > 0)
            { // Updated Blog Post
                BlogPost oldBlog = _blogRepository.GetAll().Where(x => x.Id == blogPost.Id).SingleOrDefault();
                BlogPost newBlog = Mapper.Map<BlogPost>(oldBlog);
                newBlog = Mapper.Map<BlogPostCreateViewModel, BlogPost>(blogPost, newBlog);
                newBlog.BlogType = blogPost.Type;
                newBlog.Tags = SaveTags(blogPost.Tag, UserId, blogPost.CultureId, newBlog.Verified, newBlog.Id);
                //newBlog = Mapper.Map<BlogPostCreateViewModel, BlogPost>(blogPost);
                if (!_userService.IsInRole(UserId, blogPost.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }))) return Global.AccessDenied();
                try
                {
                    try
                    {
                        _auditService.SaveAuditData(oldBlog.Id, UserId, oldBlog, newBlog);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Audit Data Could Not Be Proceeed !!");
                        // return new GenericResult()
                        // {
                        //     Succeeded = false,
                        //     Message = "Error : Audit Data Could Not Be Processed" + ex.Message,
                        // };
                    }
                    _blogRepository.Update(newBlog);

                    _blogRepository.Commit();
                    return new GenericResult()
                    {
                        Succeeded = true,
                        Message = "Success : " + blogPost.Type + " Updated.",
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeeded = false,
                        Message = "Error : " + blogPost.Type + " Couldnot Be Updated." + ex.Message,
                    };
                }
            }

            BlogPost NewblogPost = Mapper.Map<BlogPost>(blogPost);
            try
            {
                if (!_userService.IsInRole(UserId, blogPost.WebsiteId, (new string[] { "Admin", "Owner", "Editor", "Content Writer" }))) return Global.AccessDenied();
                //Console.WriteLine("Blogpost Ma Ayeko Tags Haru " + JsonConvert.SerializeObject(BlogPost.Tag));
                NewblogPost.BlogType = blogPost.Type;
                NewblogPost.CreatedById = UserId;
                NewblogPost.Verified = _userService.IsInRole(UserId, blogPost.WebsiteId, (new string[] { "Admin", "Editor", "Owner" }));
                List<BlogPostBlogTag> bpbt = new List<BlogPostBlogTag>();
                if (blogPost.Tag != null)
                {

                    // foreach (var tag in blogPost.Tag)
                    // {
                    //     var dbResultforDuplication = _blogTagRepository.ReturnIfExist(x => x.Name == tag);
                    //     if (dbResultforDuplication == null)
                    //     {
                    //         BlogTag bt = new BlogTag();
                    //         bt.Name = tag;
                    //         bt.CreatedById = UserId;//NewblogPost.CreatedById;
                    //         bt.CultureId = blogPost.CultureId;
                    //         bt.Verified = NewblogPost.Verified;
                    //         _blogTagRepository.Add(bt);
                    //         _blogTagRepository.Commit();

                    //         BlogPostBlogTag blogpostblogtag = new BlogPostBlogTag();
                    //         blogpostblogtag.Tag = bt;
                    //         bpbt.Add(blogpostblogtag);
                    //     }
                    //     else
                    //     {
                    //         BlogPostBlogTag blogpostblogtag = new BlogPostBlogTag();
                    //         blogpostblogtag.Tag = dbResultforDuplication;
                    //         bpbt.Add(blogpostblogtag);
                    //     }
                    // }
                    NewblogPost.Tags = SaveTags(blogPost.Tag, UserId, blogPost.CultureId, NewblogPost.Verified);
                }
                _blogRepository.Add(NewblogPost);
                _blogRepository.Commit();
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : " + blogPost.Type + " Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : " + blogPost.Type + " could not be Created." + ex.Message
                };
            }
        }

        public List<BlogPostViewModel> CheckIfImageIsUsed(string ImagePath)
        {
            var _result = _blogRepository.AllIncluding(x => x.Culture, x => x.Website, x => x.Category)
                    .Where(x => x.ImageUrl == ImagePath || x.ThumbnailUrl == ImagePath);
            List<BlogPostViewModel> blogPost = Mapper.Map<List<BlogPostViewModel>>(_result);
            return blogPost;
        }
        public IEnumerable<BlogCategoryViewModel> GetCategories(int UserId, int page, int pageSize, int WebsiteId, int cultureId, BlogType Type)
        {
            IEnumerable<BlogCategoryViewModel> websites = Mapper.Map<IEnumerable<BlogCategoryViewModel>>(_categoryRepository.AllIncluding(x => x.CreatedBy, x => x.Website, x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.CultureId == cultureId)
                .Where(x => x.Type == Type)
                .Where(x => x.WebsiteId == WebsiteId));

            return websites.Skip(page * pageSize).Take(pageSize);
            // int _totalWebsites = websites.Count();
            // return new IEnumerable<BlogCategoryViewModel>()
            // {
            //     Page = page,
            //     TotalCount = _totalWebsites,
            //     TotalPages = (int)Math.Ceiling((decimal)_totalWebsites / pageSize),
            //     Items = websites.Skip(page * pageSize).Take(pageSize)
            // };
        }
        public IEnumerable<BlogCategoryViewModel> GetCategoriesForWebsite(string Website, string Culture, int Pagesize, BlogType type)
        {
            // IEnumerable<BlogCategoryViewModel> Categories = Mapper.Map<IEnumerable<BlogCategoryViewModel>>(_blogRepository.AllIncluding(x=>x.Category,x=>x.Culture,x=>x.Website)
            //         .Where(x => x.Delflag == false)
            //         .Where(x => x.Website.Name == Website)
            //         .Where(x=>x.BlogType == type)
            //         .Where(x=>x.Culture.Title == Culture).Select(x=> new {x.Id,x.Url,x.Title}));

            IEnumerable<BlogCategoryViewModel> websites = Mapper.Map<IEnumerable<BlogCategoryViewModel>>(_categoryRepository
                .AllIncluding(x => x.CreatedBy, x => x.Website, x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.Website.Name == Website)
                .Where(x => x.Type == type)
                .Where(x => x.Culture.Title == Culture));



            return websites.Take(Pagesize);
        }
        public GenericResult CreateCategory(int UserId, BlogCategoryCreateViewModel BlogCategory)
        {
            if (BlogCategory.Title.Trim() == null)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : " + BlogCategory.Type + " Category Cannot Be Empty",
                };
            }
            BlogCategory blogCategory = Mapper.Map<BlogCategory>(BlogCategory);
            blogCategory.CreatedById = UserId;
            if (DuplicateCategory(blogCategory))
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : " + blogCategory.Type + " " + blogCategory.Title + " Category Already Exist.",
                };
            }
            try
            {
                _categoryRepository.Add(blogCategory);
                _categoryRepository.Commit();

                //;

                BlogCategoryViewModel returnedCategory = Mapper.Map<BlogCategoryViewModel>(_categoryRepository
                            .AllIncluding(x => x.Website, x => x.Culture)
                            .Where(x => x.Id == blogCategory.Id)
                            .SingleOrDefault());
                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : " + blogCategory.Type + " " + blogCategory.Title + "  Category Created.",
                    Data = JsonConvert.SerializeObject(returnedCategory),
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : " + blogCategory.Type + " " + blogCategory.Title + " Category could not be Created." + ex.Message
                };
            }
        }

        public List<BlogTagViewModel> GetTagsForWebsite(string Website, string Culture, int Pagesize, BlogType Type)
        {

            var _result = _context.BlogPostBlogTag
                 .Include(x => x.Post)
                 .Include(x => x.Tag)
                 .Where(x => x.Post.Website.Name == Website)
                 .Where(x => x.Post.BlogType == Type);
            //.Select(x=> new{x.Tag.Name , x.Tag.URL});

            //IEnumerable<BlogTagViewModel> tags = Mapper.Map<IEnumerable<BlogTagViewModel>>(_result);

            List<BlogTagViewModel> tags = new List<BlogTagViewModel>();
            foreach (var items in _result)
            {
                BlogTagViewModel btvm = new BlogTagViewModel();

                btvm.Url = "http://" + Website + "/" + items.Tag.URL;

                btvm.Name = items.Tag.Name;
                tags.Add(btvm);
            }

            return tags;

        }

        public bool DuplicateCategory(BlogCategory bc)
        {
            int category = _categoryRepository.AllIncluding(x => x.Website, x => x.Culture).Where(x => x.Title.ToString()
                                 .ToLower() == bc.Title.ToLower())
                            .Where(x => x.Delflag == false)
                            .Where(x => x.CultureId == bc.CultureId)
                            .Where(x => x.WebsiteId == bc.WebsiteId)
                            .Where(x => x.Type == bc.Type).Count();
            if (category > 0)
            {
                return true;
            }
            return false;
        }

    }
}