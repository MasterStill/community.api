﻿using System;
using Community.Model.Entities.Audit;
using System.Collections.Generic;
namespace Community.Services.Abstract
{
    public interface IAuditService
    {
      Boolean SaveAuditData(int libId,int userid,object oldObject, object newObject);
      List<AuditChange> GetAudit(int Id,string strObject);   
    }
}