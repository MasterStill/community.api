﻿using Community.Model.Entities.ViewModels;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Http;

namespace Community.Services.Abstract
{
    public interface IThemeService
    {
        IEnumerable<ThemeViewModel> getThemes(int page, int pageSize,int CultureId,int WebsiteId);
        //void CreateTheme(Guid UserId, Theme theme);
        bool AddOrEditTheme(int UserId,ThemeCreateViewModel theme);
        GenericResult ThemeAddingPoriton(ThemeCreateViewModel tcvm,IHostingEnvironment _hostingEnviroment,int UserId,IFormCollection fc);
    }
}