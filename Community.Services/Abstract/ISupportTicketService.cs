﻿using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using System.Collections.Generic;
namespace Community.Services.Abstract
{
    public interface ISupportTicketService
    {
        IEnumerable<SupportTicketViewModel> GetSupportTickets(int WebsiteId,int UserId);
        IEnumerable<SupportTicketViewModel> GetSupportTicketsUserId(int UserId);
        SupportTicketSingleViewModel GetSupportTicketReply(int SupportTicketId,int UserId);
        GenericResult CreateSupportTicket(int UserId, SupportTicketCreateViewModel Ticket);
        GenericResult CreateSupportTicketReply(int UserId, SupportTicketReplyCreateViewModel Ticket);
        // GenericResult CreateSupportReply(int UserId,int Id);  
    }
}