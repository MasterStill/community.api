﻿using Community.Model.Entities.Album.Photo;
using Community.Model.Entities.ViewModels;
using System.Collections.Generic;
using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
namespace Community.Services.Abstract
{
    public interface IPhotoAlbumService
    {
        List<PhotoAlbumCategoryViewModel> GetCategories(int WebsiteId);
        IEnumerable<PhotoAlbumViewModel> GetAlbums(int UserId, int Page, int Pagesize, int WebsiteId);
        IEnumerable<PhotoAlbumViewModel> GetAlbumsForWebsite(string Website, string Culture, int page, int pageSize);
        IEnumerable<PhotoViewModel> GetPhotos(int Userid, int AlbumId, int page, int pageSize);
        PhotoAlbumPhoto GetSinglePhoto(int UserId, int PhotoId);
        GenericResult DeletePhoto(int UserId, int PhotoId, int websiteId);
        GenericResult DeleteGallery(int UserId, int GalleryId);
        GenericResult CreateCategoy(int UserId, PhotoAlbumCategoryCreateViewModel Category);
        GenericResult CreateAlbum(int UserId, PhotoAlbumCreateViewModel photo, IFormFileCollection files, IHostingEnvironment HostingEnvironment);
        GenericResult CreatePhoto(int UserId, int websiteId, int Albumid, IFormFileCollection Files, IHostingEnvironment HostingEnvironment);
        GenericResult CreatePhoto(int UserId, PhotoAlbumPhotoCreateViewModel papcvm);
        GenericResult EditPhoto(int UserId, PhotoAlbumPhotoEditViewModel papevm);
        IEnumerable<PhotoViewModel> CheckIfImageIsUsed(string ImagePath);
        PaginationSet<PhotoAlbumViewModel> GetPagedAlbum(string Website, string Culture, int page, int pageSize);
        List<SiteMap> SiteMapPhotoAlbum(string website="");


    }
}