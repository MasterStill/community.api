﻿using Community.Model.Entities.ViewModels;
using System.Collections.Generic;
namespace Community.Services.Abstract
{
    public interface ISliderService
    {
       List<SliderViewModel> CheckIfImageIsUsed(string ImagePath);
    }
}