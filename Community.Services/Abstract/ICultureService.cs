﻿using Community.Model.Entities.Global;
using System.Collections.Generic;
using Community.Model.Entities.Globalization;
using Community.Model.Entities.ViewModels;
namespace Community.Services.Abstract
{
    public interface ICultureService
    {
        List<Culture> GetCultures();
        int GetCultureId (string CultureName);
        List<CultureViewModel> GetCultures(int CultureId);
        List<CultureViewModel> GetCultures(int WebsiteId,int CultureId);
        GenericResult CreateCulture(int UserId,Culture culture);
        List<CultureViewModel> GetCulturesInCultureLanguage();
    }
}  