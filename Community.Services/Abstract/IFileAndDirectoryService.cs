﻿using Community.Model.Entities.Global;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
namespace Community.Services.Abstract
{
    public interface IFileAndDirectoryService
    {
      GenericResult CreateFolder(string website,string path,IHostingEnvironment _hostingEnvironment,int UserId);
      GenericResult CreateFiles(int websiteId,string path,IFormCollection formCollection,IHostingEnvironment _hostingEnvironment,int UserId);
      GenericResult CreateFiles(string websiteName,string path,IFormCollection formCollection,IHostingEnvironment _hostingEnvironment,int UserId);
      FileAndDirectoryViewModel GetDirectoriesandFiles(string website,string filetype,string Path,IHostingEnvironment _hostingEnvironment,int UserId);   
      FileAndDirectoryViewModel GetDirectoriesandFiles(int websiteId,string filetype,string Path,IHostingEnvironment _hostingEnvironment,int UserId);
      FileAndDirectoryViewModel Search(int websiteId, string filetype, string path,string SearchTerm, IHostingEnvironment _hostingEnvironment,int UserId);
      GenericResult DeleteContent(int UserId,int WebsiteId,string FileName);
    }
}