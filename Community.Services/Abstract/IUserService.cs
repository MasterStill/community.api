﻿using Community.Model.Entities.Global;
using System.Collections.Generic;
using Community.Model.Entities;
namespace Community.Services.Abstract
{
    public interface IUserService
    {
        User GetSingleByUsername(string username);
        User GetSingleById(int UserId);
        IEnumerable<Role> GetUserRoles(string username);
        IEnumerable<Role> GetUserRoles(string username,int WebsiteId);
        IEnumerable<Role> GetUserRoles(int UserID,int WebsiteId);
        List<int> GetUserCulture(int UserId);

        bool IsInRole(int UserID, int WebsiteId,string RoleName);
        bool IsInRole(int UserID, int Website,string[] RoleName);
        int GetUserId(string username);
    }
}