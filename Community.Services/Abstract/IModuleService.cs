﻿using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using System.Collections.Generic;
namespace Community.Services.Abstract
{
    public interface IModuleService
    {
    List<ModuleViewModel> WebsiteModules(int WebsiteId,int CultureId = 1);    
    GenericResult ModifyWebstieModule(int[] websiteModule, int WebsiteId,int UserID);   

    }
}