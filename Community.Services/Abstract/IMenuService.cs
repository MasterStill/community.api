﻿using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using System.Collections.Generic;
namespace Community.Services.Abstract
{
    public interface IMenuService
    {
        IEnumerable<MenuViewModel> GetMenus(int UserId,int page, int pageSize,int Website,int CultureId);
        List<MenuViewModel> GetMenus(string CultureName,string WebsiteName);   
        GenericResult CreateMenu(int UserId, MenuCreateViewModel menuCreateViewModel);
        GenericResult DeleteMenu(int UserId,int MenuId);
        GenericResult DeleteMenu(int UserId,int WebsiteId,string MenuTitle);
        Menu SingleMenu(int UserId,int WebsiteId,string MenuTitle);
    }
}