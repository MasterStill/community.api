﻿using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using System.Collections.Generic;
using Community.Model.Entities.Module;

namespace Community.Services.Abstract
{
    public interface IWebsiteService
    {
        IEnumerable<WebsiteViewModel> SearchForWebsite(string WebsiteName,int PageNo,int PageSize);
        IEnumerable<WebsiteViewModel> GetWebsites(int UserId,int page, int pageSize);
        List<Module> GetWebsiteModule(int UserId,int WebsiteId);
        GenericResult CreateWebsite(int UserId, WebsiteCreateViewModel website);
        GenericResult DeleteWebsite(int UserId,int Id);  
        List<CultureViewModel> GetCultures(int WebsiteId,int cultureId);
        List<MenuViewModel> GetMenu(int WebsiteId,int cultureId);
        List<MenuViewModel> GetMenu(string CultureName,string  WebsiteName);
        List<CultureViewModel> GetCultures(string Website, string Culture);
        bool spaceExceeded(int websiteId);
        bool spaceExceeded(string websiteName);
        string GetWebsiteName (int websiteId);
        string GetWebsiteName (string AliasName);
        int GetWebsiteId(string WebsiteName);
        GenericResult SetConstruction(int UserId, int websiteId, bool onConstruction);
        WebisteSuggestionViewModel checkIfAvailable (string websiteName);
        bool isAliasAvailable(string aliasName);
           
    }
}