﻿using Community.Model.Entities.Global;
using System.Collections.Generic;
using Community.Model.Entities.ViewModels;
namespace Community.Services.Abstract
{
    public interface IGlobalizationService
    {
       List<StepsViewModel> ForNewUseRegistration(int Culture);
       List<StaticGlobalization> Step(int StepId);       
       GenericResult Add(StaticGlobalization sg);
       StaticGlobalization GlobalizationbyKey(string Key);
    }
}