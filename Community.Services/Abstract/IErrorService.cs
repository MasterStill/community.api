﻿using System;

namespace Community.Services.Abstract
{
    public interface IErrorService
    {
      void Error(Exception Message);
    }
}