﻿using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Community.Model.Entities.Blog;

namespace Community.Services.Abstract
{
    public interface IBlogService
    {
        IEnumerable<BlogPostViewModel> GetBlogPosts(int UserId,int page, int pageSize,int WebsiteId,int CultureId,BlogType type);
        IEnumerable<TestViewModel> GetBlogPostsForWebsite(string WebsiteName,string Culture,int page, int pageSize,BlogType Type);     
        // BlogPostSingleViewModel GetBlogPost(int BlogId,int CultureId); 
        BlogPostCreateViewModel GetBlogPost(int BlogId);
        BlogPostSingleViewModel GetBlogPost(int BlogId,string Culture);

        IEnumerable<BlogPostViewModel> GetBlogPostsForCategory(int UserId,int CategoryId,int page, int pageSize,int WebsiteId);
        IEnumerable<TestViewModel> GetBlogPostsForCategory(string Website,string Culture,string Category,int page,BlogType type);
         IEnumerable<BlogPostViewModel> GetBlogPostsForTags(string Website,string Culture,string Tags,int page,BlogType Type);
        GenericResult CreateBlogPost(int UserId, BlogPostCreateViewModel BlogPost);
        GenericResult DeleteBlog(int BlogId,int UserId);

        IEnumerable<BlogCategoryViewModel> GetCategories(int UserId,int page, int pageSize,int WebsiteId,int cultureId,BlogType type);
        IEnumerable<BlogCategoryViewModel> GetCategoriesForWebsite(string Website,string Culture,int Pagesize,BlogType type);
        List<BlogTagViewModel> GetTagsForWebsite(string Website,string Culture,int Pagesize,BlogType Type);
        GenericResult CreateCategory(int UserId, BlogCategoryCreateViewModel BlogCategory);
        List<BlogPostViewModel> CheckIfImageIsUsed(string ImagePath);
         List<SiteMap> SiteMapBlogTags(string SiteName ="");
        List<SiteMap> SiteMapBlogCategory(string SiteName ="");
        List<SiteMap> SiteMapBlogPost(string SiteName ="");
        void SeedBlogPost(int WebsiteId, int UserId);
    }
}