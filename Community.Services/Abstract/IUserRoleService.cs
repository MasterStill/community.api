﻿using Community.Model.Entities.Global;
using Community.Model.Entities;
namespace Community.Services.Abstract
{
    public interface IUserRoleService
    {
        GenericResult CreateUserRole(int UserId,UserRole userrole);
    }
}
