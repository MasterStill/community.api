﻿using Community.Model.Entities.Global;
using Community.Services.Infrastructure.Core;
namespace Community.Services.Abstract
{
    public interface IMembershipService
    {
        MembershipContext ValidateUser(string username, string password,string Ip);
        User CreateUser(string username, string email, string password, int[] roles,string FullName);
        //User GetUserById(int userId);
        //List<Role> GetUserRoles(string username);
        bool isEmailUsed(string Email);
    }
}
