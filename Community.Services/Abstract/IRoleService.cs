﻿using Community.Model.Entities.Global;
using Community.Model.Entities;
namespace Community.Services.Abstract
{
    public interface IRoleService
    {
       // Role GetRole(int roleId);
       GenericResult CreateRole(int UserId,Role role);
    }
}
