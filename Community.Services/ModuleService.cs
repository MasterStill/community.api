﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Community.Model.Entities.Global;
using System;
using Community.Model.Entities.Module;
using Community.Model.Entities.Pages;
using Microsoft.Extensions.Caching.Memory;

namespace Community.Services
{
    public class ModuleService : IModuleService
    {
        private IMapper Mapper { get; set; }
        private CommunityContext _context;
        private IMenuService _menuService;
        private IMemoryCache _memoryCache;
        public ModuleService(IMemoryCache _memoryCache, IMenuService _menuService, CommunityContext _context, IMapper Mapper, ICultureRepository _cultureRepository)
        {
            this._menuService = _menuService;
            this._memoryCache = _memoryCache;
            this._context = _context;
            this.Mapper = Mapper;
        }
        public List<ModuleViewModel> WebsiteModules(int WebsiteId, int CultureId)
        {
            List<ModuleViewModel> module = new List<ModuleViewModel>();
            var _allResults = _context.Modules.Where(x => x.Delflag == false).Where(x => x.CultureId == CultureId).ToList();
            try
            {
                var _websiteResults = _context.Websites.Include(x => x.WebsiteModule).ThenInclude(y => y.Module).Single(x => x.Id == WebsiteId);
                foreach (var allItems in _allResults)
                {
                    bool currentlyUsed = false;
                    if (_websiteResults != null)
                        foreach (var items in _websiteResults.WebsiteModule)
                        {
                            if (!items.Module.Name.Contains(" ") && !items.Module.Name.ToLower().Contains("settings") && items.Module.Id == allItems.MultiLingualId && !items.Module.Name.ToLower().Contains("html"))
                            {
                                currentlyUsed = true;
                                break;
                            }
                        }
                    if (!allItems.Name.Contains(" ") && !allItems.Name.ToLower().Contains("settings") && !allItems.Name.ToLower().Contains("html"))
                    {
                        ModuleViewModel m = new ModuleViewModel();
                        m.Id = allItems.MultiLingualId;
                        m.Name = allItems.Name;
                        m.CurrentlyUsed = currentlyUsed;
                        module.Add(m);
                    }
                }
            }
            catch
            {
                foreach (var allItems in _allResults)
                {
                    if (!allItems.Name.Contains(" ") && !allItems.Name.ToLower().Contains("settings") && !allItems.Name.ToLower().Contains("html"))
                    {
                        ModuleViewModel m = new ModuleViewModel();
                        m.Id = allItems.MultiLingualId;
                        m.Name = allItems.Name;
                        m.CurrentlyUsed = false;
                        module.Add(m);
                    }
                }
            }
            var kk = GetModuleFromCulture(_allResults, CultureId);

            return module.OrderBy(x => x.Name).ToList();
        }

        private List<Module> GetModuleFromCulture(List<Module> module, int cultureId)
        {
            var _module = _context.Modules.Where(x => x.CultureId == cultureId);
            var SelectedModule = from items in _module
                                 where module.Select(x => x.Id).Contains(items.MultiLingualId)
                                 select items;
            return SelectedModule.ToList();
        }
        public GenericResult ModifyWebstieModule(int[] websiteModule, int WebsiteId, int UserID)
        {
            var _result = new GenericResult();
            List<WebsiteModule> websitemodule = new List<WebsiteModule>();
            foreach (var items in websiteModule)
            {
                WebsiteModule wm = new WebsiteModule();
                wm.WebsiteId = WebsiteId;
                wm.ModuleId = items;
                websitemodule.Add(wm);
            }
            var _previousModule = _context.WebsiteModule.Where(x => x.WebsiteId == WebsiteId).ToList();
            try
            {
                _context.WebsiteModule.RemoveRange(_previousModule);
                _context.SaveChanges();
                _context.WebsiteModule.AddRange(websitemodule);
                _context.SaveChanges();
                _result = new GenericResult()
                {
                    Succeeded = true,
                    Message = "Modules Updated"
                };
                GenerateWebsiteComponent(WebsiteId, UserID);
            }
            catch (Exception ex)
            {
                _result = new GenericResult()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
            return _result;
        }
        private void RemoveRelatedMenu(string Module, int WebsiteId, int UserID, string Culture = "English")
        {
            var _menuToRemove = _menuService.DeleteMenu(UserID, WebsiteId, Module);
            Console.WriteLine(_menuToRemove);
        }
        private void AddRelatedMenu(string Module, int WebsiteId, int UserID, string Culture = "English")
        {
            if (Module.ToLower().Contains("page")) return;
            int intEnglishCulture = 1;
            int intNepaliCulture = 2;
            int intHindiCulture = 3;
            MenuCreateViewModel mcvm = new MenuCreateViewModel();
            mcvm.MenuUrl = "";
            mcvm.Name = GetCultureValueForMenu(Module, Culture);
            mcvm.MenuUrl = Culture + "/" + Module;
            mcvm.WebsiteId = WebsiteId;
            switch (Culture)
            {
                case "Nepali":
                    mcvm.CultureId = intNepaliCulture;
                    break;
                case "Hindi":
                    mcvm.CultureId = intHindiCulture;
                    break;
                default:
                    mcvm.CultureId = intEnglishCulture;
                    break;
            }
            var _result = _menuService.CreateMenu(UserID, mcvm);
            //_context.SaveChanges();
        }
        private string GetCultureValueForMenu(string What, string Culture)
        {
            switch (What.ToLower())
            {
                case "blog":
                    if (Culture == "Nepali" || Culture == "Hindi")
                        return "ब्लग";
                    return What;
                case "news":
                    if (Culture == "Nepali" || Culture == "Hindi") return "समाचार";
                    return What;
                case "videos":
                    if (Culture == "Nepali") return "भिडियो";
                    if (Culture == "Hindi") return "वीडियो";
                    return What;
                case "musicvideo":
                    if (Culture == "Nepali") return "म्युजिक भिडियो";
                    if (Culture == "Hindi") return "म्युजिक वीडियो";
                    return What;
                case "movies":
                    if (Culture == "Nepali" || Culture == "Hindi") return "चलचित्र";
                    return What;
                default:
                    return What;
            }
        }
        private void GenerateWebsiteComponent(int WebsiteId, int UserID)
        {
            var _websiteLanguages = _context.WebsiteCulture.Include(x => x.Culture).Where(x => x.WebsiteId == WebsiteId).ToList().Select(x => x.Culture);
            //Blog,MusicVideo,News,Movies,Projects,Videos,Page
            //Blog,News,Movies,Projects,Videos,
            var _result = _context.WebsiteModule.Include(x => x.Module).Where(x => x.WebsiteId == WebsiteId).ToList();
            bool BlogSelected = false, MusicVideoSelected = false, NewsSelected = false, MoviesSelected = false, ProjectsSelected = false, VideosSelected = false, PagesSelected = false;
            foreach (var items in _result)
            {
                if (items.Module.Name.ToString().ToLower().Contains("blog"))
                {
                    GenerateComponent(WebsiteId, "Blog");
                    BlogSelected = true;
                }
                else if (items.Module.Name.ToString().ToLower().Contains("news"))
                {
                    GenerateComponent(WebsiteId, "News");
                    NewsSelected = true;
                }
                else if (items.Module.Name.ToString().ToLower().Contains("musicvideo"))
                {
                    GenerateComponent(WebsiteId, "MusicVideo");
                    MusicVideoSelected = true;
                }
                else if (items.Module.Name.ToString().ToLower().Contains("movies"))
                {
                    GenerateComponent(WebsiteId, "Movies");
                    MoviesSelected = true;
                }
                else if (items.Module.Name.ToString().ToLower().Contains("projects"))
                {
                    GenerateComponent(WebsiteId, "Projects");
                    ProjectsSelected = true;
                }
                else if (items.Module.Name.ToString().ToLower().Contains("videos"))
                {
                    GenerateComponent(WebsiteId, "Videos");
                    VideosSelected = true;
                }
                else if (items.Module.Name.ToString().ToLower().Contains("page"))
                {
                    GenerateComponent(WebsiteId, "Page");
                    PagesSelected = true;
                }
                foreach (var culture in _websiteLanguages)
                {
                    AddRelatedMenu(items.Module.Name, WebsiteId, UserID, culture.Title);
                }
            }
            if (BlogSelected == false)
            {
                RemoveComponents("blog", WebsiteId);
                foreach (var culture in _websiteLanguages)
                {
                    RemoveRelatedMenu(GetCultureValueForMenu("Blog", culture.Title), WebsiteId, UserID);
                }
            }
            if (NewsSelected == false)
            {
                RemoveComponents("news", WebsiteId);
                foreach (var culture in _websiteLanguages)
                {
                    RemoveRelatedMenu(GetCultureValueForMenu("News", culture.Title), WebsiteId, UserID);
                }
            }
            if (MusicVideoSelected == false)
            {
                RemoveComponents("musicvideo", WebsiteId);
                foreach (var culture in _websiteLanguages)
                {
                    RemoveRelatedMenu(GetCultureValueForMenu("MusicVideo", culture.Title), WebsiteId, UserID);
                }
            }
            if (MoviesSelected == false)
            {
                RemoveComponents("movies", WebsiteId);
                foreach (var culture in _websiteLanguages)
                {
                    RemoveRelatedMenu(GetCultureValueForMenu("Movies", culture.Title), WebsiteId, UserID);
                }
            }
            if (ProjectsSelected == false)
            {
                RemoveComponents("projects", WebsiteId);
                foreach (var culture in _websiteLanguages)
                {
                    RemoveRelatedMenu(GetCultureValueForMenu("Projects", culture.Title), WebsiteId, UserID);
                }
            }
            if (VideosSelected == false)
            {
                RemoveComponents("videos", WebsiteId);
                foreach (var culture in _websiteLanguages)
                {
                    RemoveRelatedMenu(GetCultureValueForMenu("Videos", culture.Title), WebsiteId, UserID);
                }
            }
            if (PagesSelected == false)
            {
                RemoveComponents("page", WebsiteId);
                // foreach (var culture in _websiteLanguages)
                // {
                //     RemoveRelatedMenu("Page", WebsiteId);
                // }
            }
            _context.SaveChanges();
        }

        private void RemoveComponents(string ModuleName, int WebsiteId)
        {
            var _removeResultComponentBlog = _context.WebsiteComponent
                    .Include(x => x.Module)
                    .Where(x => x.WebsiteId == WebsiteId)
                    .Where(x => x.Module.Name.ToLower()
                    .Contains(ModuleName)).ToList();
            var _removeresultLayout = _context.WebsitePageLayout
                        .Include(x => x.Page)
                        .Where(x => x.WebsiteId == WebsiteId)
                        .Where(x => x.Page.Title.ToLower().Contains(ModuleName));
            var _removeResultPageLayoutDesign = _context.WebsitePage
                        .Include(x => x.WebsiteComponent)
                        .Where(x => x.WebsiteId == WebsiteId)
                        .Where(x => x.WebsiteComponent.Name.ToLower().Contains(ModuleName));

            var _removeResultComponentField = _context.WebsiteComponentFieldValue.Where(x => x.WebsiteComponent.Module.Name.ToLower().Contains(ModuleName));
            if (_removeresultLayout.Any()) _context.RemoveRange(_removeresultLayout);
            if (_removeResultPageLayoutDesign.Any()) _context.RemoveRange(_removeResultPageLayoutDesign);
            if (_removeResultComponentField.Any()) _context.RemoveRange(_removeResultComponentField);
            if (_removeResultComponentBlog.Any()) _context.RemoveRange(_removeResultComponentBlog);
            // try{
            //     _context.SaveChanges();
            // }
            // catch(Exception ex){
            //     Console.WriteLine(ex.Message);
            // }

        }

        private void GenerateComponent(int WebsiteId, string Component)
        {
            WebsiteComponent websiteComponentLatest = new WebsiteComponent();
            WebsiteComponent websiteComponentCategoryLi = new WebsiteComponent();
            WebsiteComponent websiteComponentTagsLi = new WebsiteComponent();
            WebsiteComponent websiteComponentLatestLi = new WebsiteComponent();
            WebsiteComponent websiteComponentLatestLiWithThumbnail = new WebsiteComponent();

            websiteComponentLatest.Name = "Latest " + Component;
            websiteComponentLatest.ThemeComponentId = _context.ThemeComponents.Where(x => x.Name.ToLower() == "component1").SingleOrDefault().Id;
            websiteComponentLatest.Module = _context.Modules.Where(x => x.Name == Component).SingleOrDefault();
            websiteComponentLatest.WebsiteId = WebsiteId;
            var _checkIfExist = _context.WebsiteComponent.Where(x => x.Name == websiteComponentLatest.Name).Where(x => x.WebsiteId == websiteComponentLatest.WebsiteId).SingleOrDefault();
            if (_checkIfExist != null) { return; }
            _context.WebsiteComponent.Add(websiteComponentLatest);

            websiteComponentCategoryLi.Name = Component + " Category Sidebar";
            websiteComponentCategoryLi.ThemeComponentId = _context.ThemeComponents.Where(x => x.Name.ToLower() == "lisideBar").SingleOrDefault().Id;
            websiteComponentCategoryLi.Module = _context.Modules.Where(x => x.Name == Component + " Category").SingleOrDefault();
            websiteComponentCategoryLi.WebsiteId = WebsiteId;
            _context.WebsiteComponent.Add(websiteComponentCategoryLi);

            websiteComponentTagsLi.Name = Component + " Tags Sidebar";
            websiteComponentTagsLi.ThemeComponentId = _context.ThemeComponents.Where(x => x.Name.ToLower() == "lisidebar").SingleOrDefault().Id;
            websiteComponentTagsLi.Module = _context.Modules.Where(x => x.Name == Component + " Tags").SingleOrDefault();
            websiteComponentTagsLi.WebsiteId = WebsiteId;
            _context.WebsiteComponent.Add(websiteComponentTagsLi);

            websiteComponentLatestLi.Name = Component + " Latest  Sidebar";
            websiteComponentLatestLi.ThemeComponentId = _context.ThemeComponents.Where(x => x.Name.ToLower() == "lisidebar").SingleOrDefault().Id;
            websiteComponentLatestLi.Module = _context.Modules.Where(x => x.Name == Component).SingleOrDefault();
            websiteComponentLatestLi.WebsiteId = WebsiteId;
            _context.WebsiteComponent.Add(websiteComponentLatestLi);

            websiteComponentLatestLiWithThumbnail.Name = Component + " Latest Sidebar With Thumbnail";
            websiteComponentLatestLiWithThumbnail.ThemeComponentId = _context.ThemeComponents.Where(x => x.Name.ToLower() == "liwiththumbnail sidebar").SingleOrDefault().Id;
            websiteComponentLatestLiWithThumbnail.Module = _context.Modules.Where(x => x.Name == Component).SingleOrDefault();
            websiteComponentLatestLiWithThumbnail.WebsiteId = WebsiteId;
            _context.WebsiteComponent.Add(websiteComponentLatestLiWithThumbnail);

            try
            {
                // _context.SaveChanges();
                AddComponentFieldValue("HeaderText", "Latest " + Component, websiteComponentLatest.Id, 1);
                AddComponentFieldValue("ReadMore", "View " + Component, websiteComponentLatest.Id, 1);
                AddComponentFieldValue("HeaderText", "Latest " + Component + " " + "Category", websiteComponentCategoryLi.Id, 1);
                AddComponentFieldValue("HeaderText", "Latest " + Component + "Tags", websiteComponentTagsLi.Id, 1);
                AddComponentFieldValue("HeaderText", "Latest " + Component, websiteComponentLatestLi.Id, 1);
                AddComponentFieldValue("HeaderText", "Latest " + Component, websiteComponentLatestLiWithThumbnail.Id, 1);
                _context.SaveChanges();

            }
            catch
            {
                _context.Remove(websiteComponentLatest);
                _context.Remove(websiteComponentCategoryLi);
                _context.Remove(websiteComponentTagsLi);
                _context.Remove(websiteComponentLatestLi);
            }
            AddRelatedPages(WebsiteId, Component);
            //ExitAll:;
        }
        private void AddRelatedPages(int WebsiteId, string Component)
        {
            WebsitePageLayout wpl = new WebsitePageLayout();
            WebsitePageLayout wp2 = new WebsitePageLayout();
            WebsitePageLayout wp3 = new WebsitePageLayout();
            WebsitePageLayout wp4 = new WebsitePageLayout();

            wpl.WebsiteId = WebsiteId;
            wpl.PageId = _context.Pages.Single(x => x.Title == Component + " Single").Id;
            wpl.Layout = SinglePageLayout.Middle;
            _context.WebsitePageLayout.Add(wpl);

            wp2.WebsiteId = WebsiteId;
            wp2.PageId = _context.Pages.Single(x => x.Title == Component + " List").Id;
            wp2.Layout = SinglePageLayout.Middle;
            _context.WebsitePageLayout.Add(wp2);

            wp3.WebsiteId = WebsiteId;
            wp3.PageId = _context.Pages.Single(x => x.Title == Component + " Category List").Id;
            wp3.Layout = SinglePageLayout.Middle;
            _context.WebsitePageLayout.Add(wp3);

            wp4.WebsiteId = WebsiteId;
            wp4.PageId = _context.Pages.Single(x => x.Title == Component + " Tag List").Id;
            wp4.Layout = SinglePageLayout.Middle;
            _context.WebsitePageLayout.Add(wp4);
            try
            {
                _context.SaveChanges();
            }
            catch
            {
                _context.WebsitePageLayout.Remove(wpl);
                _context.WebsitePageLayout.Remove(wp2);
                _context.WebsitePageLayout.Remove(wp3);
                _context.WebsitePageLayout.Remove(wp4);
            }
        }

        private void AddComponentFieldValue(string Field, string Value, int ComponentId, int CultureId)
        {
            WebsiteComponentFieldValue componentFieldValue = new WebsiteComponentFieldValue();
            componentFieldValue.ComponentFieldId = GetCopmonentFieldId(Field, CultureId);
            componentFieldValue.CultureId = CultureId;
            componentFieldValue.Value = Value;
            componentFieldValue.WebsiteComponentId = ComponentId;
            _context.WebsiteComponentFieldValue.Add(componentFieldValue);
        }
        private int GetCopmonentFieldId(string FieldName, int Culture)
        {
            int intReturnId = 0;
            string SetNameIn = "componentfield" + FieldName + Culture;
            if (!_memoryCache.TryGetValue(SetNameIn, out intReturnId))
            {
                intReturnId = _context.ComponentField.Where(x => x.FieldName.ToLower() == FieldName.ToLower()).Select(x => x.Id).SingleOrDefault();
                _memoryCache.Set(SetNameIn, intReturnId, new MemoryCacheEntryOptions()
                                .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheTime())));
            }
            return intReturnId;
        }
    }
}