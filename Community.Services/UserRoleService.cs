﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using AutoMapper;
using Community.Model.Entities.Global;
using Community.Model.Entities;
namespace Community.Services
{
    public class UserRoleService : IUserRoleService  
    {
        private readonly IUserRoleRepository _userRoleRepository;
        private IMapper Mapper { get; set; }
        private IUserService _userService;
        public UserRoleService(IMapper Mapper,IUserRoleRepository _userRoleRepository,IUserService _userService)
        {
            this._userService = _userService;
            this._userRoleRepository = _userRoleRepository;
            this.Mapper = Mapper;
        }

        //Create Role
        public GenericResult CreateUserRole(int UserId ,UserRole userRole)
        {
         if (!_userService.IsInRole(UserId, userRole.WebsiteId, (new string[] { "Admin", "Owner"}))) return Global.AccessDenied();   
            userRole.CreatedById = UserId;
            if(DuplicateRole(userRole)){
                 return new GenericResult()
                    {
                        Succeeded = false,
                        Message = "Error : UserRole Already Exist.",
                    };
                 }
            try
            {
                _userRoleRepository.Add(userRole);
                _userRoleRepository.Commit();
                 return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : " + "UserRole" + " Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : UserRole could not be Created." + ex.Message 
                };   
            }
        }
        public bool DuplicateRole(UserRole bc){
            //  int category = _userRoleRepository.GetAll().Where(x=>x.CreatedById == bc.CreatedById).Where(x=>x.Name.ToString().ToLower() == bc.Name.ToLower()).Where(x=>x.Delflag == false).Count();  
            //  if (category > 0 ){
            //      return true; 
            //  }
            return false;
        }
    }
}