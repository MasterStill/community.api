﻿using Community.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Community.Model.Entities.ViewModels;
using Community.Model.Entities.Global;
using Newtonsoft.Json;
using Community.Model.Entities.Support;
using Community.Data.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Serialization;

namespace Community.Services
{
    public class SupportTicketService : ISupportTicketService
    {
        private readonly ISupportTicketRepository _ticketRepository;
        private readonly ISupportTicketReplyRepository _replyRepository;
        private IMapper Mapper { get; set; }
        //private IConnectionManager _connectionManager;
        private CommunityContext _context;
        private IUserService _userService;
        public SupportTicketService(IUserService _userService, CommunityContext _context, IMapper Mapper, ISupportTicketRepository _ticketRepository, ISupportTicketReplyRepository _replyRepository)//, IConnectionManager _connectionManager)
        {

            //this._connectionManager = _connectionManager;
            this._userService = _userService;
            this._context = _context;
            this._ticketRepository = _ticketRepository;
            this._replyRepository = _replyRepository;
            this.Mapper = Mapper;
        }

        public IEnumerable<SupportTicketViewModel> GetSupportTickets(int WebsiteId, int UserId)
        {

            var _user = _context.Users.SingleOrDefault(x => x.Id == UserId);
            if (_user.UserType == UserType.CommunityAdmin)
            {
                IEnumerable<SupportTicketViewModel> tickets1 = Mapper.Map<IEnumerable<SupportTicketViewModel>>(_ticketRepository.AllIncluding(x => x.SupportTicketDepartment, x => x.CreatedBy)
            .Where(x => x.Delflag == false)
            .OrderBy(x => x.Status)
            .ThenByDescending(x => x.CreatedDate)
            .ThenByDescending(x => x.LastRepliedDate)
            );
                return tickets1;
            }
            IEnumerable<SupportTicketViewModel> tickets = Mapper.Map<IEnumerable<SupportTicketViewModel>>(_ticketRepository.AllIncluding(x => x.SupportTicketDepartment, x => x.CreatedBy)
            .Where(x => x.Delflag == false)
            .Where(x => x.WebsiteId == WebsiteId)
            .OrderBy(x => x.Status)
            .ThenByDescending(x => x.CreatedDate)
            .ThenByDescending(x => x.LastRepliedDate)
            );
            return tickets;
        }


        public IEnumerable<SupportTicketViewModel> GetSupportTicketsUserId(int UserId)
        {
            IEnumerable<SupportTicketViewModel> tickets = Mapper.Map<IEnumerable<SupportTicketViewModel>>(_ticketRepository.AllIncluding(x => x.SupportTicketDepartment, x => x.CreatedBy)
            .Where(x => x.Delflag == false)
            .Where(x => x.CreatedById == UserId)
            );
            return tickets;
        }

        public SupportTicketSingleViewModel GetSupportTicketReply(int TicketId, int UserId)
        {
            SupportTicketSingleViewModel ticket = new SupportTicketSingleViewModel();
            List<SupportTicketReplyViewModel> replies = new List<SupportTicketReplyViewModel>();

            //var _result = _ticketRepository.AllIncluding(x=>x.SupportTicketDepartment,x=>x.SupportTicketReply,x=>x.CreatedBy)
            //                            .Where(x=>x.Id == TicketId).SingleOrDefault();
            // var _result = _context.SupportTickets.Include(x=>x.CreatedBy)
            //                         .Where(x=>x.Id == TicketId)
            //                         .Include(x=>x.SupportTicketDepartment)                                        
            //                         .Include(x=>x.SupportTicketReply.Select(y=> new {y.CreatedBy.Fullname,y.CreatedDate,y.CreatedBy.Image,y.Reply,y.CreatedBy.UserType}))
            //                         .SingleOrDefault();                                        
            var _ticket = _context.SupportTickets.Where(x => x.Id == TicketId).SingleOrDefault();
            if (!_userService.IsInRole(UserId, _ticket.WebsiteId, (new string[] { "Admin", "Owner" }))) return new SupportTicketSingleViewModel();

            var _result = _context.SupportTickets
                                    .Where(x => x.Id == TicketId)
                                    .Include(x => x.CreatedBy)
                                    .Include(x => x.SupportTicketDepartment)
                                    .Include(x => x.SupportTicketReply)
                                    .ThenInclude(y => y.CreatedBy)
                                    .FirstOrDefault();
            ticket.CreatedBy = _result.CreatedBy.Fullname;// CreatedBy.Fullname;
            ticket.CreatedDate = String.Format("{0:d/M/yyyy HH:mm:ss}", _result.CreatedDate.ToString());
            ticket.Department = _result.SupportTicketDepartment.Name;
            ticket.Id = _result.Id;
            ticket.ScreenShot = _result.ScreenShot;
            ticket.TicketId = _result.TicketId;
            ticket.Status = _result.Status.ToString();
            ticket.Subject = _result.Subject;
            ticket.Description = _result.Description;
            foreach (var reply in _result.SupportTicketReply.OrderByDescending(x => x.CreatedDate))
            {
                SupportTicketReplyViewModel _replies = new SupportTicketReplyViewModel();
                _replies.CreatedBy = reply.CreatedBy.Fullname;
                _replies.CreatedDate = String.Format("{0:d/M/yyyy HH:mm:ss}", reply.CreatedDate.ToString());
                _replies.Image = reply.CreatedBy.Image;
                _replies.Reply = reply.Reply;
                _replies.ScreenShot = reply.ScreenShot;
                _replies.UserType = reply.CreatedBy.UserType.ToString();
                replies.Add(_replies);
            }
            ticket.Reply = replies;
            //SupportTicketSingleViewModel tickets = Mapper.Map<SupportTicketSingleViewModel>(_result);
            return ticket;
        }

        public GenericResult CreateSupportTicket(int UserId, SupportTicketCreateViewModel supportTicket)
        {
            SupportTicket ticket = Mapper.Map<SupportTicket>(supportTicket);
            try
            {
            RandomGenerator:
                const string AllowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                const string AllowedCharsNum = "0123456789";
                Random rng = new Random();
                string randomm = string.Empty;
                string randommInt = string.Empty;
                string randommInt1 = string.Empty;
                foreach (var randomString in RandomStrings(AllowedChars, 4, 4, 1, rng))
                {
                    randomm = randomString;
                }
                foreach (var randomString in RandomStrings(AllowedCharsNum, 4, 4, 1, rng))
                {
                    randommInt = randomString;
                }
                foreach (var randomString in RandomStrings(AllowedCharsNum, 4, 4, 1, rng))
                {
                    randommInt1 = randomString;
                }

                //Generate a Unique Support Ticket
                string UniqueSupportTicketID = randomm + '-' + randommInt + '-' + randommInt1;
                if (_ticketRepository.Duplicate(x => x.TicketId == UniqueSupportTicketID))
                {
                    goto RandomGenerator;
                }
                ticket.TicketId = UniqueSupportTicketID;
                ticket.CreatedById = UserId;
                _ticketRepository.Add(ticket);
                _ticketRepository.Commit();


                return new GenericResult()
                {
                    Succeeded = true,
                    Message = "Success : Support Ticket Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Tickets could not be Created." + ex.Message + " for Data CreateViewMode" + JsonConvert.SerializeObject(supportTicket).ToString() + "For Destination Final Data " + JsonConvert.SerializeObject(ticket).ToString()
                };
            }
        }
        public GenericResult CreateSupportTicketReply(int UserId, SupportTicketReplyCreateViewModel ticketReply)
        {
            // var _ticket = _context.SupportTickets.Where(x=>x.Id == ticketReply.TicketId).SingleOrDefault();
            // if (!_userService.IsInRole(UserId, _ticket.WebsiteId, (new string[] { "Admin", "Owner" }))) return Global.AccessDenied();

            SupportTicketReply reply = Mapper.Map<SupportTicketReply>(ticketReply);
            try
            {
                //  if(_ticketRepository.Duplicate(x=>x.TicketId == UniqueSupportTicketID)){ 
                //         goto RandomGenerator;
                //     }
                //ticket.TicketId = UniqueSupportTicketID;  
                reply.CreatedById = UserId;
                reply.Ticket = null;
                reply.TicketId = ticketReply.TicketId;
                reply.Verified = true;
                //reply.CreatedDate  = DateTime.Now;
                _replyRepository.Add(reply);
                _replyRepository.Commit();

                // var _ticketdateTimeToUpdate = _ticketRepository.GetSingle(reply.TicketId);
                // _ticketdateTimeToUpdate.LastRepliedDate = DateTime.Now;
                // _ticketdateTimeToUpdate.Status = ticketReply.Status;
                // _ticketRepository.Update(_ticketdateTimeToUpdate);
                // _ticketRepository.Commit();
                var settings = new JsonSerializerSettings();
                settings.ContractResolver = new LowercaseContractResolver();
                return new GenericResult()
                {    
                    Succeeded = true,
                    Message = "Success : Support Reply Posted.",
                    Data = JsonConvert.SerializeObject(reply, Formatting.Indented, settings).ToString()
                };
                //_connectionManager.GetHubContext<SupportHub>().Clients.All.publishPost(DateTime.Now.ToString());
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeeded = false,
                    Message = "Error : One or More Reply could not be Posted." + ex.Message + " for Data CreateViewModel" + JsonConvert.SerializeObject(ticketReply).ToString() + "For Destination Final Data " + JsonConvert.SerializeObject(reply).ToString()
                };
            }
        }


        private IEnumerable<string> RandomStrings(
            string allowedChars,
            int minLength,
            int maxLength,
            int count,
            Random rng)
        {
            char[] chars = new char[maxLength];
            int setLength = allowedChars.Length;

            while (count-- > 0)
            {
                int length = rng.Next(minLength, maxLength + 1);

                for (int i = 0; i < length; ++i)
                {
                    chars[i] = allowedChars[rng.Next(setLength)];
                }

                yield return new string(chars, 0, length);
            }
        }
    }
    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}